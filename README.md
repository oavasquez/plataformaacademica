# DataBase Connection config 

<p>
Antes de ejecutar el archivo server.js especifique en que base va a trabajar:

- **MODE_PRODUCTION:** apunta a la base en heroku
- **MODE_TEST 	   :** apunta a una base de datos local en su equipo.
 
Nota: si usa MODE_TEST, pero tiene un usuario, nombre de base de datos o contraseña diferentes 
cambiar en server/config/dataBaseConnection.js en las variable que incluya TEST.


# Project Structure 
*****************************************************************************************************
Implementation of structure MEAN with MYSQL:
    
    ┌── node_modules
    ├── dist
    ├── src(client)
    |   ├── app
    |   |   ├──modules
    |   |       ├──module-login
    |   |           ├── components
    |   |           ├── pages (optional)
    |   |           ├── module-login.service.ts
    |   |           ├── module-login.routes.ts  
    |   |           └── module-login.module.ts      
    |   |       └──module-matricula
    |   |           ├── components
    |   |           ├── pages (optional)
    |   |           ├── module-matricula.service.ts
    |   |           ├── module-matricula.routes.ts  
    |   |           └── module-matricula.module.ts
    |   |       └──module-gestion
    |   |           ├── components
    |   |           ├── pages (optional)
    |   |           ├── module-gestion.service.ts
    |   |           ├── module-gestion.routes.ts  
    |   |           └── module-gestion.module.ts
    |   |       └──module-evaluacion
    |   |           ├── components
    |   |           ├── pages (optional)
    |   |           ├── module-evaluacion.service.ts
    |   |           ├── module-evaluacion.routes.ts  
    |   |           └── module-evaluacion.module.ts
    |   |       └──module-admon
    |   |           ├── components
    |   |           ├── pages (optional)
    |   |           ├── module-admon.service.ts
    |   |           ├── module-admon.routes.ts  
    |   |           └── module-admon.module.ts  
    |   |   ├──shared
    |   |           └── components  
    |   |               └── component1 
    |   |               └── component2
    |   |           └── model
    |   |               └── nombre1.model.ts 
    |   |               └── nombre2.model.ts 
    |   ├── assets
    |   ├── env
    |   ├── app.module.ts 
    |   ├── app.component.ts
    |   └── index.html
    ├── Server  
    |   ├── config
    |       ├── dataBaseConnection.js
    |   ├── models
    |       ├── user.model.js
    |   ├── controllers
    |       ├── login.server.controller.js
    |       ├── matricula.server.controller.js
    |       ├── evaluacion.server.controller.js
    |       ├── gestion.server.controller.js    
    |       └── admon.server.controller.js
    |   └── routes
    |       ├── login.server.route.js
    |       ├── matricula.server.route.js
    |       ├── evaluacion.server.route.js
    |       ├── gestion.server.route.js    
    |       └── admon.server.route.js    
    ├── package.json
    └── server.js
    
*******************************************************************************************************

# HerokuAngular

This project was generated with [Angular CLI](https://github.com/angular/angular-cli) version 1.6.8.

## Development server

Run `ng serve` for a dev server. Navigate to `http://localhost:4200/`. The app will automatically reload if you change any of the source files.

## Code scaffolding

Run `ng generate component component-name` to generate a new component. You can also use `ng generate directive|pipe|service|class|guard|interface|enum|module`.

## Build

Run `ng build` to build the project. The build artifacts will be stored in the `dist/` directory. Use the `-prod` flag for a production build.

## Running unit tests

Run `ng test` to execute the unit tests via [Karma](https://karma-runner.github.io).

## Running end-to-end tests

Run `ng e2e` to execute the end-to-end tests via [Protractor](http://www.protractortest.org/).

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

