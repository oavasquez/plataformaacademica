const express = require('express');
const app = express();
const config = require('./server/config/config.js');
var dataBaseConnection = require('./server/config/dataBaseConnection');
const morgan = require('morgan');

//const userRoutes = require('./server/routes/usuarios.server.route');
const loginRoutes = require('./server/routes/login.server.route');
const mainRoutes = require('./server/routes/main.server.route');
const administracion = require('./server/routes/administracion.server.route');
const evaluacionPromocion = require('./server/routes/evaluacionPromocion.server.route');
const gestionAcademica = require('./server/routes/gestionAcademica.server.route');
const matriculaRegistro = require('./server/routes/matriculaRegistro.server.route');


//app.use(morgan('dev'));


//code status 
// ok: 200
// continue 100


// Iniciando conexion a la base de datos 
//MODE_PRODUCTION apunta a la base en heroku
//MODE_TEST apunta a una base de datos local pero si tiene un
//usuario, nombre de base de datos o contraseña cambiar en server/config/dataBaseConnection
//en la variable que incluya TEST



dataBaseConnection.connect(dataBaseConnection.MODE_PRODUCTION, function (err) {
  if (err) {
    console.log('Imposible conectar en la base Msql revise ' +
      'si esta en el modo (TEST o PRODUCTION) correcto ' +
      'y los datos son correctos');
    process.exit(1)
  } else {
    app.use(express.static(__dirname + '/dist'));
    // Heroku port
    app.listen(process.env.PORT || 5000, function () {
      console.log("escuchando en el puerto 5000");
      //console.log(__dirname ); 
      //console.log(dataBaseConnection.getDataBaseName());
    });
  }
})



app.all("/*", function (req, res, next) {
  res.header('Access-Control-Allow-Origin', '*');
  res.header('Access-Control-Allow-Methods', 'GET,PUT,POST,DELETE,OPTIONS');
  res.header('Access-Control-Allow-Headers', 'Content-Type, Authorization, Content-Length, X-Requested-With');

  next();
});


app.get('*', function (req, res) {
  res.sendFile(__dirname + '/dist/index.html');
})



//routes
//app.use('/usuarios', userRoutes);
app.use('/login', loginRoutes);
app.use('/main', mainRoutes);
app.use('/administracion', administracion);
app.use('/evaluacionpromocion', evaluacionPromocion);
app.use('/gestionacademica', gestionAcademica);
app.use('/matricularegistro', matriculaRegistro);




app.use((req, res, next) => {

  res.status(404).json({ mensaje: "pagina no encontrada" });
});






