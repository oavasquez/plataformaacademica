import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../../auth/auth-guard.service';

//components
import { AulasComponent } from './components/aulas/aulas.component';
import { DocentesComponent } from './components/docentes/docentes.component';
import { SeccionesComponent } from './components/secciones/secciones.component';
import { GestDashboardComponent } from './components/gest-dashboard/gest-dashboard.component';
import { FormularioDocenteComponent } from "./components/formularios/formulario-docente/formulario-docente.component"; 
import { FormularioSeccionComponent } from './components/formularios/formulario-seccion/formulario-seccion.component'; 

const routes: Routes = [
    { path: 'gestion', redirectTo: 'gestion/gest-dashboard', pathMatch: 'full' },
    { path: 'gest-dashboard', component: GestDashboardComponent, canActivate: [AuthGuard] },
    { path: 'gest-aulas', component: AulasComponent, canActivate: [AuthGuard] },
    { path: 'gest-docentes', component: DocentesComponent, canActivate: [AuthGuard] },
    { path: 'gest-secciones', component: SeccionesComponent, canActivate: [AuthGuard] },
    { path: 'formulario-docente', component: FormularioDocenteComponent, canActivate: [AuthGuard] },
    { path: 'formulario-seccion', component: FormularioSeccionComponent, canActivate: [AuthGuard] }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class GestionAcademicaRoutesModule { }