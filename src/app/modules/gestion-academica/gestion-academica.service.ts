import { Aula } from './../../shared/model/aula.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';

@Injectable()
export class GestionAcademicaService {

    headers = new HttpHeaders().set('Content-Type', 'application/json');

    constructor(private http: HttpClient) { }

    //"""""""""""""""""""""""""""""""""""""""Aulas"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarAula(aula: Aula): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/crearAula',
            JSON.stringify(aula),
            { headers: this.headers });
    }

    actualizarAula(aula: Aula): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/actualizarAula',
            JSON.stringify(aula),
            { headers: this.headers });
    }


    //mostrar todos los aulas existentes
    mostrarAulas(aula): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/mostrarAulas',
            aula,
            { headers: this.headers });
    }

    //"""""""""""""""""""""""""""""""""""""""Docentes"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarDocente(docente): Observable<any> {
        const parametros = {}
        //https://appacademica.herokuapp.com/gestionAcademica/crearDocente
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/crearDocente',
            docente,
            { headers: this.headers });
    }

    actualizarDocente(docente): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/actualizarDocente',
            docente,
            { headers: this.headers });
    }


    //mostrar todos los docentes existentes
    mostrarDocentes(docente): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/mostrarDocentes',
            docente,
            { headers: this.headers });
    }

    //""""""""""""""""""""""""""""""""""""""Secciones"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarSeccion(seccion): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/crearSeccion',
            seccion,
            { headers: this.headers });
    }

    actualizarSeccion(seccion): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/actualizarSeccion',
            seccion,
            { headers: this.headers });
    }


    //mostrar todas las secciones existentes
    mostrarSecciones(seccion): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/mostrarSecciones',
            seccion,
            { headers: this.headers });
    }

    //mostrar todas las secciones existentes
    mostrarSeccionesDocente(seccion): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/mostrarSeccionesDocente',
            seccion,
            { headers: this.headers });
    }



    //""""""""""""""""""""""""""""""""""""""Cargos"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarCargo(cargo): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/crearCargo',
            cargo,
            { headers: this.headers });
    }

    mostrarCargoDocente(cargo): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/mostrarCargoDocente',
            cargo,
            { headers: this.headers });
    }

    actualizarCargo(cargo): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/actualizarCargo',
            cargo,
            { headers: this.headers });
    }


    //mostrar todas los cargos existentes
    mostrarCargos(cargo): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/mostrarCargos',
            cargo,
            { headers: this.headers });
    }

    //""""""""""""""""""""""""""""""""""""""Departamentos"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarDepartamento(departamento): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/crearDepartamento',
            departamento,
            { headers: this.headers });
    }

    actualizarDepartamento(departamento): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/actualizarDepartamento',
            departamento,
            { headers: this.headers });
    }


    //mostrar todas los departamentos existentes
    mostrarDepartamentos(departamento): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionAcademica/mostrarDepartamentos',
            departamento,
            { headers: this.headers });
    }

    //********************************************************************************************************************/

    //servicios para obtener datos para mostrara en las tajetas de la seccion de dashboard
    mostrarDashboard(dashboard): Observable<any> {
        //console.log("consiguiendodatos");
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/gestionacademica/mostrarDashboardGestionAcademica',
            dashboard,
            { headers: this.headers });

    }


}


