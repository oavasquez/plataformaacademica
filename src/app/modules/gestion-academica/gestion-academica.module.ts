import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

//components
import { AulasComponent } from './components/aulas/aulas.component';
import { DocentesComponent } from './components/docentes/docentes.component';
import { SeccionesComponent } from './components/secciones/secciones.component';
import { GestDashboardComponent } from './components/gest-dashboard/gest-dashboard.component';
import { FormularioDocenteComponent } from "./components/formularios/formulario-docente/formulario-docente.component";
import { FormularioSeccionComponent } from './components/formularios/formulario-seccion/formulario-seccion.component';
import { DataTablesModule } from 'angular-datatables';
//route.module
import { GestionAcademicaRoutesModule } from './gestion-academica.routes.module';
//service
import { GestionAcademicaService } from './gestion-academica.service'
//ui auto-complete
import { NguiAutoCompleteModule } from '@ngui/auto-complete';



@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    GestionAcademicaRoutesModule,
    DataTablesModule,
    NguiAutoCompleteModule
  ],
  exports: [
    AulasComponent,
    DocentesComponent,
    SeccionesComponent,
    GestDashboardComponent,
    FormularioDocenteComponent,
    FormularioSeccionComponent
  ],
  declarations: [
    AulasComponent,
    DocentesComponent,
    SeccionesComponent,
    GestDashboardComponent,
    FormularioDocenteComponent,
    FormularioSeccionComponent
  ],
  providers: [GestionAcademicaService]
})
export class GestionAcademicaModule { }
