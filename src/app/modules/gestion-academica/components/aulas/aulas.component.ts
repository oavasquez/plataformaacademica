import { GestionAcademicaService } from './../../gestion-academica.service';
import { Aula } from './../../../../shared/model/aula.model';
import { Validacion_GA } from './../../../../shared/model/validaciones_GestionAcademica.model';
import { Component, NgZone, OnInit, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { DataTableDirective } from 'angular-datatables';


// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-aulas',
  templateUrl: './aulas.component.html',
  styleUrls: ['./aulas.component.css']
})
export class AulasComponent implements OnInit {
  //variables de API
  @Input() inAulasJsonArray: object[];
  @Output() outRecargarTablaAulas = new EventEmitter();

  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;
  dtOptions: DataTables.Settings[] = [];
  dtTrigger: Subject<any>[] = [new Subject(), new Subject()];

  Validacion = new Validacion_GA();

  constructor(public gestionAcademicaService: GestionAcademicaService, private zone: NgZone) { }

  ngOnInit() {
    this.iniciarTablaAulas();
    this.getTodosAulas();
    //console.log(this.inAulasJsonArray);
  }

  recargarTablaAulas() {
    this.outRecargarTablaAulas.emit();
  }

  getFilaAulasSelec(info: any): void {
    //console.log(info);
    this.crearAula.codAula= info[0];
    this.crearAula.capacidad = info[4];
    this.crearAula.habilitada = info[6];
    this.crearAula.descripcion = info[1];
    //console.log(this.crearAula); 
  }

  //*****************************Seccion Aula******************************************************/

  public crearAula = new Aula();

  getTodosAulas() {
    this.gestionAcademicaService.mostrarAulas({ codAula: 0 }).subscribe(
      (aulasData) => {
        //console.log('Recibiendo todas las aulas');
        this.inAulasJsonArray = aulasData;
        this.dtTrigger[0].next();
      },
      (err) => console.log(err)
    );
  }

  activar(codigoAula){
    this.gestionAcademicaService.mostrarAulas({ codAula: codigoAula }).subscribe(
      (aulasData) => {
        this.crearAula.codAula = aulasData[0].codAula;
        this.crearAula.capacidad = aulasData[0].capacidad;
        this.crearAula.descripcion = aulasData[0].descripcion;
        if (aulasData[0].habilitada == 0) {
            this.crearAula.habilitada = 1;
        } else {
            this.crearAula.habilitada=0;
        }
        
        //enviar data para actualzar
        this.gestionAcademicaService.actualizarAula(this.crearAula).subscribe(
          (data) => {
            if (data.mensaje == 1) {
              this.dtElements.first.dtInstance.then(
                (dtInstance: DataTables.Api) => {
                  dtInstance.destroy(); 
                  this.getTodosAulas();
                });
            }  
          },
          (erro) => {

          }
        )
      },
      (err) => console.log(err)
    );
  }

  enviarDatosCrearAula() {
    if(this.Validacion.validar_Aula(this.crearAula.descripcion,this.crearAula.capacidad,this.crearAula.habilitada)==0){
      this.gestionAcademicaService.agregarAula(this.crearAula).subscribe(
        (data) => {
          if(data.mensaje == 1)
          { 
            this.dtElements.first.dtInstance.then(
              (dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                $("#newAula").modal("hide");
                $('#modal-exito_aula .modal-body').html('<p>Se Guardo la Aula con exito</p>');
                $('#modal-exito_aula .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito_aula').modal('show');
                this.getTodosAulas();
              });  
          }else{
            $("#newAula").modal("hide");
            $('#modal-exito_aula .modal-body').html('<p>No se guardo la Aula</p>');
            $('#modal-exito_aula .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_aula').modal('show');
          }
        },
        (erro) => {

        }
      )
    }
  }


  setEstadoAula(){
    if(this.crearAula.habilitada==0){
      this.crearAula.habilitada=1;
    }
    else{
      this.crearAula.habilitada=0;
    }
    //console.log(this.crearAula);
    this.enviarDatosActualizarAula();
  }

  enviarDatosActualizarAula() {
    if(this.Validacion.validar_Aula(this.crearAula.descripcion,this.crearAula.capacidad,this.crearAula.habilitada)==0){
      this.gestionAcademicaService.actualizarAula(this.crearAula).subscribe(
        (data) => {
          if (data.mensaje == 1) {
            this.dtElements.first.dtInstance.then(
              (dtInstance: DataTables.Api) => {
                dtInstance.destroy();
                $("#newAula").modal("hide");
                $('#modal-exito_aula .modal-body').html('<p>Se Actualizo la Aula con exito</p>');
                $('#modal-exito_aula .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito_aula').modal('show');
                this.getTodosAulas();
              });
          } else {
            $("#newAula").modal("hide");
            $('#modal-exito_aula .modal-body').html('<p>No se pudo Actualizar la Aula</p>');
            $('#modal-exito_aula .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_aula').modal('show');
          }
        },
        (erro) => {

        }
      )
    }
  }



  //************************************************************************************************/

  iniciarTablaAulas() {
    //opciones para datatable
    this.dtOptions[0] = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      },
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.getFilaAulasSelec(data);
        });
        return row;
      }
    };
  }

  setCheckStatus(valor) {
    if (valor == 1) {
      return "Activo";
    } else {
      return "Innactivo";
    }
  }

  habilitado(valor) {
    if (valor == 1) {
      return true;
    } else {
      return false;
    }
  }

  deshabilitado(valor) {
    if (valor == 0) {
      return true;
    } else {
      return false;
    }
  }

  nuevaAula() {
    this.Validacion.removerValidacionesAula();
    $("#newAula").modal("show");
    $('#btn-actualizar').addClass('d-none');
    $('#btn-guardar').removeClass('d-none');
    $('.modal-title').html('Registrar Aula ');
    $("#txt-descripcionAula").val("");
    $("#txt-unidadesCurso").val("");
  }

  actualizarAula() { 
    this.Validacion.removerValidacionesAula();
    $("#newAula").modal("show");
    $('.modal-title').html('Actualizar Aula ' + this.crearAula.descripcion);
    $("#txt-descripcionAula").val(this.crearAula.descripcion);
     $("#txt-unidadesCurso").val(this.crearAula.capacidad);
    $('#btn-guardar').addClass('d-none');
    $('#btn-actualizar').removeClass('d-none');
  }





}
