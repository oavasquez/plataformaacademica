import { Component, OnInit } from '@angular/core';
import { GestionAcademicaService } from '../../gestion-academica.service';

@Component({
  selector: 'app-gest-dashboard',
  templateUrl: './gest-dashboard.component.html',
  styleUrls: ['./gest-dashboard.component.css']
})
export class GestDashboardComponent implements OnInit {

  public docenteCards={};
  public aulaCards={};
  public seccionCards={};
  public mostrar=false;

  constructor(public gestionAcademicaService:GestionAcademicaService) { }

  ngOnInit() {
    this.cargarTarjetas();

  }

  cargarTarjetas(){
    //mostrar cantidad de docente habilitados
    this.gestionAcademicaService.mostrarDashboard('{"codCard":1}').subscribe(
      (data) => {
   
        this.docenteCards=data[0];

       
        this.docenteCards=={}? this.mostrar=false:this.mostrar=true;

      },
      (err) => { console.log(err)}
    );
    //mostrar cantidad de aulas disponibles
    this.gestionAcademicaService.mostrarDashboard('{"codCard":2}').subscribe(
      (data) => {
     

        this.aulaCards=data[0];

        this.aulaCards=={}? this.mostrar=false:this.mostrar=true;

      },
      (err) => { console.log(err)}
    );
    //mostrar 
    this.gestionAcademicaService.mostrarDashboard('{"codCard":3}').subscribe(
      (data) => {
     
       
        this.seccionCards=data[0];
        

        this.seccionCards=={}? this.mostrar=false:this.mostrar=true;

      },
      (err) => { console.log(err)}
    );


  }
}
