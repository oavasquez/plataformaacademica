import { Experiencia } from './../../../../../shared/model/experiencia.model';
import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';
import { Docente } from '../../../../../shared/model/docente.model';
import { GestionAcademicaService } from '../../../gestion-academica.service';
import { Cargo } from '../../../../../shared/model/cargo.model';
import { Validacion_GA } from '../../../../../shared/model/validaciones_GestionAcademica.model';

// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-formulario-docente',
  templateUrl: './formulario-docente.component.html',
  styleUrls: ['./formulario-docente.component.css']
})
export class FormularioDocenteComponent implements OnInit {
  @Input() data: Docente;
  @Input() mensaje: string;
  @Input() objDocente: Docente; //objeto con la informacion del docente capturado
  @Input() actionFlag: number; //bader de acción--> 1 para presentacion | 2 para insertar | 3 para actualizar

  Validaciones =new Validacion_GA();

  constructor(private router: Router, private gestionAcademicaService: GestionAcademicaService) {

  }

  ngOnInit() {
    if (this.actionFlag == 1) {
      this.presentarData();
    }
    this.presentarBotonesAccion(this.actionFlag);
    this.nuevoDocente = this.data;
    this.mensaje = this.nuevoDocente.getNombre() + " " + this.nuevoDocente.getApellido();
    //funciones para los cargos del docente
    this.mostrarCargo();
    this.mostrarDepartamento();
    this.mostrarCargoDocenteActualizar()
  }

  presentarData() {
    //enlazar los campos entrantes con ngmodel
    //console.log('Data del docente', this.data);
  }


  //*************************seccion docente**************************************************
  nuevoDocente = new Docente();


  crearNuevoDocente() {
    this.nuevoDocente.setImagenPerfil("");
    this.nuevoDocente.setCargoArray(this.cargoAgregadosDocente);
    //console.log(this.nuevoDocente);
    //console.log(JSON.stringify(this.nuevoDocente));
    if(this.Validaciones.validar_Laboral(this.nuevoDocente.getNumeroExpediente(),this.nuevoDocente.getFechaIngreso())==0){
      if(this.nuevoDocente.getCargoArray().length>0){
        this.gestionAcademicaService.agregarDocente(JSON.stringify(this.nuevoDocente)).subscribe(
          (data) => {
            if (data.mensaje != 0) {
              alert('Docente Registrado con éxito.');
              this.goToDocentes();
            } else {
              alert('No se pudo Registrar');
            }
          },
          (err) => console.log(err)
        );
      }else{
        alert('No tiene cargos agregados');
      }
    }
  }

  actualizarDocente = new Docente();
  actualizaDocente() {
    //console.log(this.nuevoDocente);
    this.nuevoDocente.setCargoArray(this.cargoAgregadosDocente);
    if(this.Validaciones.validar_Laboral(this.nuevoDocente.getNumeroExpediente(),this.nuevoDocente.getFechaIngreso())==0){
      if(this.nuevoDocente.getCargoArray().length>0){
        this.gestionAcademicaService.actualizarDocente(JSON.stringify(this.nuevoDocente)).subscribe(
          (data) => {
           // console.log(data);
            if (data.mensaje != 0) {
              alert('Docente actualizado con éxito.');
              this.goToDocentes();
            } else {
              alert('No se pudo actualizar');
            }
          },
          (err) => console.log(err)
        );
      }
    }
  }
  //******************************************************************************************

  //*************************seccion Experiencia**************************************************
  nuevaExperiencia = new Experiencia();
  crearNuevaExperiencia() {
    //console.log(this.nuevaExperiencia);
  }

  //******************************************************************************************


  //*************************seccion Laboral**************************************************
  cargo = new Cargo;
  cargoAgregadosDocente: Cargo[] = [];
  //json para carga los input de autocompletar ngui
  cargosJsonArray: any;
  departamentosJsonArray: any;


  agregarNuevoCargoDocente() {
    //console.log(this.cargo);
    if(this.Validaciones.validar_cargo(this.cargo.getCargo(),this.cargo.getInstitucion(),this.cargo.getFechaInicio(),this.cargo.getFechaFinal(),this.cargo.getDepartamento())==0){
      this.cargoAgregadosDocente.push(this.cargo); 
      $('#modalDocenteLaboral').modal('hide');  
    }
  }

  //******************************************************************************************

  presentarBotonesAccion(actionFlag) {
    //presentar los botones a utilizar segun la acción requerida
    switch (actionFlag) {
      case 1: {
        //console.log('unicamente boton de volver y actualizar');
        $('#btn-guardarDocente').hide();
        $('#btn-actualizarDocente').hide();
        this.mensaje = "Nombre completo de usuario";
        break;
      }
      case 2: {
        //console.log('presentar botones de guardar');
        $('#btn-actualizarDocente').hide();
        $('#btn-guardarDocente').hide();
        this.mensaje = "Fomulario de ingreso de docente";
      }
      default:
        break;
    }
  }

  goToDocentes() {
    this.router.navigate(['/home/gestion/gest-docentes']);
  }

  /**********************************obteniendo Cargos*****************************************/

  mostrarCargo() {
    this.gestionAcademicaService.mostrarCargos({ codCargo: 0 }).subscribe(
      (data) => {
        //console.log(data)
        this.cargosJsonArray = data;

      },
      (err) => console.log(err)
    )

  }

  /**********************************obteniendo Departamento***********************************/

  mostrarDepartamento() {
    this.gestionAcademicaService.mostrarDepartamentos({ codDepartamento: 0 }).subscribe(
      (data) => {
       // console.log(data)
        this.departamentosJsonArray = data;

      },
      (err) => console.log(err)
    )

  }

  /**********************************obteniendo Cargo Docente***********************************/
  //esta funcion se activa cuando se entra al formulario de actualizar
  mostrarCargoDocenteActualizar() {
    //console.log(this.nuevoDocente.codDocente);
    //console.log(JSON.stringify(this.nuevoDocente));

    if (this.nuevoDocente.codDocente != null) {

      this.gestionAcademicaService.mostrarCargoDocente(this.nuevoDocente).subscribe(
        (data) => {
          //console.log(data)
          this.cargoAgregadosDocente = data;


        },
        (err) => console.log(err)
      )
    }

  }


  /********************************************************************************************/


  //esta funcion es solo para limpiar el arreglo que se muestra al asignar 
  //un cargo en el formulario nuevo Docente 

  eliminarAsignaturaCurso(key: any) {
    this.cargoAgregadosDocente.splice(key, 1);

  }

  //limpia el arreglo de Cargos del fomulario nueva seccion 

  limpiarDatosCargo() {
    this.cargo.construir(0, "", 0, "", "", "", "", "");
    this.Validaciones.removerValidacionesCargo();
  }

  tabActual:number=0;
  goToNewsTabs(){  
    if(this.tabActual==0){
      if(this.Validaciones.validar_datosPersonalesDocente(this.nuevoDocente.getNombre(),this.nuevoDocente.getApellido(),this.nuevoDocente.getIdentidad(),this.nuevoDocente.getEdad(),this.nuevoDocente.getSexo(),this.nuevoDocente.getFechaNacimiento(),this.nuevoDocente.getPaisOrigen(),this.nuevoDocente.getNacionalidad(),this.nuevoDocente.getDepartamentoNacimiento(),this.nuevoDocente.getLugarNacimiento())==0){
        $('#datos1').removeClass('active');
        $('#datos2').addClass('active');
        $('#datosPersonales').removeClass('active');
        $('#datosContacto').addClass('active');
        this.tabActual+=1;
      }
    }else
    if(this.tabActual==1){
      if(this.Validaciones.validar_Contacto(this.nuevoDocente.getCorreo(),this.nuevoDocente.getCelular(),this.nuevoDocente.getTelefono(),this.nuevoDocente.getDireccion())==0){
        $('#datos2').removeClass('active');
        $('#datos3').addClass('active');
        $('#datosContacto').removeClass('active');
        $('#fichaMedica').addClass('active');
        this.tabActual+=1;
      } 
    }else
    if(this.tabActual==2){
      if(this.Validaciones.validar_FichaMedica(this.nuevoDocente.getTipoSangre())==0){
        $('#datos3').removeClass('active');
        $('#datos4').addClass('active');
        $('#fichaMedica').removeClass('active');
        $('#experiencia').addClass('active');
        if(this.actionFlag==1){
          $('#btn-guardarDocente').hide();
          $('#btn-continuarDocente').hide();
          $('#btn-actualizarDocente').show();
        }else{
          $('#btn-guardarDocente').show();
          $('#btn-actualizarDocente').hide();
          $('#btn-continuarDocente').hide();
        }
      }
    }
  }
}
