import { AnoLectivo } from './../../../../../shared/model/anoLectivo.model';
import { Jornada } from './../../../../../shared/model/jornada.model';
import { Periodo } from './../../../../../shared/model/periodo.model';
import { Asignatura } from './../../../../../shared/model/asignatura.model';
import { Aula } from './../../../../../shared/model/aula.model';
import { Curso } from './../../../../../shared/model/curso.model';
import { Docente } from './../../../../../shared/model/docente.model';
import { AdministrarService } from './../../../../administracion/administracion.service';
import { Seccion } from './../../../../../shared/model/seccion.model';
import { Validacion_GA } from './../../../../../shared/model/validaciones_GestionAcademica.model';
import { GestionAcademicaService } from './../../../gestion-academica.service';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-formulario-seccion',
  templateUrl: './formulario-seccion.component.html',
  styleUrls: ['./formulario-seccion.component.css']
})
export class FormularioSeccionComponent implements OnInit {

  @Input() tituloFormulario: string;
  @Input() objSeccion: Seccion; //objeto con la informacion del docente capturado
  @Input() actionFlag: number; //bader de acción--> 1 para presentar y actualizar, 2 para guardar

  constructor(
    private router: Router,
    private gestionAcademicaService: GestionAcademicaService,
    private administrarService: AdministrarService
  ) { }


  //*******************seccion para crear secciones********************/
  public nuevaSeccion = new Seccion()
  Validacion = new Validacion_GA();

  crearSeccion() {
    //por defaultla seccion no esta suspendida al momento de crearla
    this.nuevaSeccion.setSuspendida(0);
    if (this.Validacion.validar_Seccion(this.nuevaSeccion.getCodDocente(), this.nuevaSeccion.getCodCurso(), this.nuevaSeccion.getCodAsignatura(), this.nuevaSeccion.getSeccion(), this.nuevaSeccion.getHoraInicio(), this.nuevaSeccion.getHoraFin(), this.nuevaSeccion.getCodAula(), this.nuevaSeccion.getCodPeriodo(), this.nuevaSeccion.getCodJornada(), this.nuevaSeccion.getCodLectivo()) == 0) {
      //console.log(JSON.stringify(this.nuevaSeccion))
      this.gestionAcademicaService.agregarSeccion(JSON.stringify(this.nuevaSeccion)).subscribe(
        (data) => {
          //console.log(data);
          if (data.mensaje == 1) {
            $("#newAula").modal("hide");
            $('#modal-exito_seccion .modal-body').html('<p>Se Guardo la Seccion con exito</p>');
            $('#modal-exito_seccion .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_seccion').modal('show');
            this.goToSecciones();
          } else {
            $("#newAula").modal("hide");
            $('#modal-exito_seccion .modal-body').html('<p>Revise los datos de la Seccion</p>');
            $('#modal-exito_seccion .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_seccion').modal('show');
          }
        },
        (err) => console.log(err)
      );
    }
  }

  actualizarSecciones() {
    if (this.Validacion.validar_Seccion(this.nuevaSeccion.getCodDocente(), this.nuevaSeccion.getCodCurso(), this.nuevaSeccion.getCodAsignatura(), this.nuevaSeccion.getSeccion(), this.nuevaSeccion.getHoraInicio(), this.nuevaSeccion.getHoraFin(), this.nuevaSeccion.getCodAula(), this.nuevaSeccion.getCodPeriodo(), this.nuevaSeccion.getCodJornada(), this.nuevaSeccion.getCodLectivo()) == 0) {
     // console.log(JSON.stringify(this.nuevaSeccion))
      this.gestionAcademicaService.actualizarSeccion(JSON.stringify(this.nuevaSeccion)).subscribe(
        (data) => {
       //   console.log(data);
          if (data.mensaje == 1) {
            $('#modal-exito_seccion .modal-body').html('<p>Se actualizo la Seccion con exito</p>');
            $('#modal-exito_seccion .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_seccion').modal('show');
            this.goToSecciones();
          } else {
            $('#modal-exito_seccion .modal-body').html('<p>Revise los datos de la Seccion</p>');
            $('#modal-exito_seccion .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_seccion').modal('show');
          }
        },
        (err) => console.log(err)
      );
    }
  }



  //*******************************************************************/


  //*******************jSON para autocompletacion**********************/
  public jsonNombreDocente: any;
  public jsonNombreCurso: any;
  public jsonNombreAsignatura: any;
  public jsonNombreAula: any;
  public jsonNombrePeriodo: any;
  public jsonNombreJornada: any;
  public jsonNombreLectivo: any;
  public keyboard: any;
  public buscarDocente = new Docente;
  public buscarCurso = new Curso;
  public buscarAsignatura = new Asignatura;
  public buscarAula = new Aula;
  public buscarPeriodo = new Periodo;
  public buscarJornada = new Jornada;
  public buscarLectivo = new AnoLectivo;



  /*******************funciones para recargar json*********************/
  recargarJson() {
    //agregando nombres de los Docentes al json
    this.buscarDocente.setCodDocente(0);
    this.gestionAcademicaService.mostrarDocentes(JSON.stringify(this.buscarDocente)).subscribe(
      (data) => { 
        this.jsonNombreDocente = data;

      },  //error 
      (err) => console.log(err)
    );
    //agregando nombre de los cursos al json
    this.buscarCurso.setCodigoCurso(0)
    this.administrarService.mostrarCursos(JSON.stringify(this.buscarCurso)).subscribe(
      (data) => {
        this.jsonNombreCurso = data;

      },  //error 
      (err) => console.log(err)
    );
    //agregando nombre de las asignaturas al json
    this.buscarAsignatura.setCodCurso(0)
    this.administrarService.mostrarAsignatura(JSON.stringify(this.buscarAsignatura)).subscribe(
      (data) => {
        this.jsonNombreAsignatura = data;

      },  //error 
      (err) => console.log(err)
    );
    //agregando nombre de las aulas al json
    this.buscarAula.codAula = 0;
    this.gestionAcademicaService.mostrarAulas(JSON.stringify(this.buscarAula)).subscribe(
      (data) => {
        this.jsonNombreAula = data;

      },  //error 
      (err) => console.log(err)
    );
    //agregando nombre de los periodos al json
    this.buscarPeriodo.setCodPeriodo(0);
    this.administrarService.mostrarPeriodo(JSON.stringify(this.buscarPeriodo)).subscribe(
      (data) => {
        this.jsonNombrePeriodo = data;

      },  //error 
      (err) => console.log(err)
    );
    //agregando nombre de las jornadas al json
    this.buscarJornada.setCodJornada(0);
    this.administrarService.mostrarJornada(JSON.stringify(this.buscarJornada)).subscribe(
      (data) => {
        this.jsonNombreJornada = data;

      },  //error 
      (err) => console.log(err)
    );
    this.buscarLectivo.setCodLectivo(0);
    this.administrarService.mostrarAnioLectivo(JSON.stringify(this.buscarLectivo)).subscribe(
      (data) => {
        this.jsonNombreLectivo = data;


      },  //error 
      (err) => console.log(err)
    );
  }

  llenarListaAsignaturaACurso(codCurso) {
    //console.log("buscando asignaturas a curso")
    //agregando nombre de las asignaturas al json


   // console.log(this.buscarAsignatura)
    this.administrarService.mostrarAsignatura({ codCurso: codCurso, habilitado: 0 }).subscribe(
      (data) => {
        //console.log(data);
        this.jsonNombreAsignatura = data;

      },  //error 
      (err) => console.log(err)
    );

  }

  //*************************psando valores al objeto seccion***********************************************/
  setCodigoAula(event) { return event.codAula }
  setNombreAula(event) { return event.descripcion }
  setCodigoCurso(event) { return event.codCurso }
  setNombreCurso(event) { return event.nombreCurso }
  setCodigoDocente(event) { return event.codDocente }
  setNombreCompletoDocente(event) { return event.nombreCompleto }
  setCodigoAsignatura(event) { return event.codAsignatura }
  setNombreAsignatura(event) { return event.asignatura }
  setCodigoPeriodo(event) { return event.codPeriodo }
  setNombrePeriodo(event) { return event.nombrePeriodo }
  setCodigoJornada(event) { return event.codJornada }
  setNombreJornada(event) { return event.jornada }
  setCodigoLectivo(event) { return event.codLectivo }
  setNombreLectivo(event) { return event.nombre }
  setCodigoSupendida(event) { return event ? 1 : 0 }




  //*******************************************************************/


  ngOnInit() {
    if (this.actionFlag == 1) {
      this.nuevaSeccion = this.objSeccion;
      this.presentarData();
    }
    this.presentarBotonesAccion(this.actionFlag);
    // llamar servicios con la data necesaria para rellenar formulario
    // los docentes, los cursos, etc...

    // inicializando las variables json
    this.recargarJson();
    this.nuevaSeccion = this.objSeccion;
    //console.log(this.objSeccion);
  }

  presentarData() {
    //enlazar los campos entrantes con ngmodel 
    //console.log(this.objSeccion);
  }

  presentarBotonesAccion(actionFlag) {
    //presentar los botones a utilizar segun la acción requerida
    switch (actionFlag) {
      case 1: {
        //console.log('unicamente boton de volver y actualizar');
        this.Validacion.removerValidacionesSeccion();
        $('.btn-group #btn-actualizar').show();
        $('.btn-group #btn-guardar').hide();
        this.tituloFormulario = this.nuevaSeccion.getSeccion();
        break;
      }
      case 2: {
        //console.log('presentar botones de guardar');
        this.Validacion.removerValidacionesSeccion();
        $('.btn-group #btn-actualizar').hide();
        $('.btn-group #btn-guardar').show();
        this.tituloFormulario = "Crear nueva sección";
      }
      default:
        break;
    }
  }

  goToSecciones() {
    this.router.navigate(['/home/gestion/gest-secciones']);
  }

}
