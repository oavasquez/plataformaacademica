import { Component, NgZone, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { GestionAcademicaService } from "../../../gestion-academica/gestion-academica.service";
import { Router } from '@angular/router';
import { Docente } from '../../../../shared/model/docente.model';
import { DataTableDirective } from 'angular-datatables';
// import { jQuery } from 'jquery';

// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-docentes',
  templateUrl: './docentes.component.html',
  styleUrls: ['./docentes.component.css']
})
export class DocentesComponent implements OnInit {
  //variables de API de componente
  @Input() inDocentesJsonArray: object[];
  @Output() outRecargarTablaDocentes = new EventEmitter();
  @Output() outCambiarMensaje = new EventEmitter();
  @Output() outSetParametroDocente = new EventEmitter();
  @Output() outSetActionForm = new EventEmitter();
  @Output() outSetDocentesActionFlag = new EventEmitter();
  
  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  dtDocentesOpt: DataTables.Settings = {};
  
 
  private numFilaSelected = -1;
  objDocente = new Docente();
 
  constructor(
    private router: Router,
    private GAService: GestionAcademicaService) { }

  ngOnInit() {
    this.getTodosDocentes();
    this.iniciarTablaDocente();

  }

  goToForm() {
    //enviar dato seleccionado al maincontainer para que se actualice en el formulario docente
    // this.outCambiarMensaje.emit();
    // this.outSetParametroDocente.emit(this.objDocente); 
    // this.outSetDocentesActionFlag.emit(1);
    this.router.navigate(['/home/gestion/formulario-docente']);
  }

  ingresarNuevoDocente(parametro) {
    //activar el formulario de nuevo docente:
    this.outSetParametroDocente.emit(this.objDocente);
    this.outSetDocentesActionFlag.emit(parametro);
    this.goToForm();
  }

  update(codDocente){
    this.objDocente.setCodDocente(codDocente);
    this.GAService.mostrarDocentes({ codDocente: this.objDocente.getCodDocente() }).subscribe(
      (docentesData) => {
        //console.log(docentesData);
        if (docentesData != 0) {
          this.objDocente.setNombre(docentesData[0].nombres);
          this.objDocente.setApellido(docentesData[0].apellidos);
          this.objDocente.setIdentidad(docentesData[0].identificacion);
          this.objDocente.setEdad(docentesData[0].edad);
          this.objDocente.setCelular(docentesData[0].celular);
          this.objDocente.setCondicionesEspeciales(docentesData[0].condicionesEspeciales);
          this.objDocente.setCorreo(docentesData[0].correoElectronico);
          this.objDocente.setDepartamentoNacimiento(docentesData[0].departamentoNacimiento);
          this.objDocente.setDireccion(docentesData[0].direccionActual);
          this.objDocente.setEnfermedadesComunes(docentesData[0].enfermedadesComunes);
          this.objDocente.setFechaIngreso(docentesData[0].fechaIngreso);
          this.objDocente.setFechaNacimiento(docentesData[0].fechaNacimiento);
          this.objDocente.setLugarNacimiento(docentesData[0].lugarNacimiento);
          this.objDocente.setNacionalidad(docentesData[0].nacionalidad);
          this.objDocente.setPaisOrigen(docentesData[0].paisOrigen);
          this.objDocente.setSexo(docentesData[0].sexo);
          this.objDocente.setTelefono(docentesData[0].telefonoFijo);
          this.objDocente.setTipoSangre(docentesData[0].tipoSangre);
          this.objDocente.setCodPersona(docentesData[0].codDocente);
          this.objDocente.setCodDocente(docentesData[0].codDocente);
          this.objDocente.setHabilitado(docentesData[0].habilitado);
          this.objDocente.setImagenPerfil(""); 
          
          this.outSetParametroDocente.emit(this.objDocente);
          this.outSetDocentesActionFlag.emit(1);
          this.goToForm();
        } else {
          alert('No se recibio nada');
        }

      },
      (err) => console.log(err)
    );
  }


  activar(codDocente){ 
    this.GAService.mostrarDocentes({ codDocente: codDocente }).subscribe(
      (docentesData) => {
        //console.log(docentesData);
        if (docentesData != 0) {
          this.objDocente.setCodDocente(codDocente);
          this.objDocente.setNombre(docentesData[0].nombres);
          this.objDocente.setApellido(docentesData[0].apellidos);
          this.objDocente.setIdentidad(docentesData[0].identificacion);
          this.objDocente.setEdad(docentesData[0].edad);
          this.objDocente.setCelular(docentesData[0].celular);
          this.objDocente.setCondicionesEspeciales(docentesData[0].condicionesEspeciales);
          this.objDocente.setCorreo(docentesData[0].correoElectronico);
          this.objDocente.setDepartamentoNacimiento(docentesData[0].departamentoNacimiento);
          this.objDocente.setDireccion(docentesData[0].direccionActual);
          this.objDocente.setEnfermedadesComunes(docentesData[0].enfermedadesComunes);
          this.objDocente.setFechaIngreso(docentesData[0].fechaIngreso);
          this.objDocente.setFechaNacimiento(docentesData[0].fechaNacimiento);
          this.objDocente.setLugarNacimiento(docentesData[0].lugarNacimiento);
          this.objDocente.setNacionalidad(docentesData[0].nacionalidad);
          this.objDocente.setPaisOrigen(docentesData[0].paisOrigen);
          this.objDocente.setSexo(docentesData[0].sexo);
          this.objDocente.setTelefono(docentesData[0].telefonoFijo);
          this.objDocente.setTipoSangre(docentesData[0].tipoSangre);
          this.objDocente.setCodPersona(docentesData[0].codDocente);
          this.objDocente.setCodDocente(docentesData[0].codDocente);
          this.objDocente.setImagenPerfil("");
          if (docentesData[0].habilitado == 0) {
              this.objDocente.setHabilitado(1);
          } else {
              this.objDocente.setHabilitado(0);
          }

          //llamar el servicio para actualizar
          this.GAService.actualizarDocente(JSON.stringify(this.objDocente)).subscribe(
            (data) => {
              //console.log(data);
              if (data.mensaje != 0) { 
                this.datatableElement.dtInstance.then(
                  (dtInstance: DataTables.Api) => { 
                    dtInstance.destroy(); 
                    this.getTodosDocentes();
                    this.iniciarTablaDocente();
                  });  
              } else {
                alert('No se pudo actualizar');
              }
            },
            (err) => console.log(err)
          ) 

          
        } else {
          alert('No se recibio nada');
        }

      },
      (err) => console.log(err)
    );
  }
  actualizarDocente() {
    //activar el formulario de nuevo docente:   
    this.datatableElement.dtInstance.then(
      (dtInstance: DataTables.Api) => {
        //console.log(dtInstance.row(1).id());
        if (this.numFilaSelected != -1) {
         // console.log(this.numFilaSelected);
          //console.log(dtInstance.row(this.numFilaSelected - 1).id());
          this.numFilaSelected = -1;

          //consultar la información del docente
          this.GAService.mostrarDocentes({ codDocente: this.objDocente.getCodDocente() }).subscribe(
            (docentesData) => {
              //console.log(docentesData);
              if (docentesData != 0) {
                this.objDocente.setNombre(docentesData[0].nombres);
                this.objDocente.setApellido(docentesData[0].apellidos);
                this.objDocente.setIdentidad(docentesData[0].identificacion);
                this.objDocente.setEdad(docentesData[0].edad);
                this.objDocente.setCelular(docentesData[0].celular);
                this.objDocente.setCondicionesEspeciales(docentesData[0].condicionesEspeciales);
                this.objDocente.setCorreo(docentesData[0].correoElectronico);
                this.objDocente.setDepartamentoNacimiento(docentesData[0].departamentoNacimiento);
                this.objDocente.setDireccion(docentesData[0].direccionActual);
                this.objDocente.setEnfermedadesComunes(docentesData[0].enfermedadesComunes);
                this.objDocente.setFechaIngreso(docentesData[0].fechaIngreso);
                this.objDocente.setFechaNacimiento(docentesData[0].fechaNacimiento);
                this.objDocente.setLugarNacimiento(docentesData[0].lugarNacimiento);
                this.objDocente.setNacionalidad(docentesData[0].nacionalidad);
                this.objDocente.setPaisOrigen(docentesData[0].paisOrigen);
                this.objDocente.setSexo(docentesData[0].sexo);
                this.objDocente.setTelefono(docentesData[0].telefonoFijo);
                this.objDocente.setTipoSangre(docentesData[0].tipoSangre);
                this.objDocente.setCodPersona(docentesData[0].codDocente);
                this.objDocente.setCodDocente(docentesData[0].codDocente);
                this.objDocente.setHabilitado(docentesData[0].habilitado);
                this.objDocente.setImagenPerfil("");

               // console.log(this.objDocente);
                this.outSetParametroDocente.emit(this.objDocente);
                this.outSetDocentesActionFlag.emit(1);
                this.goToForm();
              } else {
                alert('No se recibio nada');
              }

            },
            (err) => console.log(err)
          );
        } else {
          alert("Seleccione una fila");
        }
      }
    );
    //this.displayToConsole(this.datatableElement);

  }
  
  recargarTablaDocentes() {
    this.outRecargarTablaDocentes.emit();
  }

  getTodosDocentes() {
    this.GAService.mostrarDocentes({ codDocente: 0 }).subscribe(
      (docentesData) => {
        //console.log('Recibiendo todos los docentes');
        this.inDocentesJsonArray = docentesData; 
        this.dtTrigger.next();
      },
      (err) => console.log(err)
    );
  }

  

  iniciarTablaDocente() {
    //opciones para datatable
    this.dtDocentesOpt = {   
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      } 
    };
  }


  setCheckStatus(valor) {
    if (valor == 1) {
      return "Activo";
    } else {
      return "Innactivo";
    }
  }

  habilitado(valor) {
    if (valor == 1) {
      return true;
    } else {
      return false;
    }
  }

  deshabilitado(valor) {
    if (valor == 0) {
      return true;
    } else {
      return false;
    }
  }

  cambiarEstado(estado) {
    var id = '#' + estado
    if ($(id).val() == "Habilitar") {
      $(id).removeClass('btn-success');
      $(id).addClass('btn-danger');
      $(id).val("Desabilitar");
      $(id).html("Desabilitar");
    } else {
      $(id).removeClass('btn-danger');
      $(id).addClass('btn-success');
      $(id).val("Habilitar");
      $(id).html("Habilitar");
    }
  }

  cambiar(){
    
  }
 
  


}
