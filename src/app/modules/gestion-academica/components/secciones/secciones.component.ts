import { Component, OnInit, Input, Output, EventEmitter, QueryList, NgZone, ViewChildren } from '@angular/core';
import { Router } from '@angular/router';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
import { GestionAcademicaService } from "../../gestion-academica.service";
import { Seccion } from '../../../../shared/model/seccion.model';
import { Validacion_GA } from '../../../../shared/model/validaciones_GestionAcademica.model';

// import { jQuery } from 'jquery';
// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;
@Component({
  selector: 'app-secciones',
  templateUrl: './secciones.component.html',
  styleUrls: ['./secciones.component.css']
})
export class SeccionesComponent implements OnInit {

  @Output() outSetParamentroSeccion = new EventEmitter();
  @Output() outSetSeccionActionForm = new EventEmitter();
  @Output() outsetActionFlag = new EventEmitter();

  public seccionesJsonArray: object[];
  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;
  dtOptions: DataTables.Settings[] = [];
  dtTrigger: Subject<any>[] = [new Subject(), new Subject()];

  private objSeccion = new Seccion();
  constructor(
    private router: Router,
    private gestionAcademicaService: GestionAcademicaService,
    private zone: NgZone) { }

  ngOnInit() {
    this.cargarSecciones();
    this.iniciarTablaSecciones();
  }

  getSelectedSeccion(info: any): void {
   // console.log(info);
  }

  iniciarTablaSecciones() {
    this.dtOptions[0] = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No se encontraron resultados",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No records available",
        "infoFiltered": "(filtered from _MAX_ total records)"
      },
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.getSelectedSeccion(data);
        });
        return row;
      }
    };
  }

  cargarSecciones() {
    this.gestionAcademicaService.mostrarSecciones({ codSeccion: 0 }).subscribe(
      (seccionesData) => {
        //console.log('Recibiendo todas las secciones');
        this.seccionesJsonArray = seccionesData;
       // console.log(this.seccionesJsonArray);
        this.dtTrigger[0].next();
      },
      (err) => console.log(err)
    );
  }

  crearNuevaSeccion() {
    this.outSetParamentroSeccion.emit(this.objSeccion);
    this.outsetActionFlag.emit(2);
    this.goToFomularioSeccion();
  }

  actualizarSeccion(codSeccion) {

    //cargar info de seccion
    this.gestionAcademicaService.mostrarSecciones({ codSeccion: codSeccion }).subscribe(
      (seccionesData) => {
        if (seccionesData.mensaje != 0) {
          this.objSeccion.setCodSeccion(codSeccion);
          this.objSeccion.setSeccion(seccionesData[0].seccion);
          this.objSeccion.setCodAsignatura(seccionesData[0].codAsignatura);
          this.objSeccion.setCodAula(seccionesData[0].codAula);
          this.objSeccion.setCodCurso(seccionesData[0].codCurso);
          this.objSeccion.setCodDocente(seccionesData[0].codDocente);
          this.objSeccion.setCodJornada(seccionesData[0].codJornada);
          this.objSeccion.setCodLectivo(seccionesData[0].codLectivo);
          this.objSeccion.setCodPeriodo(seccionesData[0].codPeriodo);
          this.objSeccion.setHoraFin(seccionesData[0].horaFin);
          this.objSeccion.setHoraInicio(seccionesData[0].horaInicio);
          this.objSeccion.setNombreAula(seccionesData[0].nombreAula);
          this.objSeccion.setNombreAsignatura(seccionesData[0].asignatura);
          this.objSeccion.setNombreJornada(seccionesData[0].jornada);
          this.objSeccion.setNombreLectivo(seccionesData[0].nombre);
          this.objSeccion.setNombrePeriodo(seccionesData[0].nombrePeriodo);
          this.objSeccion.setNombreCurso(seccionesData[0].nombreCurso);
          this.objSeccion.setNombreCompletoDocente(seccionesData[0].nombreCompletoDocente);
          this.objSeccion.setSuspendida(seccionesData[0].habilitada);
          this.outSetParamentroSeccion.emit(this.objSeccion);
          this.outsetActionFlag.emit(1);
          this.goToFomularioSeccion();
        } else {
          alert('No encontró seccipón');
        }

      },
      (err) => console.log(err)
    );
  }


  activar(codSeccion) {
    this.gestionAcademicaService.mostrarSecciones({ codSeccion: codSeccion }).subscribe(
      (seccionesData) => {
        //console.log(seccionesData);
        if (seccionesData.mensaje != 0) {
          this.objSeccion.setCodSeccion(codSeccion);
          this.objSeccion.setSeccion(seccionesData[0].seccion);
          this.objSeccion.setCodAsignatura(seccionesData[0].codAsignatura);
          this.objSeccion.setCodAula(seccionesData[0].codAula);
          this.objSeccion.setCodCurso(seccionesData[0].codCurso);
          this.objSeccion.setCodDocente(seccionesData[0].codDocente);
          this.objSeccion.setCodJornada(seccionesData[0].codJornada);
          this.objSeccion.setCodLectivo(seccionesData[0].codLectivo);
          this.objSeccion.setCodPeriodo(seccionesData[0].codPeriodo);
          this.objSeccion.setHoraFin(seccionesData[0].horaFin);
          this.objSeccion.setHoraInicio(seccionesData[0].horaInicio);
          this.objSeccion.setNombreAula(seccionesData[0].nombreAula);
          this.objSeccion.setNombreCurso(seccionesData[0].nombreCurso);
          this.objSeccion.setNombreCompletoDocente(seccionesData[0].nombreCompletoDocente);
          if (seccionesData[0].Habilitada == 0) {
            this.objSeccion.setSuspendida(1);

          } else {
            this.objSeccion.setSuspendida(0);

          }

          this.gestionAcademicaService.actualizarSeccion(JSON.stringify(this.objSeccion)).subscribe(
            (data) => {
             // console.log(data);
              if (data.mensaje == 1) {
                this.dtElements.first.dtInstance.then(
                  (dtInstance: DataTables.Api) => {
                   // console.log(dtInstance);
                    dtInstance.destroy();
                    this.cargarSecciones();
                  });
              } else {
                alert('No se pudo guardar.');
              }
            },
            (err) => console.log(err)
          );

        } else {
          alert('No encontró sección');
        }

      },
      (err) => console.log(err)
    );
  }

  goToFomularioSeccion() {
    this.router.navigate(['/home/gestion/formulario-seccion']);
  }

  getSeccion(codSeccion) {
    this.actualizarSeccion(codSeccion);
  }

  setCheckStatus(valor) {
    if (valor == 1) {
      return "Activo";
    } else {
      return "Innactivo";
    }
  }

  habilitado(valor) {
    if (valor == 1) {
      return true;
    } else {
      return false;
    }
  }

  deshabilitado(valor) {
    if (valor == 0) {
      return true;
    } else {
      return false;
    }
  }


  nuevaSeccion() {
    $('#btn-actualizar').addClass('d-none');
    $('#btn-guardar').removeClass('d-none');
    $('.modal-title').html('Registrar Seccion');
    $("#txt-seccionNombre").val("");
    $("#txt-seccionDocente").val("");
    $("#txt-seccionAula").val("");
    $("#txt-seccionCurso").val("");
  }

  // actualizarSeccion(seccion,docente,aula,curso){
  //   $("#newSeccion").modal("show");
  //   $('.modal-title').html('Actualizar Seccion '+seccion);
  //   $("#txt-seccionNombre").val(seccion);
  //   $("#txt-seccionDocente").val(docente);
  //   $("#txt-seccionAula").val(aula);
  //   $("#txt-seccionCurso").val(curso);
  //   $('#btn-guardar').addClass('d-none');
  //   $('#btn-actualizar').removeClass('d-none');
  // }

}
