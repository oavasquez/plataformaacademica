import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';
import { platformBrowserDynamic } from '@angular/platform-browser-dynamic';
import { BrowserModule } from '@angular/platform-browser';

import { DataTablesModule } from 'angular-datatables';

import { } from '../administracion/'

//componentes
import { AdminDashboardComponent } from "./components/admin-dashboard/admin-dashboard.component";
import { AdminCursosComponent } from "./components/admin-cursos/admin-cursos.component";
import { AdminLectivosComponent } from "./components/admin-lectivos/admin-lectivos.component";
import { AdminUserComponent } from "./components/admin-user/admin-user.component";
import { AdminAjusteComponent } from "./components/admin-ajuste/admin-ajuste.component";

//router modulos
import { AdministracionRoutesModule } from "./administracion.routes.module";

//service
import { AdministrarService } from './administracion.service';

//ui auto-complete
import { NguiAutoCompleteModule } from '@ngui/auto-complete';



@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        HttpModule,
        ReactiveFormsModule,
        AdministracionRoutesModule,
        NguiAutoCompleteModule,
        DataTablesModule
    ],
    declarations: [
        AdminDashboardComponent,
        AdminCursosComponent,
        AdminLectivosComponent,
        AdminUserComponent,
        AdminAjusteComponent
    ],
    providers: [AdministrarService],
    exports: [
        AdminDashboardComponent,
        AdminCursosComponent,
        AdminLectivosComponent,
        AdminUserComponent,
        AdminAjusteComponent
    ],
    bootstrap: []
})

export class AdministracionModule { }