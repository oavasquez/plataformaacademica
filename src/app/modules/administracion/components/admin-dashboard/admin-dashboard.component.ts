import { Component, OnInit } from '@angular/core';
import { AdministrarService } from '../../administracion.service';

@Component({
  selector: 'app-admin-dashboard',
  templateUrl: './admin-dashboard.component.html',
  styleUrls: ['./admin-dashboard.component.css']
})
export class AdminDashboardComponent implements OnInit {
  public codCard:number;
  public cantUsuario:number;
  public cantCursosInhabilitados:number;
  public cantCursosHabilitados:number;
  public nombrePeriodo:string;
  public nombreLectivo:string;
  public cantidadAsignaturaImpartida:number;

  constructor(public administracionService: AdministrarService) { }

  ngOnInit() {

    this.cargarTarjetas();
  }

  cargarTarjetas(){

    //tarjeta de numero de usuarios
    this.administracionService.mostrarDashboard('{"codCard":1}').subscribe(
      (data)=>{this.cantUsuario=data[0].cantidadUsuario;},
      (err)=>{console.log(err)}
    );
    //tarjeta de numero de cursos habilitados e inhabilitadoss
    this.administracionService.mostrarDashboard('{"codCard":2}').subscribe(
      (data)=>{
        this.cantCursosInhabilitados=data[0].cantidadInhabilitadosCurso;
        this.cantCursosHabilitados=data[0].cantidadHabilitadosCurso
      },
      (err)=>{console.log(err)}
    );
    //tarjeta del nombre del periodo
    this.administracionService.mostrarDashboard('{"codCard":3}').subscribe(
      (data)=>{
  
        this.nombrePeriodo=data[0].nombrePeriodo;
      },
      (err)=>{console.log(err)}
    );
    //tarjeta de nombre año lectivo
    this.administracionService.mostrarDashboard('{"codCard":4}').subscribe(
      (data)=>{
      
        this.nombreLectivo=data[0].nombreAnioLectivo;
      },
      (err)=>{console.log(err)}
    );
     //tarjeta de nombre año lectivo
     this.administracionService.mostrarDashboard('{"codCard":4}').subscribe(
      (data)=>{
      
        this.nombreLectivo=data[0].nombreAnioLectivo;
      },
      (err)=>{console.log(err)}
    );
     //cantidad de asignatura
     this.administracionService.mostrarDashboard('{"codCard":5}').subscribe(
      (data)=>{
      
        this.cantidadAsignaturaImpartida=data[0].cantidadAsignaturasImpartidas;
      },
      (err)=>{console.log(err)}
    );

  


  }

}
