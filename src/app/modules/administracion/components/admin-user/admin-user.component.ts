
import { Component, NgZone, OnInit, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
//import service
import { AdministrarService } from '../../administracion.service';

import { Usuario } from "../../../../shared/model/usuario.model";
import { TipoUsuario } from './../../../../shared/model/tipoUsuario.model';
// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;

//import de validaciones
import { Validaciones } from '../../../../shared/model/validaciones_Administracion.model';

@Component({
  selector: 'app-admin-user',
  templateUrl: './admin-user.component.html',
  styleUrls: ['./admin-user.component.css']
})
export class AdminUserComponent implements OnInit {
  @Input() inData: any;
  @Input() inUsuariosJsonArray: object[];
  @Input() inRolesJsonArray: object[];
  @Output() change = new EventEmitter();
  @Output() outRecargarTablaUsuarios = new EventEmitter();
  @Output() outRecargarTablaRoles = new EventEmitter();

  //parametros de datatable
  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;
  dtOptions: DataTables.Settings[] = [];
  dtTrigger: Subject<any>[] = [new Subject(), new Subject()];

  message = 'asd';
  tab1 = false;
  tab2 = true;
  tab3 = true;
  cont = 0;
  tabRoles = false;
  tabNuevoRol = true;
  tabEditRol = true;

  esHabilitado = false;

  public objUsuario = new Usuario();
  public objRolSeleccionado = new TipoUsuario();
  public nuevoTipoUsuario = new TipoUsuario();
  public actualizarTipoUsuario = new TipoUsuario();
  public keyboard: string = "";
  public jsonNombrePersonas: any;
  public jsonNombreUsuarios: any;
  public jsonRoles: object[];
  public nombrePersona: any;
  public nombreUsuario: any;
  public tipoUsuarioSelecionado: any;
  public nRepetirContrasena: String;
  Validacion = new Validaciones();

  public nuevoUsuario = {
    nombreUsuario: "",
    contrasena: "",
    codPersona: Number,
    codTipoUsuario: 0,
    habilitado: 0

  };

  public actualizarUsuario = {
    nombreCompleto: "",
    codigoUsuario: 0,
    nombreUsuario: "",
    contrasena: "",
    codTipoUsuario: 0,
    habilitado: 0
  };

  constructor(private zone: NgZone, private administrarService: AdministrarService,
    private builder: FormBuilder, private _sanitizer: DomSanitizer
  ) { }

  //********************************************seccion de roles********************************************+



  public opcionesRoles = [
    { nombre: 'Modulo de Administración', value: 'ad', checked: false },
    { nombre: 'Modulo de Evaluación y Promoción', value: 'ep', checked: false },
    { nombre: 'Modulo de Matricula y Registro', value: 'mr', checked: false },
    { nombre: 'Modulo de Gestión Académica', value: 'ga', checked: false }
  ]

  //metodo para obtener todos los checkbox seleccionados
  get selectedOptions() {
    return this.opcionesRoles
      .filter(opt => opt.checked)
      .map(opt => opt.value)
  }

  mostrarAccesosRoles() {
    var accesos = this.actualizarTipoUsuario.getTipoAcceso().split(',');
    for (var i = 0; i < accesos.length; i++) {
      if (accesos[i] == "ad") {
        this.opcionesRoles[0].checked = true;
      }
      if (accesos[i] == "ep") {
        this.opcionesRoles[1].checked = true;
      }
      if (accesos[i] == "mr") {
        this.opcionesRoles[2].checked = true;
      }
      if (accesos[i] == "ga") {
        this.opcionesRoles[3].checked = true;
      }
    }
  }

  reiniciarAccesosRoles() {
    this.opcionesRoles = [
      { nombre: 'Modulo de Administración', value: 'ad', checked: false },
      { nombre: 'Modulo de Evaluación y Promoción', value: 'ep', checked: false },
      { nombre: 'Modulo de Matricula y Registro', value: 'mr', checked: false },
      { nombre: 'Modulo de Gestión Académica', value: 'ga', checked: false }
    ]
  }

  //metodo para crear un nuevo rol 
  enviarDatosActualizarRoles() {
    const arregloRolesSeleccionados = this.selectedOptions;
    this.actualizarTipoUsuario.tipoAcceso = arregloRolesSeleccionados.join();
    //antes de mandar a guardar se debe crear la cadena separada por comas con los accesos 
    //reestablecidos, ejemplo: ad,mr,ep 
    if (this.Validacion.ValidarRolAct(this.actualizarTipoUsuario.getTipoUsuario(), this.actualizarTipoUsuario.getTipoAcceso(), this.actualizarTipoUsuario.getHabilitado()) == 0) {
      this.administrarService.actualizarRol(this.actualizarTipoUsuario).subscribe(
        (data) => {
          //console.log(data);
          if (data.mensaje == 1) {
            this.dtElements.last.dtInstance.then(
              (dtInstance: DataTables.Api) => {
               // console.log(dtInstance);
                dtInstance.destroy();
                this.recargarTablaRoles();
                $('#modal-exito_user .modal-body').html('<p>Rol actualizado exitosamente</p>');
                $('#modal-exito_user .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito_user').modal('show');
                this.goToRolTab(1);
              });
          } else {
            $('#modal-exito_user .modal-body').html('<p>No se pudo Actualizar el Rol</p>');
            $('#modal-exito_user .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_user').modal('show');
          }
        },  //error 
        (err) => console.log(err)
      );
    }
  }


  enviarDatosCrearRoles() {
    const arregloRolesSeleccionados = this.selectedOptions;
    this.nuevoTipoUsuario.tipoAcceso = arregloRolesSeleccionados.join();

    if (this.Validacion.ValidarRol(this.nuevoTipoUsuario.getTipoUsuario(), this.nuevoTipoUsuario.getTipoAcceso(), this.nuevoTipoUsuario.getHabilitado()) == 0) {
      this.administrarService.agregarRol(this.nuevoTipoUsuario).subscribe(
        (data) => {
          if (data.mensaje == 0) {
           // console.log('No se pudo actualizar', this.nuevoTipoUsuario);
            $('#modal-exito_user .modal-body').html('<p>No se pudo Guardar el Rol</p>');
            $('#modal-exito_user .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_user').modal('show');
          } else {
            this.dtElements.last.dtInstance.then(
              (dtInstance: DataTables.Api) => {
               // console.log(dtInstance);
                dtInstance.destroy();
                this.recargarTablaRoles();
                $('#modal-exito_user .modal-body').html('<p>Se guardo el rol con exito</p>');
                $('#modal-exito_user .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito_user').modal('show');
                this.goToRolTab(1);
              });
          }
        },  //error 
        (err) => console.log(err)
      );
    }
  }

  LimpiarRoles() {
    this.nuevoTipoUsuario.construir(0, "", "", 0, "", "", "");
    this.Validacion.removerValidacionesRol();
  }

  getRolSeleccionado(info: any): void {
    //buscar como obtener el campo tipoacceso
    this.objRolSeleccionado.construir(info[0], info[3], info[4], info[8], info[5], info[6], info[4]);
    this.actualizarTipoUsuario = this.objRolSeleccionado;
    this.mostrarAccesosRoles();
  }
  iniciarTablaRoles() {
    this.dtOptions[1] = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No se encontraron resultados",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No records available",
        "infoFiltered": "(filtered from _MAX_ total records)"
      },
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.getRolSeleccionado(data);
        });
        return row;
      }
    };
  }
  //***********************************************USUARIOS***********************************************************//

  ngOnInit() {
    this.iniciarTablaUsuarios();
    this.iniciarTablaRoles();
    this.recargarTablaUsuarios();
    this.recargarTablaRoles();
    this.jsonNombrePersonas = this.inData;
    this.obtenerNombreUsuario();
    this.obtnerNombrePersonas();

    // console.log('Arreglo de usuarios de entrada:', this.inUsuariosJsonArray);
    // console.log('inData: ', this.jsonNombrePersonas); 
  }

  //funciones para tabla usuarios y roles, el nombre no importa, estas opciones se usan tamnien para los roles -__-
  iniciarTablaUsuarios() {
    this.dtOptions[0] = {
      "scrollY": "350px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No se encontraron resultados",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No records available",
        "infoFiltered": "(filtered from _MAX_ total records)"
      },
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.getFilaSeleccionada(data);
        });
        return row;
      }
    };
  }
  getFilaSeleccionada(info: any): void {
   // console.log(info);
    this.objUsuario.setCodUsuario(info[0]);
    this.objUsuario.setNombreUsuario(info[4]);
    this.objUsuario.setNombreCompleto(info[2]);
    this.objUsuario.setTipoUsuario(info[5]);
    this.objUsuario.setCodTipoUsuario(info[10]);
    this.objUsuario.setFechaCreacion(info[6]);
    this.objUsuario.setFechaModificacion(info[7]);
    this.objUsuario.setHabilitado(info[9]);
    //console.log('Objeto usuario:', this.objUsuario);
    //console.log('json request:', JSON.stringify(this.objUsuario));
  }

  enviarDatosActualizarUsuario() {
    if (this.Validacion.validar_userAct(this.objUsuario.getNombreUsuario(), this.objUsuario.getTipoUsuario(), this.objUsuario.getContrasena(), this.nRepetirContrasena) == 0) {
      console.log(this.objUsuario)
      this.administrarService.actualizarUsuario(JSON.stringify(this.objUsuario)).subscribe(
        (data) => {
          //console.log(data);
          if (data.mensaje == 0) {
            //console.log('No se pudo actualizar', this.objUsuario);
            $('#modal-exito_user .modal-body').html('<p>No se pudo Actualizar el Usuario</p>');
            $('#modal-exito_user .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_user').modal('show');
          } else {
            this.dtElements.first.dtInstance.then(
              (dtInstance: DataTables.Api) => {
                //console.log(dtInstance);
                dtInstance.destroy();
                this.recargarTablaUsuarios();
                $('#modal-exito_user .modal-body').html('<p>Se Actualizo el usuario con exito</p>');
                $('#modal-exito_user .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito_user').modal('show');
                this.goToTab(1);
              });
          }
        },
        (err) => console.log(err)
      );
    }
  }

  activarRol(codRol) {
    this.administrarService.mostrarRoles({ codTipoUsuario: codRol }).subscribe(
      (rolesData) => {
        this.objRolSeleccionado.setCodTipoUsuario(codRol);
        this.objRolSeleccionado.setTipoUsuario(rolesData[0].rol);
        this.objRolSeleccionado.setFechaCreacion(rolesData[0].fechaCreacion);
        this.objRolSeleccionado.setFechaModificacion(rolesData[0].fechaModificacion);
        this.objRolSeleccionado.setAccesos(rolesData[0].accesos);
        if (rolesData[0].habilitado == 0) {
          this.objRolSeleccionado.setHabilitado(1);
        } else {
          this.objRolSeleccionado.setHabilitado(0);
        }

        //enviar data
        this.administrarService.actualizarRol(this.objRolSeleccionado).subscribe(
          (data) => {
            if (data.mensaje == 1) {
              this.dtElements.last.dtInstance.then(
                (dtInstance: DataTables.Api) => {
                 // console.log(dtInstance);
                  dtInstance.destroy();
                  this.recargarTablaRoles();
                });
            } else {
              alert("No se pudo actualizar");
            }
          },  //error 
          (err) => console.log(err)
        );
      },
      (err) => console.log(err)
    );
  }

  //metodo para crear un registro de usuario apartir de los datos ingresados ----falta validar la entrada de datos
  enviarDatosCrearUsuario() {
    if (this.nombrePersona != undefined) {
      this.nuevoUsuario.codPersona = this.nombrePersona.codigoPersona;
      if (this.Validacion.validar_user(this.nombrePersona.codigoPersona, this.nuevoUsuario.nombreUsuario, this.nuevoUsuario.codTipoUsuario, this.nuevoUsuario.contrasena, this.nRepetirContrasena) == 0) {
        this.administrarService.agregarUsuario(this.nuevoUsuario).subscribe(
          (data) => {
            // console.log("Usuario creado:");
            // console.log(data);
            this.dtElements.first.dtInstance.then(
              (dtInstance: DataTables.Api) => {
                //console.log(dtInstance);
                dtInstance.destroy();
                this.recargarTablaUsuarios();
                $('#modal-exito_user .modal-body').html('<p>Se guardo el ysuario con exito</p>');
                $('#modal-exito_user .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito_user').modal('show');
                this.goToTab(1);
              });
          },  //error 
          (err) => console.log(err)
        );
      }
    } else {
      this.Validacion.validar_user("", this.nuevoUsuario.nombreUsuario, this.nuevoUsuario.codTipoUsuario, this.nuevoUsuario.contrasena, this.nRepetirContrasena)
    }
  }

  LimpiarUser() {
    this.nuevoUsuario = {
      nombreUsuario: "",
      contrasena: "",
      codPersona: Number,
      codTipoUsuario: 0,
      habilitado: 0

    };
    this.Validacion.removerValidacionesUser();
  }

  recargarTablaRoles() {
    this.administrarService.mostrarRoles({ codTipoUsuario: 0 }).subscribe(
      (rolesData) => {
        this.inRolesJsonArray = rolesData;
        //console.log(this.inRolesJsonArray);
        this.dtTrigger[1].next();
      },
      (err) => console.log(err)
    );
  }

  recargarTablaUsuarios() {
    this.administrarService.mostrarUsuarios().subscribe(
      (usuariosData) => {
        this.inUsuariosJsonArray = usuariosData;
        //console.log('inUsuariosJsonArray: ', this.inUsuariosJsonArray);
        this.dtTrigger[0].next();
      },
      (err) => console.log(err)
    );
    // this.outRecargarTablaUsuarios.emit()  
  }


  cargarTabla() {
    this.obtnerNombrePersonas();
  }

  obtnerNombrePersonas() {
    this.administrarService.obtenerNombresPersonas(this.keyboard).subscribe(
      (data) => {
        //console.log("esto es lo que recibo de obtenerNombresPersonas")
        //console.log(data);
        this.jsonNombrePersonas = data;
      },  //error 
      (err) => console.log(err)
    );
  }


  obtenerNombreUsuario() {
    this.administrarService.obtenerNombresUsuarios(this.keyboard).subscribe(
      (data) => {
        //console.log("esto es lo que recibo de obtenerNombresPersonas")
        //console.log(data);
        this.jsonNombreUsuarios = data;
        //console.log(this.jsonNombreUsuarios);

      },  //error 
      (err) => console.log(err)
    );

  }

  activarUsuario(codUsuario) {
    this.administrarService.mostrarUnUsuario(codUsuario).subscribe(
      (usuariosData) => {
        this.objUsuario.setCodPersona(usuariosData[0].codPersona);
        this.objUsuario.setCodUsuario(usuariosData[0].codUsuario);
        this.objUsuario.setCodTipoUsuario(usuariosData[0].codTipoUsuario);
        this.objUsuario.setNombreCompleto(usuariosData[0].nombreCompleto);
        this.objUsuario.setFechaCreacion(usuariosData[0].fechaCreacion);
        this.objUsuario.setAccesos(usuariosData[0].accesos);
        this.objUsuario.setContrasena(usuariosData[0].contrasena);
        if (usuariosData[0].habilitado == 0) {
          this.objUsuario.setHabilitado(1);
        } else {
          this.objUsuario.setHabilitado(0);
        }

        //enviar data
        this.administrarService.actualizarUsuario(JSON.stringify(this.objUsuario)).subscribe(
          (data) => {
            if (data.mensaje != 0) {
              this.dtElements.first.dtInstance.then(
                (dtInstance: DataTables.Api) => {
                  dtInstance.destroy();
                  this.recargarTablaUsuarios();
                });
            }
          },
          (err) => console.log(err)
        );

      },
      (err) => console.log(err)
    );
  }

  goToTab(tabId) {
    if (tabId == 1) {
      this.tab1 = false;
      this.tab2 = true;
      this.tab3 = true;
    }
    if (tabId == 2) {
      this.tab2 = false;
      this.tab1 = true;
      this.tab3 = true;
      this.LimpiarUser();
    }
    if (tabId == 3) {
      this.tab3 = false;
      this.tab1 = true;
      this.tab2 = true;
      this.Validacion.removerValidacionesUserAct();
    }
  }

  setCheckStatus(valor) {
    if (valor == 1) {
      return "Activo";
    } else {
      return "Innactivo";
    }
  }

  goToRolTab(tabId) {
    if (tabId == 1) {
      this.reiniciarAccesosRoles();
      this.tabRoles = false;
      this.tabNuevoRol = true;
      this.tabEditRol = true;
    }
    if (tabId == 2) {
      this.tabNuevoRol = false;
      this.tabRoles = true;
      this.tabEditRol = true;
      this.LimpiarRoles();
    }
    if (tabId == 3) {
      this.tabEditRol = false;
      this.tabRoles = true;
      this.tabNuevoRol = true;
      this.Validacion.removerValidacionesRolAct();
    }
  }

  cambiarEstado(estado) {
    var id = '#' + estado
    if ($(id).val() == "Habilitar") {
      $(id).removeClass('btn-success');
      $(id).addClass('btn-danger');
      $(id).val("Desabilitar");
      $(id).html("Desabilitar");
    } else {
      $(id).removeClass('btn-danger');
      $(id).addClass('btn-success');
      $(id).val("Habilitar");
      $(id).html("Habilitar");
    }
  }

  habilitado(valor) {
    if (valor == 1) {
      return true;
    } else {
      return false;
    }
  }

  deshabilitado(valor) {
    if (valor == 0) {
      return true;
    } else {
      return false;
    }
  }


}



