import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminLectivosComponent } from './admin-lectivos.component';

describe('AdminLectivosComponent', () => {
  let component: AdminLectivosComponent;
  let fixture: ComponentFixture<AdminLectivosComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminLectivosComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminLectivosComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
