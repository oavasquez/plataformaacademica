import { Component, NgZone, OnInit, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { DomSanitizer, SafeHtml } from "@angular/platform-browser";
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';

//import service
import { AdministrarService } from '../../administracion.service';

import { Periodo } from "../../../../shared/model/periodo.model";
import { Modalidad } from './../../../../shared/model/modalidad.model';
import { Jornada } from "../../../../shared/model/jornada.model";
import { AnoLectivo } from './../../../../shared/model/anoLectivo.model';
import { Validaciones } from '../../../../shared/model/validaciones_Administracion.model';

import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';

// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-admin-lectivos',
  templateUrl: './admin-lectivos.component.html',
  styleUrls: ['./admin-lectivos.component.css']
})
export class AdminLectivosComponent implements OnInit {

  //Variables de entrada y salida
  @Input() inModalidadesJsonArray: object[];
  @Input() inAnoLectivoJsonArray: object[];
  @Output() outRecargarTablaModalidades = new EventEmitter();
  @Output() outRecargarTablaAnoLectivo = new EventEmitter();
  @Output() outRecargarTablaPeriodo = new EventEmitter();
  @Output() outRecargarTablaJornada = new EventEmitter();

  //parametros de datatable
  @ViewChildren(DataTableDirective)
  // private dtElements: DataTableDirective;
  // dtTrigger: Subject<any> = new Subject();
  // dtOptionsLectivo: DataTables.Settings = {};
  // dtOptionsPeriodo: DataTables.Settings = {};
  // dtOptionsJornada: DataTables.Settings = {};
  // dtOptionsModalidad: DataTables.Settings = {};

  dtElements: QueryList<DataTableDirective>;
  dtOptions: DataTables.Settings[] = [];
  dtTrigger: Subject<any>[] = [new Subject(), new Subject(), new Subject(), new Subject()];

  contProgreso :number = 0;

  tabAnoLectivo = false;
  tabNewAnoLectivo = true;
  tabUpdateAnoLectivo = true;

  tabPeriodos = false;
  tabNewPeriodo = true;
  tabUpdatePeriodo = true;

  tabJornadas = false;
  tabNewJornadas = true;
  tabUpdateJornadas = true;

  tabModalidades = false;
  tabNewModalidades = true;
  tabUpdateModalidades = true;

  Modalidades: Modalidad[];
  Lectivos: AnoLectivo[];
  Periodos: Periodo[];
  Jornadas: Jornada[];
  Validacion = new Validaciones();

  inLectivosJsonArray: AnoLectivo[];

  constructor(
    //private zone: NgZone, 
    private administrarService: AdministrarService,
    //private builder: FormBuilder, private _sanitizer: DomSanitizer
  ) { }

  ngOnInit() {
    this.iniciarTablas();
    this.ObtenerLectivos(25);
    this.ObtenerPeriodos(25);
    this.ObtenerJornadas(25);
    this.MostrarModalidades(25);
    //console.log(this.Modalidades);
  }


  //********************************************inicio seccion del ano lectivo********************************************+
  Lectivo = new AnoLectivo();
  LectivoAct = new AnoLectivo();

  goToTab(tabId) {
    if (tabId == 1) {
      this.tabAnoLectivo = false;
      this.tabNewAnoLectivo = true;
      this.tabUpdateAnoLectivo = true;
    }
    if (tabId == 2) {
      this.tabNewAnoLectivo = false;
      this.tabAnoLectivo = true;
      this.tabUpdateAnoLectivo = true;
      this.LimpiarLectivo();
    }
    if (tabId == 3) {
      this.tabUpdateAnoLectivo = false;
      this.tabAnoLectivo = true;
      this.tabNewAnoLectivo = true;
      this.Validacion.remover_validacionesLectivoAct();
    }
  }

  LimpiarLectivo() {
    this.Lectivo.Construir(0, "", "", "");
    this.Validacion.remover_validacionesLectivo();
  }

  guardar_lectivo() {
    if (this.Validacion.validar_lectivo(this.Lectivo.getNombre(), this.Lectivo.getFechaInicio(), this.Lectivo.getFechaFinal()) == 0) {
      this.administrarService.agregarAnioLectivo((JSON.stringify(this.Lectivo))).subscribe(
        (data) => {
          //console.log(data);
          if (data.mensaje != 0) {
            this.dtElements.first.dtInstance.then(
              (dtInstance: DataTables.Api) => {
                console.log(dtInstance);
                dtInstance.destroy();
                this.ObtenerLectivos(100);
                $('#modal-exito_Lectivo .modal-body').html('<p>Se Guardo el Año Lectivo con exito</p>');
                $('#modal-exito_Lectivo .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito_Lectivo').modal('show');
                this.goToTab(1);
              });  
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  actualizar_lectivo() {
    if (this.Validacion.validar_lectivoAct(this.LectivoAct.getNombre(), this.LectivoAct.getFechaInicio(), this.LectivoAct.getFechaFinal()) == 0) {
      this.administrarService.actualizarAnioLectivo((JSON.stringify(this.LectivoAct))).subscribe(
        (data) => {
          //console.log(data);
          if (data.mensaje != 0) {
            this.dtElements.first.dtInstance.then(
              (dtInstance: DataTables.Api) => {
                console.log(dtInstance);
                dtInstance.destroy(); 
                this.ObtenerLectivos(100); 
                $('#modal-exito_Lectivo .modal-body').html('<p>Se Actualizo el Año Lectivo con exito</p>');
                $('#modal-exito_Lectivo .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito_Lectivo').modal('show'); 
                this.goToTab(1);
              }); 
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  ObtenerLectivos(valor) {
    this.administrarService.mostrarAnioLectivo({ codLectivo: 0 }).subscribe(
      (data) => {
        //console.log('Recibiendo todos los lectivos');
        //console.log(data);
        this.Lectivos = data;
        this.setProgressBar(valor);
        this.dtTrigger[0].next();

      },
      (error) => {
        console.log(error)
      }
    );
  }



  actualizarL(lectivo) {
    this.LectivoAct.Construir(lectivo.codLectivo, lectivo.nombre, lectivo.fechaInicio, lectivo.fechaFinal);
    this.goToTab(3);
  }




  //********************************************fin de seccion ano lectivo********************************************+


  //********************************************inicio seccion de periodos********************************************+
  Periodo = new Periodo();
  PeriodoAct = new Periodo();

  goToPerTab(tabId) {
    if (tabId == 1) {
      this.tabPeriodos = false;
      this.tabNewPeriodo = true;
      this.tabUpdatePeriodo = true;
    }
    if (tabId == 2) {
      this.tabNewPeriodo = false;
      this.tabPeriodos = true;
      this.tabUpdatePeriodo = true;
      this.LimpiarPeriodo();
    }
    if (tabId == 3) {
      this.tabUpdatePeriodo = false;
      this.tabPeriodos = true;
      this.tabNewPeriodo = true;
      this.Validacion.remover_validacionesPeriodoAct();
    }
  }

  LimpiarPeriodo() {
    this.Periodo.Construir(0, "", "", "", "");
    this.Validacion.remover_validacionesPeriodo();
  }

  agregar_periodo() {
    //console.log(this.Periodo);
    if (this.Validacion.validar_Periodo(this.Periodo.getNombrePeriodo(), this.Periodo.getFechaInicio(), this.Periodo.getFechaFinal(), this.Periodo.getAnio()) == 0) {
      this.administrarService.agregarPeriodo(JSON.stringify(this.Periodo)).subscribe(
        (data) => { 
          if (data.mensaje != 0) {
            this.dtElements.first.dtInstance.then(
              (dtInstance: DataTables.Api) => {
                console.log(dtInstance);
                dtInstance.destroy();
                this.ObtenerPeriodos(100);
                this.contProgreso =0;
                $('#modal-exito_Lectivo .modal-body').html('<p>Se Agrego el Periodo con exito</p>');
                $('#modal-exito_Lectivo .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito_Lectivo').modal('show');
                this.goToPerTab(1);
              });  
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }


  actualizar_periodo() {
    if (this.Validacion.validar_PeriodoAct(this.PeriodoAct.getNombrePeriodo(), this.PeriodoAct.getFechaInicio(), this.PeriodoAct.getFechaFinal(), this.PeriodoAct.getAnio()) == 0) {
      //console.log(this.PeriodoAct);
      this.administrarService.actualizarPeriodo(JSON.stringify(this.PeriodoAct)).subscribe(
        (data) => {
          console.log(data);
          if (data.mensaje != 0) { 
            this.dtElements.last.dtInstance.then(
              (dtInstance: DataTables.Api) => {
                console.log(dtInstance);
                dtInstance.destroy();
                this.ObtenerPeriodos(100); this.contProgreso = 0;
                $('#modal-exito_Lectivo .modal-body').html('<p>Se Actualizo el  Periodo con exito</p>');
                $('#modal-exito_Lectivo .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito_Lectivo').modal('show');
                this.goToPerTab(1);
              });  
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  ObtenerPeriodos(valor) {
    this.administrarService.mostrarPeriodo({ codPeriodo: 0 }).subscribe(
      (data) => {
        this.Periodos = data;
        //console.log(this.Periodos);
        this.setProgressBar(valor);
        this.dtTrigger[1].next();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  actualizarP(periodo) {
    this.PeriodoAct.Construir(periodo.codPeriodo, periodo.nombrePeriodo, periodo.fechaInicio, periodo.fechaFinal, periodo.anio);
    this.goToPerTab(3);
  }
  //********************************************fin de seccion de periodos********************************************+


  //********************************************inicio seccion de jornadas********************************************+
  Jornada = new Jornada();
  JornadaAct = new Jornada();

  goToJorTab(tabId) {
    if (tabId == 1) {
      this.tabJornadas = false;
      this.tabNewJornadas = true;
      this.tabUpdateJornadas = true;
    }
    if (tabId == 2) {
      this.tabNewJornadas = false;
      this.tabJornadas = true;
      this.tabUpdateJornadas = true;
    }
    if (tabId == 3) {
      this.tabUpdateJornadas = false;
      this.tabJornadas = true;
      this.tabNewJornadas = true;
    }
  }
 

  agregar_jornada() {
    if (this.Validacion.validar_Jornada(this.Jornada.getJornada()) == 0) {
      this.administrarService.agregarJornada(JSON.stringify(this.Jornada)).subscribe(
        (data) => {
          //console.log(data);
          if (data.mensaje != 0) { 
            this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
              dtElement.dtInstance.then((dtInstance: any) => {
                if (index == 2) {
                  dtInstance.destroy();
                }
              });
            });
            this.ObtenerJornadas(100); 
            $('#modal-exito_Lectivo .modal-body').html('<p>Se Guardo la Jornada con exito</p>');
            $('#modal-exito_Lectivo .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_Lectivo').modal('show');
            this.goToJorTab(1);
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  actualizar_jornada() {
    if (this.Validacion.validar_JornadaAct(this.JornadaAct.getJornada()) == 0) {
      //console.log(this.JornadaAct);
      this.administrarService.actualizarJornada(JSON.stringify(this.JornadaAct)).subscribe(
        (data) => {
          //console.log(data); 
          if (data.mensaje != 0) {
            this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
              dtElement.dtInstance.then((dtInstance: any) => { 
                if (index == 2) {
                  dtInstance.destroy(); 
                }
              });
            });
            this.ObtenerJornadas(100); 
            $('#modal-exito_Lectivo .modal-body').html('<p>Se Guardo la Jornada con exito</p>');
            $('#modal-exito_Lectivo .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
            $('#modal-exito_Lectivo').modal('show'); 
            this.goToJorTab(1);
          }
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  ObtenerJornadas(valor) {
    this.administrarService.mostrarJornada({ codJornada: 0 }).subscribe(
      (data) => {
        this.Jornadas = data; 
        this.setProgressBar(valor);
        this.dtTrigger[2].next();
        //console.log(this.Jornadas);

      },
      (error) => {
        //console.log(error);
      }
    );
  }

  actualizarJornada(jornada) {
    this.JornadaAct.Construir(jornada.codJornada, jornada.jornada);
    this.goToJorTab(3);
  }

  //********************************************fin de seccion de jornadas********************************************+


  //********************************************inicio seccion de modalidades********************************************+

  Modalidad = new Modalidad();
  ModalidadAct = new Modalidad();
  goToModTab(tabId) {
    if (tabId == 1) {
      this.tabModalidades = false;
      this.tabNewModalidades = true;
      this.tabUpdateModalidades = true;
    }
    if (tabId == 2) {
      this.tabNewModalidades = false;
      this.tabModalidades = true;
      this.tabUpdateModalidades = true;
    }
    if (tabId == 3) {
      this.tabUpdateModalidades = false;
      this.tabModalidades = true;
      this.tabNewModalidades = true;
    }

  }

  agregar_modalidad() {

    //if (this.Validacion.validar_modalidad(this.Jornada.getJornada()) == 0) {
    this.administrarService.agregarModalidad(JSON.stringify(this.Modalidad)).subscribe(
      (data) => {
       // console.log(data);
        if (data.mensaje != 0) {
          this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (index == 3) {
                dtInstance.destroy();
              }
            });
          });
          this.MostrarModalidades(100); 
          this.goToModTab(1);
          $('#modal-exito_Lectivo .modal-body').html('<p>Se Guardo la Modalidad con exito</p>');
          $('#modal-exito_Lectivo .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
          $('#modal-exito_Lectivo').modal('show'); 
        }
      },
      (error) => {
        console.log(error);
      }
    );

  }



  /*agregarModalidad(){
    this.administrarService.agregarModalidad(this.Modalidad).subscribe(
      (data)=>{
        console.log(data);
        this.dtElements.last.dtInstance.then(
          (dtInstance: DataTables.Api) => {
            console.log(dtInstance);
            dtInstance.destroy();
            this.getTodosModalidades();
            alert('Modalidad Agregada Exitosamente');
          }); 
      },
      (error)=>{
        console.log(error);
      })
    );            
  }
  */


  MostrarModalidades(valor) {
    this.administrarService.mostrarModalidad().subscribe(
      (data) => {
        this.Modalidades = data;
        this.setProgressBar(valor);
        this.dtTrigger[3].next();
      }
    );
  }

  actualizarM(modalidad) {

    this.ModalidadAct.Construir(modalidad.codModalidad, modalidad.modalidad);
    this.goToModTab(3);

  }

  actualizar_modalidad() {

    //console.log(this.ModalidadAct);
    this.administrarService.actualizarModalidad(JSON.stringify(this.ModalidadAct)).subscribe(
      (data) => {
        //console.log(data);

        if (data.mensaje != 0) {
          this.dtElements.forEach((dtElement: DataTableDirective, index: number) => {
            dtElement.dtInstance.then((dtInstance: any) => {
              if (index == 3) {
                dtInstance.destroy();
              }
            });
          });
          this.MostrarModalidades(100); 
          this.goToModTab(1);
          $('#modal-exito_Lectivo .modal-body').html('<p>Se Actualizo la modalidad con exito</p>');
          $('#modal-exito_Lectivo .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
          $('#modal-exito_Lectivo').modal('show');
        }
      },
      (error) => {
        console.log(error);
      }
    );

  }


  //******************************************** fin de seccion de modalidades********************************************+




  setCheckStatus(valor) {
    if (valor == 1) {
      return "Deshabilitar";
    } else {
      return "Habilitar";
    }
  }

  habilitado(valor) {
    if (valor == 1) {
      return true;
    } else {
      return false;
    }
  }

  deshabilitado(valor) {
    if (valor == 0) {
      return true;
    } else {
      return false;
    }
  }

  getTodosModalidades() {
    //llamar al servicio que cargue todas las asignaturas
    this.administrarService.mostrarModalidad().subscribe(
      (modalidadesData) => {
        //console.log('Recibiendo todas las modalidades');
        // console.log(asignaturasData);
        this.inModalidadesJsonArray = modalidadesData;
        this.dtTrigger[1].next();
        // console.log('asignaturasJsonArray: ', this.asignaturasJsonArray);
      },
      (err) => console.log(err)
    );
  }

  cambiarEstado(estado) {
    var id = '#' + estado
    if ($(id).val() == "Habilitar") {
      $(id).removeClass('btn-success');
      $(id).addClass('btn-danger');
      $(id).val("Desabilitar");
      $(id).html("Desabilitar");
    } else {
      $(id).removeClass('btn-danger');
      $(id).addClass('btn-success');
      $(id).val("Habilitar");
      $(id).html("Habilitar");
    }
  }

  getCursoSelected(info: any): void {
    //console.log(info);

  }


  //*****************************************Datatables*************************************************


  iniciarTablas() {
    //tabla lectivos
    this.dtOptions[0] = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      }
    };


    this.dtOptions[1] = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      }
    };

    this.dtOptions[2] = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      }
    };

    this.dtOptions[3] = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      }
    };
  }

  recargarTablaLectivos() {
    this.outRecargarTablaAnoLectivo.emit();
  }
  recargarTablaPeriodo() {
    this.outRecargarTablaPeriodo.emit();
  }

  recargarTablaJornada() {
    this.outRecargarTablaJornada.emit();
  }
  recargarTablaModalidad() {
    this.outRecargarTablaModalidades.emit();
  }


  setProgressBar(valor){
    this.contProgreso = this.contProgreso + valor;
    if(this.contProgreso >= 100){
      this.contProgreso =0;
    }
  }


}