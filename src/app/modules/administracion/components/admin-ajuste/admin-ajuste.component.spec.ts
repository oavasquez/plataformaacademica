import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdminAjusteComponent } from './admin-ajuste.component';

describe('AdminAjusteComponent', () => {
  let component: AdminAjusteComponent;
  let fixture: ComponentFixture<AdminAjusteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdminAjusteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdminAjusteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
