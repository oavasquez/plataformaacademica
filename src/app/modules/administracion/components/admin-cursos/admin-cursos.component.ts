import { Component, NgZone, OnInit, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { FormGroup, FormControl, FormBuilder, FormArray, Validators } from '@angular/forms';

import { Subject } from 'rxjs/Subject';
//import service
import { AdministrarService } from '../../administracion.service';
import { DataTableDirective } from 'angular-datatables';

// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;

//Import del modelo Curso
import { Curso } from '../../../../shared/model/curso.model';
import { Modalidad } from '../../../../shared/model/modalidad.model';

//Import del modelo Asignatura
import { Asignatura } from '../../../../shared/model/asignatura.model';

import { Validaciones } from '../../../../shared/model/validaciones_Administracion.model';
import { THROW_IF_NOT_FOUND } from '@angular/core/src/di/injector';

@Component({
  selector: 'app-admin-cursos',
  templateUrl: './admin-cursos.component.html',
  styleUrls: ['./admin-cursos.component.css']
})
export class AdminCursosComponent implements OnInit {
  //variables de entrada y de salida
  @Input() inCursosJsonArray: object[];
  @Input() inAsignaturasJsonArray: object[];
  @Output() outRecargarTablaCursos = new EventEmitter();
  @Output() outRecargarTablaAsignaturas = new EventEmitter();

  //parametros de datatable
  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;
  dtOptions: DataTables.Settings[] = [];
  dtTrigger: Subject<any>[] = [new Subject(), new Subject()];

  tabCursos = false;
  tabNuevoCurso = true;
  tabUpdateCurso = true;
  tabAsignaturas = false;
  tabNewAsignatura = true;
  tabUpdateAsignatura = true;
  Modalidades: Modalidad[];
  Cursos: Curso[];


  //Objeto Curso para Nuevo y Actualizar
  Curso = new Curso();
  Act_Curso = new Curso();

  //Objeto Asignaturas para Nueva y Actualizar
  Asignatura = new Asignatura();
  Act_Asignatura = new Asignatura();
  asignaturasSelecionadasXCurso: Asignatura[] = [];
  asignarAsignatura = new Asignatura();
  AsignaturaAgregadasCurso: Asignatura[] = [];
  AsignaturaListJsonArray: any;


  Validacion = new Validaciones();

  constructor(private zone: NgZone, private administrarService: AdministrarService) { }

  ngOnInit() {
    this.iniciarTablas();
    this.MostrarModalidades();
    this.MostrarCursos();
    this.getTodosAsignaturas();
    //this.getListaAsignaturas();
    this.mostrarAsignaturaLista();

  }

  MostrarModalidades() {

    this.administrarService.mostrarModalidad().subscribe(
      (data) => {
        this.Modalidades = data;
        //console.log(this.Modalidades);
      },
      (error) => {
        console.log(error);
      }
    );
  }

  MostrarCursos() {
    this.administrarService.mostrarCursos({ codCurso: 0 }).subscribe(
      (data) => {
        this.inCursosJsonArray = data;
        //console.log(this.Cursos);
        this.dtTrigger[0].next();
      },
      (error) => {
        console.log(error);
      }
    );
  }

  recargarTablaCursos() {
    this.outRecargarTablaCursos.emit();
  }
  recargarTablaAsignaturas() {
    this.outRecargarTablaAsignaturas.emit();
  }

  getCursoSelected(info: any): void {
    this.AgregarAsignaturasActualizarCurso(info)
    this.Act_Curso.ConstruirCurso(info[8], info[1], info[5], info[7], info[0]);
  }
  getAsignaturaSelected(info: any): void {
    // set asignatura seleccionada
    this.Act_Asignatura.Construir(info[1], info[3], info[7], info[5], info[8]);
    this.MostrarDias();
  }
  getFilaSeleccionada(info: any): void {
  }

  iniciarTablas() {
    //tabla cursos
    this.dtOptions[0] = {
      "scrollY": "350px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No se encontraron resultados",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No records available",
        "infoFiltered": "(filtered from _MAX_ total records)"
      },
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.getCursoSelected(data);
        });
        return row;
      }
    };

    //tabla asignatura
    this.dtOptions[1] = {
      "scrollY": "350px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No se encontraron resultados",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No records available",
        "infoFiltered": "(filtered from _MAX_ total records)"
      },
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.getAsignaturaSelected(data);
        });
        return row;
      }
    };
  }
  goToAsigTab(tabId) {
    if (tabId == 1) {
      this.reiniciarDias();
      this.tabAsignaturas = false;
      this.tabNewAsignatura = true;
      this.tabUpdateAsignatura = true;
    }
    if (tabId == 2) {
      this.tabNewAsignatura = false;
      this.tabAsignaturas = true;
      this.tabUpdateAsignatura = true;
      this.LimpiarCamposAsignatura();
    }
    if (tabId == 3) {
      this.tabUpdateAsignatura = false;
      this.tabAsignaturas = true;
      this.tabNewAsignatura = true;
      this.Validacion.removerValidacionesAsignaturaAct();
    }
  }
  goToTab(tabId) {
    if (tabId == 1) {
      this.tabCursos = false;
      this.tabNuevoCurso = true;
      this.tabUpdateCurso = true;
    }
    if (tabId == 2) {
      this.tabNuevoCurso = false;
      this.tabCursos = true;
      this.tabUpdateCurso = true;
      this.LimpiarCamposCurso();
    }
    if (tabId == 3) {
      this.tabUpdateCurso = false;
      this.tabCursos = true;
      this.tabNuevoCurso = true;
      this.Validacion.removerValidacionesActCurso()
    }
  }

  setCheckStatus(valor) {
    if (valor == 1) {
      return "Activo";
    } else {
      return "Innactivo";
    }
  }

  habilitado(valor) {
    if (valor == 1) {
      return true;
    } else {
      return false;
    }
  }

  deshabilitado(valor) {
    if (valor == 0) {
      return true;
    } else {
      return false;
    }
  }

  cambiarEstado(estado) {
    var id = '#' + estado
    if ($(id).val() == "Habilitar") {
      $(id).removeClass('btn-success');
      $(id).addClass('btn-danger');
      $(id).val("Desabilitar");
      $(id).html("Desabilitar");
    } else {
      $(id).removeClass('btn-danger');
      $(id).addClass('btn-success');
      $(id).val("Habilitar");
      $(id).html("Habilitar");
    }
  }



  //-----------------------------------------Funciones de Cursos---------------------------------------------//
  cursovalido = 0;
  agregarNuevoCurso() {
    //asinando un arreglo de asignaturas al objeto Curso
    this.Curso.setArrayAsignatura(this.AsignaturaAgregadasCurso);
    this.cursovalido = 0;
    this.cursovalido = this.Validacion.ValidarCurso(this.Curso.getCodModalidad(), this.Curso.getNombreCurso(), this.Curso.getNumeroAsignaturas(), this.Curso.getEstadoCurso());
    if (this.cursovalido == 0) {


      this.administrarService.agregarCurso(this.Curso).subscribe(
        (data) => {
          //codigo para refrescar dtCurso
          this.dtElements.first.dtInstance.then(
            (dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.MostrarCursos();
              $('#modal-exito .modal-body').html('<p>Se guardo con exito</p>');
              $('#modal-exito .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
              $('#modal-exito').modal('show');
              this.goToTab(1);
            });
        },
        (err) => console.log(err)
      );
    }
  }

  actualizarCurso() {
    //console.log("Modalidad: " + this.Act_Curso.getCodModalidad() + "Nombre Curso: " + this.Act_Curso.getNombreCurso() + "Asignaturas: " + this.Act_Curso.getNumeroAsignaturas() + "Estado: " + this.Act_Curso.getEstadoCurso());
    this.Act_Curso.setArrayAsignatura(this.AsignaturaAgregadasCurso);
    this.cursovalido = 0;

    this.cursovalido = this.Validacion.ValidarCursoAct(this.Act_Curso.getCodModalidad(), this.Act_Curso.getNombreCurso(), this.Act_Curso.getNumeroAsignaturas(), this.Act_Curso.getEstadoCurso());

    if (this.cursovalido == 0) {
      this.administrarService.actualizarCurso(JSON.stringify(this.Act_Curso)).subscribe(
        (data) => {
          if (data.mensaje == 0) {
            alert('No se pudo actualizar :(');
          } else {
            //codigo para refrescar dtCurso
            this.dtElements.first.dtInstance.then(
              (dtInstance: DataTables.Api) => {
                //console.log(`The DataTable instance ID is: ${dtInstance.table().node().id}`);
                dtInstance.destroy();
                this.MostrarCursos();
                $('#modal-exito .modal-body').html('<p>Se Actualizo con exito</p>');
                $('#modal-exito .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
                $('#modal-exito').modal('show');
                this.goToTab(1);
              });
          }

        },
        (err) => console.log(err)
      );
    }
  }

  LimpiarCamposCurso() {
    this.Curso.ConstruirCurso(0, "", "", "", 0);
    this.Validacion.removerValidacionesCurso();
    this.AsignaturaAgregadasCurso = [];
  }

  activarCurso(codigo) {
    this.administrarService.mostrarCursos({ codCurso: codigo }).subscribe(
      (data) => {
        this.Curso.setCodigoCurso(data[0].codCurso);
        this.Curso.setNombreCurso(data[0].nombreCurso);
        this.Curso.setNumeroAsignaturas(data[0].numeroAsignaturas);
        this.Curso.setCodModalidad(data[0].codModalidad);
        if (data[0].habilitado == 0) {
          this.Curso.setEstadoCurso(1);
        } else {
          this.Curso.setEstadoCurso(0);
        }

        //enviar data
        this.administrarService.actualizarCurso(JSON.stringify(this.Curso)).subscribe(
          (data) => {
            if (data.mensaje == 0) {
              alert('No se pudo actualizar :(');
            } else {
              //codigo para refrescar dtCurso
              this.dtElements.first.dtInstance.then(
                (dtInstance: DataTables.Api) => {
                  dtInstance.destroy();
                  this.MostrarCursos();
                });
            }

          },
          (err) => console.log(err)
        );
      },
      (error) => {
        console.log(error);
      }
    );
  }

  AgregarAsignaturasActualizarCurso(info: any) {
    this.administrarService.mostrarAsignatura('{"codCurso":' + info[0] + ',"habilitado":' + info[7] + '}').subscribe(
      (data) => {
        this.AsignaturaAgregadasCurso = data;
      },
      (err) => console.log(err)
    )

  }

  //-----------------------------------------Funciones de Asignaturas---------------------------------------------//
  //variable para validacion
  asignatura_valida = 0;
  //Arreglo para selecionar los dias para la asignatura 
  Dias = [
    { name: 'Lunes', value: 'Lu', checked: false },
    { name: 'Martes', value: 'Ma', checked: false },
    { name: 'Miercoles', value: 'Mi', checked: false },
    { name: 'Jueves', value: 'Ju', checked: false },
    { name: 'Viernes', value: 'Vi', checked: false },
    { name: 'Sabado', value: 'Sa', checked: false },
    { name: 'Domingo', value: 'Do', checked: false }
  ]

  agregarAsignatura() {
    this.guardardias(this.Asignatura);
    this.asignatura_valida = 0;
    this.asignatura_valida = this.Validacion.validar_Asignatura(this.Asignatura.getNombreAsignatura(), this.Asignatura.getCodCurso(), this.Asignatura.getDiasAsignatura(), this.Asignatura.getEstadoAsignatura());
    if (this.asignatura_valida == 0) {
      //console.log(JSON.stringify(this.Asignatura)+" "+this.asignatura_valida);

      

      this.administrarService.agregarAsignatura(JSON.stringify(this.Asignatura)).subscribe(
        (data) => {
          this.dtElements.last.dtInstance.then(
            (dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.getTodosAsignaturas();
              $('#modal-exito .modal-body').html('<p>Se Guardo la Asignatura con exito</p>');
              $('#modal-exito .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
              $('#modal-exito').modal('show');
              this.goToAsigTab(1);
            });
          this.mostrarAsignaturaLista();
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }

  mostrarAsignaturaLista() {
    this.Asignatura.setCodAsignatura("0");
    this.administrarService.mostrarAsignaturaLista(JSON.stringify(this.Asignatura)).subscribe(
      (data) => {
        this.AsignaturaListJsonArray = data;

      },
      (error) => {
        console.log(error);
      }

    )

  }





  actualizarAsignatura() {
    this.guardardias(this.Act_Asignatura);
    this.asignatura_valida = 0;
    this.asignatura_valida = this.Validacion.validar_AsignaturaAct(this.Act_Asignatura.getCodAsignatura(), this.Act_Asignatura.getNombreAsignatura(), this.Act_Asignatura.getCodCurso(), this.Act_Asignatura.getDiasAsignatura(), this.Act_Asignatura.getEstadoAsignatura());
    if (this.asignatura_valida == 0) {
      this.administrarService.actualizarAsignaturaXCurso(JSON.stringify(this.Act_Asignatura), ).subscribe(
        (data) => {
          this.dtElements.last.dtInstance.then(
            (dtInstance: DataTables.Api) => {
              dtInstance.destroy();
              this.getTodosAsignaturas();
              $('#modal-exito .modal-body').html('<p>Se Actualizo la Asignatura con exito</p>');
              $('#modal-exito .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
              $('#modal-exito').modal('show');
              this.goToAsigTab(1);
            });
        },
        (error) => {
          console.log(error);
        }
      );
    }
  }


  activarAsignatura(asignatura, curso) {
    this.administrarService.mostrarAsignatura({ codCurso: 0, habilitado: 0 }).subscribe(
      (asignaturasData) => {

      },
      (err) => console.log(err)
    );
  }

  LimpiarCamposAsignatura() {
    this.Asignatura.Construir("", "", 0, "", 0);;
    this.Validacion.removerValidacionesAsignatura();
    this.reiniciarDias();
  }

  MostrarDias() {
    var dias = this.Act_Asignatura.getDiasAsignatura().split(',');
    for (var i = 0; i < dias.length; i++) {
      if (dias[i] == "Lu") {
        this.Dias[0].checked = true;
      }
      if (dias[i] == "Ma") {
        this.Dias[1].checked = true;
      }
      if (dias[i] == "Mi") {
        this.Dias[2].checked = true;
      }
      if (dias[i] == "Ju") {
        this.Dias[3].checked = true;
      }
      if (dias[i] == "Vi") {
        this.Dias[4].checked = true;
      }
      if (dias[i] == "Sa") {
        this.Dias[5].checked = true;
      }
      if (dias[i] == "Do") {
        this.Dias[6].checked = true;
      }
    }
  }

  reiniciarDias() {
    this.Dias = [
      { name: 'Lunes', value: 'Lu', checked: false },
      { name: 'Martes', value: 'Ma', checked: false },
      { name: 'Miercoles', value: 'Mi', checked: false },
      { name: 'Jueves', value: 'Ju', checked: false },
      { name: 'Viernes', value: 'Vi', checked: false },
      { name: 'Sabado', value: 'Sa', checked: false },
      { name: 'Domingo', value: 'Do', checked: false }
    ]
  }

  //Guarda los dias de la asignatura a la clase asignatura

  guardardias(objeto) {
    var selected = "";
    for (var i = 0; i < this.Dias.length; i++) {
      if (this.Dias[i].checked == true) {
        if (selected == "") {
          selected = selected + this.Dias[i].value;
        } else {
          selected = selected + "," + this.Dias[i].value;
        }
      }
    }
    objeto.setDiasAsignatura(selected);
  }
  
  getListaAsignaturas(){
    this.administrarService.mostrarAsignaturaLista({ codAsignatura: 0}).subscribe(
      (asignaturasData)=>{
        this.inAsignaturasJsonArray = asignaturasData;
        this.dtTrigger[1].next();
      }
    );
  }

  getTodosAsignaturas() {
    //llamar al servicio que cargue todas las asignatura
    this.administrarService.mostrarAsignatura({ codCurso: 0, habilitado: 0 }).subscribe(
      (asignaturasData) => {
        // console.log(asignaturasData);
        this.inAsignaturasJsonArray = asignaturasData;
        this.dtTrigger[1].next();
        // console.log('asignaturasJsonArray: ', this.asignaturasJsonArray);
      },
      (err) => console.log(err)
    );
  }




  //funciones para el modal y la tabla de cursos al agregar asignaturas
  AgregarAsignaturaACurso() {
    this.guardardias(this.asignarAsignatura);

    this.AsignaturaAgregadasCurso.push(new Asignatura(this.asignarAsignatura.getCodAsignatura(), this.asignarAsignatura.getNombreAsignatura(), this.asignarAsignatura.getDiasAsignatura()));
    this.limpiarDatosAsignaturaCurso();
    this.reiniciarDias();


  }
  //esta funcion es solo para limpiar el arreglo que se muestra al asignar 
  //una asignatura en el formularionuevo curso 

  eliminarAsignaturaCurso(key: any) {
    this.AsignaturaAgregadasCurso.splice(key, 1);

  }

  //limpia el arreglo de asignaturas del fomulario nueva seccion 

  limpiarDatosAsignaturaCurso() {
    this.asignarAsignatura.Construir(0, "", 0, "", 0)

  }



}