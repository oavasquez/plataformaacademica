import { TipoUsuario } from './../../shared/model/tipoUsuario.model';
import { Modalidad } from './../../shared/model/modalidad.model';
import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';


//interface
import { Mensaje } from '../../shared/model/mensaje.model';
import { Curso } from '../../shared/model/curso.model';
import { Asignatura } from '../../shared/model/asignatura.model';


@Injectable()
export class AdministrarService {
    headers = new HttpHeaders()
        .set('Content-Type', 'application/json');

    constructor(private http: HttpClient) { }

    //"""""""""""""""""""""""""""""""""""""""Usuarios"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarUsuario(usuario: any): Observable<any> {
        //https://appacademica.herokuapp.com/
        //http://127.0.0.1:5000
       // console.log("enviando datos desde agregar usuario");
        const parametros = {}
        //console.log(usuario);
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/crearUsuario',
            usuario,
            { headers: this.headers });
    }

    actualizarUsuario(actualizarUsuario): Observable<any> {
        //console.log(actualizarUsuario);

        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/actualizarUsuario',
            actualizarUsuario,
            { headers: this.headers });
    }


    //mostrar todos los usuarios existentes
    mostrarUsuarios(): Observable<any> {
        //console.log("Obteniedo todos los usuarios");
        const parametros = { codUsuario: 0 }
        //return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarUsuario',
            parametros,
            { headers: this.headers });
    }

    mostrarUnUsuario(codigo): Observable<any> {
        //console.log("Obteniedo todos los usuarios");
        const parametros = { codUsuario: codigo }
        //return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarUsuario',
            parametros,
            { headers: this.headers });
    }

    //"""""""""""""""""""""""""""""""""""""""cursos"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    agregarCurso(curso: Curso): Observable<Curso> {
        //console.log("consiguiendodatos para servicio curso");

        //https://appacademica.herokuapp.com/
        return this.http.post<Curso>('https://appacademica.herokuapp.com/administracion/crearCurso',
            JSON.stringify(curso),
            { headers: this.headers });
    }

    actualizarCurso(curso): Observable<any> {
        //console.log("consiguiendodatos para servicio curso");
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/actualizarCurso',
            curso,
            { headers: this.headers });
    }

    //mostrar todos los cursos existentes
    mostrarCursos(curso): Observable<any> {
        //console.log("consiguiendodatos para servicio curso");

        //return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarCursos',
            curso,
            { headers: this.headers });
    }

    //"""""""""""""""""""""""""""""""""""""""Tipo usuario"""""""""""""""""""""""""""""""""""""""""""""""""""""""""
    agregarRol(rol: TipoUsuario): Observable<any> {
        //console.log("consiguiendodatos");
        //console.log(JSON.stringify(rol));

        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/crearRol',
            JSON.stringify(rol),
            { headers: this.headers });
    }

    actualizarRol(rol): Observable<any> {
        //console.log("consiguiendodatos");

        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/actualizarRol',
            rol,
            { headers: this.headers });
    }

    //mostrar todos los cursos existentes
    mostrarRoles(rol): Observable<any> {
        //console.log("consiguiendodatos");
        //return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarRoles',
            rol,
            { headers: this.headers });
    }
    //"""""""""""""""""""""""""""""""""""""""Asignatura"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarAsignatura(asignatura): Observable<Asignatura> {
        //console.log("consiguiendodatos");

        //https://appacademica.herokuapp.com/
        return this.http.post<Asignatura>('https://appacademica.herokuapp.com/administracion/crearAsignatura',
            asignatura,
            { headers: this.headers });
    }

    actualizarAsignatura(asignatura): Observable<any> {
        //console.log("consiguiendodatos");

        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/actualizarAsignatura',
            asignatura,
            { headers: this.headers });
    }

    //mostrar todos los cursos existentes
    mostrarAsignatura(asignatura): Observable<any> {
        //console.log("consiguiendodatos");
        //return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarAsignatura',
            asignatura,
            { headers: this.headers });
    }

    //mostrar todos los cursos existentes
    mostrarAsignaturaLista(asignatura): Observable<any> {
        //console.log("consiguiendodatos");
        //return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarAsignaturaLista',
            asignatura,
            { headers: this.headers });
    }

    //mostrar todos los cursos existentes
    insertarAsignaturaXCurso(asignatura): Observable<any> {
        //console.log("consiguiendodatos");
        //return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/asignaturaCurso',
            asignatura,
            { headers: this.headers });
    }

    //mostrar todos los cursos existentes
    actualizarAsignaturaXCurso(asignatura): Observable<any> {
        //console.log("consiguiendodatos");
        //return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/actualizarAsignaturaCurso',
            asignatura,
            { headers: this.headers });
    }

    //"""""""""""""""""""""""""""""""""""""""Modalidad"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarModalidad(modalidad): Observable<any> {
        //console.log("consiguiendodatos");

        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/crearModalidad',
            modalidad,
            { headers: this.headers });
    }

    actualizarModalidad(modalidad): Observable<any> {
        //console.log("consiguiendodatos");

        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/actualizarModalidad',
            modalidad,
            { headers: this.headers });
    }

    //mostrar todas las modalidades existentes
    mostrarModalidad(): Observable<Modalidad[]> {
        //console.log("consiguiendodatos");
        //return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<Modalidad[]>('https://appacademica.herokuapp.com/administracion/mostrarModalidad',
            { headers: this.headers });
    }


    //**************************************************anio lectivo ********************************************************/

    agregarAnioLectivo(anioLectivo): Observable<any> {
        //console.log("consiguiendodatos");
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/crearLectivo',
            anioLectivo,
            { headers: this.headers });
    }

    actualizarAnioLectivo(anioLectivo): Observable<any> {
        //console.log("consiguiendodatos");
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/actualizarLectivo',
            anioLectivo,
            { headers: this.headers });
    }

    //mostrar todas las modalidades existentes
    mostrarAnioLectivo(anioLectivo): Observable<any> {
        //console.log("consiguiendodatos");
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarLectivo',
            anioLectivo, { headers: this.headers });
    }


    //************************************************Periodo*************************************************************/
    agregarPeriodo(periodo): Observable<any> {
        //console.log("consiguiendodatos");
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/crearPeriodo',
            periodo,
            { headers: this.headers });
    }

    actualizarPeriodo(periodo): Observable<any> {
        //console.log("consiguiendodatos");
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/actualizarPeriodo',
            periodo,
            { headers: this.headers });
    }

    //mostrar todas las modalidades existentes
    mostrarPeriodo(periodo): Observable<any> {
        //console.log("consiguiendodatos");
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarPeriodo',
            periodo, { headers: this.headers });
    }

    //************************************************Jornada*************************************************************/
    agregarJornada(jornada): Observable<any> {
        //console.log("consiguiendodatos");
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/crearJornada',
            jornada,
            { headers: this.headers });
    }

    actualizarJornada(jornada): Observable<any> {
        //console.log("consiguiendodatos");
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/actualizarJornada',
            jornada,
            { headers: this.headers });
    }

    //mostrar todas las modalidades existentes
    mostrarJornada(jornada): Observable<any> {
        //console.log("consiguiendodatos");
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarJornada',
            jornada, { headers: this.headers });
    }

    //********************************************************************************************************************/

    //servicios para obtener solo los nombre de las personas o usuarios con su id respectivo

    obtenerNombresPersonas(keyword: string): Observable<any> {
        //console.log("consiguiendodatos");
        const parametros = {}
        //https://appacademica.herokuapp.com/
        //http://127.0.0.1:5000
        //return this.http.get<any>('https://appacademica.herokuapp.com/administracion/mostrarNombresPersonas', parametros)
        //return this.http.get<any>('http://127.0.0.1:5000/administracion/mostrarNombresPersonas')
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarNombresPersonas',
            parametros,
            { headers: this.headers });

    }
    obtenerNombresUsuarios(keyword: string): Observable<any> {
        //console.log("consiguiendodatos");
        const parametros = {}
        //https://appacademica.herokuapp.com/
        //http://127.0.0.1:5000

        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarNombreUsuarios',
            parametros,
            { headers: this.headers });

    }


    //********************************************************************************************************************/

    //servicios para obtener datos para mostrara en las tajetas de la seccion de dashboard
    mostrarDashboard(dashboard): Observable<any> {
        //console.log("consiguiendodatos");
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/administracion/mostrarDashboardAdministracion',
            dashboard,
            { headers: this.headers });

    }



}

