import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../../auth/auth-guard.service';
import { AdminDashboardComponent } from "./components/admin-dashboard/admin-dashboard.component";
import { AdminCursosComponent } from "./components/admin-cursos/admin-cursos.component";
import { AdminLectivosComponent } from "./components/admin-lectivos/admin-lectivos.component";
import { AdminUserComponent } from "./components/admin-user/admin-user.component";
import { AdminAjusteComponent } from "./components/admin-ajuste/admin-ajuste.component";



const routes: Routes = [
    { path: 'admiministrar', redirectTo: 'admiministrar/admin-dashboard', pathMatch: 'full' },
    { path: 'admin-dashboard', component: AdminDashboardComponent },
    { path: 'admin-cursos', component: AdminCursosComponent },
    { path: 'admin-usuarios', component: AdminUserComponent },
    { path: 'admin-lectivo', component: AdminLectivosComponent },
    { path: 'admin-ajuste', component: AdminAjusteComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class AdministracionRoutesModule { }