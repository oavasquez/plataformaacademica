import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';


//interface
import { Mensaje } from '../../shared/model/mensaje.model';


@Injectable()
export class MainService {
    headers = new HttpHeaders()
        .set('Content-Type', 'application/json');

    constructor(private http: HttpClient) { }

    getUserInfo(): Observable<any> {
        //console.log("consiguiendodatos");
        const parametros = {
            codigoPersona: localStorage.getItem("codigoPersona")

        }

        // return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/main/obtenerDatosPersona',
            parametros,
            { headers: this.headers });
    }

}
