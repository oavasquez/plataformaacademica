import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { MainContentComponent } from './components/main-content/main-content.component'
import { MainSiteContainerComponent } from './components/main-site-container/main-site-container.component'
import { MySideNavComponent } from './components/my-side-nav/my-side-nav.component'
import { TopNavBarComponent } from './components/top-nav-bar/top-nav-bar.component'

//my-side-nav components
import { AdSideMenuComponent } from './components/my-side-nav/ad-side-menu/ad-side-menu.component';
import { EpSideMenuComponent } from './components/my-side-nav/ep-side-menu/ep-side-menu.component';
import { GaSideMenuComponent } from './components/my-side-nav/ga-side-menu/ga-side-menu.component';
import { MrSideMenuComponent } from './components/my-side-nav/mr-side-menu/mr-side-menu.component';

import { AdminCursosComponent } from "../administracion/components/admin-cursos/admin-cursos.component";

import { AuthGuardService as AuthGuard } from '../../auth/auth-guard.service';

const routes: Routes = [

    {
        path: '', canActivate: [AuthGuard], component: MainSiteContainerComponent,
        children: [
            {
                path: 'admiministrar', component: AdSideMenuComponent
                , children: [
                    { path: '', loadChildren: 'app/modules/administracion/administracion.module#AdministracionModule' }
                ]
            },
            {
                path: 'matriculaRegistro', component: MrSideMenuComponent
                , children: [
                    { path: '', loadChildren: 'app/modules/matriculaRegistro/matriculaRegistro.module#MatriculaRegistroModule' }
                ]
            },
            {
                path: 'evaluacionPromocion', component: EpSideMenuComponent
                , children: [
                    { path: '', loadChildren: 'app/modules/evaluacionPromocion/evaluacionPromocion.module#EvaluacionPromocionModule' }
                ]
            },
            {
                path: 'gestion', component: GaSideMenuComponent
                , children: [
                    { path: '', loadChildren: 'app/modules/gestion-academica/gestion-academica.module#GestionAcademicaModule' }
                ]
            },
        ]
    }

];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class MainRoutesModule { }