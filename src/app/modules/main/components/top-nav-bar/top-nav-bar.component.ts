import { Component, OnInit, Input } from '@angular/core';
import { MainService } from '../../main.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-top-nav-bar',
  templateUrl: './top-nav-bar.component.html',
  styleUrls: ['./top-nav-bar.component.css']
})
export class TopNavBarComponent implements OnInit {
  @Input() loggedUserName: string;
  @Input() strAccesos: string;
  lista_accesos: string[];
  modulosAcessos: {
    nombre: string,
    path: string
  }[] = [];

  public MostrarModuloActual: string;

  constructor(private mainService: MainService, private router: Router) { }

  ngOnInit() {

    this.lista_accesos = this.strAccesos.split(",", 4);
    for (let i in this.lista_accesos) {
      this.lista_accesos[i] = this.matchModuleUser(this.lista_accesos[i]);
    }
    // this.lista_accesos[0] = this.matchModuleUser(this.lista_accesos[0]);
    // this.lista_accesos[1] = this.matchModuleUser(this.lista_accesos[1]);
    // this.lista_accesos[2] = this.matchModuleUser(this.lista_accesos[2]);
    // this.lista_accesos[3] = this.matchModuleUser(this.lista_accesos[3]);
    //console.log(this.strAccesos);
    //console.log(this.lista_accesos[0]);
    //console.log(this.lista_accesos[3]);

    //console.log(this.modulosAcessos);

    this.MostrarModuloActual = localStorage.getItem('moduloActual') != null ? localStorage.getItem('moduloActual') : 'Home';
    this.mostrarModuloDefault();

  }

  matchModuleUser(abrr) {
    let retorno = "";
    switch (abrr) {
      case 'ad': {
        retorno = "Modulo administración";
        this.modulosAcessos.push({ nombre: "Modulo Administración", path: "admiministrar" });
        break;
      }
      case 'mr': {
        retorno = "Modulo Matricula y registro";
        this.modulosAcessos.push({ nombre: "Modulo Matricula y registro", path: "matriculaRegistro" });
        break;
      }
      case 'ga': {
        retorno = "Modulo Gestión académica";
        this.modulosAcessos.push({ nombre: "Modulo Gestión académica", path: "gestion" });
        break;
      }
      case 'ep': {
        retorno = "Modulo Evaluación y promoción";
        this.modulosAcessos.push({ nombre: "Modulo Evaluación y promoción", path: "evaluacionPromocion" });
        break;
      }
      default:
        break;
    }
    return retorno;
  }

  actualizarTituloModulo(modulo: string) {
    this.MostrarModuloActual = modulo;
    localStorage.setItem('moduloActual', modulo)
    //console.log(localStorage.getItem("moduloActual"));

  }

  mostrarModuloDefault() {
    localStorage.setItem('moduloDefault', this.modulosAcessos[0].path)
    localStorage.setItem('moduloActual', this.modulosAcessos[0].nombre)
    this.router.navigate(['/home/' + this.modulosAcessos[0].path]);
    this.MostrarModuloActual = this.modulosAcessos[0].nombre;


  }

  salir(): void {
    localStorage.removeItem("token");
    localStorage.removeItem("moduloActual");
    if (localStorage.removeItem("token") == null) {
      this.router.navigate(['/login']);
    }
  }
}
