import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-mr-side-menu',
  templateUrl: './mr-side-menu.component.html',
  styleUrls: ['./mr-side-menu.component.css']
})
export class MrSideMenuComponent implements OnInit {

  public expedienteIsActive: boolean = false;
  public matriculaIsActive: boolean = false;
  public fichaIsActive: boolean = false;
  public dashboardIsActive: boolean = false;
  public historialIsActive: boolean = false;
  public incidenciasIsActive: boolean=false;

  constructor() { }

  ngOnInit() {
  }

  isActive(index: number) {
    this.expedienteIsActive = false;
    this.matriculaIsActive = false;
    this.fichaIsActive = false;
    this.dashboardIsActive = false;
    this.historialIsActive = false;
    this.incidenciasIsActive = false;
    

    if (index == 0) {
      this.expedienteIsActive = !this.expedienteIsActive;
    }
    if (index == 1) {
      this.fichaIsActive = !this.fichaIsActive;
    }
    if (index == 2) {
      this.matriculaIsActive = !this.matriculaIsActive;
    }
    if (index == 3) {
      this.dashboardIsActive = !this.dashboardIsActive;
    }
    if (index == 4){
      this.historialIsActive = !this.historialIsActive;
    }
    if(index==5){
      this.incidenciasIsActive = !this.incidenciasIsActive;
    }

  }

}
