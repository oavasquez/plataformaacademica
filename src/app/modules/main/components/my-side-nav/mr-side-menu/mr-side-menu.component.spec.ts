import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MrSideMenuComponent } from './mr-side-menu.component';

describe('MrSideMenuComponent', () => {
  let component: MrSideMenuComponent;
  let fixture: ComponentFixture<MrSideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MrSideMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MrSideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
