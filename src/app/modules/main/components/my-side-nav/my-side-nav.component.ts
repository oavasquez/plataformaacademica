import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';

// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-my-side-nav',
  templateUrl: './my-side-nav.component.html',
  styleUrls: ['./my-side-nav.component.css']
})
export class MySideNavComponent implements OnInit {
  @Input() accesos: string;

  constructor(public router: Router) { }

  ngOnInit() {
  }

 
  ocultar() {
     $('#close-side').hide();
     $('#show-side').show();
  }

  mostrar() {
    $('#show-side').hide();
    $('#close-side').show();
 }

}
