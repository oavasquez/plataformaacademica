import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EpSideMenuComponent } from './ep-side-menu.component';

describe('EpSideMenuComponent', () => {
  let component: EpSideMenuComponent;
  let fixture: ComponentFixture<EpSideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EpSideMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EpSideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
