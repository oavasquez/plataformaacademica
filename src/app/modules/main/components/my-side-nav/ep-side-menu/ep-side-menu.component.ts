import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ep-side-menu',
  templateUrl: './ep-side-menu.component.html',
  styleUrls: ['./ep-side-menu.component.css']
})
export class EpSideMenuComponent implements OnInit {


  public asistenciaIsActive: boolean = false;
  public calificacionesIsActive: boolean = false;
  public cursosIsActive: boolean = false;
  public imprimirIsActive: boolean = false;
  public dashboardIsActive: boolean = false;

  constructor() { }

  ngOnInit() {
  }

  isActive(index: number) {


    this.asistenciaIsActive = false;
    this.calificacionesIsActive = false;
    this.cursosIsActive = false;
    this.imprimirIsActive = false;
    this.dashboardIsActive=false;


    if (index == 0) {
      this.asistenciaIsActive = !this.asistenciaIsActive;
    } else if (index == 1) {
      this.calificacionesIsActive = !this.calificacionesIsActive;
    }
    else if (index == 2) {
      this.cursosIsActive = !this.cursosIsActive;
    }
    else if (index == 3) {
      this.imprimirIsActive = !this.imprimirIsActive;
    }
    else if (index == 4) {
      this.dashboardIsActive = !this.dashboardIsActive;
    }


  }

}
