import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { GaSideMenuComponent } from './ga-side-menu.component';

describe('GaSideMenuComponent', () => {
  let component: GaSideMenuComponent;
  let fixture: ComponentFixture<GaSideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ GaSideMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(GaSideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
