import { Component, OnInit } from '@angular/core';

@Component({
  selector: 'app-ga-side-menu',
  templateUrl: './ga-side-menu.component.html',
  styleUrls: ['./ga-side-menu.component.css']
})
export class GaSideMenuComponent implements OnInit {

  public aulasIsActive: boolean = false;
  public docentesIsActive: boolean = false;
  public seccionesIsActive: boolean = false;
  public dashboardIsActive: boolean = false;
  


  constructor() { }

  ngOnInit() {
  }

  isActive(index: number) {


    this.aulasIsActive = false;
    this.docentesIsActive = false;
    this.seccionesIsActive = false;
    this.dashboardIsActive = false;


    if (index == 0) {
      this.aulasIsActive = !this.aulasIsActive;
    } else if (index == 1) {
      this.docentesIsActive = !this.docentesIsActive;
    }
    else if (index == 2) {
      this.seccionesIsActive = !this.seccionesIsActive;
    }
    else if (index == 3) {
      this.dashboardIsActive = !this.dashboardIsActive;
    }


  }

}
