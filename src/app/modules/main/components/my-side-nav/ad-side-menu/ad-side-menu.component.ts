import { Component, OnInit, Renderer } from '@angular/core';

@Component({
  selector: 'app-ad-side-menu',
  templateUrl: './ad-side-menu.component.html',
  styleUrls: ['./ad-side-menu.component.css']
})
export class AdSideMenuComponent implements OnInit {

  public dashboardIsActive: boolean = false;
  public usuarioIsActive: boolean = false;
  public cursosdIsActive: boolean = false;
  public lectivosIsActive: boolean = false;
  public ajusteIsActive: boolean = false;



  constructor(private renderer: Renderer) { }

  ngOnInit() {
  }

  isActive(index: number) {
    this.dashboardIsActive = false;
    this.usuarioIsActive = false;
    this.cursosdIsActive = false;
    this.lectivosIsActive = false;
    this.ajusteIsActive = false;

    if (index == 0) {
      this.dashboardIsActive = !this.dashboardIsActive;
    } else if (index == 1) {
      this.usuarioIsActive = !this.usuarioIsActive;
    }
    else if (index == 2) {
      this.cursosdIsActive = !this.cursosdIsActive;
    }
    else if (index == 3) {
      this.lectivosIsActive = !this.lectivosIsActive;
    }
    else if (index == 4) {
      this.ajusteIsActive = !this.ajusteIsActive;
    }
  }



}
