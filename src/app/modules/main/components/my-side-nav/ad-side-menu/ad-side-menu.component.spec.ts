import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { AdSideMenuComponent } from './ad-side-menu.component';

describe('AdSideMenuComponent', () => {
  let component: AdSideMenuComponent;
  let fixture: ComponentFixture<AdSideMenuComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ AdSideMenuComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(AdSideMenuComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
