import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
//Importamos el servicio de administracion para descargar datos
import { AdministrarService } from '../../../administracion/administracion.service';
import { GestionAcademicaService } from "../../../gestion-academica/gestion-academica.service";
import { Persona } from '../../../../shared/model/persona.model';

@Component({
  selector: 'app-main-content',
  templateUrl: './main-content.component.html',
  styleUrls: ['./main-content.component.css']
})
export class MainContentComponent implements OnInit {

  @Input() personaLogeada:Persona;

  public jsonNombrePersonas: any;
  public usuariosJsonArray: object[];
  public rolesJsonArray: object[];
  public cursosJsonArray: object[];
  public aulasJsonArray: object[];
  public docentesJsonArray: object[];
  public asignaturasJsonArray: object[];
  public cursosLoaded = 0;
  public keyboard: string = "";

  

  private top:number;
  public flagActionForm: number;
  mensajeToForm = "Este es un mensaje";

  public parametroDocente:object;
  public parametroSeccion:object;
  public objParametro:object;

  constructor(public router: Router, private administrarService: AdministrarService, private GAService: GestionAcademicaService) { }

  ngOnInit() {
    //console.log('Persona logueada:',this.personaLogeada);
    this.obtnerNombrePersonas();
    this.getTodosUsuarios();
    this.getTodosRoles();
    this.getTodosCursos();
    //this.getTodosAsignaturas();
    this.getTodosAulas();
    this.getTodosDocentes();
  }

  hasLoaded(flag) {
    this.obtnerNombrePersonas();
  }
 
  setParametroAlumno(obj:object){
    this.objParametro = obj; 
  }

  setParamentroSeccion(objSeccion:object){
    this.parametroSeccion = objSeccion;
  }

  setSeccionActionForm(flag: number, obj: object){
    this.setActionFlag(flag);
    this.setParamentroSeccion(obj);
  }

  setParametroDocente(objDocente:object){
    this.parametroDocente= objDocente; 
  }

  setActionFlag(flag){
    this.flagActionForm=flag;
  }

  setActionForm(flag:number, obj:object){
    this.setActionFlag(flag);
    this.setParametroDocente(obj);
  }

  setMensajeToForm() {
    //console.log("se cambia el mensje");
    this.mensajeToForm = "mensaje cambiado";
  }

  getTodosDocentes() {
    this.GAService.mostrarDocentes({ codDocente: 0 }).subscribe(
      (docentesData) => {
        //console.log('Recibiendo todos los docentes');
        this.docentesJsonArray = docentesData;
      },
      (err) => console.log(err)
    );
  }

  getTodosAulas() {
    this.GAService.mostrarAulas({ codAula: 0 }).subscribe(
      (aulasData) => {
        //console.log('Recibiendo todas las aulas');
        this.aulasJsonArray = aulasData;
      },
      (err) => console.log(err)
    );
  }

  getTodosCursos() {
    //llamar al servicio que cargue todos los usuarios
    this.administrarService.mostrarCursos({ codCurso: 0 }).subscribe(
      (cursosData) => {
        //console.log('Recibiendo todos los cursos');
        // console.log(cursosData);
        this.cursosJsonArray = cursosData;
        // console.log('cursosJsonArray: ', this.cursosJsonArray);
      },
      (err) => console.log(err)
    );
  }

  getTodosAsignaturas() {
    //llamar al servicio que cargue todas las asignaturas
    this.administrarService.mostrarAsignatura({ codCurso: 0, habilitado: 0 }).subscribe(
      (asignaturasData) => {
       // console.log('Recibiendo todas las asignaturas');
        // console.log(asignaturasData);
        this.asignaturasJsonArray = asignaturasData;
        // console.log('asignaturasJsonArray: ', this.asignaturasJsonArray);
      },
      (err) => console.log(err)
    );
  }

  getTodosUsuarios() {
    if (this.usuariosJsonArray != undefined) {
      this.top = this.usuariosJsonArray.length;
      this.usuariosJsonArray.splice(0, this.top);
    }
    //llamar al servicio que cargue todos los usuarios
    this.administrarService.mostrarUsuarios().subscribe(
      (usuariosData) => {
        this.usuariosJsonArray = usuariosData;
        // console.log('usuariosJsonArray: ', this.usuariosJsonArray);
      },
      (err) => console.log(err)
    );
  }

  getTodosRoles() {
    //llamar al servicio para que cargue todos los roles de usuarios
    this.administrarService.mostrarRoles({ codTipoUsuario: 0 }).subscribe(
      (rolesData) => {
        //console.log('Recibiendo los roles');
        // console.log(rolesData);
        this.rolesJsonArray = rolesData;
      },
      (err) => console.log(err)
    );
  }

  obtnerNombrePersonas() {
    this.administrarService.obtenerNombresPersonas(this.keyboard).subscribe(
      (data) => {
        //console.log("Recibiendo nombre de personas")
        //console.log(data);
        this.jsonNombrePersonas = data;
        //console.log(this.jsonNombrePersonas); 
      },  //error 
      (err) => console.log(err)
    );
  }


}
