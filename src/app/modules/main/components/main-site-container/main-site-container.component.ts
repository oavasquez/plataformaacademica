import { Component, OnInit } from '@angular/core';

import { MainService } from '../../../main/main.service';
import { Persona } from '../../../../shared/model/persona.model';
import { Usuario } from '../../../../shared/model/usuario.model';

import { ModuleService } from "../../../modules.service";


@Component({
  selector: 'app-main-site-container',
  templateUrl: './main-site-container.component.html',
  styleUrls: ['./main-site-container.component.css']
})
export class MainSiteContainerComponent implements OnInit {

  public persona: Persona;
  private usuario: Usuario;
  private lista_accesos: string[];
  postUsuario = {
    nombre: "", accesos: "", default: ""
  }

  constructor(private modulerService: ModuleService, private mainService: MainService) { }

  ngOnInit() {
    //this.modulerService.currentMessage.subscribe(usuario => this.usuario = usuario); 
    //this.postUsuario.accesos = this.usuario.accesos.toString();
    this.postUsuario.accesos = localStorage.getItem('accesos');
    this.obtenerInformacion();
    //aqui te estoy pasando los datos del usuario del componenete login a main-site-containter
    //mediante una suscripcion de del moduler.service que esta en la raiz de la carpeta modules

  }

  obtenerInformacion() {
    //console.log("me voy a buscar datos")
    this.mainService.getUserInfo().subscribe((data) => {
      // console.log(data);
      this.persona = data;
      //console.log('mostrando objeto persona:',this.persona);
      this.postUsuario.nombre = this.persona.nombre.toString();
      //this.lista_accesos = this.usuario.accesos.split(',', 4);
      this.lista_accesos = localStorage.getItem('accesos').toString().split(',', 4);
      this.postUsuario.default = this.lista_accesos[0];
    },  //changed
      (err) => console.log(err)

    );

  }

}
