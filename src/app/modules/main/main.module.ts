import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { MainService } from './main.service';
import { MainRoutesModule } from './main.routes.module';
import { ModuleService } from '../modules.service';

import { AdministracionModule } from '../administracion/administracion.module';
import { EvaluacionPromocionModule } from '../evaluacionPromocion/evaluacionPromocion.module';
import { GestionAcademicaModule } from '../gestion-academica/gestion-academica.module'
import { MatriculaRegistroModule } from '../matriculaRegistro/matriculaRegistro.module'

import { MainContentComponent } from './components/main-content/main-content.component'
import { MainSiteContainerComponent } from './components/main-site-container/main-site-container.component'
import { MySideNavComponent } from './components/my-side-nav/my-side-nav.component'
import { TopNavBarComponent } from './components/top-nav-bar/top-nav-bar.component'
import { AdSideMenuComponent } from './components/my-side-nav/ad-side-menu/ad-side-menu.component';
import { MrSideMenuComponent } from './components/my-side-nav/mr-side-menu/mr-side-menu.component';
import { GaSideMenuComponent } from './components/my-side-nav/ga-side-menu/ga-side-menu.component';
import { EpSideMenuComponent } from './components/my-side-nav/ep-side-menu/ep-side-menu.component';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';





@NgModule({
    imports: [CommonModule,
        HttpClientModule,
        FormsModule,
        HttpModule,
        AdministracionModule,
        EvaluacionPromocionModule,
        GestionAcademicaModule,
        MatriculaRegistroModule,
        MainRoutesModule
    ],
    declarations: [
        MainContentComponent,
        MainSiteContainerComponent,
        MySideNavComponent,
        TopNavBarComponent,
        AdSideMenuComponent,
        MrSideMenuComponent,
        GaSideMenuComponent,
        EpSideMenuComponent,

    ],
    providers: [MainService, ModuleService],
    exports: [
        MainContentComponent,
        MainSiteContainerComponent,
        MySideNavComponent,
        TopNavBarComponent,
        AdSideMenuComponent,
        MrSideMenuComponent,
        GaSideMenuComponent,
        EpSideMenuComponent,

    ],
    bootstrap: []
})
export class MainModule { }
