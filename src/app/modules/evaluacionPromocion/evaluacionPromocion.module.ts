import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';

//import { AdminDashboardComponent } from "./components/admin-dashboard/admin-dashboard.component";
//import { AdminCursosComponent } from "./components/admin-cursos/admin-cursos.component";
//import { AdminLectivosComponent } from "./components/admin-lectivos/admin-lectivos.component";
//import { AdminUserComponent } from "./components/admin-user/admin-user.component";

import { EvaluacionPromocionRoutesModule } from "./evaluacionPromocion.routes.module";
import { EvaluacionPromocionService } from "./evaluacionPromocion.service";

//components
import { AsistenciaComponent } from "./components/asistencia/asistencia.component";
import { CalificacionesComponent } from "./components/calificaciones/calificaciones.component";
import { CursosComponent } from "./components/cursos/cursos.component";
import { ImprimirComponent } from "./components/imprimir/imprimir.component";
import { EvaPromDashboardComponent } from "./components/eva-prom-dashboard/eva-prom-dashboard.component";
import { DataTablesModule } from 'angular-datatables';
//gestionAcademica

//ui auto-complete
import { NguiAutoCompleteModule } from '@ngui/auto-complete';
@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        HttpModule,
        DataTablesModule,
        EvaluacionPromocionRoutesModule,
        NguiAutoCompleteModule
    ],
    declarations: [
        AsistenciaComponent,
        CalificacionesComponent,
        CursosComponent,
        ImprimirComponent,
        EvaPromDashboardComponent,

    ],
    providers: [EvaluacionPromocionService],
    exports: [
        AsistenciaComponent,
        CalificacionesComponent,
        CursosComponent,
        ImprimirComponent,
        EvaPromDashboardComponent,
    ],
    bootstrap: []
})

export class EvaluacionPromocionModule { }