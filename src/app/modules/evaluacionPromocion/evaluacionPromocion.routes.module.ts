import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../../auth/auth-guard.service';
//import { AdminDashboardComponent } from "./components/admin-dashboard/admin-dashboard.component";
import { AsistenciaComponent } from './components/asistencia/asistencia.component';
import { CalificacionesComponent } from './components/calificaciones/calificaciones.component';
import { CursosComponent } from './components/cursos/cursos.component';
import { ImprimirComponent } from './components/imprimir/imprimir.component';
import { EvaPromDashboardComponent } from "./components/eva-prom-dashboard/eva-prom-dashboard.component";

import { DatePipe } from '@angular/common';


const routes: Routes = [
    { path: 'evaluacionPromocion', redirectTo: 'evaluacionPromocion/evaProm-dashboard', pathMatch: 'full' },
    { path: 'evaProm-dashboard', component: EvaPromDashboardComponent },
    { path: 'evaProm-asistencia', component: AsistenciaComponent },
    { path: 'evaProm-calificaciones', component: CalificacionesComponent },
    { path: 'evaProm-cursos', component: CursosComponent },
    { path: 'evaProm-imprimir', component: ImprimirComponent }
    //{ path: 'admin-lectivo', component: AdminLectivosComponent }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule],
    providers: [DatePipe]
})

export class EvaluacionPromocionRoutesModule { }