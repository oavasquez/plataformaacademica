import { Component, OnInit } from '@angular/core';
import { EvaluacionPromocionService } from '../../evaluacionPromocion.service';
import { Router } from '@angular/router';

@Component({
  selector: 'app-eva-prom-dashboard',
  templateUrl: './eva-prom-dashboard.component.html',
  styleUrls: ['./eva-prom-dashboard.component.css']
})
export class EvaPromDashboardComponent implements OnInit {
  public codDocente = localStorage.getItem('codigoPersona');
  public showSecciones: boolean = false;
  public showAsistencia: boolean = false
  public seccionesDocente = {};
  public asistenciaDocente: {};

  constructor(public router: Router, public evaluacionPromocionService: EvaluacionPromocionService) { }

  ngOnInit() {

    this.cargarTarjetas();

  }



  cargarTarjetas() {
    this.evaluacionPromocionService.mostrarDashboard('{"codCard":1,"codDocente":' + this.codDocente + '}').subscribe(
      (data) => {
        if (data.mensaje != 0) {
          this.showSecciones = true;
          this.seccionesDocente = data;
        };
      },
      (err) => { console.log(err) }
    );

    this.evaluacionPromocionService.mostrarDashboard('{"codCard":2,"codDocente":' + this.codDocente + '}').subscribe(
      (data) => {
        if (data.mensaje != 0) {
          this.showAsistencia = true;
          this.asistenciaDocente = data[0];
        };
      },
      (err) => { console.log(err) }
    );
  }

  goToAsistencia() {

    this.router.navigate(['/home/evaluacionPromocion/evaProm-asistencia']);

  }

  goToNotas() {

    this.router.navigate(['/home/evaluacionPromocion/evaProm-calificaciones']);

  }

  goToImprimir() {
    this.router.navigate(['/home/evaluacionPromocion/evaProm-imprimir']);

  }

}
