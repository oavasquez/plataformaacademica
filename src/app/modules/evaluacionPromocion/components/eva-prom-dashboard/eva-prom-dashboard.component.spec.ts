import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { EvaPromDashboardComponent } from './eva-prom-dashboard.component';

describe('EvaPromDashboardComponent', () => {
  let component: EvaPromDashboardComponent;
  let fixture: ComponentFixture<EvaPromDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ EvaPromDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(EvaPromDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
