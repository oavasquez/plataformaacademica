import { Component, OnInit, NgZone, Input, Output, EventEmitter, ViewChildren, QueryList} from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { DataTableDirective } from 'angular-datatables';
import { Router } from '@angular/router';
import { EvaluacionPromocionService } from '../../evaluacionPromocion.service';
import { Seccion } from '../../../../shared/model/seccion.model';
import { Matricula } from '../../../../shared/model/matricula.model';
import { GestionAcademicaService } from '../../../gestion-academica/gestion-academica.service'; 
import { Persona } from '../../../../shared/model/persona.model';


@Component({
  selector: 'app-calificaciones',
  templateUrl: './calificaciones.component.html',
  styleUrls: ['./calificaciones.component.css']
})
export class CalificacionesComponent implements OnInit {

  @Input() objSeccion: Seccion;
  @Input() docenteLogueado: Persona;

  contProgreso: number = 0;

  public notasJsonArray: object[];  
  public seccionesJsonArray: object[];  
  public objNotas:Matricula[]=[]; 
  public vacio: Matricula[] = []; 
  public codSeccionSeleccionada =0;

  @ViewChildren(DataTableDirective)
  public datatableElement: DataTableDirective;
  dtOptions: DataTables.Settings = {}; 
  dtTrigger: Subject<any> = new Subject();

  constructor(
              private router: Router,
              private EVService: EvaluacionPromocionService,
              private GAService: GestionAcademicaService,
              private zone: NgZone
  ) { }

  ngOnInit() { 
    if (this.objSeccion == undefined) { 
    } else { 
      this.codSeccionSeleccionada = this.objSeccion.getCodSeccion();
    }
   // this.iniciarTablaNotas();
    this.getSeccionesDocente();
    //this.getCuadroNotasSeccion();
    //this.dtTrigger.next(); 
    //this.getCuadroNotasSeccion();
   // console.log('Docente logueado: ', this.docenteLogueado);
  }

  /*  Servicios  */

  getSeccionesDocente(){
    this.GAService.mostrarSeccionesDocente({ codDocente: this.docenteLogueado.codPersona}).subscribe(
      (secciones)=>{
        this.seccionesJsonArray = secciones;
        //console.log(this.seccionesJsonArray);
      }
    );
  }

  cargarCuadroNotas(){ 
        //this.getCuadroNotasSeccion(); 
  }

  getCuadroNotasSeccion(codSeccionSeleccionada){
    //console.log('codSeccionSeleccionada: ', this.codSeccionSeleccionada);

   // console.log({ codSeccion: codSeccionSeleccionada, codDocente: this.docenteLogueado.codPersona })
    this.EVService.mostrarCuadroNotas({ codSeccion: codSeccionSeleccionada, codDocente: this.docenteLogueado.codPersona }).subscribe(
      (notas)=>{    
        this.objNotas = this.vacio;
        this.notasJsonArray = notas;  
        //this.dtTrigger.next(); 
        if (this.notasJsonArray.length >0){
          for (let index = 0; index < notas.length; index++) {
            this.objNotas.push(new Matricula());
            this.objNotas[index].construir(
              notas[index].codSeccion,
              notas[index].codAlumno,
              notas[index].nombreAlumno,
              notas[index].fechaMatricula,
              notas[index].fechaModificaicon,
              notas[index].promedio,
              notas[index].na1,
              notas[index].ne1,
              notas[index].nf1,
              notas[index].na2,
              notas[index].ne2,
              notas[index].nf2,
              notas[index].na3,
              notas[index].ne3,
              notas[index].nf3,
              notas[index].na4,
              notas[index].ne4,
              notas[index].nf4);
          }
          //console.log('probando: ',this.objNotas[0]); 
        }
       
      },
      (err) => console.log(err)
    )

  }

  actualizarNotasSeccion(){ 
    let filas =0;
    for (let index = 0; index < this.objNotas.length; index++) { 
       console.log('enviando fila['+index+']', this.objNotas[index]);
        this.EVService.actualizarMatriculaNotas(this.objNotas[index]).subscribe(
        (data)=>{
          if (data.mensaje != 0) {
            filas = filas+1;
            console.log('Actualizado con exito');
          }
        } ); 
    }
    alert('Registros actualizados.');

    
    // this.EVService.actualizarMatriculaNotas(this.objNotas).subscribe(
    //   (data)=>{
    //     if (data.mensaje != 0) {
    //       alert('Actualizado con exito');
    //     }
    //   } );
  }


  /*    Funcionalidades generales.....             */

  setProgressBar(valor) {
    this.contProgreso = this.contProgreso -valor;
    if (this.contProgreso >= 100) {
      this.contProgreso = 0;
    }
  }

  iniciarTablaNotas() {
    this.dtOptions = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      }
    };
  }

  seccionSeleccionada(codigo){
    this.objSeccion.setCodSeccion(codigo);
   // console.log(this.objSeccion.getCodSeccion());
  }

}
