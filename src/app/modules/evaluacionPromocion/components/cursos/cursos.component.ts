import { Component, OnInit, Input, Output, EventEmitter} from '@angular/core';
import { Router } from '@angular/router';
import { GestionAcademicaService } from '../../../gestion-academica/gestion-academica.service';
import { Persona } from '../../../../shared/model/persona.model'; 
import { Seccion } from '../../../../shared/model/seccion.model';

@Component({
  selector: 'app-cursos',
  templateUrl: './cursos.component.html',
  styleUrls: ['./cursos.component.css']
})
export class CursosComponent implements OnInit {

  @Input() docenteLogueado: Persona;
  @Output() outSetParametroSeccion= new EventEmitter();

  public seccionesJsonArray: object[];  
  public objSeccion = new Seccion;

  constructor(
              private router: Router,
    private GAService: GestionAcademicaService,) { }

  ngOnInit() {
    this.getSeccionesDocente();
  }

  getSeccionesDocente() {
    this.GAService.mostrarSeccionesDocente({ codDocente: this.docenteLogueado.codPersona }).subscribe(
      (secciones) => {
        this.seccionesJsonArray = secciones;
       // console.log(this.seccionesJsonArray);
      }
    );
  }

  goToNotas(codSeccion){ 
    this.objSeccion.setCodSeccion(codSeccion);
    this.outSetParametroSeccion.emit(this.objSeccion);
    this.router.navigate(['/home/evaluacionPromocion/evaProm-calificaciones']);
  }

  goToImprimir() {
    this.router.navigate(['/home/evaluacionPromocion/evaProm-imprimir']);
  }

  goToAsistencia(codSeccion){
    this.objSeccion.setCodSeccion(codSeccion);
    this.outSetParametroSeccion.emit(this.objSeccion);
    this.router.navigate(['/home/evaluacionPromocion/evaProm-asistencia']);
  }

}
