import { Alumnos } from './../../../../shared/model/alumnos.model';
import { Seccion } from './../../../../shared/model/seccion.model';
import { GestionAcademicaService } from './../../../gestion-academica/gestion-academica.service';
import { EvaluacionPromocionService } from './../../evaluacionPromocion.service';
import { Asistencia } from './../../../../shared/model/asistencia.model';
import { Component, OnInit, ViewChildren, QueryList, NgZone } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { DataTableDirective } from 'angular-datatables';
import { DatePipe } from '@angular/common';


@Component({
  selector: 'app-asistencia',
  templateUrl: './asistencia.component.html',
  styleUrls: ['./asistencia.component.css']
})
export class AsistenciaComponent implements OnInit {

  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;
  dtOptions: DataTables.Settings[] = [];
  dtTrigger: Subject<any>[] = [new Subject(), new Subject()];

  dateRangoFecha: number = 3;

  ArrayAsistencia: any[] = [];
  ArrayAsistenciaSeccion: any[] = [];

  fechas: any[] = [];





  public hoy: string;
  public fechaInicial: string;
  public fechaFinal: string;
  public diaActual: Date = new Date();


  public codDocente = localStorage.getItem('codigoPersona');


  constructor(
    private zone: NgZone,
    private datePipe: DatePipe,
    private evaluacionPromocionService: EvaluacionPromocionService,
    private gestionAcademicaService: GestionAcademicaService
  ) {

    this.fechaInicial = this.datePipe.transform(new Date().setDate(this.diaActual.getDate() - 0), "yyyy-MM-dd");
    this.fechaFinal = this.datePipe.transform(new Date().setDate(this.diaActual.getDate() - 4), "yyyy-MM-dd");
  }

  ngOnInit() {
    this.iniciarTablaAsistencia();
    this.dtTrigger[0].next();

    this.ArrayAsistencia.push({ nombre: "oscar1", identidad: "1232-3412-12321", arrayAsistencia: [{ asistio: 1, }, { asistio: 0 }, { asistio: 0 }, { asistio: 1 }] });
    this.ArrayAsistencia.push({ nombre: "oscar2", identidad: "1232-3412-12321", arrayAsistencia: [{ asistio: 1, }, { asistio: 0 }, { asistio: 0 }, { asistio: 1 }] });
    this.ArrayAsistencia.push({ nombre: "oscar3", identidad: "1232-3412-12321", arrayAsistencia: [{ asistio: 1, }, { asistio: 0 }, { asistio: 0 }, { asistio: 1 }] });
    this.ArrayAsistencia.push({ nombre: "oscar4", identidad: "1232-3412-12321", arrayAsistencia: [{ asistio: 1, }, { asistio: 0 }, { asistio: 0 }, { asistio: 1 }] });
    this.ArrayAsistencia.push({ nombre: "oscar5", identidad: "1232-3412-12321", arrayAsistencia: [{ asistio: 1, }, { asistio: 0 }, { asistio: 0 }, { asistio: 1 }] });



    this.fechas.push({ fecha: this.datePipe.transform(new Date().setDate(this.diaActual.getDate() - 4), "yyyy-MM-dd") });
    this.fechas.push({ fecha: this.datePipe.transform(new Date().setDate(this.diaActual.getDate() - 3), "yyyy-MM-dd") });
    this.fechas.push({ fecha: this.datePipe.transform(new Date().setDate(this.diaActual.getDate() - 2), "yyyy-MM-dd") });
    this.fechas.push({ fecha: this.datePipe.transform(new Date().setDate(this.diaActual.getDate() - 1), "yyyy-MM-dd") });
    this.fechas.push({ fecha: this.datePipe.transform(new Date().setDate(this.diaActual.getDate() - 0), "yyyy-MM-dd") });



    this.cargarSeccionDocente();

  }

  public asistenciaHoy:Alumnos[]=[];
  public showAsistenciaButtom:boolean=false
  crearDiaAssitencia() {
   // console.log(this.fechas.length)
    if (this.fechas.length<5){
    this.fechas.push({ fecha: this.datePipe.transform(new Date().setDate(this.diaActual.getDate()), "yyyy-MM-dd") });
   
    }
    this.showAsistenciaButtom=true
  }

  public fechayHoy=this.datePipe.transform(new Date().setDate(this.diaActual.getDate()), "yyyy-MM-dd");
  guardarAsistencia(alumno,asistio){
    //console.log(alumno)
    this.evaluacionPromocionService.agregarAsistencia({codAlumno:alumno.codAlumno,codSeccion:alumno.codSeccion,asistio:asistio,fechaHoy:this.fechayHoy}).subscribe(
      (data)=>{
        if(data.mensaje>0){
        alert("alumno"+alumno.nombreAlumno+"se agrego exitosamente sus asistencia")
      }else{
        alert("alumno"+alumno.nombreAlumno+"se agrego exitosamente sus asistencia")
      }

      },
      (err)=>console.log(err)
    )


  }

  crearActualizarTabla(){
    this.jsonAlumnos.length = 0;
    this.cargarSeccionAlumnos(this.seccionAtual)


  }


  public nuevaAsistencia: Asistencia;

  cargarAsistenciaAlumnos(alumno: Alumnos, codAlumno, seccion) {
    //console.log("codAlumno" + codAlumno + " codSeccion:" + seccion)
    this.nuevaAsistencia = new Asistencia(0, "", 0, codAlumno, seccion, "", this.fechaFinal, this.fechaInicial, )
    //console.log(this.nuevaAsistencia);
    this.evaluacionPromocionService.mostrarAsistenciaFecha(JSON.stringify(this.nuevaAsistencia)).subscribe(
      (data) => {
       // console.log("mostrando cargar Asistencias Alumno")
       // console.log(data)

        alumno.arrayAsistencia = data;

        //objAlumno.setArrayAsistencia(data);
        //console.log(this.jsonAlumnos)



      },
      (error) => console.log(error)
    );

  }


  public jsonAlumnos: any[] = [];
  public seccionAtual;
  cargarSeccionAlumnos(seccion) {
    this.seccionAtual=seccion.codSeccion;
    this.evaluacionPromocionService.mostrarSeccionAlumnos({ codAlumno: seccion.codSeccion }).subscribe(
      (data) => {

        this.jsonAlumnos = data;
        this.jsonAlumnos.forEach(alumno => {
         // console.log(alumno + "   " + alumno.codAlumno + "  " + alumno.codSeccion)
          this.cargarAsistenciaAlumnos(alumno, alumno.codAlumno, alumno.codSeccion)

        })


      },
      (error) => console.log(error)
    );



  }

  public jsonSeccions = new Seccion;
  cargarSeccionDocente() {
    this.gestionAcademicaService.mostrarSeccionesDocente({ codDocente: this.codDocente }).subscribe(
      (data) => {
        this.jsonSeccions = data;
       // console.log(this.jsonSeccions);

      },
      (error) => console.log(error)
    )



  }

  getAlumnoSelected(info: any): void {
    //console.log(info);
  }

  iniciarTablaAsistencia() {
    //opciones para datatable
    this.dtOptions[0] = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      },
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.getAlumnoSelected(data);
        });
        return row;
      }
    };
  }
}
