import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';


//interface
import { Mensaje } from '../../shared/model/mensaje.model';


@Injectable()
export class EvaluacionPromocionService {




    headers = new HttpHeaders()
        .set('Content-Type', 'application/json');

    constructor(private http: HttpClient) { }

    getUserInfo(): Observable<any> {
        //console.log("consiguiendodatos");
        const parametros = {
            codigoPersona: localStorage.getItem("codigoPersona")

        }

        // return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})
        //https://appacademica.herokuapp.com/
        return this.http.post<any>('https://appacademica.herokuapp.com/main/obtenerDatosPersona',
            parametros,
            { headers: this.headers });
    }

    //********************************************************************************************************************/

    //servicios para obtener datos para mostrara en las tajetas de la seccion de dashboard
    mostrarDashboard(dashboard): Observable<any> {
        //console.log("consiguiendodatos");
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/evaluacionpromocion/mostrarDashboardEvaluacionPromocion',
            dashboard,
            { headers: this.headers });

    }

    //********************************************Calificaciones***************************************************************/

    //servicios para obtener datos para mostrara en las tajetas de la seccion de dashboard
    mostrarCuadroNotas(calificaciones): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/evaluacionpromocion/mostrarCuadroNotas',
            calificaciones,
            { headers: this.headers });

    }

    //********************************************matricula***************************************************************/

    //servicios para obtener datos para mostrara en las tajetas de la seccion de dashboard
    crearMatricula(matricula): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/evaluacionpromocion/crearMatricula',
            matricula,
            { headers: this.headers });

    }


    actualizarMatriculaNotas(matricula): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/evaluacionpromocion/actualizarMatriculaNotas',
            matricula,
            { headers: this.headers });

    }

    //seccion

    mostrarSeccionAlumnos(seccion): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/evaluacionpromocion/mostrarSeccionAlumnos',
            seccion,
            { headers: this.headers });

    }

    //asistencia

    mostrarAsistenciaFecha(asistencia): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/evaluacionpromocion/mostrarAsistenciaFecha',
            asistencia,
            { headers: this.headers });

    }


    agregarAsistencia(asistencia): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/evaluacionpromocion/agregarAsistencia',
            asistencia,
            { headers: this.headers });

    }












}
