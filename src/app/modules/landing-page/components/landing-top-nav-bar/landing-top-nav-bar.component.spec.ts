import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { LandingTopNavBarComponent } from './landing-top-nav-bar.component';

describe('LandingTopNavBarComponent', () => {
  let component: LandingTopNavBarComponent;
  let fixture: ComponentFixture<LandingTopNavBarComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ LandingTopNavBarComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(LandingTopNavBarComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
