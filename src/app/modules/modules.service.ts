import { Injectable } from '@angular/core';
import { BehaviorSubject } from 'rxjs/BehaviorSubject';

import { Usuario } from '../shared/model/usuario.model';
@Injectable()
export class ModuleService {

    private usuario: Usuario;

    private messageSource = new BehaviorSubject<Usuario>(this.usuario);
    currentMessage = this.messageSource.asObservable();

    constructor() { }



    setUser(usuario: Usuario) {
        this.messageSource.next(usuario)
    }

}