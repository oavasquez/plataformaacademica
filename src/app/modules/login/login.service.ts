import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { HttpClientModule } from '@angular/common/http';
import { HttpModule } from '@angular/http';

import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';


import * as moment from "moment";

import { Usuario } from '../../shared/model/usuario.model'
import { TipoUsuario } from '../../shared/model/tipoUsuario.model'

//interface
import { Mensaje } from '../../shared/model/mensaje.model';




@Injectable()
export class LoginService {


    headers = new HttpHeaders()
        .set('Content-Type', 'application/json');



    constructor(private http: HttpClient) { }

    sendlogin(usuario: Usuario): Observable<any> {
        // return this.http.get<any>('http://127.0.0.1:3000/login/iniciarSesion', {params:{nombre:"prueba"}})


        return this.http.post<Usuario>('https://appacademica.herokuapp.com/login/iniciarSesion',
            JSON.stringify(usuario),
            { headers: this.headers })




    }


}





/*
return this.http.get<IGetsMensaje>('http://127.0.0.1:5000/login/obtenerDatos', {params: {nombre:"prueba"}, 
                                                       headers: this.headers,
                                                       observe: 'response'}).subscribe(data => {
                                                        console.log(data);
                                                      });

*/