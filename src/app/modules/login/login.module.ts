import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';


import { LoginComponent } from './components/login/login.component';
import { LoginService } from './login.service'
import { ModuleService } from '../modules.service'
import { SesionIniciadaComponent } from './components/home/sesion-iniciada.component';

import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';



@NgModule({
  imports: [CommonModule,
    HttpClientModule,
    FormsModule],
  declarations: [
    LoginComponent,
    SesionIniciadaComponent
  ],
  providers: [LoginService, ModuleService],
  exports: [LoginComponent, SesionIniciadaComponent],
  bootstrap: []
})
export class LoginModule { }
