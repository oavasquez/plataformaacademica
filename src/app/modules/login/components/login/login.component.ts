import { Component, OnInit } from '@angular/core';

import { LoginService } from '../../login.service';
import { ModuleService } from "../../../modules.service";

import { Mensaje } from '../../../../shared/model/mensaje.model'
import { Usuario } from '../../../../shared/model/usuario.model'
import { TipoUsuario } from '../../../../shared/model/tipoUsuario.model'


import { Router } from '@angular/router';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {

  model = new Usuario();
  usuario: Usuario;
  tipoUsuario: TipoUsuario;
  respuesta: any;


  mensaje: Mensaje;
  errorMessage: String;

  public flagsCredenciales: boolean = false;
  public flagsCredencialesOk: boolean = false;



  constructor(private moduleService: ModuleService, private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }

  enviarDatos(): void {
    //console.log("nombre: " + this.model.getNombreUsuario() + " contrasena: " + this.model.getContrasena());
    this.loginService.sendlogin(this.model).subscribe(
      (data) => {

        if (data.mensaje == 1) {
          this.respuesta = 0;
          this.flagsCredenciales = false;
          this.flagsCredencialesOk = true;

          //console.log("mensaje: cargarseccion");
          this.model.setSession(data);
          this.model = data.datosUsuario;
          this.model = new Usuario();
          this.model.setNombreUsuario(data.datosUsuario.nombreUsuario);
          this.model.setAccesos(data.datosUsuario.accesos);

          setTimeout(() => { this.CargarSeccion() }, 1000);
          //this.model = new Usuario(data.datosUsuario.nombreUsuario, "", data.datosUsuario.accesos)

        } else {
          if (data.mensaje == 2) {
            this.respuesta = 2;
            //console.log("mensaje: Usuario bloqueado ");



          } else {
            this.respuesta = 1;
            this.flagsCredenciales = true;
            //console.log("mensaje: mostrar mensaje de error de contraseña o usuario incorrecto");
          }
        }



      },  //changed
      (err) => console.log(err)

    );

  }

  CargarSeccion() {
    //console.log("seccion cargada");
    this.newMessage();
    this.router.navigate(['/home']);
    //this.router.navigate(['/main']);
  }

  newMessage() {
    this.moduleService.setUser(this.model)
  }

}
