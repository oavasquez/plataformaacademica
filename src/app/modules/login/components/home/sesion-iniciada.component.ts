import { Component, OnInit, Input } from '@angular/core';

import { LoginService } from '../../login.service';

import { Router } from '@angular/router';
import { Usuario } from '../../../../shared/model/usuario.model'

@Component({
  selector: 'app-sesion-iniciada',
  templateUrl: './sesion-iniciada.component.html',
  styleUrls: ['./sesion-iniciada.component.css']
})
export class SesionIniciadaComponent implements OnInit {

  @Input() usuario: Usuario;

  constructor(private loginService: LoginService, private router: Router) { }

  ngOnInit() {
  }


  salir(): void {
    localStorage.removeItem("token");

    if (localStorage.removeItem("token") == null) {
     // console.log("seccion saliendo");
      this.router.navigate(['/login']);

    }

  }
}
