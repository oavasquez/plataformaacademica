import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { SesionIniciadaComponent } from './components/home/sesion-iniciada.component'
import { AuthGuardService as AuthGuard } from '../../auth/auth-guard.service';

const routes: Routes = [
    { path: 'mostarMensaje', component: SesionIniciadaComponent, canActivate: [AuthGuard] }
];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class LoginRoutesModule { }