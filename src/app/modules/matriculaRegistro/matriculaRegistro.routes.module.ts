import { NgModule } from '@angular/core';
import { RouterModule, Routes } from '@angular/router';
import { AuthGuardService as AuthGuard } from '../../auth/auth-guard.service';

//import { AdminDashboardComponent } from "./components/admin-dashboard/admin-dashboard.component";
import { ExpedienteComponent } from './components/expediente/expediente.component'
import { MatriRegDashboardComponent } from './components/matri-reg-dashboard/matri-reg-dashboard.component'
import { FichaComponent } from './components/ficha/ficha.component';
import { FormularioFichaComponent } from './components/ficha/formularios/formulario-ficha/formulario-ficha.component';
import { MatriculaComponent } from './components/matricula/matricula.component';
import { FormularioExpedienteComponent } from './components/expediente/formularios/formulario-expediente/formulario-expediente.component'
import { FormularioMatriculaComponent } from './components/matricula/formulario/formulario-matricula/formulario-matricula.component';
import { FormularioIncidenciaComponent } from './components/expediente/formularios/formulario-incidencia/formulario-incidencia.component'
import { FormularioTrasladoComponent } from './components/expediente/formularios/formulario-traslado/formulario-traslado.component'
import { HistorialComponent } from './components/historial/historial.component'

const routes: Routes = [
    { path: 'matriculaRegistro', redirectTo: 'matriculaRegistro/matriReg-dashboard', pathMatch: 'full' },
    { path: 'matriReg-expediente', component: ExpedienteComponent, canActivate: [AuthGuard] },
    { path: 'matriReg-dashboard', component: MatriRegDashboardComponent, canActivate: [AuthGuard] },
    { path: 'matriReg-ficha/:idTabs', component: FichaComponent, canActivate: [AuthGuard] },
    { path: 'matriReg-ficha', component: FichaComponent, canActivate: [AuthGuard] },
    { path: 'matriReg-matricula', component: MatriculaComponent, canActivate: [AuthGuard] },
    { path: 'formulario-alumno', component: FormularioFichaComponent, canActivate: [AuthGuard] },
    { path: 'formulario-expediente/:idTabs', component: FormularioExpedienteComponent, canActivate: [AuthGuard] },
    { path: 'formulario-matricula', component: FormularioMatriculaComponent, canActivate: [AuthGuard] },
    { path: 'formulario-incidencia', component: FormularioIncidenciaComponent, canActivate: [AuthGuard] },
    { path: 'formulario-traslado', component: FormularioTrasladoComponent, canActivate: [AuthGuard] },
    { path: 'matriReg-historial', component: HistorialComponent, canActivate: [AuthGuard] }


];

@NgModule({
    imports: [RouterModule.forChild(routes)],
    exports: [RouterModule]
})

export class MatriculaRegistroRoutesModule { }