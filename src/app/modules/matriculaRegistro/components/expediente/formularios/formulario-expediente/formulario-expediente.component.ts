import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Alumnos } from '../../../../../../shared/model/alumnos.model';

@Component({
  selector: 'app-formulario-expediente',
  templateUrl: './formulario-expediente.component.html',
  styleUrls: ['./formulario-expediente.component.css']
})
export class FormularioExpedienteComponent implements OnInit {

  @Input() inTabsActiva:number;
  @Input() actionFlag:number;
  @Input() objAlumno: Alumnos;

  tabsTraslado: string;
  tabsTutor:string ;
  tabsIncidencia:string ;
  nuevoAlumno= new Alumnos;
  mensaje:string;

  constructor(public router:Router) { }

  ngOnInit() { 
   // console.log("parametro bandera: ",this.actionFlag);
    this.presentarBotonesAccion(this.actionFlag);
    this.activarTabsCorrespondiente();
  }

  activarTabsCorrespondiente(){
    this.tabsTraslado= "";
    this.tabsTutor= "" ;
    this.tabsIncidencia= "" ;

    if(this.router.url=="/home/matriculaRegistro/formulario-expediente/1"){
      this.tabsTraslado='active';
      this.tabsTutor='';
      this.tabsIncidencia='';
    }
    if(this.router.url=="/home/matriculaRegistro/matriReg-ficha/2"){
      this.tabsTraslado='';
      this.tabsTutor='active';
      this.tabsIncidencia='';
    } 
    if(this.router.url=="/home/matriculaRegistro/formulario-expediente/3"){
      this.tabsTraslado='';
      this.tabsTutor='';
      this.tabsIncidencia='active';
    } 


  }


  goToExpediente() {
    this.router.navigate(['/home/matriculaRegistro/matriReg-expediente']);
  }

  presentarBotonesAccion(actionFlag) {
    //presentar los botones a utilizar segun la acción requerida
    switch (actionFlag) {
      case 1: {
        //console.log('Presentar info de: ', this.objAlumno); 
        break;
      }
      case 2: {
        //console.log('presentar botones de guardar nuevo expediente');
        this.mensaje = "Fomulario de ingreso de docente";
      }
      default:
        break;
    }
  }

  crearNuevoAlumno(){}

}
