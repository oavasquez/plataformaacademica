import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioExpedienteComponent } from './formulario-expediente.component';

describe('FormularioExpedienteComponent', () => {
  let component: FormularioExpedienteComponent;
  let fixture: ComponentFixture<FormularioExpedienteComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioExpedienteComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioExpedienteComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
