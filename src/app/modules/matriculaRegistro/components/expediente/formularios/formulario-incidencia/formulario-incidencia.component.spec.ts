import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioIncidenciaComponent } from './formulario-incidencia.component';

describe('FormularioIncidenciaComponent', () => {
  let component: FormularioIncidenciaComponent;
  let fixture: ComponentFixture<FormularioIncidenciaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioIncidenciaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioIncidenciaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
