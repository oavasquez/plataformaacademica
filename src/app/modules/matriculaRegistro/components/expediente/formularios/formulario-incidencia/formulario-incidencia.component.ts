import { TipoIncidente } from './../../../../../../shared/model/tipoIncidente.model';
import { Incidente } from './../../../../../../shared/model/incidente.model';
import { Component, NgZone, OnInit, Input, Output, EventEmitter, ViewChildren, QueryList } from '@angular/core';
import { Router } from '@angular/router';
import { MatriculaRegistroService } from '../../../../matriculaRegistro.service';
import { GestionAcademicaService } from "../../../../../gestion-academica/gestion-academica.service";
import { Alumnos } from '../../../../../../shared/model/alumnos.model';
import { DataTableDirective } from 'angular-datatables';
import { Subject } from 'rxjs/Subject';
import { Validaciones_Matricula } from "./../../../../../../shared/model/Vadicacion_Matricula.model"

// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;


@Component({
  selector: 'app-formulario-incidencia', 
  templateUrl: './formulario-incidencia.component.html',
  styleUrls: ['./formulario-incidencia.component.css']
})
export class FormularioIncidenciaComponent implements OnInit {

  @Input() inObjAlumno: Alumnos;

  @ViewChildren(DataTableDirective)
  dtElements: QueryList<DataTableDirective>;
  dtOptions: DataTables.Settings[] = [];
  dtTrigger: Subject<any>[] = [new Subject(), new Subject()];

  mensaje: string;

  public objAlumno = new Alumnos;
  public incidenciasJsonArray: object[] = [];
  public nameTag = "";

  validacion=new Validaciones_Matricula();

  constructor(public router: Router, private MRService: MatriculaRegistroService,  private gestionAcademicaService: GestionAcademicaService) { }

  ngOnInit() {
    if (this.inObjAlumno == undefined) {
      //No se cargara la tabla
     // console.log('no se carga la tabla');
    } else {
      //secargara la tabla con el codigo del alumno
      this.objAlumno = this.inObjAlumno;
      this.nameTag = this.objAlumno.getNombre() + ' ' + this.objAlumno.getApellido();
      this.getArregloIncidencias(this.objAlumno.getCodPersona(), 1);
      //this.getArregloIncidencias(0, 0);
      this.cargarTipoIncidentes();
    }
    this.iniciarTablaIncidencias();
    this.ObtenerDocente();
  }

  ObtenerDocente(){
    //console.log('Obteniendo docente');
    //console.log(localStorage.getItem('codigoPersona'))
    this.gestionAcademicaService.mostrarDocentes({codDocente:localStorage.getItem('codigoPersona')}).subscribe(
      (data)=>{
        //console.log("nombre del infrator")
        //console.log(data)
        if(data.length>0){
        this.nuevaIncidencia.setNombreInfractor(data[0].nombreCompleto);
        }
      },
      (error)=>{
      }
    )
  }

  iniciarTablaIncidencias() {
    this.dtOptions[0] = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      }
    };
  }


  /** **/
  public showIncidenciasJsonArray: boolean = false;
  getArregloIncidencias(codPersona, parametro) {
    this.MRService.mostrarIncidencias({ codPersona: codPersona, parametro: parametro }).subscribe(
      (incidenciasResult) => {
        //console.log("incidenciaResult")
        //console.log(incidenciasResult);
        this.incidenciasJsonArray = incidenciasResult;

        if (this.incidenciasJsonArray.length>0) {

          //console.log("true incidencia")
          //console.log(this.incidenciasJsonArray)
          this.showIncidenciasJsonArray = true;
        }
        this.dtTrigger[0].next();
      },
      (err) => console.log(err)
    );
  }

  goToExpediente() {
    this.router.navigate(['/home/matriculaRegistro/matriReg-expediente']);
  }


  /**********************datos para insertar y actualizar una incidencia************************/
  public nuevaIncidencia = new Incidente;
  public IncidenciaAgregadaAlumno: Incidente[] = [];

  crearNuevoIncidencia() {
    this.nuevaIncidencia.setCodAlumno(this.inObjAlumno.codPersona)
    this.nuevaIncidencia.setCodDocente(+localStorage.getItem('codigoPersona'))
    //console.log("console.log nueva incidencia")
    if(this.validacion.validar_Incidencia(this.nuevaIncidencia.getTituloIncidencia(),this.nuevaIncidencia.getFechaIncidencia(),this.nuevaIncidencia.getCodTipoIncidencia(),this.nuevaIncidencia.getDescripcion())==0){
      //console.log(this.nuevaIncidencia);
      this.MRService.agregarIncidencia(JSON.stringify(this.nuevaIncidencia)).subscribe(
        (incidenciasResult) => {
          //console.log(incidenciasResult);
          this.incidenciasJsonArray = incidenciasResult;
          this.dtTrigger[0].next();
        },
        (err) => console.log(err)
      );
    }
  }


  actualizarIncidente() {
    if(this.validacion.validar_Incidencia(this.nuevaIncidencia.getTituloIncidencia(),this.nuevaIncidencia.getFechaIncidencia(),this.nuevaIncidencia.getCodTipoIncidencia(),this.nuevaIncidencia.getDescripcion())==0){
      this.MRService.actualizarIncidencia(JSON.stringify(this.nuevaIncidencia)).subscribe(
        (incidenciasResult) => {
          //console.log(incidenciasResult);
          this.incidenciasJsonArray = incidenciasResult;
          this.dtTrigger[0].next();
        },
        (err) => console.log(err)
      );
    }
  }

  removerDatos(){
    this.validacion.remover_validacionesIncidencia();
    this.nuevaIncidencia.construir("","","","");
  }

  /****************************mostrar tipos de incidencia****************************+*/
  public tipoIncidenciaArray: TipoIncidente[];
  cargarTipoIncidentes() {
    this.MRService.mostrarTipoIncidencias("").subscribe(
      (tiposIncidenciasResult) => {
        //console.log("tipoIncidentes")
        //console.log(tiposIncidenciasResult);
        this.tipoIncidenciaArray = tiposIncidenciasResult;

      },
      (err) => console.log(err)
    );


  }



  presentarBotonesAccion(actionFlag) {
    //presentar los botones a utilizar segun la acción requerida
    switch (actionFlag) {
      case 1: {
        //console.log('unicamente boton de volver y actualizar');
        this.mensaje = "Nombre completo de usuario";
        break;
      }
      case 2: {
        //console.log('presentar botones de guardar');
        this.mensaje = "Fomulario de ingreso de docente";
      }
      default:
        break;
    }
  }

}
