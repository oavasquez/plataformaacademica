import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioTrasladoComponent } from './formulario-traslado.component';

describe('FormularioTrasladoComponent', () => {
  let component: FormularioTrasladoComponent;
  let fixture: ComponentFixture<FormularioTrasladoComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioTrasladoComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioTrasladoComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
