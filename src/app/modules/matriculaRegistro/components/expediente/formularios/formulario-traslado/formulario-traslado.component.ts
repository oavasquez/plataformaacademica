import { AdministrarService } from './../../../../../administracion/administracion.service';
import { MatriculaRegistroService } from './../../../../matriculaRegistro.service';
import { EvaluacionPromocionService } from './../../../../../evaluacionPromocion/evaluacionPromocion.service';
import { Traslado } from './../../../../../../shared/model/traslados.model';
import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { Alumnos } from "./../../../../../../shared/model/alumnos.model"
import { Validaciones_Matricula } from "./../../../../../../shared/model/Vadicacion_Matricula.model"

// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-formulario-traslado',
  templateUrl: './formulario-traslado.component.html',
  styleUrls: ['./formulario-traslado.component.css']
})
export class FormularioTrasladoComponent implements OnInit {
  mensaje: string;
  Validacion= new Validaciones_Matricula();
  @Input() inObjAlumno: Alumnos;

  constructor(

    public router: Router,
    public matriculaRegistroService: MatriculaRegistroService,
    public administrarService: AdministrarService

  ) { }

  ngOnInit() {
    //console.log(this.inObjAlumno);
    this.llenarSelectArray();
    this.mostrarTraslado();
  }


  goToFicha() {
    this.router.navigate(['/home/matriculaRegistro/matriReg-ficha']);
  }

  goToExpediente() {
    this.router.navigate(['/home/matriculaRegistro/matriReg-expediente']);
  }

  presentarBotonesAccion(actionFlag) {
    //presentar los botones a utilizar segun la acción requerida
    switch (actionFlag) {
      case 1: {
        //console.log('unicamente boton de volver y actualizar');
        this.mensaje = "Nombre completo de usuario";
        break;
      }
      case 2: {
        //console.log('presentar botones de guardar');
        this.mensaje = "Fomulario de ingreso de docente";
      }
      default:
        break;
    }
  }


  /***************************************************Traslado*********************************************************/

  public nuevoTralado = new Traslado;


  crearNuevoTraslado() {
    //console.log(this.arrayAsignaturaTralado);
    if(this.arrayAsignaturaTralado.length>0){
      this.matriculaRegistroService.agregarTraslado(JSON.stringify(this.arrayAsignaturaTralado)).subscribe(
        (data) => {
        // console.log(data);
        },
        (err) => console.log(err)
      );
    }else{
      $('#modal-exito_traslado .modal-body').html('<p>No hay Traslados agregados par el Alumno</p>');
      $('#modal-exito_traslado .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
      $('#modal-exito_traslado').modal('show');
    }
  }

  mostrarTraslado() {
    //console.log(this.nuevoTralado);
    this.matriculaRegistroService.mostrarHistorial({ codAlumno: this.inObjAlumno.codPersona }).subscribe(
      (data) => {
 
       
        if(data.length>0){
          //console.log("data de historial trasdlado")
          //console.log(data)
        this.arrayAsignaturaTralado=data;
      }
      
      },
      (err) => console.log(err)
    )
  }


  /**************************************************cargar datos a select***************************************************/
  anioLectivoArray: any[];
  periodoArray: any[];
  cursoArray: any[];
  jornadaArray: any[];
  asignaturaArray: any[];

  llenarSelectArray() {

    this.administrarService.mostrarAnioLectivo({ codLectivo: 0 }).subscribe(
      (data) => {
        this.anioLectivoArray = data;


      },
      (err) => {

      })


    this.administrarService.mostrarPeriodo({ codPeriodo: 0 }).subscribe(
      (data) => {
        this.periodoArray = data;

      },
      (err) => {

      })

    this.administrarService.mostrarCursos({ codCurso: 0 }).subscribe(
      (data) => {
        this.cursoArray = data;

      },
      (err) => {

      })

    this.administrarService.mostrarJornada({ codJornada: 0 }).subscribe(
      (data) => {
        this.jornadaArray = data;

      },
      (err) => {

      })
    this.administrarService.mostrarAsignatura({ codAsignatura: 0 }).subscribe(
      (data) => {
        this.asignaturaArray = data;
        //console.log(data)

      },
      (err) => {

      })



  }

  /******************************************************arreglo de asignaturas tralado*************************/
  public arrayAsignaturaTralado: Traslado[] = []
  guardarAsignaturaTralado() {
    if(this.Validacion.validar_traslado(this.nuevoTralado.getInstitucion(),this.nuevoTralado.getFechaTraslado(),this.nuevoTralado.getNombreLectivo(),this.nuevoTralado.getPeriodo(),this.nuevoTralado.getNombreCurso(),this.nuevoTralado.getCodJornada(),this.nuevoTralado.getAsignatura(),this.nuevoTralado.getPromedioActual())==0){
      this.arrayAsignaturaTralado.push(
        new Traslado(this.nuevoTralado.getInstitucion(), this.nuevoTralado.getFechaTraslado(), 
        this.nuevoTralado.getCodLectivo(), this.nuevoTralado.getNombreLectivo(), 
        this.nuevoTralado.getCodPeriodo(), this.nuevoTralado.getPeriodo(), 
        this.nuevoTralado.getCodCurso(), this.nuevoTralado.getNombreCurso(), 
        this.nuevoTralado.getCodJornada(), this.nuevoTralado.getCodAsignatura(), 
        this.nuevoTralado.getAsignatura(), this.nuevoTralado.getPromedioActual(), 
        this.inObjAlumno.codPersona)
      );
      this.nuevoTralado.Construir("","","","","","","","");
      this.Validacion.remover_validacionesTraslado();
    }
    //console.log(this.arrayAsignaturaTralado);
    //console.log(this.inObjAlumno);
  }

  eliminarAsignaturaTralado(key: any) {
    this.arrayAsignaturaTralado.splice(key, 1);
  }





}
