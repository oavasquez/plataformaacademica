import { Component, NgZone, OnInit, Input, Output, EventEmitter, ViewChild } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { Alumnos } from '../../../../shared/model/alumnos.model';
import { MatriculaRegistroService } from '../../matriculaRegistro.service';
import { DataTableDirective } from 'angular-datatables';

@Component({
  selector: 'app-expediente',
  templateUrl: './expediente.component.html',
  styleUrls: ['./expediente.component.css']
})
export class ExpedienteComponent implements OnInit {

  //variables de API de componente
  @Input() inAlumnosJsonArray: any[] = [];
  @Output() outRecargarTablaAlumnos = new EventEmitter();
  @Output() outSetParametroAlumno = new EventEmitter();
  @Output() outsetActionFlag = new EventEmitter();
  @Output() outSetAlumnosActionFlag = new EventEmitter();
  @Output() outTabsActiva = new EventEmitter();

  public tabsActiva: number;

  @ViewChild(DataTableDirective)
  private datatableElement: DataTableDirective;
  dtTrigger: Subject<any> = new Subject();
  dtAlumnosOpt: DataTables.Settings = {};

  objAlumno = new Alumnos();

  constructor(private router: Router, private matriculaRegistroService: MatriculaRegistroService) { }

  ngOnInit() {
    this.cargarTodosAlumnosResumen();
    this.iniciarTablaAlumno();
  }

  goToFicha() {
    console.log(localStorage.getItem('codigoUsuario'));
    this.outsetActionFlag.emit(2);
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.router.navigate(['/home/matriculaRegistro/matriReg-ficha']);
  }

  goToForm() {

    //enviar dato seleccionado al maincontainer para que se actualice en el formulario Alumno

    this.router.navigate(['/home/matriculaRegistro/formulario-traslado']);
  }

  recargarTablaAlumno() {
    this.outRecargarTablaAlumnos.emit();
  }

  ingresarNuevoExpediente(parametro, tabsActiva) {
    //activar el formulario de nuevo Alumno:
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.outSetAlumnosActionFlag.emit(parametro);
    this.tabsActiva = tabsActiva;
    this.goToForm();

  }

  ingresarNuevoTraslado() {
    this.router.navigate(['/home/matriculaRegistro/formulario-traslado']);

  }

  ingresarNuevoIncidencia() {
    this.router.navigate(['/home/matriculaRegistro/formulario-incidencia']);

  }

  cargarTodosAlumnosResumen() {
    this.matriculaRegistroService.mostrarAlumnoResumen('{"codAlumno": 0 }').subscribe(
      (alumnosData) => {
        //console.log(alumnosData);
        this.inAlumnosJsonArray = alumnosData;
        this.dtTrigger.next();
      },
      (err) => console.log(err)
    );

    this.matriculaRegistroService.mostrarAlumnos('{"codAlumno": 0 }').subscribe(
      (alumnosData) => {
       // console.log(alumnosData);

      },
      (err) => console.log(err)
    );


  }

  actualizarAlumno(codAlumno) {
    //activar el formulario de nuevo Alumno:
    //this.outSetParametroAlumno.emit(this.objAlumno);
    //this.outSetAlumnosActionFlag.emit(parametro);
    this.objAlumno.setCodAlumno(codAlumno);
    this.outsetActionFlag.emit(1);
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.router.navigate(['/home/matriculaRegistro/matriReg-ficha']);
  }

  actualizarTraslado(parametro) {
    //activar el formulario de nuevo Alumno:
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.outSetAlumnosActionFlag.emit(parametro);
    this.router.navigate(['/home/matriculaRegistro/formulario-traslado']);
  }

  actualizarTutor(parametro) {
    //activar el formulario de nuevo Alumno:
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.outSetAlumnosActionFlag.emit(parametro);
    this.router.navigate(['/home/matriculaRegistro/matriReg-ficha', 2]);
  }
  actualizarIncidencia(parametro) {
    //activar el formulario de nuevo Alumno:
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.outSetAlumnosActionFlag.emit(parametro);
    this.router.navigate(['/home/matriculaRegistro/formulario-incidencia']);
  }

  iniciarTablaAlumno() {
    //opciones para datatable
    this.dtAlumnosOpt = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      },
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.getFilaAlumnoSel(data);
        });
        return row;
      }
    };
  }

  getFilaAlumnoSel(info: any): void {
   // console.log(info);
  }

  setCheckStatus(valor) {
    if (valor == 1) {
      return "Activo";
    } else {
      return "Innactivo";
    }
  }

  habilitado(valor) {
    if (valor == 1) {
      return true;
    } else {
      return false;
    }
  }

  deshabilitado(valor) {
    if (valor == 0) {
      return true;
    } else {
      return false;
    }
  }


}



