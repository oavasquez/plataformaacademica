import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { Alumnos } from '../../../../../../shared/model/alumnos.model';
import { MatriculaRegistroService } from '../../../../matriculaRegistro.service';

@Component({
  selector: 'app-formulario-ficha',
  templateUrl: './formulario-ficha.component.html',
  styleUrls: ['./formulario-ficha.component.css']
})
export class FormularioFichaComponent implements OnInit {

  @Input() data: Alumnos;
  @Input() mensaje: string;
  @Input() objAlumno: Alumnos; //objeto con la informacion del alumno capturado
  @Input() actionFlag: number; //bader de acción--> 1 para presentacion | 2 para insertar | 3 para actualizar
  public nuevoAlumno = new Alumnos;

  constructor(private router:Router,private matriculaResgistroService: MatriculaRegistroService ) { }

  ngOnInit() {

    if (this.actionFlag == 1) {
      this.presentarData();
    }
    this.presentarBotonesAccion(this.actionFlag);
    //this.nuevoAlumno = this.data;
  
  }


  crearNuevoAlumno() {

  }

  actualizaAlumno() {

  }

  goToAlumnos() {
    this.router.navigate(['/home/matriculaRegistro/matriReg-ficha']);
  }

  presentarBotonesAccion(actionFlag) {
    //presentar los botones a utilizar segun la acción requerida
    switch (actionFlag) {
      case 1: {
        //console.log('unicamente boton de volver y actualizar');
        this.mensaje = "Nombre completo de usuario";
        break;
      }
      case 2: {
        //console.log('presentar botones de guardar');
        this.mensaje = "Fomulario de ingreso de Alumno";
      }
      default:
        break;
    }
  }

  presentarData() {
    //enlazar los campos entrantes con ngmodel
    //console.log('Data del Alumno', this.data);
    //console.log(this.objAlumno);
  }

}
