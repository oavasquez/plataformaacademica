import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { FormularioFichaComponent } from './formulario-ficha.component';

describe('FormularioFichaComponent', () => {
  let component: FormularioFichaComponent;
  let fixture: ComponentFixture<FormularioFichaComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ FormularioFichaComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(FormularioFichaComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
