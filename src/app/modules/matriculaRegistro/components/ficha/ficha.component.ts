import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Router } from '@angular/router';

import { Alumnos } from '../../../../shared/model/alumnos.model';
import { Tutor } from '../../../../shared/model/tutor.model';

import { MatriculaRegistroService } from '../../matriculaRegistro.service';
import { Validaciones_Matricula } from '../../../../shared/model/Vadicacion_Matricula.model';

// Declaramos las variables para jQuery
declare var jQuery: any;
declare var $: any;

@Component({
  selector: 'app-ficha',
  templateUrl: './ficha.component.html',
  styleUrls: ['./ficha.component.css']
})
export class FichaComponent implements OnInit {
  @Input() data: Alumnos;
  @Input() mensaje: string;
  @Input() objAlumno: Alumnos; //objeto con la informacion del alumno capturado
  @Input() actionFlag: number; //bader de acción--> 1 para presentacion | 2 para insertar | 3 para actualizar

  @Output() outSetParametroAlumno = new EventEmitter();
  @Output() outSetActionForm = new EventEmitter();

  public nuevoAlumno = new Alumnos;
  Validaciones = new Validaciones_Matricula();

  public nuevoTutor = new Tutor;
  TutoresAgregadosAlumno: Tutor[] = [];

  public tabsTutor: string;
  public tabsAlumno: string;

  constructor(private router: Router, private matriculaResgistroService: MatriculaRegistroService) { }

  ngOnInit() {

    if (this.objAlumno == undefined) {
     // console.log("No se cargara los datos");
    } else {
      this.nuevoAlumno = this.objAlumno;


    }

    this.cargarTabsTutor();
    this.presentarBotonesAccion(this.actionFlag);
    //this.nuevoAlumno = this.data;

  }
  cargarTabsTutor() {
    if (this.router.url == "/home/matriculaRegistro/matriReg-ficha/2") {
      this.tabsTutor = 'active';
      this.tabsAlumno = '';
    } else {
      this.tabsTutor = '';
      this.tabsAlumno = 'active';

    }
  }

  guardarTutor() {
   // console.log(this.nuevoTutor);
    if (this.Validaciones.validar_Tutor(this.nuevoTutor.getIdentidad(), this.nuevoTutor.getNombre(), this.nuevoTutor.getApellido(), this.nuevoTutor.getParentesco(), this.nuevoTutor.getSexo(), this.nuevoTutor.getCelular(), this.nuevoTutor.getTelefonoTrabajo(), this.nuevoTutor.getLugarTrabajo(), this.nuevoTutor.getCorreo()) == 0) {
      this.TutoresAgregadosAlumno.push(
        new Tutor(this.nuevoTutor.getIdentidad(), this.nuevoTutor.getNombre(), this.nuevoTutor.getApellido(), this.nuevoTutor.getParentesco(), this.nuevoTutor.getSexo(), this.nuevoTutor.getCelular(), this.nuevoTutor.getTelefonoTrabajo(), this.nuevoTutor.getLugarTrabajo(), this.nuevoTutor.getCorreo(),this.nuevoTutor.getOcupacion())
      );
     // console.log(this.TutoresAgregadosAlumno);
      $('#exampleModalLong').modal('hide');
    }
  }

  limpiarDatosTutor() {
    this.Validaciones.removerValidacionesTutor();
  }

  eliminarTutoresAlumnos(key: any) {
    this.TutoresAgregadosAlumno.splice(key, 1);
  }

  crearNuevoAlumno() {
    this.nuevoAlumno.setArrayTutores(this.TutoresAgregadosAlumno);
    this.nuevoAlumno.setImagenPerfil("");

    //console.log(JSON.stringify(this.nuevoAlumno));
    //console.log(this.nuevoAlumno);
    if (this.nuevoAlumno.getArrayTutores().length > 0) {
      this.matriculaResgistroService.agregarAlumno(JSON.stringify(this.nuevoAlumno)).subscribe(
        (data) => {
          if(data.mensaje==1){
            this.goToAlumnos();
          }
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      $('#modal-exito_alumno .modal-body').html('<p>No hay Tutores agregados par el Alumno</p>');
      $('#modal-exito_alumno .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
      $('#modal-exito_alumno').modal('show');
    }
  }

  estado:number;

  actualizaAlumno() {
    this.estado=0;
    this.nuevoAlumno.setArrayTutores(this.TutoresAgregadosAlumno);
    //console.log(this.nuevoAlumno);
    if (this.nuevoAlumno.getArrayTutores().length > 0) {
      this.matriculaResgistroService.actualizarAlumno(JSON.stringify(this.nuevoAlumno)).subscribe(
        (data) => {
          if(data.mensaje==1){
            this.goToAlumnos();
          }
        },
        (error) => {
          console.log(error);
        }
      );
    } else {
      $('#modal-exito_alumno .modal-body').html('<p>No hay Tutores agregados par el Alumno</p>');
      $('#modal-exito_alumno .modal-footer').html('<button type="button" class="btn btn-secondary" data-dismiss="modal">Volver</button>');
      $('#modal-exito_alumno').modal('show');
    }
  }



  goToAlumnos() {
    this.router.navigate(['/home/matriculaRegistro/matriReg-expediente']);
  }


  presentarBotonesAccion(actionFlag) {
    //presentar los botones a utilizar segun la acción requerida
    switch (actionFlag) {
      case 1: {
        //console.log(this.objAlumno);
        //consultar la info del alumno 
        this.getAlumnos(this.objAlumno.getCodAlumno());
        this.getTutoresDeAlumno(this.objAlumno.getCodAlumno());
        //console.log(this.objAlumno);
        $('#btn_guardar_Alumno').hide();
        $('#btn_actualizar_Alumno').hide(); 
        break;
      }
      case 2: {
       // console.log('presentar botones de guardar');
        $('#btn_guardar_Alumno').hide();
        $('#btn_actualizar_Alumno').hide();
        $('#btn-Incidencias').hide();
        $('#btn-Matricula').hide();
        $('#btn-Historial').hide();
        this.mensaje = "Fomulario de ingreso de Alumno";
      }
      default:
        break;
    }
  }


  /* SERVICIOS */
  getAlumnos(codigoAlumno) {
    this.matriculaResgistroService.mostrarAlumnos({ codAlumno: codigoAlumno }).subscribe(
      (alumnoData) => {
        this.objAlumno.setNumExpediente(alumnoData[0].numExpediente);
        this.objAlumno.setIndiceGeneral(alumnoData[0].indiceGeneral);
        this.objAlumno.setTraslado(alumnoData[0].traslado);
        this.objAlumno.setFechaCreacion(alumnoData[0].fechaCreacion);
        this.objAlumno.setFechaModificacion(alumnoData[0].fechaModificacion);
        this.objAlumno.setCodPersona(alumnoData[0].codPersona);
        this.objAlumno.setCodAlumno(alumnoData[0].codAlumno);
        this.objAlumno.setNombre(alumnoData[0].nombreAlumno);
        this.objAlumno.setApellido(alumnoData[0].apellidoAlumno);
        this.objAlumno.setIdentidad(alumnoData[0].identificacion);
        this.objAlumno.setEdad(alumnoData[0].edadAlumno);
        this.objAlumno.setSexo(alumnoData[0].sexoAlumno);
        this.objAlumno.setNacionalidad(alumnoData[0].nacionalidad);
        this.objAlumno.setPaisOrigen(alumnoData[0].paisOrigen);
        this.objAlumno.setCelular(alumnoData[0].celular);
        this.objAlumno.setCondicionesEspeciales(alumnoData[0].condicionesEspeciales);
        this.objAlumno.setCorreo(alumnoData[0].correoElectronico);
        this.objAlumno.setDepartamentoNacimiento(alumnoData[0].departamentoNacimiento);
        this.objAlumno.setDireccion(alumnoData[0].direccionActual);
        this.objAlumno.setEnfermedadesComunes(alumnoData[0].efermedadesComunes);
        this.objAlumno.setFechaNacimiento(alumnoData[0].fechaNacimiento);
        this.objAlumno.setLugarNacimiento(alumnoData[0].lugarNacimiento);
        this.objAlumno.setTelefono(alumnoData[0].telefonoFijo);
        this.objAlumno.setTipoSangre(alumnoData[0].tipoSangre);
        this.objAlumno.setAspirante(alumnoData[0].aspirante);
        this.objAlumno.setImagenPerfil("");
        this.TutoresAgregadosAlumno = alumnoData[0].arrayTutores;
      },
      (err) => console.log(err)
    );
  }

  getTutoresDeAlumno(codAlumno) {
    this.matriculaResgistroService.mostrarTutores({codTutor:0,codAlumno:codAlumno}).subscribe(
      (data) => {
       // console.log(data);
        this.TutoresAgregadosAlumno=data;

      },
      (err) => console.log(err)

    )
  }


  /*     */
  presentarData() {
    //enlazar los campos entrantes con ngmodel
    //console.log('Data del Alumno', this.data);
    //console.log(this.objAlumno);
  }

  goToNuevoTraslado() {
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.router.navigate(['/home/matriculaRegistro/formulario-traslado']);
  }

  goToHistorial() {
   // console.log('Enviando este parametro: ', this.objAlumno);
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.router.navigate(['/home/matriculaRegistro/matriReg-historial']);
  }

  goToNuevoIncidencia() {
    //console.log('Enviando este parametro: ',this.objAlumno);
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.router.navigate(['/home/matriculaRegistro/formulario-incidencia']);
  }

  goToMatricula() {
    //console.log('Enviando este parametro: ',this.objAlumno);
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.router.navigate(['/home/matriculaRegistro/matriReg-matricula']);
  }

  tabActual: number = 0;
  goToNewsTabs() {
    if (this.tabActual == 0) {
      if (this.Validaciones.validar_datosPersonalesDocente(this.objAlumno.getNombre(), this.objAlumno.getApellido(), this.objAlumno.getIdentidad(), this.objAlumno.getEdad(), this.objAlumno.getSexo(), this.objAlumno.getFechaNacimiento(), this.objAlumno.getPaisOrigen(), this.objAlumno.getNacionalidad(), this.objAlumno.getDepartamentoNacimiento(), this.objAlumno.getLugarNacimiento()) == 0) {
        $('#datos1').removeClass('active');
        $('#datos2').addClass('active');
        $('#datosPersonales').removeClass('active');
        $('#datosContacto').addClass('active');
        this.tabActual += 1;
      }
    } else
      if (this.tabActual == 1) {
        if (this.Validaciones.validar_Contacto(this.objAlumno.getCorreo(), this.objAlumno.getCelular(), this.objAlumno.getTelefono(), this.objAlumno.getDireccion()) == 0) {
          $('#datos2').removeClass('active');
          $('#datos3').addClass('active');
          $('#datosContacto').removeClass('active');
          $('#fichaMedica').addClass('active');
          this.tabActual += 1;
        }
      } else
        if (this.tabActual == 2) {
          if (this.Validaciones.validar_FichaMedica(this.objAlumno.getTipoSangre()) == 0) {
            $('#datos3').removeClass('active');
            $('#datos4').addClass('active');
            $('#fichaMedica').removeClass('active');
            $('#datosTutor').addClass('active');
            if (this.actionFlag == 1) {
              $('#btn_guardar_Alumno').hide();
              $('#btn_continuar_Alumno').hide();
              $('#btn_actualizar_Alumno').show();
            } else {
              $('#btn_guardar_Alumno').show();
              $('#btn_actualizar_Alumno').hide();
              $('#btn_continuar_Alumno').hide();
            }
          }
        }
  }


}


