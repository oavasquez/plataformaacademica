import { Component, NgZone, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { MatriculaRegistroService } from '../../matriculaRegistro.service';
import { Alumnos } from '../../../../shared/model/alumnos.model';

@Component({
  selector: 'app-historial',
  templateUrl: './historial.component.html',
  styleUrls: ['./historial.component.css']
})
export class HistorialComponent implements OnInit {

  @Input() inObjAlumno: Alumnos;

  public asignaturasJsonArray: any[] = [];

  dtTrigger: Subject<any> = new Subject();
  dtHistorialOpt: DataTables.Settings = {};


  public objAlumno = new Alumnos;
  public nameTag = "";

  constructor(
    private router: Router,
    private MRService: MatriculaRegistroService
  ) { }

  ngOnInit() {

    //console.log('Este es el objeto que recibo',this.inObjAlumno);
    if (this.inObjAlumno == undefined) {
      //No se cargara la tabla
      this.getHistorialSecciones(0);
    } else {
      //secargara la tabla con el codigo del alumno
      this.objAlumno = this.inObjAlumno;
      this.nameTag = this.objAlumno.getNombre() + ' ' + this.objAlumno.getApellido();
      this.getHistorialSecciones(this.inObjAlumno.getCodAlumno());
    }
    this.iniciarTablaHistorial();
  }

  iniciarTablaHistorial() {
    this.dtHistorialOpt = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      }
    };
  }


  getHistorialSecciones(codigo) {
    this.MRService.mostrarHistorial({ codAlumno: codigo }).subscribe(
      (historial) => {
        //console.log('historial', historial);
        this.asignaturasJsonArray = historial;
        this.dtTrigger.next();
      },
      (err) => console.log(err)
    );
  }

}
