import { async, ComponentFixture, TestBed } from '@angular/core/testing';

import { MatriRegDashboardComponent } from './matri-reg-dashboard.component';

describe('MatriRegDashboardComponent', () => {
  let component: MatriRegDashboardComponent;
  let fixture: ComponentFixture<MatriRegDashboardComponent>;

  beforeEach(async(() => {
    TestBed.configureTestingModule({
      declarations: [ MatriRegDashboardComponent ]
    })
    .compileComponents();
  }));

  beforeEach(() => {
    fixture = TestBed.createComponent(MatriRegDashboardComponent);
    component = fixture.componentInstance;
    fixture.detectChanges();
  });

  it('should create', () => {
    expect(component).toBeTruthy();
  });
});
