import { Component, OnInit } from '@angular/core';
import { MatriculaRegistroService } from '../../matriculaRegistro.service';

@Component({
  selector: 'app-matri-reg-dashboard',
  templateUrl: './matri-reg-dashboard.component.html',
  styleUrls: ['./matri-reg-dashboard.component.css']
})
export class MatriRegDashboardComponent implements OnInit {
  public showCards = false;
  public cantAlumno = {};
  public cantAlumnoGenero = {};

  constructor(public matriculaRegistroService: MatriculaRegistroService) { }

  ngOnInit() {
    this.cargarTarjetas();
  }


  cargarTarjetas() {
    //cantidad de alumnos Matriculados
    this.matriculaRegistroService.mostrarDashboard('{"codCard":1}').subscribe(
      (data) => {
        //console.log(data);
        this.cantAlumno = data[0];
        this.cantAlumno == {} ? this.showCards = false : this.showCards = true;
      },
      (err) => { console.log(err) }
    );
    //cantidad de alumno matriculados por genero
    this.matriculaRegistroService.mostrarDashboard('{"codCard":2}').subscribe(
      (data) => {
        //console.log(data);
          this.cantAlumnoGenero=data[0];
          this.cantAlumnoGenero=={}? this.showCards=false:this.showCards=true;
      },
      (err) => { console.log(err)}
    );

  }

}
