import { Component, OnInit, Input } from '@angular/core';
import { Router } from '@angular/router';
import { MatriculaRegistroService } from '../../../../matriculaRegistro.service';
import { AdministrarService } from '../../../../../administracion/administracion.service';
import { GestionAcademicaService } from '../../../../../gestion-academica/gestion-academica.service';
import { Alumnos } from '../../../../../../shared/model/alumnos.model';
import { Asignatura } from '../../../../../../shared/model/asignatura.model';
import { Curso } from '../../../../../../shared/model/curso.model';
import { Jornada } from '../../../../../../shared/model/jornada.model';
import { Seccion } from '../../../../../../shared/model/seccion.model';

@Component({
  selector: 'app-formulario-matricula',
  templateUrl: './formulario-matricula.component.html',
  styleUrls: ['./formulario-matricula.component.css']
})
export class FormularioMatriculaComponent implements OnInit {

  @Input() objAlumno:Alumnos; 

  constructor(private router: Router, 
              private matriculaResgistroService: MatriculaRegistroService,
              private administrarservice:AdministrarService,
              private gestionAcademicaService:GestionAcademicaService) { }

  ngOnInit() {
    //cargar los cursos del estudiante para el selec cursos
    //cargar jornadas en el select de jornadas
    //cargar asignaturas de curso seleccionado
    //cargar secciones con lo antes especificados 
    this.recargarJson();
  }

  goToMatricula(){
    this.router.navigate(['/home/matriculaRegistro/matriReg-matricula']);
  }

  //*******************jSON para autocompletacion**********************/
  public keyboard: any;
  public jsonNombreCurso: any;
  public jsonNombreAsignatura: any;
  public jsonSecciones:any
  public buscarCurso = new Curso;
  public buscarAsignatura = new Asignatura;
  /*******************funciones para recargar json*********************/
  recargarJson(){
    //agregando nombre de los cursos al json
    this.buscarCurso.setCodigoCurso(0);
    this.administrarservice.mostrarCursos(JSON.stringify(this.buscarCurso)).subscribe(
      (data) => {
        this.jsonNombreCurso = data;

      },  //error 
      (err) => console.log(err)
    );
  }

  public codCurso:any;
  public codAsiganatura:any;
  
  Obtener_Asignaturas(Curso){
    this.codCurso=Curso.codCurso;
    this.administrarservice.mostrarAsignatura({ codCurso: this.codCurso, habilitado: 0 }).subscribe(
      (data) => {
        this.jsonNombreAsignatura = data;

      },  //error 
      (err) => console.log(err)
    );
  }

  Obtener_Secciones(Asignatura){
    this.codAsiganatura=Asignatura.codAsignatura;
    this.matriculaResgistroService.mostrarSeccionesDisponibles({codAsignatura:this.codAsiganatura,codCurso:this.codCurso}).subscribe(
      (data) => {
        
        this.jsonSecciones = data;

      },  //error 
      (err) => console.log(err)
    );
  }

  matricular_Seccion(codSeccion){
    this.matriculaResgistroService.agregarMatricula({ codAlumno: this.objAlumno.getCodAlumno(), codSeccion: codSeccion}).subscribe(
      (data)=>{
        this.goToMatricula();
      },
      (error)=>{
        console.log(error);
      }
    );
  }
}
