
import { Component, NgZone, OnInit, Input, Output, EventEmitter } from '@angular/core';
import { Subject } from 'rxjs/Subject';
import { Router } from '@angular/router';
import { Alumnos } from '../../../../shared/model/alumnos.model';
import { MatriculaRegistroService } from '../../matriculaRegistro.service';

@Component({
  selector: 'app-matricula',
  templateUrl: './matricula.component.html',
  styleUrls: ['./matricula.component.css']
})
export class MatriculaComponent implements OnInit {

  //variables de API de componente 
  @Output() outSetParametroAlumno = new EventEmitter();
  @Output() outSetActionForm = new EventEmitter();
  @Output() outTabsActiva = new EventEmitter();
  @Input() inObjAlumno: Alumnos;

  dtTrigger: Subject<any> = new Subject();
  dtAlumnosOpt: DataTables.Settings = {};

  public matriculaJson: any[] = [];
  objAlumno = new Alumnos();
  public nameTag: string;

  constructor(private router: Router, private MRService: MatriculaRegistroService) { }

  ngOnInit() {
    this.iniciarTablaAlumno();
    //console.log('Este es el objeto que recibo',this.inObjAlumno);
    if (this.inObjAlumno == undefined) {
      //No se cargara la tabla
    } else {
      //secargara la tabla con el codigo del alumno
      this.objAlumno = this.inObjAlumno;
      this.nameTag = this.objAlumno.getNombre() + ' ' + this.objAlumno.getApellido();
      //console.log(this.inObjAlumno.getCodAlumno());
      this.getMatriculaAlumno(this.inObjAlumno.getCodAlumno());
    }
  }

  /* consultas de servicios*/

  getMatriculaAlumno(codigo) {
    this.MRService.mostrarMatriculas({ codAlumno: codigo }).subscribe(
      (matricula) => {
       // console.log('matricula', matricula);

        this.matriculaJson = matricula;
        this.dtTrigger.next();
      },
      (err) => console.log(err)
    );
  }

  goToForm() {

    //enviar dato seleccionado al maincontainer para que se actualice en el formulario Alumno

    this.router.navigate(['/home/matriculaRegistro/formulario-expediente']);
  }


  actualizarAlumno(parametro) {
    //activar el formulario de nuevo Alumno:
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.outSetActionForm.emit(parametro);
    this.router.navigate(['home/matriculaRegistro/formulario-matricula']);
  }

  actualizarTraslado(parametro) {
    //activar el formulario de nuevo Alumno:
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.outSetActionForm.emit(parametro);
    this.router.navigate(['/home/matriculaRegistro/formulario-expediente', 1]);
  }
  actualizarTutor(parametro) {
    //activar el formulario de nuevo Alumno:
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.outSetActionForm.emit(parametro);
    this.router.navigate(['/home/matriculaRegistro/matriReg-ficha', 2]);
  }
  actualizarIncidencia(parametro) {
    //activar el formulario de nuevo Alumno:
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.outSetActionForm.emit(parametro);
    this.router.navigate(['/home/matriculaRegistro/formulario-expediente', 3]);
  }

  ingresarNuevoMatricula(parametro) {
    this.outSetParametroAlumno.emit(this.objAlumno);
    this.outSetActionForm.emit(parametro);
    this.router.navigate(['/home/matriculaRegistro/formulario-matricula']);
  }


  iniciarTablaAlumno() {
    //opciones para datatable
    this.dtAlumnosOpt = {
      "scrollY": "320px",
      "scrollCollapse": true,
      "paging": false,
      "info": false,
      "language": {
        "lengthMenu": "Mostrar _MENU_ filas por pagina",
        "zeroRecords": "No hay datos para mostrar",
        "info": "Showing page _PAGE_ of _PAGES_",
        "infoEmpty": "No hay datos disponibles",
        "infoFiltered": "(filtered from _MAX_ total records)"
      },
      rowCallback: (row: Node, data: any[] | Object, index: number) => {
        const self = this;
        $('td', row).off('click');
        $('td', row).on('click', () => {
          self.getFilaAlumnoSel(data);
        });
        return row;
      }
    };


  }

  getFilaAlumnoSel(info: any): void {
    //console.log(info);
  }

  setCheckStatus(valor) {
    if (valor == 1) {
      return "Activo";
    } else {
      return "Innactivo";
    }
  }

  habilitado(valor) {
    if (valor == 1) {
      return true;
    } else {
      return false;
    }
  }

  deshabilitado(valor) {
    if (valor == 0) {
      return true;
    } else {
      return false;
    }
  }


}
