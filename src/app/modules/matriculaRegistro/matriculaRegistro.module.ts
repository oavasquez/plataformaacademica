import { NgModule } from "@angular/core";
import { CommonModule } from '@angular/common';
import { HttpClientModule } from '@angular/common/http';
import { FormsModule } from '@angular/forms';
import { HttpModule } from '@angular/http';


//service
import { MatriculaRegistroService } from './matriculaRegistro.service';
//routing
import { MatriculaRegistroRoutesModule } from './matriculaRegistro.routes.module';
//components
import { ExpedienteComponent } from './components/expediente/expediente.component';
import { FichaComponent } from './components/ficha/ficha.component';
import { MatriculaComponent } from './components/matricula/matricula.component';
import { MatriRegDashboardComponent } from './components/matri-reg-dashboard/matri-reg-dashboard.component';
import { HistorialComponent } from './components/historial/historial.component';

//componenete-formulario
import { FormularioFichaComponent } from './components/ficha/formularios/formulario-ficha/formulario-ficha.component';
import { FormularioExpedienteComponent } from './components/expediente/formularios/formulario-expediente/formulario-expediente.component';
import { FormularioMatriculaComponent } from './components/matricula/formulario/formulario-matricula/formulario-matricula.component';

import { FormularioTrasladoComponent } from './components/expediente/formularios/formulario-traslado/formulario-traslado.component';
import { FormularioIncidenciaComponent } from './components/expediente/formularios/formulario-incidencia/formulario-incidencia.component';

import { DataTablesModule } from 'angular-datatables';
//ui auto-complete
import { NguiAutoCompleteModule } from '@ngui/auto-complete';


//gestionAcademica
@NgModule({
    imports: [
        CommonModule,
        HttpClientModule,
        FormsModule,
        HttpModule,
        MatriculaRegistroRoutesModule,
        DataTablesModule,
        NguiAutoCompleteModule


    ],
    declarations: [
        ExpedienteComponent,
        MatriRegDashboardComponent,
        FichaComponent,
        MatriculaComponent,
        FormularioFichaComponent,
        FormularioExpedienteComponent,
        FormularioMatriculaComponent,
        FormularioTrasladoComponent,
        FormularioIncidenciaComponent,
        HistorialComponent
    ],
    providers: [
        MatriculaRegistroService,

    ],
    exports: [
        ExpedienteComponent,
        MatriRegDashboardComponent,
        FichaComponent,
        MatriculaComponent,
        FormularioFichaComponent,
        FormularioExpedienteComponent,
        FormularioMatriculaComponent,
        FormularioTrasladoComponent,
        FormularioIncidenciaComponent,
        HistorialComponent
    ],
    bootstrap: []
})

export class MatriculaRegistroModule { }