import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Observable } from 'rxjs/Observable';
import { of } from 'rxjs/observable/of';


//interface
import { Mensaje } from '../../shared/model/mensaje.model';


@Injectable()
export class MatriculaRegistroService {

    headers = new HttpHeaders()
        .set('Content-Type', 'application/json');

    constructor(private http: HttpClient) { }

    //""""""""""""""""""""""""""""""""""""""Alumnos"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarAlumno(alumno): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/crearAlumno',
            alumno,
            { headers: this.headers });
    }

    actualizarAlumno(alumno): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/actualizarAlumno',
            alumno,
            { headers: this.headers });
    }


    //mostrar todas las Alumnos existentes
    mostrarAlumnos(alumno): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/mostrarAlumnos',
            alumno,
            { headers: this.headers });
    }

    //mostrar todas las Alumnos existentes
    mostrarAlumnoResumen(alumno): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/mostrarAlumnoResumen',
            alumno,
            { headers: this.headers });
    }



    //""""""""""""""""""""""""""""""""""""""Tutor"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarTutor(tutor): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/crearTutor',
            tutor,
            { headers: this.headers });
    }

    actualizarTutor(tutor): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/actualizarTutor',
            tutor,
            { headers: this.headers });
    }


    //mostrar todas las Tutores existentes
    mostrarTutores(tutor): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/mostrarTutores',
            tutor,
            { headers: this.headers });
    }


    //""""""""""""""""""""""""""""""""""""""Matricula"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarMatricula(matricula): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/crearMatricula',
            matricula,
            { headers: this.headers });
    }

    actualizarMatricula(matricula): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/actualizarMatricula',
            matricula,
            { headers: this.headers });
    }


    //mostrar todas las Matriculas existentes
    mostrarMatriculas(matricula): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/mostrarMatriculas',
            matricula,
            { headers: this.headers });
    }


    //""""""""""""""""""""""""""""""""""""""TipoIncidencia"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarTipoIncidencia(tipoIncidencia): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/crearTipoIncidencia',
            tipoIncidencia,
            { headers: this.headers });
    }

    actualizarTipoIncidencia(tipoIncidencia): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/actualizarTipoIncidencia',
            tipoIncidencia,
            { headers: this.headers });
    }


    //mostrar todas las TipoIncidencias existentes
    mostrarTipoIncidencias(tipoIncidencia): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/mostrarTipoIncidencia',
            tipoIncidencia,
            { headers: this.headers });
    }

    //""""""""""""""""""""""""""""""""""""""Incidencia"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarIncidencia(incidencia): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/crearIncidencia',
            incidencia,
            { headers: this.headers });
    }

    actualizarIncidencia(incidencia): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/actualizarIncidencia',
            incidencia,
            { headers: this.headers });
    }


    //mostrar todas las Incidencias existentes
    mostrarIncidencias(incidencia): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/mostrarIncidentes',
            incidencia,
            { headers: this.headers });
    }
    /**********************************************historial**************************************************************/
    //mostrar todas las Incidencias existentes
    mostrarHistorial(historial): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/mostrarHistorial',
            historial,
            { headers: this.headers });
    }

    //""""""""""""""""""""""""""""""""""""""Traslado"""""""""""""""""""""""""""""""""""""""""""""""""""""""""

    agregarTraslado(traslado): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/crearTraslado',
            traslado,
            { headers: this.headers });
    }

    actualizarTraslado(traslado): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/actualizarTraslado',
            traslado,
            { headers: this.headers });
    }


    //mostrar todas las Traslados existentes
    mostrarTraslados(traslado): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/mostrarTraslados',
            traslado,
            { headers: this.headers });
    }

    ////////////*************************************secciones*****************************************/

      //mostrar todas las Traslados existentes
      mostrarSeccionesDisponibles(secciones): Observable<any> {
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/mostrarSeccionesDisponibles',
            secciones,
            { headers: this.headers });
    }






    //********************************************************************************************************************/

    //servicios para obtener datos para mostrara en las tajetas de la seccion de dashboard
    mostrarDashboard(dashboard): Observable<any> {
        //console.log("consiguiendodatos");
        const parametros = {}
        return this.http.post<any>('https://appacademica.herokuapp.com/matricularegistro/mostrarDashboardMatriculaRegistro',
            dashboard,
            { headers: this.headers });

    }

}
