import { BrowserModule } from '@angular/platform-browser';
import { NgModule } from '@angular/core';
import { HttpClientModule } from '@angular/common/http';

//components
import { AppComponent } from './app.component';

//modules
import { LoginModule } from './modules/login/login.module';
import { MainModule } from './modules/main/main.module';
import { AdministracionModule } from './modules/administracion/administracion.module';
import { EvaluacionPromocionModule } from './modules/evaluacionPromocion/evaluacionPromocion.module';
import { MatriculaRegistroModule } from './modules/matriculaRegistro/matriculaRegistro.module';
import { GestionAcademicaModule } from './modules/gestion-academica/gestion-academica.module';


//componenet
import { LandingTopNavBarComponent } from './modules/landing-page/components/landing-top-nav-bar/landing-top-nav-bar.component';
import { LandingBodyComponent } from './modules/landing-page/components/landing-body/landing-body.component';
import { AuthGuardService } from './auth/auth-guard.service';

//router.module 
import { MainRoutesModule } from './modules/main/main.routes.module';
import { AppRoutingModule } from './app-routing.module';
import { ClickIconDirective } from './shared/directives/click-icon.directive';




@NgModule({
  declarations: [
    AppComponent,
    LandingTopNavBarComponent,
    LandingBodyComponent,
    ClickIconDirective,

  ],
  imports: [
    BrowserModule,
    HttpClientModule,
    AppRoutingModule,
    LoginModule,
    MainModule,
    AdministracionModule,
    MainRoutesModule,
    EvaluacionPromocionModule
  ],
  providers: [AuthGuardService],
  bootstrap: [AppComponent]
})
export class AppModule { }
