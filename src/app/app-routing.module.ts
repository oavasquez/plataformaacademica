import { NgModule } from '@angular/core';
import { RouterModule, Routes, CanActivate } from '@angular/router';


import { LoginComponent } from './modules/login/components/login/login.component'
import { SesionIniciadaComponent } from './modules/login/components/home/sesion-iniciada.component'
import { LandingBodyComponent } from './modules/landing-page/components/landing-body/landing-body.component';
import { AuthGuardService as AuthGuard } from './auth/auth-guard.service';
import { MainSiteContainerComponent } from './modules/main/components/main-site-container/main-site-container.component';

import { AdminCursosComponent } from './modules/administracion/components/admin-cursos/admin-cursos.component';

import { MainRoutesModule } from './modules/main/main.routes.module';

const routes: Routes = [
    { path: '', redirectTo: 'home', pathMatch: 'full' },
    //{ path: '', canActivate: [AuthGuard], component: MainSiteContainerComponent },
    { path: 'login', component: LoginComponent },
    { path: 'salir', canActivate: [AuthGuard], component: SesionIniciadaComponent },
    { path: 'home', loadChildren: 'app/modules/main/main.module#MainModule' }
];
@NgModule({
    imports: [RouterModule.forRoot(routes)],
    exports: [RouterModule]
})


export class AppRoutingModule { }





