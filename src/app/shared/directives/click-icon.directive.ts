import { Directive, ElementRef, HostListener, HostBinding, Input, Output, EventEmitter } from '@angular/core';

@Directive({
  selector: '[appClickIcon]'
})
export class ClickIconDirective {

  constructor(public element: ElementRef) {
    element.nativeElement.style.backgroundColor = 'yellow';
  }

  @Input('selectableClass')
  toggleClass = 'active';



  @HostListener('document:click', ['$event'])
  onClick(e) {
    /*if (this.element.nativeElement.contains(event.target)) {
      this.highlight('red');
    } else {
      this.highlight(null);
    }*/
    e.target.classList.add('active');
  }

  private highlight(color: string) {
    this.element.nativeElement.style.backgroundColor = color;
  }

}
