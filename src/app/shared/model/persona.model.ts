 
export class Persona {

    constructor(
        public codPersona?: number,
        public nombre?: string,
        public apellido?: string,
        public identidad?: string,
        public fechaNacimiento?: string,
        public telefono?: string,
        public celular?: string,
        public correo?: string,
        public direccion?: string,
        public edad?: number,
        public paisOrigen?: string,
        public departamentoNacimiento?: string,
        public lugarNacimiento?: string,
        public nacionalidad?: string,
        public imagenPerfil?: string,
        public sexo?: string,
        public tipoSangre?: string,
        public enfermedadesComunes?: string,
        public condicionesEspeciales?: String,
        public aspirante?: number
    ) { }


    //********************set******************************

    setCodPersona(codPersona: number) {
        this.codPersona = codPersona;
    }
    setNombre(nombre: string) {
        this.nombre = nombre;
    }
    setApellido(apellido: string) {
        this.apellido = apellido;
    }
    setIdentidad(identidad: string) {
        this.identidad = identidad;
    }
    setFechaNacimiento(fechaNacimiento: string) {
        this.fechaNacimiento = fechaNacimiento;
    }
    setTelefono(telefono: string) {
        this.telefono = telefono;
    }
    setCelular(celular: string) {
        this.celular = celular;
    }
    setCorreo(correo: string) {
        this.correo = correo;
    }
    setDireccion(direccion: string) {
        this.direccion = direccion;
    }
    setEdad(edad: number) {
        this.edad = edad;
    }
    setPaisOrigen(paisOrigen: string) {
        this.paisOrigen = paisOrigen;
    }
    setDepartamentoNacimiento(departamentoNacimiento: string) {
        this.departamentoNacimiento = departamentoNacimiento;
    }
    setLugarNacimiento(lugarNacimiento: string) {
        this.lugarNacimiento = lugarNacimiento;
    }
    setNacionalidad(nacionalidad: string) {
        this.nacionalidad = nacionalidad;
    }
    setImagenPerfil(imagenPerfil: string) {
        this.imagenPerfil = imagenPerfil;
    }
    setSexo(sexo: string) {
        this.sexo = sexo;
    }
    setTipoSangre(tipoSangre: string) {
        this.tipoSangre = tipoSangre;
    }
    setEnfermedadesComunes(enfermedadesComunes: string) {
        this.enfermedadesComunes = enfermedadesComunes;
    }
    setCondicionesEspeciales(condicionesEspeciales: string) {
        this.condicionesEspeciales = condicionesEspeciales;
    }
    setAspirante(aspirante: number) {
        this.aspirante = aspirante;
    }


    //**********get******************************
    getCodPersona() {
        return this.codPersona;
    }
    getNombre() {
        return this.nombre;
    }
    getApellido() {
        return this.apellido;
    }
    getIdentidad() {
        return this.identidad;
    }
    getFechaNacimiento() {
        return this.fechaNacimiento;
    }
    getTelefono() {
        return this.telefono;
    }
    getCelular() {
        return this.celular;
    }
    getCorreo() {
        return this.correo;
    }
    getDireccion() {
        return this.direccion;
    }
    getEdad() {
        return this.edad;
    }
    getPaisOrigen() {
        return this.paisOrigen;
    }
    getDepartamentoNacimiento() {
        return this.departamentoNacimiento;
    }
    getLugarNacimiento() {
        return this.lugarNacimiento;
    }
    getNacionalidad() {
        return this.nacionalidad;
    }
    getImagenPerfil() {
        return this.imagenPerfil;
    }
    getSexo() {
        return this.sexo;
    }
    getTipoSangre() {
        return this.tipoSangre;
    }
    getEnfermedadesComunes() {
        return this.enfermedadesComunes;
    }
    getCondicionesEspeciales() {
        return this.condicionesEspeciales;
    }
    getAspirante() {
        return this.aspirante;
    }
}
