import { Alumnos } from './alumnos.model';
export class Asignatura extends Alumnos {


    constructor(
        private codAsignatura?: String,
        private asignatura?: String,
        private dias?: String,
        private estadoAsignatura?: number,
        private codCurso?: number,
        private nuevoCodCurso?: number,
    ) {
        super();

    }


    Construir(in_codAsignatura,
        in_nombreAsignatura,
        in_codCurso,
        in_diasAsignatura,
        in_estadoAsignatura) {

        this.codAsignatura = in_codAsignatura;
        this.asignatura = in_nombreAsignatura;
        this.codCurso = in_codCurso;
        this.dias = in_diasAsignatura;
        this.estadoAsignatura = in_estadoAsignatura;

    }

    getAsignaturaJsonFormat() {
        return JSON.stringify(this);
    }

    setCodAsignatura(codigoAsignatura: string) {
        this.codAsignatura = codigoAsignatura;
    }

    setNombreAsignatura(asignatura: string) {
        this.asignatura = asignatura;
    }

    setCodCurso(curso: number) {
        this.codCurso = curso;
    }

    setDiasAsignatura(dias: String) {
        this.dias = dias;
    }

    setEstadoAsignatura(estado: number) {
        this.estadoAsignatura = estado;
    }
    setNuevoCodCurso(curso: number) {
        this.nuevoCodCurso = curso;
    }

    getCodAsignatura() {
        return this.codAsignatura;
    }

    getNombreAsignatura() {
        return this.asignatura;
    }

    getCodCurso() {
        return this.codCurso;
    }

    getDiasAsignatura() {
        return this.dias;
    }

    getEstadoAsignatura() {
        return this.estadoAsignatura;
    }
    getNuevoCodCurso() {
        return this.nuevoCodCurso;
    }



}