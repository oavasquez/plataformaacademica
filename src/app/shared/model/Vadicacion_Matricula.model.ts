import * as $ from 'jquery';
import * as jQuery from 'jquery';

export class Validaciones_Matricula {

    //-------------------------------Validaciones para Alumnos-----------------------------------------//
    
    //-------Validaciones para Datos Personales--------------//
    datos_personales=0;

    validar_datosPersonalesDocente(nombre,apellidos,ide,edad,genero,fechaNac,pais,nacionalidad,departamento,lugar){
       this.datos_personales=0;
       this.datos_personales+=this.campos_vacios(nombre,'#txt-nombreAlumno');
       this.datos_personales+=this.campos_vacios(apellidos,'#txt-apellidoAlumno');
       this.datos_personales+=this.validarIdentidadHND(ide,'#txt-identidadAlumno');
       this.datos_personales+=this.numeros(edad,'#txtEdadAlumno');
       this.datos_personales+=this.campos_vacios(genero,'#select_generoAlumno');
       this.datos_personales+=this.campos_vacios(fechaNac,'#txt-fechaNacimientoAlumno');
       this.datos_personales+=this.campos_vacios(pais,'#txt-paisOrigenAlumno');
       this.datos_personales+=this.campos_vacios(nacionalidad,'#txt-nacionalidadAlumno');
       this.datos_personales+=this.campos_vacios(departamento,'#txt-deptoOrigenAlumno');
       this.datos_personales+=this.campos_vacios(lugar,'#txt-lugarNacimientoAlumno');
       return this.datos_personales;
   }
   

   //-------Validaciones para Datos Contacto-------------//
   datos_contacto=0;

   validar_Contacto(correo,celular,telefono,direccion){
       this.datos_contacto=0;
       this.datos_contacto+=this.validarEmails(correo,'#txt-correoAlumno');
       this.datos_contacto+=this.validarPhone(celular,'#txt-celularAlumno');
       this.datos_contacto+=this.validarPhone(telefono,'#txt-telefonoAlumno');
       this.datos_contacto+=this.campos_vacios(direccion,'#txt-direccionAlumno');
       return this.datos_contacto;
   }
   
   
   //-------Validaciones para Datos Medicos--------------//
   datos_fichaMedica=0;

   validar_FichaMedica(tipo){
       this.datos_fichaMedica=0;
       this.datos_fichaMedica+=this.campos_vacios(tipo,'#select_tipoSangre');
       return this.datos_fichaMedica;
   }
  
   //-------Validaciones para Tutores--------------//
   datos_tutor=0;

   validar_Tutor(ide,nombre,apellido,parentesco,genero,celular,telefono,lugar,correo){
       this.datos_tutor=0;
       this.datos_tutor+=this.validarIdentidadHND(ide,'#txt-identificacionTutor');
       this.datos_tutor+=this.campos_vacios(nombre,'#txt-nombreTutor');
       this.datos_tutor+=this.campos_vacios(apellido,'#txt-apellidoTutor');
       this.datos_tutor+=this.campos_vacios(parentesco,'#txt-parentescoTutor');
       this.datos_tutor+=this.campos_vacios(genero,'#txt-sexoTutor');
       this.datos_tutor+=this.validarPhone(celular,'#txt-telefonoCelularTutor');
       this.datos_tutor+=this.validarPhone(telefono,'#txt-telefonoTrabajoTutor');
       this.datos_tutor+=this.campos_vacios(lugar,'#txt-lugarDeTrabajoTutor');
       this.datos_tutor+=this.validarEmails(correo,'#txt-correoTutor');
       return this.datos_tutor;
   }
   
   removerValidacionesTutor(){
       $('#txt-identificacionTutor').removeClass('is-invalid');
       $('#txt-identificacionTutor').removeClass('is-valid');
       $('#txt-identificacionTutor').val("");
       $('#txt-nombreTutor').removeClass('is-invalid');
       $('#txt-nombreTutor').removeClass('is-valid');
       $('#txt-nombreTutor').val("");
       $('#txt-apellidoTutor').removeClass('is-invalid');
       $('#txt-apellidoTutor').removeClass('is-valid');
       $('#txt-apellidoTutor').val("");
       $('#txt-parentescoTutor').removeClass('is-invalid');
       $('#txt-parentescoTutor').removeClass('is-valid');
       $('#txt-parentescoTutor').val();
       $('#txt-sexoTutor').removeClass('is-invalid');
       $('#txt-sexoTutor').removeClass('is-valid');
       $('#txt-sexoTutor').val("");
       $('#txt-telefonoCelularTutor').removeClass('is-invalid');
       $('#txt-telefonoCelularTutor').removeClass('is-valid');
       $('#txt-telefonoCelularTutor').val("");
       $('#txt-telefonoTrabajoTutor').removeClass('is-invalid');
       $('#txt-telefonoTrabajoTutor').removeClass('is-valid');
       $('#txt-telefonoTrabajoTutor').val("");
       $('#txt-lugarDeTrabajoTutor').removeClass('is-invalid');
       $('#txt-lugarDeTrabajoTutor').removeClass('is-valid');
       $('#txt-lugarDeTrabajoTutor').val("");
       $('#txt-correoTutor').removeClass('is-invalid');
       $('#txt-correoTutor').removeClass('is-valid');
       $('#txt-correoTutor').val("");
   }

   //-------------------------------Validaciones para Traslados-----------------------------------------//
   traslado=0;

   validar_traslado(institucion,egreso,lectivo,periodo,curso,jornada,asignatura,nota){
        this.traslado=0;
        this.traslado+=this.campos_vacios(institucion,'input[name=institucionTraslado]');
        this.traslado+=this.campos_vacios(egreso,'input[name=fechaEgresoTraslado]');
        this.traslado+=this.campos_vacios(lectivo,'#anioTraslado');
        this.traslado+=this.campos_vacios(periodo,'#periodoTraslado');
        this.traslado+=this.campos_vacios(curso,'#cursoTraslado');
        this.traslado+=this.campos_vacios(jornada,'#jornadaTraslado');
        this.traslado+=this.campos_vacios(asignatura,'#AsignaturaTraslado');
        this.traslado+=this.numeros(nota,'input[name=txt-calificacionFinal]');
       return this.traslado;
   }

   remover_validacionesTraslado(){
       $('input[name=institucionTraslado]').removeClass('is-valid');
       $('input[name=institucionTraslado]').val("");
       $('input[name=fechaEgresoTraslado]').removeClass('is-valid');
       $('input[name=fechaEgresoTraslado]').val("");
       $('#periodoTraslado').removeClass('is-valid');
       $('#periodoTraslado').val("");
       $('#cursoTraslado').removeClass('is-valid');
       $('#cursoTraslado').val("");
       $('#jornadaTraslado').removeClass('is-valid');
       $('#jornadaTraslado').val("");
       $('#AsignaturaTraslado').removeClass('is-valid');
       $('#AsignaturaTraslado').val("");
       $('#anioTraslado').removeClass('is-valid');
       $('#anioTraslado').val("");
       $('input[name=txt-calificacionFinal]').removeClass('is-valid');
       $('input[name=txt-calificacionFinal]').val("");
   }

   //-------------------------------Validaciones para Incidencias-----------------------------------------//
   incidencia=0;

   validar_Incidencia(titulo,fecha,tipo,descripcion){
        this.incidencia=0;
        this.incidencia+=this.campos_vacios(titulo,'#txt-tituloIncidencia');
        this.incidencia+=this.campos_vacios(fecha,'#txt-fechaIncidencia');
        this.incidencia+=this.campos_vacios(tipo,'#txt-TipoIncidencia');
        this.incidencia+=this.campos_vacios(descripcion,'#txt-descripcionIncidencia');
        return this.incidencia;
   }

   remover_validacionesIncidencia(){
       $('#txt-tituloIncidencia').removeClass('is-valid');
       $('#txt-tituloIncidencia').removeClass('is-invalid');
       $('#txt-fechaIncidencia').removeClass('is-valid');
       $('#txt-fechaIncidencia').removeClass('is-invalid');
       $('#txt-TipoIncidencia').removeClass('is-valid');
       $('#txt-TipoIncidencia').removeClass('is-invalid');
       $('#txt-descripcionIncidencia').removeClass('is-valid');
       $('#txt-descripcionIncidencia').removeClass('is-invalid');
   }

   campos_vacios(valor,id){
       if(valor=="" || valor==undefined){
           $(id).removeClass('is-valid');
           $(id).addClass('is-invalid');
           return 1;
       }else{
           $(id).removeClass('is-invalid');
           $(id).addClass('is-valid');
           return 0;
       }
   }

   numeros(valor,id){
       if(valor<=0 || valor==undefined){
           $(id).removeClass('is-valid');
           $(id).addClass('is-invalid');
           return 1;
       }else{
           $(id).removeClass('is-invalid');
           $(id).addClass('is-valid');
           return 0;
       }
   }


   validarIdentidadHND(valor,id){
       if(valor!=""){
           if(!/^\d{4}-\d{4}-\d{5}$/.test(String(valor))){
               $(id).removeClass('is-valid');
               $(id).addClass('is-invalid');
               return 1;
           }else{
               $(id).removeClass('is-invalid');
               $(id).addClass('is-valid');
               return 0;
           }
       }else{
           $(id).removeClass('is-valid');
           $(id).addClass('is-invalid');
           return 1;
       }	
   }

   validarPhone(valor,id){
       if(valor!=""){
           if(!/^\d{4}-\d{4}$/.test(String(valor))){
               $(id).removeClass('is-valid');
               $(id).addClass('is-invalid');
               return 1;
           }else{
               $(id).removeClass('is-invalid');
               $(id).addClass('is-valid');
               return 0;
           }
       }else{
           $(id).removeClass('is-valid');
           $(id).addClass('is-invalid');
           return 1;
       }	
   }
   
   validarEmails(valor,id){
       if(valor!=""){
           if(!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(String(valor))){
               $(id).removeClass('is-valid');
               $(id).addClass('is-invalid');
               return 1;
           }else{
               $(id).removeClass('is-invalid');
               $(id).addClass('is-valid');
               return 0;
           }
       }else{
           $(id).removeClass('is-valid');
           $(id).addClass('is-invalid');
           return 1;
       }
           
   }

}