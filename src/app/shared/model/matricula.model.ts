export class Matricula {

    private codSeccion: number;
    private codAlumno: string;
    private nombreAlumno: string;
    private fechaMatricula:string;
    private fechaModificaicon: string;
    private promedio: number;
    private na1:number;
    private ne1:number;
    private nf1:number;
    private na2: number;
    private ne2: number;
    private nf2: number;
    private na3: number;
    private ne3: number;
    private nf3: number;
    private na4: number;
    private ne4: number;
    private nf4: number;

    constructor( ) { }

    public construir(
        codSeccion,
        codAlumno,
        nombreAlumno,
        fechaMatricula,
        fechaModificaicon,
        promedio,
        na1,
        ne1,
        nf1,
        na2,
        ne2,
        nf2,
        na3,
        ne3,
        nf3,
        na4,
        ne4,
        nf4
    ){
        this.codSeccion = codSeccion;
        this.codAlumno = codAlumno;
        this.nombreAlumno = nombreAlumno;
        this.fechaMatricula = fechaMatricula;
        this.fechaModificaicon = fechaModificaicon;
        this.promedio = promedio;
        this.na1 = na1 ;
        this.ne1 = ne1;
        this.nf1 = nf1;
        this.na2 = na2;
        this.ne2 = ne2;
        this.nf2 = nf2;
        this.na3 = na3;
        this.ne3 = ne3;
        this.nf3 = nf3;
        this.na4 = na4;
        this.ne4 = ne4;
        this.nf4 = nf4;
    }

    public getNotaFinal4(){
        return this.nf4;
    }
    public setNotaFinal4(nf4) {
        this.nf4 = nf4;
    }
    public getNotaE4() {
        return this.ne4
    }
    public setNotaE4(ne4) {
        this.ne4 = ne4;
    }
    public getNotaA4() {
        return this.na4;
    }
    public setNotaA4(na4) {
        this.na4 = na4;
    }

    public getNotaFinal3(){
        return this.nf3;
    }
    public setNotaFinal3(nf3) {
        this.nf3 = nf3;
    }
    public getNotaE3() {
        return this.ne3
    }
    public setNotaE3(ne3) {
        this.ne3 = ne3;
    }
    public getNotaA3() {
        return this.na3;
    }
    public setNotaA3(na3) {
        this.na3 = na3;
    }

    public getNotaFinal2(){
        return this.nf2;
    }
    public setNotaFinal2(nf2) {
        this.nf2 = nf2;
    }
    public getNotaE2() {
        return this.ne2
    }
    public setNotaE2(ne2) {
        this.ne2 = ne2;
    }
    public getNotaA2() {
        return this.na2;
    }
    public setNotaA2(na2) {
        this.na2 = na2;
    }

    public getNotaFinal1(){
        return this.nf1;
    }

    public getNotaFinal1Sumanda(){
        return (this.na1+this.ne1);
    }

    public setNotaFinal1(nf1){
        this.nf1 = nf1;
    }
    public getNotaE1(){
        return this.ne1
    }
    public setNotaE1(ne1){
        this.ne1 = ne1;
    }
    public getNotaA1(){
        return this.na1;
    }
    public setNotaA1(na1){
        this.na1 = na1;
    }
    public getFechaModificacion(){
        return this.fechaModificaicon;
    }
    public setFechaModificacion(fecha){
        this.fechaModificaicon = fecha;
    }
    public getFechaMatricula(){
        return this.fechaMatricula;
    }
    public setFechaMatricula(fecha){
        this.fechaMatricula = fecha;
    }
    public getNombreAlumno(){
        return this.nombreAlumno;
    }
    public setNombreAlumno(nombre){
        this.nombreAlumno = nombre;
    }
    public getCodAlumno(){
        return this.codAlumno;
    }
    public setCodAlumno(codAlumno){
        this.codAlumno = codAlumno;
    }
    public getCodigoSeccion(){
        return this.codSeccion;
    }
    public setCodSeccion(codSeccion){
        this.codSeccion = codSeccion;
    }
    public getPromedio() {
        return this.promedio;
    }

    public getPromedioTotal() {
        return (this.getNotaFinal1() + this.getNotaFinal2()+this.getNotaFinal3()+this.getNotaFinal4());
    }
    public setPromedio(prom) {
        this.promedio = prom;
    }

}