import * as $ from 'jquery';
import * as jQuery from 'jquery';

export class Validacion_GA{
    //-------------------------------Validaciones para Aulas-----------------------------------------//

    aula_valida=0;

    validar_Aula(descripcion,capacidad,estado){
        this.aula_valida=0;
        this.aula_valida+=this.campos_vacios(descripcion,'#txt-descripcionAula');
        this.aula_valida+=this.numeros(capacidad,'#txt-unidadesCurso');
        this.aula_valida+=this.campos_vacios(estado,'input[name=gridRadiosAula]');
        return this.aula_valida;
    }

    removerValidacionesAula(){
        $('#txt-descripcionAula').removeClass('is-valid');
        $('#txt-descripcionAula').removeClass('is-invalid');
        $('#txt-unidadesCurso').removeClass('is-valid');
        $('#txt-unidadesCurso').removeClass('is-invalid');
        $('input[name=gridRadiosAula]').removeClass('is-valid');
        $('input[name=gridRadiosAula]').removeClass('is-invalid');
    }
    
    //-------------------------------Validaciones para Secciones-----------------------------------------//

    seccion_valida=0;

    validar_Seccion(docente,curso,asignatura,seccion,inicio,fin,aula,periodo,jornada,ano){
        this.seccion_valida=0;
        this.seccion_valida+=this.campos_vacios(docente,'#txt-seccionDocente');
        this.seccion_valida+=this.campos_vacios(curso,'#txt-seccionCurso');
        this.seccion_valida+=this.campos_vacios(asignatura,'#txt-asignatura');
        this.seccion_valida+=this.campos_vacios(seccion,'#txt-seccionNombre');
        this.seccion_valida+=this.campos_vacios(inicio,'#select-horainicio');
        this.seccion_valida+=this.campos_vacios(fin,'#select-horaFinal');
        this.seccion_valida+=this.campos_vacios(aula,'#txt-seccionAula');
        this.seccion_valida+=this.campos_vacios(periodo,'#txt-seccionPeriodo');
        this.seccion_valida+=this.campos_vacios(jornada,'#txt-seccionJornada');
        this.seccion_valida+=this.campos_vacios(ano,'#txt-seccionAnioLectivo');
        return this.seccion_valida;
    }

    removerValidacionesSeccion(){
        $('#txt-seccionDocente').removeClass('is-valid');
        $('#txt-seccionDocente').removeClass('is-invalid');
        $('#txt-seccionCurso').removeClass('is-valid');
        $('#txt-seccionCurso').removeClass('is-invalid');
        $('#txt-asignatura').removeClass('is-valid');
        $('#txt-asignatura').removeClass('is-invalid');
        $('#txt-seccionNombre').removeClass('is-valid');
        $('#txt-seccionNombre').removeClass('is-invalid');
        $('#select-horainicio').removeClass('is-valid');
        $('#select-horainicio').removeClass('is-invalid');
        $('#select-horaFinal').removeClass('is-valid');
        $('#select-horaFinal').removeClass('is-invalid');
        $('#txt-seccionAula').removeClass('is-valid');
        $('#txt-seccionAula').removeClass('is-invalid');
        $('#txt-seccionPeriodo').removeClass('is-valid');
        $('#txt-seccionPeriodo').removeClass('is-invalid');
        $('#txt-seccionJornada').removeClass('is-valid');
        $('#txt-seccionJornada').removeClass('is-invalid');
        $('#txt-seccionAnioLectivo').removeClass('is-valid');
        $('#txt-seccionAnioLectivo').removeClass('is-invalid');
    } 

    //-------------------------------Validaciones para Docentes-----------------------------------------//
    
    //-------Validaciones para Datos Personales--------------//
     datos_personales=0;

     validar_datosPersonalesDocente(nombre,apellidos,ide,edad,genero,fechaNac,pais,nacionalidad,departamento,lugar){
        this.datos_personales=0;
        this.datos_personales+=this.campos_vacios(nombre,'#txt-nombreDocente');
        this.datos_personales+=this.campos_vacios(apellidos,'#txt-apellidoDocente');
        this.datos_personales+=this.validarIdentidadHND(ide,'#txt-identidadDocente');
        this.datos_personales+=this.numeros(edad,'#txtEdad');
        this.datos_personales+=this.campos_vacios(genero,'#select-genero');
        this.datos_personales+=this.campos_vacios(fechaNac,'#txt-fechaNacimientoDocente');
        this.datos_personales+=this.campos_vacios(pais,'#txt-paisOrigenDocente');
        this.datos_personales+=this.campos_vacios(nacionalidad,'#txt-nacionalidadDocente');
        this.datos_personales+=this.campos_vacios(departamento,'#txt-deptoOrigen');
        this.datos_personales+=this.campos_vacios(lugar,'#txt-lugarNacimiento');
       // console.log(this.datos_personales);
        return this.datos_personales;
    }
    
    removerValidacionesDatosPersonalesDoc(){
        $('#txt-nombreDocente').removeClass('is-invalid');
        $('#txt-nombreDocente').removeClass('is-valid');
        $('#txt-nombreDocente').val("");
        $('#txt-apellidoDocente').removeClass('is-invalid');
        $('#txt-apellidoDocente').removeClass('is-valid');
        $('#txt-apellidoDocente').val("");
        $('#txt-identidadDocente').removeClass('is-invalid');
        $('#txt-identidadDocente').removeClass('is-valid');
        $('#txt-identidadDocente').val("");
        $('#txtEdad').removeClass('is-invalid');
        $('#txtEdad').removeClass('is-valid');
        $('#txtEdad').val("");
        $('#select-genero').removeClass('is-invalid');
        $('#select-genero').removeClass('is-valid');
        $('#select-genero').val();
        $('#txt-fechaNacimientoDocente').removeClass('is-invalid');
        $('#txt-fechaNacimientoDocente').removeClass('is-valid');
        $('#txt-fechaNacimientoDocente').val("");
        $('#txt-paisOrigenDocente').removeClass('is-invalid');
        $('#txt-paisOrigenDocente').removeClass('is-valid');
        $('#txt-paisOrigenDocente').val("");
        $('#txt-nacionalidadDocente').removeClass('is-invalid');
        $('#txt-nacionalidadDocente').removeClass('is-valid');
        $('#txt-nacionalidadDocente').val("");
        $('#txt-deptoOrigen').removeClass('is-invalid');
        $('#txt-deptoOrigen').removeClass('is-valid');
        $('#txt-deptoOrigen').val("");
        $('#txt-lugarNacimiento').removeClass('is-invalid');
        $('#txt-lugarNacimiento').removeClass('is-valid');
        $('#txt-lugarNacimiento').val();
    }

    //-------Validaciones para Datos Contacto-------------//
    datos_contacto=0;

    validar_Contacto(correo,celular,telefono,direccion){
        this.datos_contacto=0;
        this.datos_contacto+=this.validarEmails(correo,'#txt-correoDocente');
        this.datos_contacto+=this.validarPhone(celular,'#txt-celularDocente');
        this.datos_contacto+=this.validarPhone(telefono,'#txt-telefonoDocente');
        this.datos_contacto+=this.campos_vacios(direccion,'#txt-direccion');
        return this.datos_contacto;
    }
    
    removerValidacionesConctacto(){
        $('#txt-correoDocente').removeClass('is-invalid');
        $('#txt-correoDocente').removeClass('is-valid');
        $('#txt-correoDocente').val("");
        $('#txt-celularDocente').removeClass('is-invalid');
        $('#txt-celularDocente').removeClass('is-valid');
        $('#txt-celularDocente').val("");
        $('#txt-telefonoDocente').removeClass('is-invalid');
        $('#txt-telefonoDocente').removeClass('is-valid');
        $('#txt-telefonoDocente').val("");
        $('#txt-direccion').removeClass('is-invalid');
        $('#txt-direccion').removeClass('is-valid');
        $('#txt-direccion').val("");
    }
    //-------Validaciones para Datos Medicos--------------//
    datos_fichaMedica=0;

    validar_FichaMedica(tipo){
        this.datos_fichaMedica=0;
        this.datos_fichaMedica+=this.campos_vacios(tipo,'#select-tipoSangre');
        return this.datos_fichaMedica;
    }
    
    removerValidacionesFichaMedica(){
        $('#select-tipoSangre').removeClass('is-invalid');
        $('#select-tipoSangre').removeClass('is-valid');
        $('#select-tipoSangre').val();
        
    }
    //-------Validaciones para Datos Laborales--------------//
    datos_laboral=0;

    validar_Laboral(expediente,ingreso){
        this.datos_laboral=0;
        this.datos_laboral+=this.campos_vacios(expediente,'#txtExpediente');
        this.datos_laboral+=this.campos_vacios(ingreso,'#txtINgreso');
        return this.datos_laboral;
    }
    
    removerValidacionesLaboral(){
        $('#txtExpediente').removeClass('is-invalid');
        $('#txtExpediente').removeClass('is-valid');
        $('#txtExpediente').val();
        $('#txtINgreso').removeClass('is-invalid');
        $('#txtINgreso').removeClass('is-valid');
        $('#txtINgreso').val();
    }
    //-------Validaciones para Cargos--------------//
    datos_cargo=0;

    validar_cargo(cargo,institucion,inicio,final,departamento){
        this.datos_cargo=0;
        this.datos_cargo+=this.campos_vacios(cargo,'#modalCargosDocente #txt-cargoDocente');
        this.datos_cargo+=this.campos_vacios(institucion,'#txt-institucionDocente');
        this.datos_cargo+=this.campos_vacios(inicio,'#txt-fechaIncioCargo');
        this.datos_cargo+=this.campos_vacios(final,'#txt-fechaFinalCargo');
        this.datos_cargo+=this.campos_vacios(departamento,'#txt-departamentoDocente');
        return this.datos_cargo;
    }
    
    removerValidacionesCargo(){
        $('#modalDocenteLaboral #txt-cargoDocente').removeClass('is-invalid');
        $('#modalDocenteLaboral #txt-cargoDocente').removeClass('is-valid');
        $('#modalDocenteLaboral #txt-cargoDocente').val("");
        $('#modalDocenteLaboral #txt-institucionDocente').removeClass('is-invalid');
        $('#modalDocenteLaboral #txt-institucionDocente').removeClass('is-valid');
        $('#modalDocenteLaboral #txt-institucionDocente').val("");
        $('#modalDocenteLaboral #txt-fechaIncioCargo').removeClass('is-invalid');
        $('#modalDocenteLaboral #txt-fechaIncioCargo').removeClass('is-valid');
        $('#modalDocenteLaboral #txt-fechaIncioCargo').val("");
        $('#modalDocenteLaboral #txt-fechaFinalCargo').removeClass('is-invalid');
        $('#modalDocenteLaboral #txt-fechaFinalCargo').removeClass('is-valid');
        $('#modalDocenteLaboral #txt-fechaFinalCargo').val("");
        $('#modalDocenteLaboral #txt-departamentoDocente').removeClass('is-invalid');
        $('#modalDocenteLaboral #txt-departamentoDocente').removeClass('is-valid');
        $('#modalDocenteLaboral #txt-departamentoDocente').val("");
    }


    campos_vacios(valor,id){
        if(valor=="" || valor==undefined){
            $(id).removeClass('is-valid');
            $(id).addClass('is-invalid');
            return 1;
        }else{
            $(id).removeClass('is-invalid');
            $(id).addClass('is-valid');
            return 0;
        }
    }

    numeros(valor,id){
        if(valor<=0 || valor==undefined){
            $(id).removeClass('is-valid');
            $(id).addClass('is-invalid');
            return 1;
        }else{
            $(id).removeClass('is-invalid');
            $(id).addClass('is-valid');
            return 0;
        }
    }


    validarIdentidadHND(valor,id){
        if(valor!=""){
            if(!/^\d{4}-\d{4}-\d{5}$/.test(String(valor))){
                $(id).removeClass('is-valid');
                $(id).addClass('is-invalid');
                return 1;
            }else{
                $(id).removeClass('is-invalid');
                $(id).addClass('is-valid');
                return 0;
            }
        }else{
            $(id).removeClass('is-valid');
            $(id).addClass('is-invalid');
            return 1;
        }	
    }

    validarPhone(valor,id){
        if(valor!=""){
            if(!/^\d{4}-\d{4}$/.test(String(valor))){
                $(id).removeClass('is-valid');
                $(id).addClass('is-invalid');
                return 1;
            }else{
                $(id).removeClass('is-invalid');
                $(id).addClass('is-valid');
                return 0;
            }
        }else{
            $(id).removeClass('is-valid');
            $(id).addClass('is-invalid');
            return 1;
        }	
    }
    
    validarEmails(valor,id){
        if(valor!=""){
            if(!/^[a-zA-Z0-9.!#$%&'*+/=?^_`{|}~-]+@[a-zA-Z0-9-]+(?:\.[a-zA-Z0-9-]+)*$/.test(String(valor))){
                $(id).removeClass('is-valid');
                $(id).addClass('is-invalid');
                return 1;
            }else{
                $(id).removeClass('is-invalid');
                $(id).addClass('is-valid');
                return 0;
            }
        }else{
            $(id).removeClass('is-valid');
            $(id).addClass('is-invalid');
            return 1;
        }
            
    }
    
}