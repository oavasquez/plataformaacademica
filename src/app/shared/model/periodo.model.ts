import { THIS_EXPR } from "@angular/compiler/src/output/output_ast";

export class Periodo {
        public codPeriodo?: number;
        public nombrePeriodo?: string;
        public fechaInicio?: string;
        public fechaFinal?: string;
        public anio?: string;

    constructor() {
        this.Construir(0,"","","","");
     }

    Construir(CodigoPeriodo,NombrePeriodo,FechaInicio,FechaFinal,Anio){
        this.codPeriodo=CodigoPeriodo;
        this.nombrePeriodo=NombrePeriodo;
        this.fechaInicio=FechaInicio;
        this.fechaFinal=FechaFinal;
        this.anio=Anio;
    }


    setCodPeriodo(codPeriodo: number) {
        this.codPeriodo = codPeriodo;
    }
    setNombrePeriodo(nombrePeriodo: string) {
        this.nombrePeriodo = nombrePeriodo;
    }
    setFechaInicio(fechaInicio: string) {
        this.fechaInicio = fechaInicio;
    }
    setFechaFinal(fechaFinal: string) {
        this.fechaFinal = fechaFinal;

    }
    setAnio(anio: string) {
        this.anio = anio;
    }




    getCodPeriodo() {
        return this.codPeriodo;
    }
    getNombrePeriodo() {
        return this.nombrePeriodo;
    }
    getFechaInicio() {
        return this.fechaInicio;
    }
    getFechaFinal() {
        return this.fechaFinal;
    }
    getAnio() {
        return this.anio;
    }


}