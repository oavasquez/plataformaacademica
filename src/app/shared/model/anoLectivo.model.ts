export class AnoLectivo {
	private codLectivo?: number;
	private nombre?: string;
	private fechaInicio?: string;
	private fechaFinal?: string;

	constructor() {
		this.Construir(0, "", "", "");

	}

	Construir(codigoLectivo,
		nombreLectivo,
		fechaInicioLectivo,
		fechaFinalLectivo) {
		this.codLectivo = codigoLectivo;
		this.nombre = nombreLectivo;
		this.fechaInicio = fechaInicioLectivo;
		this.fechaFinal = fechaFinalLectivo;
	}

	getCodLectivo(): number {
		return this.codLectivo;
	}
	getNombre(): string {
		return this.nombre;
	}
	getFechaInicio(): string {
		return this.fechaInicio;
	}
	getFechaFinal(): string {
		return this.fechaFinal;
	}



	setCodLectivo(codLectivo: number) {
		this.codLectivo = codLectivo;
	}
	setNombre(nombre: string) {
		this.nombre = nombre;
	}

	setFechaInicio(fechaInicio: string) {
		this.fechaInicio = fechaInicio;
	}
	setFechaFinal(fechaFinal: string) {
		this.fechaFinal = fechaFinal;
	}

}