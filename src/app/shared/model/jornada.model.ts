
export class Jornada {
        private codJornada?: number;
        private jornada?: string;

    constructor() { 
        this.Construir(0,"");
    }

    Construir(CodigoJornada,Jornada){
        this.codJornada=CodigoJornada;
        this.jornada=Jornada;
    }

    setCodJornada(codJornada: number) {
        this.codJornada = codJornada;
    }
    setJornada(jornada: string) {
        this.jornada = jornada;
    }

    getCodJornada() {
        return this.codJornada;
    }
    getJornada() {
        return this.jornada;
    }



}