export class Institucion {

    constructor(
        public nombreInstitucion: String,
        public direccion: String,
        public telefono: String,
        public correo: String

    ) { }

}