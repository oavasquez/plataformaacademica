export class TipoIncidente {

    private codIncidencia: number;
    private tipoIncidencia: string;



    constructor(


    ) { }

    construir(codIncidencia, tipoIncidencia) {
        this.codIncidencia = codIncidencia;
        this.tipoIncidencia = tipoIncidencia;

    }



    public getCodIncidencia(): number {
        return this.codIncidencia;
    }


    public getTipoIncidencia(): string {
        return this.tipoIncidencia;
    }


    public setCodIncidencia(value: number) {
        this.codIncidencia = value;
    }


    public setTipoIncidencia(value: string) {
        this.tipoIncidencia = value;
    }


}