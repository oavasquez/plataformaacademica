export class Seccion {

    constructor(
        private codSeccion?: number,
        private seccion?: string,
        private horaInicio?: string,
        private horaFin?: string,
        private codAula?: number,
        private codDocente?: number,
        private codPeriodo?: number,
        private codLectivo?: number,
        private codCurso?: number,
        private suspendida?: number,
        private codJornada?: number,
        private codAsignatura?: number,
        private nombreAula?: string,
        private nombreCompletoDocente?: string,
        private nombreCurso?: string,
        private nombreAsignatura?: string,
        private nombrePeriodo?: string,
        private nombreJornada?: string,
        private nombreLectivo?: string



    ) { }


    getCodSeccion() {
        return this.codSeccion;
    }
    getSeccion() {
        return this.seccion;
    }
    getHoraInicio() {
        return this.horaInicio;
    }
    getHoraFin() {
        return this.horaFin
    }
    getCodAula() {
        return this.codAula;
    }
    getCodDocente() {
        return this.codDocente;
    }
    getCodPeriodo() {
        return this.codPeriodo;
    }
    getCodLectivo() {
        return this.codLectivo;
    }
    getCodCurso() {
        return this.codCurso;
    }
    getSuspendida() {
        return this.suspendida;
    }
    getCodJornada() {
        return this.codJornada;
    }
    getCodAsignatura() {
        return this.codAsignatura
    }

    getNombreAula() {
        return this.nombreAula;
    }
    getNombreCompletoDocente() {
        return this.nombreCompletoDocente;
    }
    getNombreCurso() {
        return this.nombreCurso;
    }
    getNombreAsignatura() {
        return this.nombreAsignatura;
    }
    getNombrePeriodo() {
        return this.nombrePeriodo;
    }
    getNombreJornada() {
        return this.nombreJornada;
    }
    getNombreLectivo() {
        return this.nombreLectivo;
    }


    setCodSeccion(codSeccion: number) {
        this.codSeccion = codSeccion;
    }
    setSeccion(seccion: string) {
        this.seccion = seccion;
    }
    setHoraInicio(horaInicio: string) {
        this.horaInicio = horaInicio;
    }
    setHoraFin(horaFin: string) {
        this.horaFin = horaFin;
    }
    setCodAula(codAula: number) {
        this.codAula = codAula;
    }
    setCodDocente(codDocente: number) {
        this.codDocente = codDocente;
    }
    setCodPeriodo(codPeriodo: number) {
        this.codPeriodo = codPeriodo;
    }
    setCodLectivo(codLectivo: number) {
        this.codLectivo = codLectivo;
    }
    setCodCurso(codCurso: number) {
        this.codCurso = codCurso;
    }
    setSuspendida(suspendida: number) {
        this.suspendida = suspendida;
    }
    setCodJornada(codJornada: number) {
        this.codJornada = codJornada
    }
    setCodAsignatura(codAsignatura: number) {
        this.codAsignatura = codAsignatura;
    }

    setNombreAula(value: string) {
        this.nombreAula = value;
    }
    setNombreCompletoDocente(value: string) {
        this.nombreCompletoDocente = value;
    }
    setNombreCurso(value: string) {
        this.nombreCurso = value;
    }
    setNombreAsignatura(value: string) {
        this.nombreAsignatura = value;
    }

    setNombrePeriodo(value: string) {
        return this.nombrePeriodo = value;
    }
    setNombreJornada(value: string) {
        return this.nombreJornada = value;
    }
    setNombreLectivo(value: string) {
        return this.nombreLectivo = value;
    }





}
