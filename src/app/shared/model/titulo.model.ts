export class Titulos {

    constructor(
        public nombreTitulo: String,
        public fechaInicio: Date,
        public fechaFinal: Date

    ) { }

}