export class Aula {

    constructor(
        public codAula?: number,
        public capacidad?: number,
        public habilitada?: number,
        public descripcion?: string
    ) { }
 

}