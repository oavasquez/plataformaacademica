import { Asignatura } from "./asignatura.model";

export class Curso {

    constructor(
        private codModalidad?: number,
        private nombreCurso?: string,
        private numeroAsignaturas?: string,
        private estadoCurso?: number,
        private codCurso?: number,
        private arrayAsignatura?: Asignatura[]
    ) {
        this.ConstruirCurso(0, "", "", "", 0);

    }

    getJSON() {
        return JSON.stringify(this);
    }
    ConstruirCurso(in_CodModalidad,
        in_nombreCurso,
        in_numeroAsignaturas,
        in_estadoCurso,
        in_CodCurso, ) {

        this.codModalidad = in_CodModalidad;
        this.nombreCurso = in_nombreCurso;
        this.numeroAsignaturas = in_numeroAsignaturas;
        this.estadoCurso = in_estadoCurso;
        this.codCurso = in_CodCurso;

    }

    ConstruirCursoActualizar(in_CodModalidad,
        in_nombreCurso,
        in_numeroAsignaturas,
        in_estadoCurso,
        in_CodCurso,
        in_ArrayAsignaturas
    ) {

        this.codModalidad = in_CodModalidad;
        this.nombreCurso = in_nombreCurso;
        this.numeroAsignaturas = in_numeroAsignaturas;
        this.estadoCurso = in_estadoCurso;
        this.codCurso = in_CodCurso;
        this.arrayAsignatura = in_ArrayAsignaturas;


    }

    getCursoJsonFormat() {
        return JSON.stringify(this);
    }

    setCodModalidad(codigoModalidad: number) {
        this.codModalidad = codigoModalidad;
    }

    setNombreCurso(nombre: string) {
        this.nombreCurso = nombre;
    }

    setNumeroAsignaturas(numero: string) {
        this.numeroAsignaturas = numero;
    }

    setEstadoCurso(estado: number) {
        this.estadoCurso = estado;
    }
 
    setCodigoCurso(codigoCurso: number) {
        this.codCurso = codigoCurso;
    }
    setArrayAsignatura(values: any) {
        this.arrayAsignatura = values;
    }

    getCodModalidad() {
        return this.codModalidad;
    }

    getNombreCurso() {
        return this.nombreCurso;
    }

    getNumeroAsignaturas() {
        return this.numeroAsignaturas;
    }

    getEstadoCurso() {
        return this.estadoCurso;
    }

    getCodigoCurso() {
        return this.codCurso;
    }

    getArrayAsignatura() {
        return this.arrayAsignatura;
    }


}