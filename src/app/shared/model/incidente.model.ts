import { TipoIncidente } from './tipoIncidente.model';
export class Incidente {

    private tituloIncidencia: string;
    private fechaIncidencia: string;
    private tipoIncidencia: TipoIncidente;
    private codTipoIncidencia: number;
    private nombreInfractor: string;
    private descripcion: string;
    private codAlumno: number;
    private codDocente: number;
    private codPersonaReporta: number;
    private codPersonaReportada: number;

    constructor(

    ) { }

    construir(in_tituloIncidencia, in_fechaIncidencia, in_tipoIncidente, in_descripcion) {
        this.tituloIncidencia = in_tituloIncidencia;
        this.fechaIncidencia = in_fechaIncidencia;
        this.tipoIncidencia = in_tipoIncidente;
        this.descripcion = in_descripcion;

    }

    public getTituloIncidencia(): string {
        return this.tituloIncidencia;
    }


    public getFechaIncidencia(): string {
        return this.fechaIncidencia;
    }


    public getTipoIncidencia(): TipoIncidente {
        return this.tipoIncidencia;
    }
    public getCodTipoIncidencia(): number {
        return this.codTipoIncidencia;
    }


    public getNombreInfractor(): string {
        return this.nombreInfractor;
    }


    public getDescripcion(): string {
        return this.descripcion;
    }

    public getCodAlumno(): number {
        return this.codAlumno;
    }

    public getCodDocente(): number {
        return this.codDocente;
    }

    public getCodPersonaReporta(): number {
        return this.codPersonaReporta;
    }

    public getCodPersonaReportada(): number {
        return this.codPersonaReportada;
    }


    public setTituloIncidencia(value: string) {
        this.tituloIncidencia = value;
    }

    public setFechaIncidencia(value: string) {
        this.fechaIncidencia = value;
    }


    public setTipoIncidencia(value: TipoIncidente) {
        this.tipoIncidencia = value;
    }

    public setCodTipoIncidencia(value: number) {
        this.codTipoIncidencia = value;
    }


    public setNombreInfractor(value: string) {
        this.nombreInfractor = value;
    }


    public setDescripcion(value: string) {
        this.descripcion = value;
    }

    public setCodAlumno(value: number) {
        this.codAlumno = value;
    }

    public setCodDocente(value: number) {
        this.codDocente = value;
    }

    public setCodPersonaReporta(value: number) {
        this.codPersonaReporta = value;
    }

    public setCodPersonaReportada(value: number) {
        this.codPersonaReportada = value;
    }


}