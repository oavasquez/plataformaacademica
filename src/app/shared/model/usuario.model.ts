
import * as moment from "moment";

export class Usuario {

    private codigoUsuario: number;
    public nombreUsuario: string;
    public contrasena: string;
    private codPersona: number;
    private codTipoUsuario: number;
    private tipoUsuario: string;
    private habilitado: number;
    private fechaCreacion: string;
    private fechaModificacion: string;
    private accesos: string;
    private nombreCompleto: string;

    getUsuarioJsonFormat() {
        return JSON.stringify(this);
        //     return '{'
        //  +'codUsuario: "'+ this.getCodUsuario()+'"'
        //         + 'nombreUsuario:"' + this.getNombreUsuario() + '"'
        //         + 'contrasena:"' + this.getContrasena() + '"'
        //         + 'codPersona:"' + this.getCodPersona() + '"'
        //         + 'codTipoUsuario:"' + this.getCodTipoUsuario() + '"'
        //         + 'tipoUsuario:"' + this.getTipoUsuario() + '"'
        //         + 'habilitado:"' + this.getHabilitado() + '"'
        //         + 'fechaCreacion:"' + this.getFechaCreacion() + '"'
        //         + 'fechaModificacion:"' + this.getFechaModificacion() + '"'
        //         + 'accesos:"' + this.getAccesos() + '"'
        //         + 'nombreCompleto:"' + this.getNombreCompleto() + '"'
        //     +'}';
    }

    constructor() {
        this.construir(0, '', '', 0, 0, '', 0, '', '', '', '');
    }
    construir(in_codUsuario: number,
        in_nombreUsuario: string,
        in_contrasena: string,
        in_codPersona: number,
        in_codTipoUsuario: number,
        in_tipoUsuario: string,
        in_habilitado: number,
        in_fechaCreacion: string,
        in_fechaModificacion: string,
        in_accesos: string,
        in_nombreCompleto: string) {

        this.codigoUsuario = in_codUsuario;
        this.nombreUsuario = in_nombreUsuario;
        this.contrasena = in_contrasena;
        this.codPersona = in_codPersona;
        this.codTipoUsuario = in_codTipoUsuario;
        this.tipoUsuario = in_tipoUsuario;
        this.habilitado = in_habilitado;
        this.fechaCreacion = in_fechaCreacion;
        this.fechaModificacion = in_fechaModificacion;
        this.accesos = in_accesos;
        this.nombreCompleto = in_nombreCompleto;
    }

    setNombreCompleto(nombre: string) {
        this.nombreCompleto = nombre;
    }
    setNombreUsuario(nombreUsuario: string) {
        this.nombreUsuario = nombreUsuario;
    }
    setAccesos(accesos: string) {
        this.accesos = accesos;
    }
    setCodUsuario(codUsuario: number) {
        this.codigoUsuario = codUsuario;
    }
    setContrasena(contrasena: string) {
        this.contrasena = contrasena;
    }
    setCodPersona(codPersona: number) {
        this.codPersona = codPersona;
    }
    setCodTipoUsuario(codTipoUsuario: number) {
        this.codTipoUsuario = codTipoUsuario;
    }
    setHabilitado(habilitado: number) {
        this.habilitado = habilitado;
    }
    setFechaCreacion(fecha) {
        this.fechaCreacion = fecha;
    }
    setFechaModificacion(fecha) {
        this.fechaModificacion = fecha;
    }
    setTipoUsuario(tipo: string) {
        this.tipoUsuario = tipo;
    }

    getNombreCompleto() {
        return this.nombreCompleto;
    }
    getNombreUsuario() {
        return this.nombreUsuario;
    }
    getCodUsuario() {
        return this.codigoUsuario;
    }
    getContrasena() {
        return this.contrasena;
    }
    getAccesos() {
        return this.accesos;
    }
    getCodPersona() {
        return this.codPersona;
    }
    getCodTipoUsuario() {
        return this.codTipoUsuario;
    }
    getHabilitado() {
        return this.habilitado;
    }
    getFechaCreacion() {
        return this.fechaCreacion;
    }
    getFechaModificacion() {
        return this.fechaModificacion;
    }
    getTipoUsuario() {
        return this.tipoUsuario;
    }
    setSession(authResult) {
        const expiresAt = moment().add(authResult.expiresIn, 'second');
        localStorage.setItem('token', authResult.token);
        localStorage.setItem('codigoPersona', authResult.datosUsuario.codigoPersona);
        localStorage.setItem('codigoUsuario', authResult.datosUsuario.codigoUsuario);
        localStorage.setItem('accesos', authResult.datosUsuario.accesos);
        localStorage.setItem("expiraEn", JSON.stringify(expiresAt.valueOf()));
        const token = localStorage.getItem('token');
    }

    logout(): Boolean {
        localStorage.removeItem("token");
        localStorage.removeItem("expiraEn");
        if (localStorage.removeItem("token") === null && localStorage.removeItem("expiraEn")) {
            //console.log("saliendo")
            return true;
        }

        return false;
    }

    public isLoggedIn() {
        return moment().isBefore(this.getExpiration());
    }

    isLoggedOut() {
        return !this.isLoggedIn();
    }

    getExpiration() {
        const expiration = localStorage.getItem("expiraEn");
        const expiresAt = JSON.parse(expiration);
        return moment(expiresAt);
    }


}
