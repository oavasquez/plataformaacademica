export class Experiencia {

    constructor(
        private codExperiencia?: string,
        private cargoDesempenado?: string,
        private fechaInicio?: string,
        private fechaFinal?: string,
        private areaEnsenanza?: string,
        private codDocente?: number,
        private institucion?: string

    ) { }

    getCodExperiencia() {
        return this.codExperiencia;
    }
    getCargoDesempenado() {
        return this.codExperiencia;
    }
    getFechaInicio() {
        return this.codExperiencia;
    }
    getFechaFinal() {
        return this.codExperiencia;
    }
    getAreaEnsenanza() {
        return this.codExperiencia;
    }
    getCodDocente() {
        return this.codExperiencia;
    }
    getInstitucion() {
        return this.codExperiencia;
    }

    setCodExperiencia(codExperiencia: string) {
        this.codExperiencia = codExperiencia;
    }
    setCargoDesempenado(cargoDesempenado: string) {
        this.cargoDesempenado = cargoDesempenado;
    }
    setFechaInicio(fechaInicio: string) {
        this.fechaInicio = fechaInicio;
    }
    setFechaFinal(fechaFinal: string) {
        this.fechaFinal = fechaFinal;
    }
    setAreaEnsenanza(areaEnsenanza: string) {
        this.areaEnsenanza = areaEnsenanza;
    }
    setCodDocente(codDocente: number) {
        this.codDocente = codDocente;
    }
    setInstitucion(institucion: string) {
        this.institucion = institucion;
    }


}