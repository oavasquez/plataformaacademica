export class Traslado {





    constructor(
        private institucion?: string,
        private fechaTraslado?: string,
        private codLectivo?: number,
        private nombreLectivo?: string,
        private codPeriodo?: number,
        private periodo?: string,
        private codCurso?: number,
        private nombreCurso?: string,
        private codJornada?: number,
        private codAsignatura?: number,
        private asignatura?: string,
        private promedioActual?: number,
        private codAlumno?: number,

    ) { }



    Construir(institucion,fecha,nombreLectivo,periodo,nombreCurso,codJor,asignatura,nota){
        this.institucion=institucion;
        this.fechaTraslado=fecha;
        this.nombreLectivo=nombreLectivo;
        this.periodo=periodo;
        this.nombreCurso=nombreCurso;
        this.codJornada=codJor;
        this.asignatura=asignatura;
        this.promedioActual=nota;
    }




    public getInstitucion(): string {
        return this.institucion;
    }


    public getFechaTraslado(): string {
        return this.fechaTraslado;
    }

    public getCodLectivo(): number {
        return this.codLectivo;
    }


    public getNombreLectivo(): string {
        return this.nombreLectivo;
    }
    public getCodCurso(): number {
        return this.codCurso;
    }

    public getNombreCurso(): string {
        return this.nombreCurso;
    }


    public getCodPeriodo(): number {
        return this.codPeriodo;
    }


    public getPeriodo(): string {
        return this.periodo;
    }


    public getCodJornada(): number {
        return this.codJornada;
    }


    public getCodAsignatura(): number {
        return this.codAsignatura;
    }
    public getAsignatura(): string {
        return this.asignatura;
    }


    public getPromedioActual(): number {
        return this.promedioActual;
    }
    public getCodAlumno(): number {
        return this.codAlumno;
    }




    public setInstitucion(value: string) {
        this.institucion = value;
    }


    public setFechaTraslado(value: string) {
        this.fechaTraslado = value;
    }


    public setCodLectivo(value: number) {
        this.codLectivo = value;
    }


    public setNombreLectivo(value: string) {
        this.nombreLectivo = value;
    }

    public setCodCurso(value: number) {
        this.codCurso = value;
    }
    public setNombreCurso(value: string) {
        this.nombreCurso = value;
    }
    public setCodPeriodo(value: number) {
        this.codPeriodo = value;
    }


    public setPeriodo(value: string) {
        this.periodo = value;
    }


    public setCodJornada(value: number) {
        this.codJornada = value;
    }


    public setCodAsignatura(value: number) {
        this.codAsignatura = value;
    }

    public setAsignatura(value: string) {
        this.asignatura = value;
    }


    public setPromedioActual(value: number) {
        this.promedioActual = value;
    }

    public setCodAlumno(value: number) {
        this.codAlumno = value;
    }




}