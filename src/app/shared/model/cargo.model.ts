export class Cargo {


    constructor(
        private codCargo?: number,
        private cargo?: string,
        private codDepartamento?: number,
        private departamento?: string,
        private institucion?: string,
        private fechaInicio?: string,
        private fechaFinal?: string,
        private fechaAsignacion?: string
    ) { 

    
    }

    construir(in_codCargo?: number,
        in_cargo?: string,
        in_codDepartamento?: number,
        in_departamento?: string,
        in_institucion?: string,
        in_fechaInicio?: string,
        in_fechaFinal?: string,
        in_fechaAsignacion?: string) {

        this.codCargo = in_codCargo;
        this.cargo = in_cargo;
        this.codDepartamento = in_codDepartamento;
        this.departamento = in_departamento;
        this.institucion = in_institucion;
        this.fechaInicio = in_fechaInicio;
        this.fechaFinal = in_fechaFinal;
        this.fechaAsignacion = in_fechaAsignacion;



    }

    public getCodCargo(): number {
        return this.codCargo;
    }
    public getCargo(): string {
        return this.cargo;
    }
    public getCodDepartamento(): number {
        return this.codDepartamento;
    }
    public getDepartamento(): string {
        return this.departamento;
    }
    public getInstitucion(): string {
        return this.institucion;
    }
    public getFechaInicio(): string {
        return this.fechaInicio;
    }
    public getFechaFinal(): string {
        return this.fechaFinal;
    }
    public getFechaAsignacion(): string {
        return this.fechaAsignacion;
    }
    public setCodCargo(value: number) {
        this.codCargo = value;
    }
    public setCargo(value: string) {
        this.cargo = value;
    }
    public setCodDepartamento(value: number) {
        this.codDepartamento = value;
    }
    public setDepartamento(value: string) {
        this.departamento = value;
    }
    public setInstitucion(value: string) {
        this.institucion = value;
    }
    public setFechaInicio(value: string) {
        this.fechaInicio = value;
    }
    public setFechaFinal(value: string) {
        this.fechaFinal = value;
    }
    public setFechaAsignacion(value: string) {
        this.fechaAsignacion = value;
    }


}