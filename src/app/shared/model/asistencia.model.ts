export class Asistencia {



    constructor(
        private semana?: number,
        private dia?: string,
        private Asistio?: number,
        private codAlumno?: number,
        private codSeccion?: number,
        private fechaAsistio?: string,
        private fechaInicial?: string,
        private fechaFinal?: string

    ) { }



    public getSemana(): number {
        return this.semana;
    }


    public getDia(): string {
        return this.dia;
    }


    public getAsistio(): number {
        return this.Asistio;
    }


    public get getCodAlumno(): number {
        return this.codAlumno;
    }


    public get getFechaAsistio(): string {
        return this.fechaAsistio;
    }
    public get getFechaIncial(): string {
        return this.fechaInicial;
    }
    public get getFechaFinal(): string {
        return this.fechaFinal;
    }
    public get getCodSeccion(): number {
        return this.codSeccion;
    }


    public setSemana(value: number) {
        this.semana = value;
    }


    public setDia(value: string) {
        this.dia = value;
    }

    public setAsistio(value: number) {
        this.Asistio = value;
    }


    public setCodAlumno(value: number) {
        this.codAlumno = value;
    }

    public setFechaAsistio(value: string) {
        this.fechaAsistio = value;
    }

    public setFechaFinal(value: string) {
        this.fechaFinal = value;
    }
    public setFechaInicial(value: string) {
        this.fechaInicial = value;
    }
    public setCodSeccion(value: number) {
        this.codSeccion = value;
    }


}