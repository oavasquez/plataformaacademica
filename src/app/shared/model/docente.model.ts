import { Experiencia } from './experiencia.model';
import { Persona } from "./persona.model";
import { Cargo } from './cargo.model';



export class Docente extends Persona {

    constructor(
        public codDocente?: number,
        public numeroExpediente?: number,
        public experiencia?: Experiencia,
        public fechaIngreso?: string,
        public habilitado?: number,
        public cargoArray?: Cargo[]

    ) {
        super();
    }

    getNumeroExpediente() {
        return this.numeroExpediente;
    }
    getCodDocente() {
        return this.codDocente;
    }
    getFechaIngreso() {
        return this.codDocente;
    }
    getHabilitado() {
        return this.codDocente;
    }
    getExperiencia() {
        return this.experiencia;
    }
    getCargoArray() {
        return this.cargoArray;
    }

    setNumeroExpediente(numeroExpediente: number) {
        this.numeroExpediente = numeroExpediente;
    }
    setCodDocente(codDocente: number) {
        this.codDocente = codDocente;
    }
    setFechaIngreso(fechaIngreso: string) {
        this.fechaIngreso = fechaIngreso;
    }
    setHabilitado(habilitado: number) {
        this.habilitado = habilitado;
    }
    setExperiencia(experiencia: Experiencia) {
        this.experiencia = experiencia;
    }
    setCargoArray(value : any) {
        this.cargoArray = value;
    }

}