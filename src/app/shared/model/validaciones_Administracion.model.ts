import * as $ from 'jquery';
import * as jQuery from 'jquery';
import { getLocaleExtraDayPeriodRules } from '@angular/common';

export class Validaciones {
    //-------------------------------Validaciones para Usuarios-----------------------------------------//
    user_valido = 0;

    validar_user(nombre, user, rol, pass, re_pass) {
        this.user_valido = 0;
        this.user_valido += this.campos_vacios(nombre, '#nombrePersona');
        this.user_valido += this.campos_vacios(user, '#txt-nombreUser');
        this.user_valido += this.campos_vacios(rol, '#txt-rolUser');
        this.user_valido += this.campos_vacios(pass, '#txt-passwordUser');
        this.user_valido += this.validarContraseña(pass, re_pass, '#txt-rep_passwordUser');
        return this.user_valido;
    }
    validar_userAct(user, rol, pass, re_pass) {
        this.user_valido = 0;
        this.user_valido += this.campos_vacios(user, '#txt-nombreUserUpdate');
        this.user_valido += this.campos_vacios(rol, '#txt-rolUserUpdate');
        this.user_valido += this.campos_vacios(pass, '#txt-passwordUserUpdate');
        this.user_valido += this.validarContraseña(pass, re_pass, '#txt-rep_passwordUserUpdate');
        return this.user_valido;
    }

    removerValidacionesUser() {
        $('input[name=nombrePersona]').removeClass('is-valid');
        $('input[name=nombrePersona]').removeClass('is-invalid');
        $('#txt-nombreUser').removeClass('is-valid');
        $('#txt-nombreUser').removeClass('is-invalid');
        $('#txt-rolUser').removeClass('is-valid');
        $('#txt-rolUser').removeClass('is-invalid');
        $('#txt-passwordUser').removeClass('is-valid');
        $('#txt-passwordUser').removeClass('is-invalid');
        $('#txt-rep_passwordUser').removeClass('is-valid');
        $('#txt-rep_passwordUser').removeClass('is-invalid');
    }

    removerValidacionesUserAct() {
        $('#txt-nombreUserUpdate').removeClass('is-valid');
        $('#txt-nombreUserUpdate').removeClass('is-invalid');
        $('#txt-rolUserUpdate').removeClass('is-valid');
        $('#txt-rolUserUpdate').removeClass('is-invalid');
        $('#txt-passwordUserUpdate').removeClass('is-valid');
        $('#txt-passwordUserUpdate').removeClass('is-invalid');
        $('#txt-rep_passwordUserUpdate').removeClass('is-valid');
        $('#txt-rep_passwordUserUpdate').removeClass('is-invalid');
    }
    //---------------------------------Validaciones para Roles-----------------------------------------//
    rol_valido = 0;

    ValidarRol(rol, tipoAcceso, estado) {
        this.rol_valido = 0;
        this.rol_valido += this.campos_vacios(rol, '#txt-nombreRol');
        this.rol_valido += this.campos_vacios(tipoAcceso, 'input[name=accesos]');
        this.rol_valido += this.campos_vacios(estado, 'input[name=estado_Rol]');
        return this.rol_valido;
    }
    ValidarRolAct(rol, tipoAcceso, estado) {
        this.rol_valido = 0;
        this.rol_valido += this.campos_vacios(rol, '#txt-nombreRolUpdate');
        this.rol_valido += this.campos_vacios(tipoAcceso, 'input[name=accesosUpdate]');
        this.rol_valido += this.campos_vacios(estado, 'input[name=estado_RolUpdate]');
        return this.rol_valido;
    }
    removerValidacionesRol() {
        $('#txt-nombreRol').removeClass('is-valid');
        $('#txt-nombreRol').removeClass('is-invalid');
        $('input[name=accesos]').removeClass('is-valid');
        $('input[name=accesos]').removeClass('is-invalid');
        $('input[name=estado_Rol]').removeClass('is-valid');
        $('input[name=estado_Rol]').removeClass('is-invalid');
    }
    removerValidacionesRolAct() {
        $('#txt-nombreRolUpdate').removeClass('is-valid');
        $('#txt-nombreRolUpdate').removeClass('is-invalid');
        $('input[name=accesosUpdate]').removeClass('is-valid');
        $('input[name=accesosUpdate]').removeClass('is-invalid');
        $('input[name=estado_RolUpdate]').removeClass('is-valid');
        $('input[name=estado_RolUpdate]').removeClass('is-invalid');
    }
    //---------------------------------Validaciones para Curso-----------------------------------------//
    curso_valido = 0;

    ValidarCurso(id1, id2, id3, id4) {
        this.curso_valido = 0;
        this.curso_valido += this.campos_vacios(id1, '#slct-modalidades');
        this.curso_valido += this.campos_vacios(id2, '#txt-nombreCurso');
        //this.curso_valido+=this.validarNumeroAsignaturas(id3,'#txt-unidadesCurso');
        this.curso_valido += this.campos_vacios(id4, 'input[name=gridRadiosCurso]');
        return this.curso_valido;
    }
    ValidarCursoAct(id1, id2, id3, id4) {
        this.curso_valido = 0;
        this.curso_valido += this.campos_vacios(id1, '#slct-modalidadAct');
        this.curso_valido += this.campos_vacios(id2, '#txt-nombreCursoUpdate');
        //this.curso_valido+=this.validarNumeroAsignaturas(id3,'#txt-unidadesCursoUpdate');
        this.curso_valido += this.campos_vacios(id4, 'input[name=gridRadiosCursoUpdate]');
        return this.curso_valido;
    }
    removerValidacionesCurso() {
        $('#slct-modalidades').removeClass('is-valid');
        $('#slct-modalidades').removeClass('is-invalid');
        $('#txt-nombreCurso').removeClass('is-valid');
        $('#txt-nombreCurso').removeClass('is-invalid');
        //$('#txt-unidadesCurso').removeClass('is-valid');
        //$('#txt-unidadesCurso').removeClass('is-invalid');
        $('input[name=gridRadiosCurso]').removeClass('is-valid');
        $('input[name=gridRadiosCurso]').removeClass('is-invalid');
    }
    removerValidacionesActCurso() {
        $('#slct-modalidadAct').removeClass('is-valid');
        $('#slct-modalidadAct').removeClass('is-invalid');
        $('#txt-nombreCursoUpdate').removeClass('is-valid');
        $('#txt-nombreCursoUpdate').removeClass('is-invalid');
        //$('#txt-unidadesCursoUpdate').removeClass('is-valid');
        //$('#txt-unidadesCursoUpdate').removeClass('is-invalid');
        $('input[name=gridRadiosCursoUpdate]').removeClass('is-valid');
        $('input[name=gridRadiosCursoUpdate]').removeClass('is-invalid');
    }

    //---------------------------------Validaciones para Asignaturas-----------------------------------------//
    asignatura_valida = 0;

    validar_Asignatura(id1, id2, id3, id4) {
        this.asignatura_valida = 0;
        //this.asignatura_valida += this.campos_vacios(id0, 'input[name=txt-codAsignatura]');
        this.asignatura_valida += this.campos_vacios(id1, 'input[name=txt-nombreAsignatura]');
        //this.asignatura_valida+=this.campos_vacios(id2,'#slct-cursos');
        //this.asignatura_valida+=this.campos_vacios(id3,'input[name=chkdias]');
        this.asignatura_valida += this.campos_vacios(id4, 'input[name=estado-asignatura]');
        return this.asignatura_valida;
    }

    validar_AsignaturaAct(id0, id1, id2, id3, id4) {
        this.asignatura_valida = 0;
        this.asignatura_valida += this.campos_vacios(id0, 'input[name=txt-codAsignaturaUpdate]');
        this.asignatura_valida += this.campos_vacios(id1, 'input[name=txt-nombreAsignaturaUptade');
        //this.asignatura_valida+=this.campos_vacios(id2,'#slct-cursosUpdate');
        //this.asignatura_valida+=this.campos_vacios(id3,'input[name=chkdiasUpdate]');
        this.asignatura_valida += this.campos_vacios(id4, 'input[name=estado-asignaturaUpdate]');
        return this.asignatura_valida;
    }

    removerValidacionesAsignatura() {
        $('input[name=txt-codAsignatura]').removeClass('is-valid');
        $('input[name=txt-codAsignatura]').removeClass('is-invalid');
        $('input[name=txt-nombreAsignatura]').removeClass('is-valid');
        $('input[name=txt-nombreAsignatura]').removeClass('is-invalid');
        //$('#slct-cursos').removeClass('is-valid');
        //$('#slct-cursos').removeClass('is-invalid');
        //$('input[name=chkdias]').removeClass('is-valid');
        //$('input[name=chkdias]').removeClass('is-invalid');
        $('input[name=estado-asignatura]').removeClass('is-valid');
        $('input[name=estado-asignatura]').removeClass('is-invalid');
    }

    removerValidacionesAsignaturaAct() {
        $('input[name=txt-codAsignaturaUpdate]').removeClass('is-valid');
        $('input[name=txt-codAsignaturaUpdate').removeClass('is-invalid');
        $('input[name=txt-nombreAsignaturaUptade]').removeClass('is-valid');
        $('input[name=txt-nombreAsignaturaUpdate]').removeClass('is-invalid');
        //$('#slct-cursosUpdate').removeClass('is-valid');
        //$('#slct-cursosUpdate').removeClass('is-invalid');
        //$('input[name=chkdiasUpdate]').removeClass('is-valid');
        //$('input[name=chkdiasUpdate]').removeClass('is-invalid');
        $('input[name=estado-asignaturaUpdate]').removeClass('is-valid');
        $('input[name=estado-asignaturaUpdate]').removeClass('is-invalid');
    }

    //---------------------------------Validaciones para Año Lectivo-----------------------------------------//
    lectivo_valido = 0;

    validar_lectivo(v1, v2, v3) {
        this.lectivo_valido = 0;
        this.lectivo_valido += this.campos_vacios(v1, 'input[name=txt-nombreAnoLectivo]');
        this.lectivo_valido += this.campos_vacios(v2, 'input[name=txt-FechaInicioAnoLectivo]');
        this.lectivo_valido += this.campos_vacios(v3, 'input[name=txt-FechaFinalAnoLectivo]');
        return this.lectivo_valido;
    }
    validar_lectivoAct(v1, v2, v3) {
        this.lectivo_valido = 0;
        this.lectivo_valido += this.campos_vacios(v1, 'input[name=txt-nombreAnoLectivoUpdate]');
        this.lectivo_valido += this.campos_vacios(v2, 'input[name=txt-FechaInicioAnoLectivoUpdate]');
        this.lectivo_valido += this.campos_vacios(v3, 'input[name=txt-FechaFinalAnoLectivoUpdate]');
        return this.lectivo_valido;
    }
    remover_validacionesLectivo() {
        $('input[name=txt-nombreAnoLectivo]').removeClass('is-valid');
        $('input[name=txt-nombreAnoLectivo]').removeClass('is-invalid');
        $('input[name=txt-FechaInicioAnoLectivo]').removeClass('is-valid');
        $('input[name=txt-FechaInicioAnoLectivo]').removeClass('is-invalid');
        $('input[name=txt-FechaFinalAnoLectivo]').removeClass('is-valid');
        $('input[name=txt-FechaFinalAnoLectivo]').removeClass('is-invalid');
    }
    remover_validacionesLectivoAct() {
        $('input[name=txt-nombreAnoLectivoUpdate]').removeClass('is-valid');
        $('input[name=txt-nombreAnoLectivoUpdate]').removeClass('is-invalid');
        $('input[name=txt-FechaInicioAnoLectivoUpdate]').removeClass('is-valid');
        $('input[name=txt-FechaInicioAnoLectivoUpdate]').removeClass('is-invalid');
        $('input[name=txt-FechaFinalAnoLectivoUpdate]').removeClass('is-valid');
        $('input[name=txt-FechaFinalAnoLectivoUpdate]').removeClass('is-invalid');
    }

    //---------------------------------Validaciones para Periodos-----------------------------------------//
    periodo_valido = 0;

    validar_Periodo(v1, v2, v3, v4) {
        this.periodo_valido = 0;
        this.periodo_valido += this.campos_vacios(v1, 'input[name=txt-Periodo]');
        this.periodo_valido += this.campos_vacios(v2, 'input[name=txt-FechaInicioPeriodo]');
        this.periodo_valido += this.campos_vacios(v3, 'input[name=txt-FechaFinalPeriodo]');
        this.periodo_valido += this.campos_vacios(v4, 'input[name=txt-AnioPeriodo]');
        return this.periodo_valido;
    }
    validar_PeriodoAct(v1, v2, v3, v4) {
        this.periodo_valido = 0;
        this.periodo_valido += this.campos_vacios(v1, 'input[name=txt-PeriodoUpdate]');
        this.periodo_valido += this.campos_vacios(v2, 'input[name=txt-FechaInicioPeriodoUpdate]');
        this.periodo_valido += this.campos_vacios(v3, 'input[name=txt-FechaFinalPeriodoUpdate]');
        this.periodo_valido += this.campos_vacios(v4, 'input[name=txt-AnioPeriodoUpdate]');
        return this.periodo_valido;
    }
    remover_validacionesPeriodo() {
        $('input[name=txt-Periodo]').removeClass('is-valid');
        $('input[name=txt-Periodo]').removeClass('is-invalid');
        $('input[name=txt-FechaInicioPeriodo]').removeClass('is-valid');
        $('input[name=txt-FechaInicioPeriodo]').removeClass('is-invalid');
        $('input[name=txt-FechaFinalPeriodo]').removeClass('is-valid');
        $('input[name=txt-FechaFinalPeriodo]').removeClass('is-invalid');
        $('input[name=txt-AnioPeriodo]').removeClass('is-valid');
        $('input[name=txt-AnioPeriodo]').removeClass('is-invalid');
    }
    remover_validacionesPeriodoAct() {
        $('input[name=txt-PeriodoUpdate]').removeClass('is-valid');
        $('input[name=txt-PeriodoUpdate]').removeClass('is-invalid');
        $('input[name=txt-FechaInicioPeriodoUpdate]').removeClass('is-valid');
        $('input[name=txt-FechaInicioPeriodoUpdate]').removeClass('is-invalid');
        $('input[name=txt-FechaFinalPeriodoUpdate]').removeClass('is-valid');
        $('input[name=txt-FechaFinalPeriodoUpdate]').removeClass('is-invalid');
        $('input[name=txt-AnioPeriodoUpdate]').removeClass('is-valid');
        $('input[name=txt-AnioPeriodoUpdate]').removeClass('is-invalid');
    }

    //---------------------------------Validaciones para Jornadas-----------------------------------------//
    Jornada_valida = 0;

    validar_Jornada(v1) {
        this.Jornada_valida = 0;
        this.Jornada_valida += this.campos_vacios(v1, 'input[name=txt-Jornada]');
        return this.Jornada_valida;
    }
    validar_JornadaAct(v1) {
        this.Jornada_valida = 0;
        this.Jornada_valida += this.campos_vacios(v1, 'input[name=txt-JornadaUpdate]');
        return this.Jornada_valida;
    }
    remover_validacionesJornada() {
        $('input[name=txt-Jornada]').removeClass('is-valid');
        $('input[name=txt-Jornada]').removeClass('is-invalid');
    }
    remover_validacionesJornadaAct() {
        $('input[name=txt-JornadaUpdate]').removeClass('is-valid');
        $('input[name=txt-JornadaUpdate]').removeClass('is-invalid');
    }


    campos_vacios(valor, id) {
        if (valor == "") {
            $(id).removeClass('is-valid');
            $(id).addClass('is-invalid');
            return 1;
        } else {
            $(id).removeClass('is-invalid');
            $(id).addClass('is-valid');
            return 0;
        }
    }

    validar_nombres(valor, id) {
        if (parseInt(valor.length) > 5 && valor != "") {
            if (!/^([A-ZÁÉÍÓÚ]{1}[a-zñáéíóú]+[\s]*)+$/.test(String(valor))) {
                $(id).removeClass('is-valid');
                $(id).addClass('is-invalid');
                return 1;
            } else {
                $(id).removeClass('is-invalid');
                $(id).addClass('is-valid');
                return 0;
            }
        } else {
            $(id).removeClass('is-valid');
            $(id).addClass('is-invalid');
            return 1;
        }
    }

    validarNumeroAsignaturas(valor, id) {
        if (parseInt(valor) < 10 || valor == "") {
            $(id).removeClass('is-valid');
            $(id).addClass('is-invalid');
            return 1;
        } else {
            $(id).removeClass('is-invalid');
            $(id).addClass('is-valid');
            return 0;
        }

    }

    validarContraseña(valor, valor2, id) {
        if (valor != valor2) {
            $(id).removeClass('is-valid');
            $(id).addClass('is-invalid');
            return 1;
        } else {
            $(id).removeClass('is-invalid');
            $(id).addClass('is-valid');
            return 0;
        }
    }


}