export class Modalidad {


    constructor(
        private codModalidad?: number,
        private modalidad?: string


    ) { }

    Construir(in_codModalidad, in_modalidad) {
        this.codModalidad = in_codModalidad;
        this.modalidad = in_modalidad;

    }
    public getCodModalidad(): number {
        return this.codModalidad;
    }


    public getModalidad(): string {
        return this.modalidad;
    }


    public setCodModalidad(value: number) {
        this.codModalidad = value;
    }


    public setModalidad(value: string) {
        this.modalidad = value;
    }


}