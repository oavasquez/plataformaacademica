import { Asistencia } from './asistencia.model';
import { Persona } from "./persona.model";
import { Tutor } from "./tutor.model";

export class Alumnos extends Persona {

    constructor(
        private codAlumno?: number,
        private numExpediente?: number,
        private indiceGeneral?: number,
        private traslado?: number,
        private fechaCreacion?: string,
        private fechaModificacion?: string,
        private arrayTutores?: Tutor[],
        public arrayAsistencia?: Asistencia[]
    ) {
        super();
    }

    getCodAlumno(): number {
        return this.codAlumno;
    }

    getNumExpediente(): number {
        return this.numExpediente;
    }

    getIndiceGeneral(): number {
        return this.indiceGeneral;
    }

    getTraslado(): number {
        return this.traslado;
    }

    getFechaCreacion(): string {
        return this.fechaCreacion;
    }

    getFechaModificacion(): string {
        return this.fechaModificacion;
    }

    getArrayTutores() {
        return this.arrayTutores;
    }
    getArrayAsistencia() {
        return this.arrayAsistencia;
    }

    setCodAlumno(value: number) {
        this.codAlumno = value;
    }


    setNumExpediente(value: number) {
        this.numExpediente = value;
    }


    setIndiceGeneral(value: number) {
        this.indiceGeneral = value;
    }


    setTraslado(value: number) {
        this.traslado = value;
    }


    setFechaCreacion(value: string) {
        this.fechaCreacion = value;
    }


    setFechaModificacion(value: string) {
        this.fechaModificacion = value;
    }

    setArrayTutores(values: any) {
        this.arrayTutores = values;
    }

    setArrayAsistencia(values: any) {
        this.arrayAsistencia = values;
    }



}