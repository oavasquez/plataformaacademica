import { Persona } from "./persona.model";

export class Tutor extends Persona{
     
    constructor(
		public identidad?: string,
		public nombre?: string,
        public apellido?: string,
		private  parentesco?: string,
		public sexo?: string,
		public celular?: string,
		private telefonoTrabajo?: string,
        private lugarTrabajo?: string,
		public correo?: string,
        private ocupacion?: string
    ) { 
        super();
	}
	




	getParentesco(): string {
		return this.parentesco;
		
	}

   
	getOcupacion(): string {
		return this.ocupacion;
	}


	getLugarTrabajo(): string {
		return this.lugarTrabajo;
	}

  
	getTelefonoTrabajo(): string {
		return this.telefonoTrabajo;
	}

 
	setParentesco(value: string) {
		this.parentesco = value;
	}


	setOcupacion(value: string) {
		this.ocupacion = value;
	}


	setLugarTrabajo(value: string) {
		this.lugarTrabajo = value;
	}


	setTelefonoTrabajo(value: string) {
		this.telefonoTrabajo = value;
	}
        


}