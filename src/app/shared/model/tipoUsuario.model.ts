export class TipoUsuario {
    public codTipoUsuario: number;
    public tipoUsuario: string;
    public tipoAcceso: string;
    public habilitado: number;
    private fechaCreacion:string;
    private fechaModificacion: string;
    public accesos:string;

    constructor() { 
        this.construir(0,"","",0,"","","");
    }
    

    construir(
            codTipoUsuario: number,
            tipoUsuario: string,
            tipoAcceso: string,
            habilitado: number,
            fechaCreacion: string,
            fechaModificacion: string,
            accesos: string
            ){
        this.codTipoUsuario = codTipoUsuario;
        this.tipoUsuario = tipoUsuario;
        this.tipoAcceso = tipoAcceso;
        this.habilitado = habilitado;
        this.fechaCreacion = fechaCreacion;
        this.fechaModificacion = fechaModificacion ;
        this.accesos = accesos;
    }

    getCodTipoUsuario(){
        return this.codTipoUsuario;
    }
    getTipoUsuario(){
        return this.tipoUsuario;
    }
    getTipoAcceso(){
        return this.tipoAcceso;
    }
    getHabilitado(){
        return this.habilitado;
    }
    getFechaCreacion(){
        return this.fechaCreacion;
    }
    getFechaModificacion(){
        return this.fechaModificacion;
    }
    getAccesos(){
        return this.accesos;
    }    

    setCodTipoUsuario(parametro:number) {
        this.codTipoUsuario=parametro;
    }
    setTipoUsuario(parametro:string) {
        this.tipoUsuario=parametro;
    }
    setTipoAcceso(parametro:string) {
        this.tipoAcceso=parametro;
    }
    setHabilitado(parametro:number) {
        this.habilitado=parametro;
    }
    setFechaCreacion(parametro:string) {
        this.fechaCreacion=parametro;
    }
    setFechaModificacion(parametro:string) {
        this.fechaModificacion=parametro;
    }
    setAccesos(parametro:string) {
        this.accesos=parametro;
    }    
}
