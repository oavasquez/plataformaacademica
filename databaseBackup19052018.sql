-- MySQL dump 10.13  Distrib 5.7.17, for Win64 (x86_64)
--
-- Host: us-cdbr-iron-east-05.cleardb.net    Database: heroku_39a7bb7a85bd88d
-- ------------------------------------------------------
-- Server version	5.6.36-log

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `tblalumnos`
--

DROP TABLE IF EXISTS `tblalumnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblalumnos` (
  `codAlumno` int(11) NOT NULL,
  `numExpediente` varchar(25) DEFAULT NULL,
  `indiceGeneral` decimal(6,2) DEFAULT NULL,
  `traslado` int(11) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `codEstado` int(11) DEFAULT NULL,
  PRIMARY KEY (`codAlumno`),
  KEY `fk_table1_tblPersona1_idx` (`codAlumno`),
  KEY `fk_tblalumnos_tblestados1_idx` (`codEstado`),
  CONSTRAINT `fk_table1_tblPersona1` FOREIGN KEY (`codAlumno`) REFERENCES `tblpersonas` (`codPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblalumnos_tblestados1` FOREIGN KEY (`codEstado`) REFERENCES `tblestados` (`codEstado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblalumnos`
--

LOCK TABLES `tblalumnos` WRITE;
/*!40000 ALTER TABLE `tblalumnos` DISABLE KEYS */;
INSERT INTO `tblalumnos` VALUES (102,'0801199012345',0.00,NULL,NULL,NULL,1),(361,'361-1233-1233-1233',NULL,NULL,'2018-05-08 00:55:39',NULL,NULL),(371,NULL,NULL,NULL,'2018-05-08 01:21:10',NULL,NULL),(372,'372-1233-1233',NULL,NULL,'2018-05-08 01:34:54',NULL,NULL),(382,'382-1234-1244-12344',NULL,NULL,'2018-05-08 01:38:05',NULL,NULL),(391,'391-1234-1244-12344',NULL,NULL,'2018-05-08 01:40:37',NULL,NULL),(402,'402-1234-1234-12345',NULL,NULL,'2018-05-08 09:32:23',NULL,NULL),(612,'612-0897-1997-08731',NULL,NULL,'2018-05-11 01:14:49',NULL,NULL),(632,'632-0897-1997-08731',NULL,NULL,'2018-05-11 01:14:51',NULL,NULL),(652,'652-0803-1997-97731',NULL,NULL,'2018-05-11 01:23:25',NULL,NULL),(851,'851-1234-1234-12345',NULL,NULL,'2018-05-11 08:30:28',NULL,NULL);
/*!40000 ALTER TABLE `tblalumnos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblasignaturas`
--

DROP TABLE IF EXISTS `tblasignaturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblasignaturas` (
  `codAsignatura` int(11) NOT NULL AUTO_INCREMENT,
  `asignatura` varchar(45) DEFAULT NULL,
  `habilitada` int(11) DEFAULT NULL,
  PRIMARY KEY (`codAsignatura`)
) ENGINE=InnoDB AUTO_INCREMENT=173 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblasignaturas`
--

LOCK TABLES `tblasignaturas` WRITE;
/*!40000 ALTER TABLE `tblasignaturas` DISABLE KEYS */;
INSERT INTO `tblasignaturas` VALUES (1,'ESPAÑOL GENERAL',1),(21,'Ciencias Sociales',1),(31,'Ciencias Naturales',NULL),(41,'Agropecuaria 1',1),(51,'Fisica I',1),(61,'Quimica I',NULL),(71,'Programacion ',NULL),(81,'Taller I',NULL),(122,'Prueba Asignatura',NULL),(132,'Prueba 2 asignatura',NULL),(141,'Matematicas Especial',1),(151,'Español Especial',1),(162,'Matemáticas 11',1),(172,'Matemáticas ',1);
/*!40000 ALTER TABLE `tblasignaturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblasignaturas_x_curso`
--

DROP TABLE IF EXISTS `tblasignaturas_x_curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblasignaturas_x_curso` (
  `codCurso` int(11) NOT NULL,
  `codAsignatura` int(11) NOT NULL,
  `dias` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`codCurso`,`codAsignatura`),
  KEY `fk_tblAsignaturas_x_curso_tblCursos1_idx` (`codCurso`),
  KEY `fk_tblAsignaturas_x_curso_tblAsignaturas1_idx` (`codAsignatura`),
  CONSTRAINT `fk_tblAsignaturas_x_curso_tblAsignaturas1` FOREIGN KEY (`codAsignatura`) REFERENCES `tblasignaturas` (`codAsignatura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblAsignaturas_x_curso_tblCursos1` FOREIGN KEY (`codCurso`) REFERENCES `tblcursos` (`codCurso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblasignaturas_x_curso`
--

LOCK TABLES `tblasignaturas_x_curso` WRITE;
/*!40000 ALTER TABLE `tblasignaturas_x_curso` DISABLE KEYS */;
INSERT INTO `tblasignaturas_x_curso` VALUES (11,141,'Lu,Mi,Vi'),(21,1,'Lu,Ma,Mi'),(21,21,'Lu,Ma,Mi,Ju,Vi'),(21,31,''),(21,172,''),(81,1,'Lu,Ma,Mi,Ju,Vi'),(81,21,'Lu,Ma,Mi,Ju,Vi'),(81,31,'Lu,Ma,Mi,Ju,Vi'),(81,172,'Lu,Ma,Mi,Ju,Vi'),(252,1,'Lu,Ma,Mi,Ju,Vi'),(252,21,'Lu,Ma,Mi,Ju,Vi'),(252,31,'Lu,Ma,Mi,Ju,Vi'),(252,51,'Lu,Ma,Mi,Ju,Vi'),(252,61,'Lu,Ma,Mi,Ju,Vi'),(252,162,'Lu,Ma,Mi,Ju,Vi'),(312,41,'Mi,Ju,Vi'),(392,1,'Lu,Ma'),(392,51,'Lu,Ma'),(412,162,'Lu,Ma,Mi,Ju,Vi'),(421,41,'Lu,Sa'),(421,51,'Lu,Ma,Ju,Sa'),(421,141,'Lu,Do'),(432,51,'Lu,Do'),(441,1,'Lu,Ma,Mi,Ju,Vi'),(441,21,'Lu,Ma,Mi,Ju,Vi'),(441,162,'Lu,Ma,Mi,Ju,Vi,Do'),(451,1,'Lu,Do'),(451,141,'Lu,Ma'),(461,21,'Lu,Ma'),(471,21,'Lu,Do'),(471,41,'Lu,Ma,Vi'),(471,162,'Lu,Ma,Vi,Sa'),(481,1,'Lu,Ma,Mi,Ju,Vi');
/*!40000 ALTER TABLE `tblasignaturas_x_curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblasistencia`
--

DROP TABLE IF EXISTS `tblasistencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblasistencia` (
  `codDiaClase` int(11) DEFAULT NULL,
  `codAlumno` int(11) DEFAULT NULL,
  `codSeccion` int(11) DEFAULT NULL,
  `asistio` int(11) DEFAULT NULL,
  KEY `fk_tblAsistencia_tblDiasClase1_idx` (`codDiaClase`),
  KEY `fk_tblasistencia_tblmatricula1_idx` (`codAlumno`,`codSeccion`),
  CONSTRAINT `fk_tblAsistencia_tblDiasClase1` FOREIGN KEY (`codDiaClase`) REFERENCES `tbldiasclase` (`codDiaClase`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblasistencia_tblmatricula1` FOREIGN KEY (`codAlumno`, `codSeccion`) REFERENCES `tblmatricula` (`codAlumno`, `codSeccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblasistencia`
--

LOCK TABLES `tblasistencia` WRITE;
/*!40000 ALTER TABLE `tblasistencia` DISABLE KEYS */;
INSERT INTO `tblasistencia` VALUES (12,102,162,1),(22,102,162,1),(32,102,162,1),(12,382,162,1),(22,382,162,1),(32,382,162,1),(42,102,162,1),(42,382,162,0),(12,102,162,1),(12,102,172,1),(22,102,172,1),(141,102,172,1),(142,102,172,0),(143,102,172,1),(144,102,172,1),(145,102,172,1),(141,372,172,1),(142,372,172,1),(143,372,172,1),(144,372,172,1),(145,372,172,1),(141,391,172,0),(142,391,172,1),(143,391,172,1),(144,391,172,1),(145,391,172,0),(141,402,172,0),(142,402,172,1),(143,402,172,1),(144,402,172,1),(145,402,172,0),(141,612,172,0),(142,612,172,1),(143,612,172,1),(144,612,172,1),(145,612,172,0),(621,102,172,1),(631,372,172,0),(641,391,172,1),(651,402,172,1),(661,612,172,1),(671,102,162,1);
/*!40000 ALTER TABLE `tblasistencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblaulas`
--

DROP TABLE IF EXISTS `tblaulas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblaulas` (
  `codAula` int(11) NOT NULL AUTO_INCREMENT,
  `capacidad` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `habilitada` int(11) DEFAULT NULL,
  PRIMARY KEY (`codAula`)
) ENGINE=InnoDB AUTO_INCREMENT=152 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblaulas`
--

LOCK TABLES `tblaulas` WRITE;
/*!40000 ALTER TABLE `tblaulas` DISABLE KEYS */;
INSERT INTO `tblaulas` VALUES (1,34,'Aula de uso multiple',1),(11,3,'Aula de laboratorio',0),(31,20,'Aula audivisual',0),(32,56,'Aula recurso',1),(132,35,'8002',0),(142,3,'El aula es',0),(151,30,'Laboratorio',1);
/*!40000 ALTER TABLE `tblaulas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblbitacora`
--

DROP TABLE IF EXISTS `tblbitacora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblbitacora` (
  `codLog` int(11) NOT NULL AUTO_INCREMENT,
  `codUsuario` int(11) NOT NULL,
  `fechaHora` datetime DEFAULT NULL,
  `descripcion` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codLog`),
  KEY `fk_tblBitacora_tblUsuarios1_idx` (`codUsuario`),
  CONSTRAINT `fk_tblBitacora_tblUsuarios1` FOREIGN KEY (`codUsuario`) REFERENCES `tblusuarios` (`codUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=32 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblbitacora`
--

LOCK TABLES `tblbitacora` WRITE;
/*!40000 ALTER TABLE `tblbitacora` DISABLE KEYS */;
INSERT INTO `tblbitacora` VALUES (1,41,'2018-04-02 13:12:23','0 el usuar'),(11,41,'2018-04-02 13:14:06','0 el usuario allan r'),(21,41,'2018-04-02 13:16:46','en la tabla tblUsuar'),(31,41,'2018-04-02 13:19:03','En la tabla tblUsuarios el usuario allan realizo una accion de consulta de datos');
/*!40000 ALTER TABLE `tblbitacora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblcargos`
--

DROP TABLE IF EXISTS `tblcargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblcargos` (
  `codCargo` int(11) NOT NULL AUTO_INCREMENT,
  `cargo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codCargo`)
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblcargos`
--

LOCK TABLES `tblcargos` WRITE;
/*!40000 ALTER TABLE `tblcargos` DISABLE KEYS */;
INSERT INTO `tblcargos` VALUES (1,'Director'),(2,'Sub Director'),(3,'Coordinado'),(4,'Jefe Departamento'),(5,'Consejero');
/*!40000 ALTER TABLE `tblcargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblcargos_x_docente`
--

DROP TABLE IF EXISTS `tblcargos_x_docente`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblcargos_x_docente` (
  `codDocente` int(11) NOT NULL,
  `codCargo` int(11) NOT NULL,
  `fechaAsignacion` date DEFAULT NULL,
  `codDepartamento` int(11) NOT NULL,
  `fechaInicio` datetime DEFAULT NULL,
  `fechaFinal` datetime DEFAULT NULL,
  `institucion` varchar(45) DEFAULT NULL,
  KEY `fk_tblcargos_x_docente_tbldocentes1_idx` (`codDocente`),
  KEY `fk_tblcargos_x_docente_tblcargos1_idx` (`codCargo`),
  KEY `fk_tblcargos_x_docente_tbldepartamento1_idx` (`codDepartamento`),
  CONSTRAINT `fktblcargo_x_docente_tbldocente` FOREIGN KEY (`codDocente`) REFERENCES `tbldocentes` (`codDocente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fktblcargos_x_docente_tblcargos` FOREIGN KEY (`codCargo`) REFERENCES `tblcargos` (`codCargo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fktblcargos_x_docente_tbldepartamento` FOREIGN KEY (`codDepartamento`) REFERENCES `tbldepartamentos` (`codDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblcargos_x_docente`
--

LOCK TABLES `tblcargos_x_docente` WRITE;
/*!40000 ALTER TABLE `tblcargos_x_docente` DISABLE KEYS */;
INSERT INTO `tblcargos_x_docente` VALUES (262,3,NULL,2,'2018-05-07 00:00:00','2018-05-07 00:00:00','IRPP'),(271,3,NULL,1,'2018-05-07 00:00:00','2019-05-07 00:00:00','HPU'),(92,1,NULL,2,'2018-05-07 00:00:00','2018-05-07 00:00:00','ITH'),(292,2,NULL,2,'2018-05-07 00:00:00','2018-05-07 00:00:00','ith'),(91,5,'0000-00-00',4,'2010-06-09 00:00:00','2015-07-14 00:00:00','ITLB');
/*!40000 ALTER TABLE `tblcargos_x_docente` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblcursos`
--

DROP TABLE IF EXISTS `tblcursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblcursos` (
  `codCurso` int(11) NOT NULL AUTO_INCREMENT,
  `codModalidad` int(11) NOT NULL,
  `nombreCurso` varchar(45) DEFAULT NULL,
  `numeroAsignaturas` int(11) DEFAULT NULL,
  `habilitado` int(11) DEFAULT NULL,
  PRIMARY KEY (`codCurso`),
  KEY `fk_tblCursos_tblModalidades1_idx` (`codModalidad`),
  CONSTRAINT `fk_tblCursos_tblModalidades1` FOREIGN KEY (`codModalidad`) REFERENCES `tblmodalidades` (`codModalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=491 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblcursos`
--

LOCK TABLES `tblcursos` WRITE;
/*!40000 ALTER TABLE `tblcursos` DISABLE KEYS */;
INSERT INTO `tblcursos` VALUES (1,1,'Primer Grado A',4,1),(11,1,'Segundo Grado',1,1),(21,1,'Rosalba',5,1),(81,3,'Primer curso',4,0),(91,3,'Segundo Curso',1,0),(101,3,'Tercer Curso',10,1),(252,4,'Primer Curso BCYL',6,1),(272,1,'Cuarto Grado',10,1),(282,1,'Quinto Grado',12,1),(292,1,'Sexto Grado',11,1),(302,1,'Primer Grado',10,1),(312,4,'Segundo Curso BCYL',1,1),(321,1,'prueba Curso con nue',0,NULL),(392,61,'El mejor curso',3,1),(401,2,'Primer Curso BTF',0,1),(411,1,'Primer Grado 1',0,1),(412,3,'Solo Matematicas',2,1),(421,3,'Curso agregando Asig',3,1),(431,61,'Seccion con Español',2,0),(432,61,'Fisica Especial',2,1),(441,61,'Primero BTI Matutino',3,1),(442,3,'prueba curso 5',1,1),(451,3,'prueba curso 6',2,1),(461,1,'Curso Avanzado',1,1),(471,3,'curso 123123',3,1),(481,2,'Segundo Curso BTF',1,1);
/*!40000 ALTER TABLE `tblcursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbldepartamentos`
--

DROP TABLE IF EXISTS `tbldepartamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldepartamentos` (
  `codDepartamento` int(11) NOT NULL AUTO_INCREMENT,
  `nombreDepartamento` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`codDepartamento`)
) ENGINE=InnoDB AUTO_INCREMENT=5 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldepartamentos`
--

LOCK TABLES `tbldepartamentos` WRITE;
/*!40000 ALTER TABLE `tbldepartamentos` DISABLE KEYS */;
INSERT INTO `tbldepartamentos` VALUES (1,'Matematicas'),(2,'Español'),(3,'Estudio Sociales'),(4,'Musica');
/*!40000 ALTER TABLE `tbldepartamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbldiasclase`
--

DROP TABLE IF EXISTS `tbldiasclase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldiasclase` (
  `codDiaClase` int(11) NOT NULL AUTO_INCREMENT,
  `semana` int(11) DEFAULT NULL,
  `fechaDia` datetime DEFAULT NULL,
  `habilitado` int(11) DEFAULT NULL,
  PRIMARY KEY (`codDiaClase`)
) ENGINE=InnoDB AUTO_INCREMENT=681 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldiasclase`
--

LOCK TABLES `tbldiasclase` WRITE;
/*!40000 ALTER TABLE `tbldiasclase` DISABLE KEYS */;
INSERT INTO `tbldiasclase` VALUES (2,1,'2018-02-05 00:00:00',1),(12,1,'2018-02-06 00:00:00',1),(22,1,'2018-02-07 00:00:00',1),(32,1,'2018-02-08 00:00:00',1),(42,1,'2018-02-09 00:00:00',1),(52,2,'2018-02-12 00:00:00',1),(53,2,'2018-02-13 00:00:00',1),(62,2,'2018-02-14 00:00:00',1),(72,2,'2018-02-15 00:00:00',1),(82,2,'2018-02-16 00:00:00',1),(92,3,'2018-02-19 00:00:00',1),(102,3,'2018-02-20 00:00:00',1),(112,3,'2018-02-21 00:00:00',1),(122,3,'2018-02-22 00:00:00',1),(132,3,'2018-02-23 00:00:00',1),(141,2,'2018-05-09 00:00:00',1),(142,4,'2018-05-10 00:00:00',1),(143,4,'2018-05-11 00:00:00',1),(144,4,'2018-05-12 00:00:00',0),(145,4,'2018-05-13 00:00:00',1),(151,NULL,'2018-05-14 00:00:00',1),(161,NULL,'2018-05-14 00:00:00',1),(171,NULL,'2018-05-14 00:00:00',1),(181,NULL,'2018-05-14 00:00:00',1),(191,NULL,'2018-05-14 00:00:00',1),(201,NULL,'2018-05-14 00:00:00',1),(211,NULL,'2018-05-14 00:00:00',1),(221,NULL,'2018-05-14 00:00:00',1),(231,NULL,'2018-05-14 00:00:00',1),(241,NULL,'2018-05-14 00:00:00',1),(251,NULL,'2018-05-14 00:00:00',1),(261,NULL,'2018-05-14 00:00:00',1),(271,NULL,'2018-05-14 00:00:00',1),(281,NULL,'2018-05-14 00:00:00',1),(291,NULL,'2018-05-14 00:00:00',1),(301,NULL,'2018-05-14 00:00:00',1),(311,NULL,'2018-05-14 00:00:00',1),(321,NULL,'2018-05-14 00:00:00',1),(331,NULL,'2018-05-14 00:00:00',1),(341,NULL,'2018-05-14 00:00:00',1),(351,NULL,'2018-05-14 00:00:00',1),(361,NULL,'2018-05-14 00:00:00',1),(371,NULL,'2018-05-14 00:00:00',1),(381,NULL,'2018-05-14 00:00:00',1),(391,NULL,'2018-05-14 00:00:00',1),(401,NULL,'2018-05-14 00:00:00',1),(411,NULL,'2018-05-14 00:00:00',1),(421,NULL,'2018-05-14 00:00:00',1),(431,NULL,'2018-05-14 00:00:00',1),(441,NULL,'2018-05-14 00:00:00',1),(451,NULL,'2018-05-14 00:00:00',1),(461,NULL,'2018-05-14 00:00:00',1),(471,NULL,'2018-05-14 00:00:00',1),(481,NULL,'2018-05-14 00:00:00',1),(491,NULL,'2018-05-14 00:00:00',1),(501,NULL,'2018-05-14 00:00:00',1),(511,NULL,'2018-05-14 00:00:00',1),(521,NULL,'2018-05-15 00:00:00',1),(531,NULL,'2018-05-15 00:00:00',1),(541,NULL,'2018-05-15 00:00:00',1),(551,NULL,'2018-05-15 00:00:00',1),(561,NULL,'2018-05-15 00:00:00',1),(571,NULL,'2018-05-15 00:00:00',1),(581,NULL,'2018-05-15 00:00:00',1),(591,NULL,'2018-05-15 00:00:00',1),(601,NULL,'2018-05-15 00:00:00',1),(611,NULL,'2018-05-15 00:00:00',1),(621,NULL,'2018-05-14 00:00:00',1),(631,NULL,'2018-05-14 00:00:00',1),(641,NULL,'2018-05-14 00:00:00',1),(651,NULL,'2018-05-14 00:00:00',1),(661,NULL,'2018-05-14 00:00:00',1),(671,NULL,'2018-05-15 00:00:00',1);
/*!40000 ALTER TABLE `tbldiasclase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbldocentes`
--

DROP TABLE IF EXISTS `tbldocentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbldocentes` (
  `codDocente` int(11) NOT NULL AUTO_INCREMENT,
  `numExpediente` varchar(25) DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  `habilitado` int(11) DEFAULT NULL,
  PRIMARY KEY (`codDocente`),
  KEY `fk_tblDocentes_tblPersona_idx` (`codDocente`),
  CONSTRAINT `fk_tblDocentes_tblPersona` FOREIGN KEY (`codDocente`) REFERENCES `tblpersonas` (`codPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=293 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbldocentes`
--

LOCK TABLES `tbldocentes` WRITE;
/*!40000 ALTER TABLE `tbldocentes` DISABLE KEYS */;
INSERT INTO `tbldocentes` VALUES (91,NULL,'2018-03-31',0),(92,NULL,'2018-04-14',0),(102,NULL,'2018-04-14',0),(112,NULL,'2018-04-14',1),(122,NULL,'2018-04-14',1),(132,NULL,'2018-04-18',1),(142,NULL,'2018-04-29',1),(152,NULL,'2018-04-29',1),(162,NULL,'2018-05-01',1),(211,NULL,'2018-05-07',1),(221,NULL,'2018-05-07',1),(222,NULL,'2018-05-07',1),(231,NULL,'2018-05-07',1),(232,NULL,'2018-05-07',1),(242,NULL,'2018-05-07',1),(251,NULL,'2018-05-07',1),(261,NULL,'2018-05-07',1),(262,NULL,'2018-05-07',1),(271,NULL,'2018-05-07',1),(281,NULL,'2018-05-07',0),(292,NULL,'2018-05-07',1);
/*!40000 ALTER TABLE `tbldocentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblestados`
--

DROP TABLE IF EXISTS `tblestados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblestados` (
  `codEstado` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codEstado`)
) ENGINE=InnoDB AUTO_INCREMENT=3 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblestados`
--

LOCK TABLES `tblestados` WRITE;
/*!40000 ALTER TABLE `tblestados` DISABLE KEYS */;
INSERT INTO `tblestados` VALUES (1,'Matriculado'),(2,'Suspendido');
/*!40000 ALTER TABLE `tblestados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblexperiencia`
--

DROP TABLE IF EXISTS `tblexperiencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblexperiencia` (
  `codExperiencia` int(11) NOT NULL AUTO_INCREMENT,
  `cargoDesempenado` varchar(45) DEFAULT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechaFin` date DEFAULT NULL,
  `areaEnsenanza` varchar(45) DEFAULT NULL,
  `institucion` varchar(50) DEFAULT NULL,
  `codDocente` int(11) NOT NULL,
  PRIMARY KEY (`codExperiencia`),
  KEY `fk_tblExperiencia_tblDocentes1_idx` (`codDocente`),
  CONSTRAINT `fk_tblExperiencia_tblDocentes1` FOREIGN KEY (`codDocente`) REFERENCES `tbldocentes` (`codDocente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblexperiencia`
--

LOCK TABLES `tblexperiencia` WRITE;
/*!40000 ALTER TABLE `tblexperiencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblexperiencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblhistorial`
--

DROP TABLE IF EXISTS `tblhistorial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblhistorial` (
  `fechaMatricula` date DEFAULT NULL,
  `codAlumno` int(11) NOT NULL,
  `codTraslado` int(11) DEFAULT NULL,
  `asignatura` varchar(25) DEFAULT NULL,
  `promedio` decimal(4,2) DEFAULT NULL,
  `comentario` varchar(20) DEFAULT NULL,
  `codAula` int(11) DEFAULT NULL,
  `codDocente` int(11) DEFAULT NULL,
  `codJornada` int(11) DEFAULT NULL,
  `codLectivo` int(11) DEFAULT NULL,
  `codPeriodo` int(11) DEFAULT NULL,
  `codAsignatura` int(11) DEFAULT NULL,
  `horaInicio` datetime DEFAULT NULL,
  `horaFin` datetime DEFAULT NULL,
  `curso` varchar(45) DEFAULT NULL,
  `codCurso` int(11) DEFAULT NULL,
  KEY `fk_tblHistorial_tblAlumnos1_idx` (`codAlumno`),
  KEY `fk_tblHistorial_tblTraslados1_idx` (`codTraslado`),
  CONSTRAINT `fk_tblHistorial_tblAlumnos1` FOREIGN KEY (`codAlumno`) REFERENCES `tblalumnos` (`codAlumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblHistorial_tblTraslados1` FOREIGN KEY (`codTraslado`) REFERENCES `tbltraslados` (`codTraslado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblhistorial`
--

LOCK TABLES `tblhistorial` WRITE;
/*!40000 ALTER TABLE `tblhistorial` DISABLE KEYS */;
INSERT INTO `tblhistorial` VALUES ('2017-02-12',102,NULL,'Fisica I',0.00,'Reprobado',1,92,11,11,11,51,'0000-00-00 14:00:00','0000-00-00 15:00:00','Segundo Grado',11),('2018-01-28',102,NULL,'ESPAÑOL GENERAL',0.00,'Reprobado',31,132,11,11,11,1,'0000-00-00 15:00:00','0000-00-00 16:00:00','Cuarto Grado',272),('0000-00-00',361,82,'Matematicas Especial',78.00,'asignatura para tras',NULL,NULL,11,1,21,141,'0000-00-00 00:00:00','0000-00-00 00:00:00','Primer curso',81),('0000-00-00',361,82,'ESPAÑOL GENERAL',89.00,'asignatura para tras',NULL,NULL,11,1,21,1,'0000-00-00 00:00:00','0000-00-00 00:00:00','prueba Curso con nue',331);
/*!40000 ALTER TABLE `tblhistorial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblincidencias`
--

DROP TABLE IF EXISTS `tblincidencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblincidencias` (
  `codIncidencia` int(11) NOT NULL AUTO_INCREMENT,
  `tituloIncidencia` varchar(25) DEFAULT NULL,
  `descripcion` varchar(300) DEFAULT NULL,
  `codTipoIncidencia` int(11) DEFAULT NULL,
  `codAlumno` int(11) DEFAULT NULL,
  `codDocente` int(11) DEFAULT NULL,
  `fecha` date DEFAULT NULL,
  `codPersonaReporta` int(11) DEFAULT NULL,
  `codPersonaReportada` int(11) DEFAULT NULL,
  PRIMARY KEY (`codIncidencia`),
  KEY `fk_tblIncidencias_tblTiposIncidencias1_idx` (`codTipoIncidencia`),
  KEY `fk_tblIncidencias_tblAlumnos1_idx` (`codAlumno`),
  KEY `fk_tblIncidencias_tblDocentes1_idx` (`codDocente`),
  KEY `fk_tblincidencias_tblpersonas1_idx` (`codPersonaReporta`),
  KEY `fk_tblincidencias_tblpersonas2_idx` (`codPersonaReportada`),
  CONSTRAINT `fk_tblIncidencias_tblAlumnos1` FOREIGN KEY (`codAlumno`) REFERENCES `tblalumnos` (`codAlumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblIncidencias_tblDocentes1` FOREIGN KEY (`codDocente`) REFERENCES `tbldocentes` (`codDocente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblIncidencias_tblTiposIncidencias1` FOREIGN KEY (`codTipoIncidencia`) REFERENCES `tbltiposincidencias` (`codTipoIncidencia`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblincidencias_tblpersonas1` FOREIGN KEY (`codPersonaReporta`) REFERENCES `tblpersonas` (`codPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblincidencias_tblpersonas2` FOREIGN KEY (`codPersonaReportada`) REFERENCES `tblpersonas` (`codPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=101 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblincidencias`
--

LOCK TABLES `tblincidencias` WRITE;
/*!40000 ALTER TABLE `tblincidencias` DISABLE KEYS */;
INSERT INTO `tblincidencias` VALUES (1,'Suspención de examen','Se suspendio examen por copiar en el aula',2,102,162,'2018-02-20',162,102),(2,'Reclamo no aceptado',NULL,2,102,162,'2018-03-01',102,162),(71,'hay pedo','paso algo f3o',2,851,92,'2018-05-02',NULL,NULL),(72,'asdas','qweqw',3,851,92,'2018-05-11',92,851),(81,'Entrada tarde','El alumno entró tard',2,391,132,'2018-03-25',132,391),(91,'Entrada tarde','El alumno entró tard',2,391,132,'2018-03-25',132,391);
/*!40000 ALTER TABLE `tblincidencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbljornadas`
--

DROP TABLE IF EXISTS `tbljornadas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbljornadas` (
  `codJornada` int(11) NOT NULL AUTO_INCREMENT,
  `jornada` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codJornada`)
) ENGINE=InnoDB AUTO_INCREMENT=31 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbljornadas`
--

LOCK TABLES `tbljornadas` WRITE;
/*!40000 ALTER TABLE `tbljornadas` DISABLE KEYS */;
INSERT INTO `tbljornadas` VALUES (1,'Matutina'),(11,'Vespertina'),(12,'Nocturna'),(21,'Diurna ');
/*!40000 ALTER TABLE `tbljornadas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbllectivos`
--

DROP TABLE IF EXISTS `tbllectivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbllectivos` (
  `codLectivo` int(11) NOT NULL AUTO_INCREMENT,
  `fechaInicio` date DEFAULT NULL,
  `fechaFinal` date DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codLectivo`)
) ENGINE=InnoDB AUTO_INCREMENT=33 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbllectivos`
--

LOCK TABLES `tbllectivos` WRITE;
/*!40000 ALTER TABLE `tbllectivos` DISABLE KEYS */;
INSERT INTO `tbllectivos` VALUES (1,'2016-02-02','2016-10-30','Francisco Morazan'),(11,'2013-02-01','2013-10-30','Formando ciudadano y ciudadanas para engrandecer honduras'),(12,'0000-00-00','0000-00-00','2019 test'),(22,'2018-02-08','2018-12-29','Año lectivo 2018'),(32,'0000-00-00','0000-00-00','Prócer José Cecilio del valle');
/*!40000 ALTER TABLE `tbllectivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblmatricula`
--

DROP TABLE IF EXISTS `tblmatricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblmatricula` (
  `codAlumno` int(11) NOT NULL,
  `codSeccion` int(11) NOT NULL,
  `promedio` decimal(6,2) DEFAULT NULL,
  `fechaMatricula` date DEFAULT NULL,
  `na1` decimal(4,2) DEFAULT NULL,
  `ne1` decimal(4,2) DEFAULT NULL,
  `nf1` decimal(4,2) DEFAULT NULL,
  `na2` decimal(4,2) DEFAULT NULL,
  `ne2` decimal(4,2) DEFAULT NULL,
  `nf2` decimal(4,2) DEFAULT NULL,
  `na3` decimal(4,2) DEFAULT NULL,
  `ne3` decimal(4,2) DEFAULT NULL,
  `nf3` decimal(4,2) DEFAULT NULL,
  `na4` decimal(4,2) DEFAULT NULL,
  `ne4` decimal(4,2) DEFAULT NULL,
  `nf4` decimal(4,2) DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`codAlumno`,`codSeccion`),
  KEY `fk_tblMatricula_tblAlumnos1_idx` (`codAlumno`),
  KEY `fk_tblMatricula_tblSecciones1_idx` (`codSeccion`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblmatricula`
--

LOCK TABLES `tblmatricula` WRITE;
/*!40000 ALTER TABLE `tblmatricula` DISABLE KEYS */;
INSERT INTO `tblmatricula` VALUES (102,162,90.00,'2018-01-28',0.00,1.00,1.00,1.00,1.00,NULL,1.00,1.00,1.00,1.00,1.00,NULL,'2018-05-15 23:21:29'),(102,172,0.00,'2017-02-12',10.00,0.00,0.00,0.00,0.00,10.00,0.00,0.00,0.00,0.00,0.00,NULL,'2018-05-15 14:35:41'),(102,251,NULL,'0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-15 19:06:43'),(102,301,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-15 03:26:21'),(291,361,0.00,'0000-00-00',0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'2018-05-15 01:33:29'),(361,182,NULL,'0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-15 04:17:46'),(361,192,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-15 01:52:20'),(361,311,90.00,NULL,40.00,90.00,90.00,90.00,90.00,90.00,90.00,90.00,90.00,90.00,90.00,90.00,'2018-05-15 14:49:12'),(372,172,0.00,'2018-01-28',10.00,0.00,0.00,0.00,0.00,10.00,0.00,0.00,0.00,0.00,0.00,NULL,'2018-05-15 14:35:41'),(372,182,NULL,'0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-15 04:19:01'),(382,162,96.00,'2018-01-28',0.00,0.00,0.00,0.00,0.00,NULL,0.00,0.00,0.00,0.00,0.00,NULL,'2018-05-15 23:21:29'),(391,172,0.00,'2018-01-28',0.00,0.00,0.00,0.00,0.00,NULL,0.00,0.00,0.00,0.00,0.00,NULL,'2018-05-15 14:35:41'),(391,311,0.00,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'2018-05-15 14:49:12'),(402,172,0.00,'2018-01-28',0.00,0.00,0.00,0.00,0.00,NULL,0.00,0.00,0.00,0.00,0.00,NULL,'2018-05-15 14:35:41'),(402,291,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'2018-05-15 03:15:02'),(612,172,0.00,'2018-01-28',0.00,0.00,0.00,0.00,0.00,NULL,0.00,0.00,0.00,0.00,0.00,NULL,'2018-05-15 14:35:41'),(612,311,0.00,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'2018-05-15 14:49:12'),(851,311,0.00,NULL,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,0.00,'2018-05-15 14:49:12');
/*!40000 ALTER TABLE `tblmatricula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblmodalidades`
--

DROP TABLE IF EXISTS `tblmodalidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblmodalidades` (
  `codModalidad` int(11) NOT NULL AUTO_INCREMENT,
  `modalidad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codModalidad`)
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblmodalidades`
--

LOCK TABLES `tblmodalidades` WRITE;
/*!40000 ALTER TABLE `tblmodalidades` DISABLE KEYS */;
INSERT INTO `tblmodalidades` VALUES (1,'Primaria '),(2,'Bachillerato Técnico en Finanzas'),(3,'Ciclo Comun Cultura general'),(4,'Bachillerato Ciencias y Letras'),(61,'Bachillerato Técnico en informática'),(71,'Pre-escolar  '),(81,'Nursery');
/*!40000 ALTER TABLE `tblmodalidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblperiodos`
--

DROP TABLE IF EXISTS `tblperiodos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblperiodos` (
  `codPeriodo` int(11) NOT NULL AUTO_INCREMENT,
  `nombrePeriodo` varchar(45) NOT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechafin` date DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  PRIMARY KEY (`codPeriodo`)
) ENGINE=InnoDB AUTO_INCREMENT=71 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblperiodos`
--

LOCK TABLES `tblperiodos` WRITE;
/*!40000 ALTER TABLE `tblperiodos` DISABLE KEYS */;
INSERT INTO `tblperiodos` VALUES (1,'Primer Bimestre','2006-02-09','2006-01-01',2010),(11,'Primer Semestre','2009-02-10','2009-10-25',2009),(12,'Periodo I','2018-02-01','2018-11-01',2018),(21,'Periodo escolar unico','2018-02-07','2018-10-30',2018);
/*!40000 ALTER TABLE `tblperiodos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblpersonas`
--

DROP TABLE IF EXISTS `tblpersonas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblpersonas` (
  `codPersona` int(11) NOT NULL AUTO_INCREMENT,
  `identificacion` varchar(25) DEFAULT NULL,
  `nombres` varchar(45) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `sexo` varchar(5) DEFAULT NULL,
  `paisOrigen` varchar(20) DEFAULT NULL,
  `nacionalidad` varchar(20) DEFAULT NULL,
  `departamentoNacimiento` varchar(45) DEFAULT NULL,
  `lugarNacimiento` varchar(45) DEFAULT NULL,
  `direccionActual` varchar(45) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `telefonoFijo` varchar(15) DEFAULT NULL,
  `correoElectronico` varchar(45) DEFAULT NULL,
  `tipoSangre` varchar(10) DEFAULT NULL,
  `efermedadesComunes` varchar(200) DEFAULT NULL,
  `condicionesEspeciales` varchar(200) DEFAULT NULL,
  `aspirante` int(11) DEFAULT NULL,
  `urlFotoPerfil` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=901 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblpersonas`
--

LOCK TABLES `tblpersonas` WRITE;
/*!40000 ALTER TABLE `tblpersonas` DISABLE KEYS */;
INSERT INTO `tblpersonas` VALUES (1,'0000-0000-00000','Administrador','Master','0000-00-00',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,0,NULL),(11,'0801-1480-84187','Stuart Emerson','Conley','2000-12-06',15,'F','Honduras','Hondureñas','FranciscoMorazan','MDC','4232 Amet Rd.','9958-2966','2223-6075','Donec.est.mauris@adlitoratorquent.ca','B','Ninguna','Ninguna',1,'img/1.png'),(21,'0801-1480-84187','Jose Antonio','Lopez','2000-12-06',15,'F','Honduras','Hondureña','FranciscoMorazan','MDC','4232 Amet Rd.','9958-2966','2223-6075','Donec.est.mauris@adlitoratorquent.ca','B','Ninguna','Ninguna',1,'img/1.png'),(31,'0801-1480-84187','Allan Noel','Lopez','2000-12-06',15,'F','Honduras','Hondureñas','FranciscoMorazan','MDC','4232 Amet Rd.','9958-2966','2223-6075','Donec.est.mauris@adlitoratorquent.ca','B','Ninguna','Ninguna',1,'img/1.png'),(41,'0801-1480-84187','Hanier Isaias','Rodas','2000-12-06',15,'F','Honduras','Hondureñas','FranciscoMorazan','MDC','4232 Amet Rd.','9958-2966','2223-6075','Donec.est.mauris@adlitoratorquent.ca','B','Ninguna','Ninguna',1,'img/1.png'),(51,'1234-1234-12345','Jose Antonio','Lopez','0000-00-00',NULL,'','paisorigen1','nacionalidad1','','','direccion 1','','2222-2222','prueba@prueba.com','','','',NULL,'img/imagen1'),(91,'0801-1980-12345','Carlos Miguel','Cruz','0000-00-00',30,'M','paisorigen1','nacionalidad1','Francisco Morazán','Distrito Central, te','direccion1','9892-9812','2222-2222','docente01@gmail.com','A negativo',NULL,'ninguna',NULL,''),(92,'1234.1234-1234','Jesus','Morales Jimenez','1993-12-12',21,'M','Honduras ','Hondureño','Departamento','0',NULL,'9989-3467','1234-1234','JesusMM@asd.com','O negativo',NULL,'ninguna',NULL,''),(102,'1234-1234-12234','Axel','Lopez Torres','1993-12-12',23,'M','Honduras ','Hondureño','Francisco Morazan','0','Col Suyapa, sector 3','1233-1234','1234-1234','asd@asd.com','O negativo',NULL,'ninguna',0,''),(112,'1234.1234-1234','Edgardo','Flores','1993-12-12',23,'M','Honduras ','Hondureño','Francisco Morazan','0','Col Suyapa, sector 3','1233-1234','1234-1234','asd@asd.com','O negativo',NULL,'ninguna',NULL,''),(122,'1234.1234-1234','Blanca A','Ordoñez ','1993-12-12',23,'M','Honduras ','Hondureño','Francisco Morazan','MDC','Col Suyapa, sector 3','1233-1234','1234-1234','blancao@asd.com','O negativo',NULL,'ninguna',NULL,''),(132,'0801-1980-12345','Mario Roberto','Suazo Perez','1980-02-12',32,'M','Honduras','Hondureña','Fco Morazan','Tegucigalpa','A la par d eun lado','9900-2122','2234-1222','mariorr@algo.com','O negativo',NULL,NULL,NULL,''),(142,'0801-1980-12345','Carlos Miguel','Barrientos','0000-00-00',28,'M','paisorigen1','nacionalidad1','Fco Morazan','','direccion1','','2222-2222','docente01@gmail.com','',NULL,'',NULL,''),(152,'0802-1990-90897','Juana Maria','Sarmiento','1990-04-02',31,'F','Honduras','Hondureña','Cortés','San Pedro Sula',NULL,'9812-1233',NULL,'juanaSM@gmail.com','A positivo',NULL,NULL,NULL,''),(162,'1234-1234-12345','Mirna Yadira','Puerto','1904-12-12',21,'M','pais1','nacionalidad1','departemento1','municipio 1','calle 1','1234-1234','1234-1234','asd@asd.com','O negativo',NULL,'NA',NULL,''),(172,'0801-1970-12345','Maria Jose ','Perez Seledon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9987-1234','','majoseledon@algo.com',NULL,NULL,NULL,NULL,NULL),(182,'0801-1970-12345','Maria Jose ','Perez Seledon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9987-1234','','majoseledon@algo.com',NULL,NULL,NULL,NULL,NULL),(192,'0801-1975-09872','Juan Anastasio','Torres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9967-3456','','dr.juan@algo.com',NULL,NULL,NULL,NULL,NULL),(201,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(211,'1233-1233-12312','docente con cargos','prueba','2018-01-31',21,'M','adas','asdas','asdas','asdas','asdsad','1232-1231','1231-1233','asdas@asd.com','O negativo',NULL,NULL,NULL,''),(221,'1233-1233-12312','docente con cargos','prueba','2018-01-31',21,'M','adas','asdas','asdas','asdas','asdsad','1232-1231','1231-1233','asdas@asd.com','O negativo',NULL,NULL,NULL,''),(222,'1233-1233-12312','docente con cargos 1','prueba','2018-01-31',21,'M','adas','asdas','asdas','asdas','asdsad','1232-1231','1231-1233','asdas@asd.com','O negativo',NULL,NULL,NULL,''),(231,'1233-1233-12333','docente cargos 3','asdas','0002-03-12',21,'M','asdasd','asdasd','asdasd','asdasd','asdasd','12312323','123123','asdas@asdas.com','O positivo',NULL,NULL,NULL,''),(232,'1233-1233-12333','docente 4','asdas','2012-02-03',21,'M','asdasd','asdasd','asdasd','asdasd','asdasd','12312323','123123','asdas@asdas.com','O positivo',NULL,NULL,NULL,''),(242,'0801-1990-45668','Matías ','Vega','2012-02-03',28,'M','asdasd','asdasd','asdasd','asdasd','asdasd','12312323','123123','asdas@asdas.com','O positivo',NULL,NULL,NULL,''),(251,'1233-1233-12333','docente 6','asdas','2012-02-03',21,'M','asdasd','asdasd','asdasd','asdasd','asdasd','12312323','123123','asdas@asdas.com','O positivo',NULL,NULL,NULL,''),(261,'0801-1233-76811','Francisco Jovito ','Leiva Juanes','2012-02-03',21,'M','asdasd','asdasd','asdasd','asdasd','asdasd','12312323','123123','asdas@asdas.com','O positivo',NULL,NULL,NULL,''),(262,'1233-1233-12333','docente 8','asdas','2012-02-03',21,'M','asdasd','asdasd','asdasd','asdasd','asdasd','12312323','123123','asdas@asdas.com','O positivo',NULL,NULL,NULL,''),(271,'1233-1233-12333','Alfredo ','Lopez','2012-02-03',21,'M','asdasd','asdasd','asdasd','asdasd','asdasd','12312323','123123','asdas@asdas.com','O positivo',NULL,NULL,NULL,''),(281,'0801-1980-12345','Jose miguel ','Cañadas','0000-00-00',30,'M','paisorigen1','nacionalidad1','Francisco Morazán','Distrito Central, te','direccion1','1233-1233','2222-2222','docente01@gmail.com','A negativo',NULL,'ninguna',NULL,''),(291,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL),(292,'1231-1231-12321','prueba hanier','docente','1993-05-07',21,'M','pais1','nacionalidad1','dep 1','mun 1','asdasd','1233-1233','1233-1233','asdas@asdsa.com','O positivo',NULL,NULL,NULL,''),(301,'1233-1233-1233','jose feliciano','cordova','2003-05-08',12,'M','pais1','mun1','dep1','mun1','asdsad','12321-1231','1231-123','asdas@asda','O positivo',NULL,NULL,NULL,NULL),(311,'123213','asdasa','sdsadsa','2018-05-08',21,'M','asdasas','dsad','asdsaas','dsad','direcion1','asdsa.com',NULL,'asdasd@asdas.com','A positivo',NULL,NULL,NULL,NULL),(312,'123213','asdasa','sdsadsa','2018-05-08',21,'M','asdasas','dsad','asdsaas','dsad','direcion1','asdsa.com',NULL,'asdasd@asdas.com','A positivo',NULL,NULL,NULL,NULL),(322,'123213','asdasa','sdsadsa','2018-05-08',21,'M','asdasas','dsad','asdsaas','dsad','direcion1','asdsa.com',NULL,'asdasd@asdas.com','A positivo',NULL,NULL,NULL,NULL),(331,'123213','asdasa','sdsadsa','2018-05-08',21,'M','asdasas','dsad','asdsaas','dsad','direcion1','asdsa.com',NULL,'asdasd@asdas.com','A positivo','asda','asds',NULL,NULL),(332,'123213','asdasa','sdsadsa','2018-05-08',21,'M','asdasas','dsad','asdsaas','dsad','direcion1','123211','2321','asdasd@asdas.com','A positivo','asda','asds',NULL,NULL),(342,'1233-1233-1233','Mario Saul','Cordova','2018-05-08',21,'M','pais1','municipio1','departemento1','municipio1','direccion 1','1233-1233','1233-1233','sds@asd.com','A positivo','dsaa','sds',NULL,''),(352,'1233-1233-1233','Mario Saul','Cordova','2018-05-08',21,'M','pais1','municipio1','departemento1','municipio1','direccion 1','1233-1233','1233-1233','sds@asd.com','A positivo','dsaa','sds',NULL,''),(361,'1233-1233-12336','Mario Saul','Cordova','2018-05-08',21,'M','pais1','municipio1','departemento1','municipio1','direccion 1','1233-1233','1233-1233','sds@asd.com','A positivo','dsaa','sds',0,''),(371,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,''),(372,NULL,'Rosa Maria','Perez','0000-00-00',21,'F','pasi1','mun1','dep1','mun1','asda','233-133','1233-123','asda@asdas.com1','A negativo',NULL,'asdsa',0,''),(382,NULL,'Allan ','brito','0000-00-00',21,'M','pasi1','mun1','dep1','mun1','asds','1233-1233','1233-1233','asd@asd.com','O negativo',NULL,'sdas',0,''),(391,NULL,'Jose','Rivas','0000-00-00',21,'M','pasi1','mun1','dep1','mun1','asds','1233-1233','1233-1233','asd@asd.com','O negativo',NULL,'sdas',0,''),(401,'12323','Fernando Ricon','Apellido 1',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1233-1233',NULL,'asdsa@asd.com',NULL,NULL,NULL,NULL,NULL),(402,'1234-1234-12345','hanier isaias','rodas','2018-05-08',21,'M','pais1','mun1','dep1','mun1','col1','1234-1245','1234-1234','asd@asd.com','O negativo','asd','con1',0,''),(412,'1234-1234-12345','Alan','brito',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,'corre1',NULL,NULL,NULL,NULL,NULL),(421,'1234','alanbrito','lopez',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,'asd@asd.com',NULL,NULL,NULL,NULL,NULL),(431,NULL,'alanbrito','lopez',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(432,NULL,'alanbrito','lopez',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(441,NULL,'Maria Jose ','Perez Seledon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9987-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(451,NULL,'Juan Anastasio','Torres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9967-3456',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(452,NULL,'Maria Jose ','Perez Seledon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9987-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(461,NULL,'Juan Anastasio','Torres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9967-3456',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(471,'1234-1234-12345','asd','asd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,'asd@asd.com',NULL,NULL,NULL,NULL,NULL),(472,NULL,'Maria Jose ','Perez Seledon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9987-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(482,NULL,'Juan Anastasio','Torres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9967-3456',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(491,NULL,'Maria Jose ','Perez Seledon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9987-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(501,NULL,'Juan Anastasio','Torres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9967-3456',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(502,'1234-1234-12344','aasdasdas','asdasdas',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,'asd@asd.com',NULL,NULL,NULL,NULL,NULL),(512,'1234-1234-12344','asd','asd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,'asd@asd.com',NULL,NULL,NULL,NULL,NULL),(522,NULL,'Juan Anastasio','Torres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9967-3456',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(532,NULL,'Maria Jose ','Perez Seledon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9987-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(541,'1234-1234-12345','asd','asd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1233-4123',NULL,'asd@as.com',NULL,NULL,NULL,NULL,NULL),(542,NULL,'alanbrito','lopez',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(551,'1234-1234-12345','asd','asd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1233-4123',NULL,'asd@as.com',NULL,NULL,NULL,NULL,NULL),(552,NULL,'alanbrito','lopez',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(561,NULL,'alanbrito','lopez',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(562,NULL,'asd','asd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1233-4123',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(571,NULL,'asd','asd',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1233-4123',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(572,NULL,'alanbrito','lopez',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(582,NULL,'alanbrito','lopez',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(591,NULL,'Juan Anastasio','Torres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9967-3456',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(592,NULL,'Maria Jose ','Perez Seledon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9987-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(601,NULL,'Juan Anastasio','Torres',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9967-3456',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(602,NULL,'Maria Jose ','Perez Seledon',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9987-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(612,'0897-1997-08731','Norma','Moya','1997-07-17',18,'F','Honduras','Tegucigalpa','Francisco Morazán','Tegucigalpa','Col. Morazan','9782-1893','2019-0823','normita12@yahoo.com','A positivo',NULL,NULL,NULL,''),(622,'9803-1982-87391','Enma ','Morales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9832-1883',NULL,'enmamora@gmail.com',NULL,NULL,NULL,NULL,NULL),(632,'0897-1997-08731','Norma','Moya','1997-07-17',18,'F','Honduras','Tegucigalpa','Francisco Morazán','Tegucigalpa','Col. Morazan','9782-1893','2019-0823','normita12@yahoo.com','A positivo',NULL,NULL,NULL,''),(642,'9803-1982-87391','Enma ','Morales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9832-1883',NULL,'enmamora@gmail.com',NULL,NULL,NULL,NULL,NULL),(652,'0803-1997-97731','Juan','Rosales','1997-08-13',21,'M','Honduras','Tegucigalpa','Francisco Morazán','Tegucigalpa','Col. Quezada','9822-1881','2222-2222','juan78mora@gmail.com','B negativo',NULL,NULL,NULL,''),(662,'9882-1977-17721','Ricardo','Sosa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9733-1938',NULL,'richisos79@yahoo.com',NULL,NULL,NULL,NULL,NULL),(671,'9883-1998-63627','Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,'josecanales@yahoo.com',NULL,NULL,NULL,NULL,NULL),(681,'9883-1998-63627','Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,'josecanales@yahoo.com',NULL,NULL,NULL,NULL,NULL),(691,'9883-1998-63627','Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,'josecanales@yahoo.com',NULL,NULL,NULL,NULL,NULL),(701,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(702,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(711,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(721,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(731,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(741,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(751,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(752,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(761,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(771,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(781,NULL,'Jose','Canales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9838-8271',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(791,NULL,'Ricardo','Sosa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9733-1938',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(792,NULL,'Ricardo','Sosa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9733-1938',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(802,NULL,'Ricardo','Sosa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9733-1938',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(812,NULL,'Ricardo','Sosa',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9733-1938',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(821,NULL,'Enma ','Morales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9832-1883',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(831,NULL,'Enma ','Morales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9832-1883',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(832,NULL,'Enma ','Morales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9832-1883',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(841,NULL,'Enma ','Morales',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'9832-1883',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(851,'1234-1234-12345','Jose Fabricio','Medina','2018-05-11',21,'M','honduras','municipio1 ','departamento 1','municipio1 ','direccion 1','1234-1234','1234-1234','asd@asd.com','O negativo','ninguno','ninguna',0,''),(861,'1234-1234-12345','Rosa Maria','Medina',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,'asd@asd.com',NULL,NULL,NULL,NULL,NULL),(871,NULL,'Rosa Maria','Medina',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(881,NULL,'Rosa Maria','Medina',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL),(891,NULL,'Rosa Maria','Medina',NULL,NULL,NULL,NULL,NULL,NULL,NULL,NULL,'1234-1234',NULL,NULL,NULL,NULL,NULL,NULL,NULL);
/*!40000 ALTER TABLE `tblpersonas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblsecciones`
--

DROP TABLE IF EXISTS `tblsecciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblsecciones` (
  `codSeccion` int(11) NOT NULL AUTO_INCREMENT,
  `seccion` varchar(20) DEFAULT NULL,
  `horaInicio` datetime DEFAULT NULL,
  `horaFin` datetime DEFAULT NULL,
  `codAula` int(11) NOT NULL,
  `codDocente` int(11) DEFAULT NULL,
  `codPeriodo` int(11) NOT NULL,
  `codLectivo` int(11) NOT NULL,
  `codCurso` int(11) NOT NULL,
  `codAsignatura` int(11) NOT NULL,
  `suspendida` int(11) DEFAULT NULL,
  `codJornada` int(11) NOT NULL,
  PRIMARY KEY (`codSeccion`),
  KEY `fk_tblSecciones_tblAulas1_idx` (`codAula`),
  KEY `fk_tblSecciones_tblPeriodos1_idx` (`codPeriodo`),
  KEY `fk_tblSecciones_tblJornadas1_idx` (`codJornada`),
  KEY `fk_tblsecciones_tblDocente1_idx` (`codDocente`),
  KEY `fk_tblsecciones_tblLectivos_idx` (`codLectivo`),
  CONSTRAINT `fk_tblSecciones_tblAulas1` FOREIGN KEY (`codAula`) REFERENCES `tblaulas` (`codAula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblSecciones_tblJornadas1` FOREIGN KEY (`codJornada`) REFERENCES `tbljornadas` (`codJornada`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblSecciones_tblPeriodos1` FOREIGN KEY (`codPeriodo`) REFERENCES `tblperiodos` (`codPeriodo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblsecciones_tblLectivos` FOREIGN KEY (`codLectivo`) REFERENCES `tbllectivos` (`codLectivo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=321 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblsecciones`
--

LOCK TABLES `tblsecciones` WRITE;
/*!40000 ALTER TABLE `tblsecciones` DISABLE KEYS */;
INSERT INTO `tblsecciones` VALUES (142,'ESPAÑOL GENERAL ','0000-00-00 10:00:00','0000-00-00 13:00:00',11,102,11,1,272,51,0,1),(152,'Matematicas septimo','0000-00-00 16:00:00','0000-00-00 17:00:00',31,132,11,11,21,141,0,11),(162,'ESPAÑOL GENERAL 1500','0000-00-00 15:00:00','0000-00-00 16:00:00',31,132,11,11,272,1,1,11),(172,'Fisica I segundo gra','0000-00-00 14:00:00','0000-00-00 15:00:00',1,92,11,11,11,51,1,11),(182,'ESPAÑOL GENERAL 15:0','0000-00-00 15:00:00','0000-00-00 16:00:00',11,112,1,11,11,1,0,1),(192,'Español 1001','0000-00-00 10:00:00','0000-00-00 11:00:00',132,91,1,1,11,1,1,1),(202,'Matematicas septimo ','0000-00-00 00:00:00','0000-00-00 00:00:00',1,91,11,1,21,11,1,1),(212,'Matematicas septimo ','0000-00-00 00:00:00','0000-00-00 00:00:00',1,91,1,1,11,11,0,1),(221,'Matematicas septimo ','0000-00-00 00:00:00','0000-00-00 00:00:00',11,92,11,11,11,11,0,1),(222,'secciondeprueba','0000-00-00 00:00:00','0000-00-00 00:00:00',31,162,11,11,21,11,0,11),(231,'Seccion de prueba 2','0000-00-00 00:00:00','0000-00-00 00:00:00',11,92,11,12,1,1,0,1),(241,'Seccion para hora','0000-00-00 00:00:00','0000-00-00 00:00:00',31,92,12,11,11,1,0,11),(251,'Seccion de hora 2','0000-00-00 01:00:00','0000-00-00 02:00:00',32,162,12,1,21,1,0,1),(261,'Matematicas septimo','0000-00-00 08:00:00','0000-00-00 09:00:00',31,132,12,11,441,162,0,1),(271,'ESPAÑOL GENERAL segu','0000-00-00 08:00:00','0000-00-00 09:00:00',31,112,12,11,11,11,1,1),(281,'ESPAÑOL GENERAL terc','0000-00-00 12:00:00','0000-00-00 13:00:00',31,92,12,11,21,1,0,11),(291,'seccion 13:00','0000-00-00 13:00:00','0000-00-00 14:00:00',32,152,21,22,11,1,0,11),(301,'Agropecuaria 1 1000','0000-00-00 10:00:00','0000-00-00 11:00:00',132,92,1,11,312,41,0,1),(311,'Agropecuaria 1 1000','0000-00-00 10:00:00','0000-00-00 11:00:00',132,92,1,11,312,41,0,1);
/*!40000 ALTER TABLE `tblsecciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltiposincidencias`
--

DROP TABLE IF EXISTS `tbltiposincidencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltiposincidencias` (
  `codTipoIncidencia` int(11) NOT NULL AUTO_INCREMENT,
  `tipoIncidencia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codTipoIncidencia`)
) ENGINE=InnoDB AUTO_INCREMENT=4 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltiposincidencias`
--

LOCK TABLES `tbltiposincidencias` WRITE;
/*!40000 ALTER TABLE `tbltiposincidencias` DISABLE KEYS */;
INSERT INTO `tbltiposincidencias` VALUES (1,'Detención'),(2,'suspensión'),(3,'Expulsión');
/*!40000 ALTER TABLE `tbltiposincidencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltiposusuarios`
--

DROP TABLE IF EXISTS `tbltiposusuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltiposusuarios` (
  `codTipoUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `tipoUsuario` varchar(45) DEFAULT NULL,
  `accesos` varchar(20) NOT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  `habilitado` int(11) DEFAULT NULL,
  PRIMARY KEY (`codTipoUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=151 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltiposusuarios`
--

LOCK TABLES `tbltiposusuarios` WRITE;
/*!40000 ALTER TABLE `tbltiposusuarios` DISABLE KEYS */;
INSERT INTO `tbltiposusuarios` VALUES (1,'Administrador','ad,ep,mr,ga','2018-03-06 00:00:00','2018-04-30 23:58:40',1),(2,'Docente','ep,mr','2018-03-07 00:00:00','2018-05-09 21:50:40',1),(3,'Sistema','mr','2018-03-09 00:00:00','2018-05-14 21:12:29',1),(31,'Estudiante','ep','2018-04-04 01:13:11','2018-05-15 09:15:37',1),(141,'Tutor','ep,mr','2018-05-15 08:20:33','0000-00-00 00:00:00',1);
/*!40000 ALTER TABLE `tbltiposusuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltitulos`
--

DROP TABLE IF EXISTS `tbltitulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltitulos` (
  `codTitulo` int(11) NOT NULL AUTO_INCREMENT,
  `nombreTitulo` varchar(50) DEFAULT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechaFinal` date DEFAULT NULL,
  `areaEnsenanza` varchar(45) DEFAULT NULL,
  `institucion` varchar(45) DEFAULT NULL,
  `codDocente` int(11) NOT NULL,
  PRIMARY KEY (`codTitulo`),
  KEY `fk_tblTitulos_tblDocentes1_idx` (`codDocente`),
  CONSTRAINT `fk_tblTitulos_tblDocentes1` FOREIGN KEY (`codDocente`) REFERENCES `tbldocentes` (`codDocente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltitulos`
--

LOCK TABLES `tbltitulos` WRITE;
/*!40000 ALTER TABLE `tbltitulos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tbltitulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltraslados`
--

DROP TABLE IF EXISTS `tbltraslados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltraslados` (
  `codTraslado` int(11) NOT NULL AUTO_INCREMENT,
  `codAlumno` int(11) NOT NULL,
  `institucion` varchar(45) DEFAULT NULL,
  `fechaFinal` date DEFAULT NULL,
  `fechaTraslado` date DEFAULT NULL,
  `promedioTraslado` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`codTraslado`),
  KEY `fk_tblTraslados_tblAlumnos1_idx` (`codAlumno`),
  CONSTRAINT `fk_tblTraslados_tblAlumnos1` FOREIGN KEY (`codAlumno`) REFERENCES `tblalumnos` (`codAlumno`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=91 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltraslados`
--

LOCK TABLES `tbltraslados` WRITE;
/*!40000 ALTER TABLE `tbltraslados` DISABLE KEYS */;
INSERT INTO `tbltraslados` VALUES (2,361,'ITH','2018-05-11','2018-05-11',89.00),(12,361,'hpu','2018-05-11','2018-05-11',78.00),(22,361,'hpu','2018-05-11','2018-05-11',78.00),(82,361,'hpu','2018-05-11','2018-05-11',78.00);
/*!40000 ALTER TABLE `tbltraslados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tbltutores`
--

DROP TABLE IF EXISTS `tbltutores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tbltutores` (
  `codTutor` int(11) NOT NULL,
  `codAlumno` int(11) NOT NULL,
  `parentesco` varchar(20) DEFAULT NULL,
  `ocupacion` varchar(20) DEFAULT NULL,
  `lugarDeTrabajo` varchar(20) DEFAULT NULL,
  `telefonoTrabajo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`codTutor`),
  KEY `fk_tblTutores_tblPersona1_idx` (`codTutor`),
  KEY `fk_tblTutores_tblAlumnos1_idx` (`codAlumno`),
  CONSTRAINT `fk_tblTutores_tblAlumnos1` FOREIGN KEY (`codAlumno`) REFERENCES `tblalumnos` (`codAlumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblTutores_tblPersona1` FOREIGN KEY (`codTutor`) REFERENCES `tblpersonas` (`codPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tbltutores`
--

LOCK TABLES `tbltutores` WRITE;
/*!40000 ALTER TABLE `tbltutores` DISABLE KEYS */;
INSERT INTO `tbltutores` VALUES (401,391,'padre',NULL,'trabj1','12345'),(582,402,'papa',NULL,NULL,'1234-1234'),(592,102,'Madre','Ama de casa',NULL,'2291-0099'),(602,102,'Madre','Ama de casa',NULL,'2291-0099'),(812,652,'Padre',NULL,NULL,'2091-9812'),(832,632,'Madre',NULL,NULL,'2987-1982'),(841,612,'Madre',NULL,NULL,'2987-1982'),(891,851,'madre',NULL,NULL,'1234-1234');
/*!40000 ALTER TABLE `tbltutores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblusuarios`
--

DROP TABLE IF EXISTS `tblusuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblusuarios` (
  `codUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombreUsuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(200) DEFAULT NULL,
  `codPersona` int(11) NOT NULL,
  `codTipoUsuario` int(11) NOT NULL,
  `habilitado` int(11) DEFAULT NULL,
  `fechaCreacion` datetime DEFAULT NULL,
  `fechaModificacion` datetime DEFAULT NULL,
  PRIMARY KEY (`codUsuario`),
  KEY `fk_tblUsuarios_tblPersona1_idx` (`codPersona`),
  KEY `fk_tblUsuarios_tblTiposUsuarios1_idx` (`codTipoUsuario`),
  CONSTRAINT `fk_tblUsuarios_tblPersona1` FOREIGN KEY (`codPersona`) REFERENCES `tblpersonas` (`codPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblUsuarios_tblTiposUsuarios1` FOREIGN KEY (`codTipoUsuario`) REFERENCES `tbltiposusuarios` (`codTipoUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=761 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblusuarios`
--

LOCK TABLES `tblusuarios` WRITE;
/*!40000 ALTER TABLE `tblusuarios` DISABLE KEYS */;
INSERT INTO `tblusuarios` VALUES (1,'admin','admin',1,1,1,'2018-02-06 00:00:00','2018-05-01 00:26:55'),(41,'allan','asd',31,3,0,'2018-02-08 00:00:00','2018-04-30 23:20:53'),(51,'hanier','hanier',41,2,1,'2018-02-05 00:00:00','2018-05-13 21:26:22'),(601,'lopezj','lopezj',21,31,1,'2018-04-02 21:46:40','2018-05-15 08:27:25'),(611,'carloscr edited','cruzc',91,2,1,'2018-04-02 21:51:29','2018-05-14 21:09:41'),(621,'haniertest','asd',41,3,1,'2018-04-02 21:57:23','2018-05-03 20:23:21'),(651,'StuartEC editado','asd',11,2,0,'2018-04-05 01:41:13','2018-04-30 23:20:58'),(661,'conleys','asd123',11,31,1,'2018-04-06 23:29:41','2018-05-03 08:29:04'),(672,'Hanier39','1234',1,1,1,'2018-04-18 22:46:25','2018-04-18 22:46:25'),(682,'blancaO','asd',122,2,0,'2018-04-19 01:08:06','2018-04-30 23:20:57'),(692,'LopezA','LopezA',102,2,1,'2018-05-02 11:10:16','2018-05-02 11:10:16'),(701,'SarmientoJ','sarmientoj',152,2,1,'2018-05-02 11:34:31','2018-05-14 20:57:54'),(711,'jimenezj','asd.123',92,2,1,'2018-05-02 15:59:28','2018-05-15 17:24:15'),(712,'marioR','asd',132,2,1,'2018-05-08 23:58:17','2018-05-08 23:58:17'),(721,'Carlos','1234',1,31,1,'2018-05-14 09:10:11','2018-05-14 09:10:11'),(731,'perezma','perezma',172,141,1,'2018-05-15 08:23:28','2018-05-15 08:23:28'),(741,'Axel','axel',102,31,1,'2018-05-15 09:07:44','2018-05-15 09:07:44'),(751,'Alfredo','alfredo',271,31,1,'2018-05-15 13:00:20','2018-05-15 13:00:20');
/*!40000 ALTER TABLE `tblusuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `temp_tbl_alumnos_seccion`
--

DROP TABLE IF EXISTS `temp_tbl_alumnos_seccion`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `temp_tbl_alumnos_seccion` (
  `index` int(11) DEFAULT NULL,
  `codAlumno` int(11) DEFAULT NULL,
  `codSeccion` int(11) DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `temp_tbl_alumnos_seccion`
--

LOCK TABLES `temp_tbl_alumnos_seccion` WRITE;
/*!40000 ALTER TABLE `temp_tbl_alumnos_seccion` DISABLE KEYS */;
INSERT INTO `temp_tbl_alumnos_seccion` VALUES (1,102,162),(2,382,162),(1,102,162),(1,382,162);
/*!40000 ALTER TABLE `temp_tbl_alumnos_seccion` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping routines for database 'heroku_39a7bb7a85bd88d'
--
/*!50003 DROP PROCEDURE IF EXISTS `sp_delete_asignatura_x_curso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_delete_asignatura_x_curso`( 
                                in codCurso int(11)
							 )
BEGIN     

DELETE FROM tblasignaturas_x_curso 
WHERE tblasignaturas_x_curso.codCurso= codCurso;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_delete_cargo_docente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_delete_cargo_docente`(
        in codDocente int (11)
)
BEGIN

	DELETE FROM tblcargos_x_docente
	WHERE tblcargos_x_docente.codDocente=codDocente;

	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_delete_matricula` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_delete_matricula`(
        in codAlumno int (11),
        in codSeccion int(11),
        in fechaModificaicon varchar(15)
)
BEGIN

DELETE FROM heroku_39a7bb7a85bd88d.tblmatricula
WHERE heroku_39a7bb7a85bd88d.tblmatricula.codAlumno=codAlumno 
and heroku_39a7bb7a85bd88d.tblmatricula.codSeccion=codSeccion(11) 
and date_format(heroku_39a7bb7a85bd88d.tblmatricula.fechaModificacionm,'%Y-%m-%d')=fechaModificacion;

	
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_delete_tutores_alumno` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_delete_tutores_alumno`(in codAlumno int(11))
BEGIN

DELETE FROM heroku_39a7bb7a85bd88d.tbltutores
WHERE tbltutores.codAlumno=codAlumno;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_alumno` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_alumno`( 
									in codAlumno varchar(20)
							 )
BEGIN     
	if codAlumno=0 then
	SELECT 	A.codAlumno,
		A.numExpediente,
		A.indiceGeneral,
		A.traslado,
		date_format(A.fechaCreacion, '%m-%d-%Y %H:%i%s') fechaCreacion,
        date_format(A.fechaModificacion,'%m-%d-%Y %H:%i') fechaModificacion,
		B.codPersona,
		B.identificacion ,
		B.nombres as nombreAlumno,
		B.apellidos as apellidoAlumno,
		date_format(B.fechaNacimiento ,'%m-%d-%Y %H:%i') as fechaNacimiento,
		B.edad as edadAlumno,
		B.sexo as sexoAlumno,
		B.paisOrigen,
		B.nacionalidad,
		B.departamentoNacimiento,
		B.lugarNacimiento,
		B.direccionActual,
		B.celular,
		B.telefonoFijo,
		B.correoElectronico,
		B.tipoSangre,
		B.efermedadesComunes,
		B.condicionesEspeciales,
		B.aspirante,
		B.urlFotoPerfil   
	FROM heroku_39a7bb7a85bd88d.tblalumnos as A
	Inner join heroku_39a7bb7a85bd88d.tblpersonas as B
	on (A.codAlumno=B.codPersona)  ;
    
    elseif codAlumno>0 then 
    SELECT 	A.codAlumno,
		A.numExpediente,
		A.indiceGeneral,
		A.traslado,
		date_format(A.fechaCreacion, '%m-%d-%Y %H:%i%s') fechaCreacion,
        date_format(A.fechaModificacion,'%m-%d-%Y %H:%i') fechaModificacion,
		B.codPersona,
		B.identificacion,
		B.nombres as nombreAlumno,
		B.apellidos as apellidoAlumno,
		date_format(B.fechaNacimiento ,'%Y-%m-%d') as fechaNacimiento,
		B.edad as edadAlumno,
		B.sexo as sexoAlumno,
		B.paisOrigen,
		B.nacionalidad,
		B.departamentoNacimiento,
		B.lugarNacimiento,
		B.direccionActual,
		B.celular,
		B.telefonoFijo,
		B.correoElectronico,
		B.tipoSangre,
		B.efermedadesComunes,
		B.condicionesEspeciales,
		B.aspirante,
		B.urlFotoPerfil   
	FROM heroku_39a7bb7a85bd88d.tblalumnos as A
	Inner join heroku_39a7bb7a85bd88d.tblpersonas as B
	on (A.codAlumno=B.codPersona) 
	where A.codAlumno=codAlumno;
    
    end if;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_alumno_resumen` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_alumno_resumen`(in in_codAlumno int)
BEGIN
	if in_codAlumno =0 then
		select 	t1.codAlumno,
			t1.numExpediente,
			concat(t2.nombres,' ', t2.apellidos) as nombreCompleto,
			DATE_FORMAT(t1.fechaCreacion, '%m-%d-%Y %H:%i')  fechaIngreso, 
			t1.codEstado,
			t3.estado,
			ifnull(t4.incidencias, 0) incidencias,
            DATE_FORMAT(t5.ultimaMatricula, '%m-%d-%Y %H:%i') ultimaMatricula ,
			t5.ultimoCursado
		from heroku_39a7bb7a85bd88d.tblalumnos as t1
		left join 
		heroku_39a7bb7a85bd88d.tblpersonas as t2
		on t1.codAlumno = t2.codPersona
		left join heroku_39a7bb7a85bd88d.tblestados as t3
		on t1.codEstado = t3.codEstado
		left join 
		(
			select codAlumno, count(*) incidencias 
			from heroku_39a7bb7a85bd88d.tblincidencias
			group by codAlumno
		) as t4 
		on t1.codAlumno = t4.codAlumno
		left join
		(
			select A.codAlumno, B.codCurso, max(A.fechaMatricula) ultimaMatricula, C.nombreCurso ultimoCursado
			from heroku_39a7bb7a85bd88d.tblmatricula as A
			left join heroku_39a7bb7a85bd88d.tblsecciones as B
			on A.codSeccion = B.codSeccion
			left join heroku_39a7bb7a85bd88d.tblcursos as C
			on B.codCurso = C.codCurso
		) as t5
		on t1.codAlumno = t5.codAlumno;  
        
    elseif in_codAlumno >0 then 
		select 	t1.codAlumno,
				t1.numExpediente,
				concat(t2.nombres,' ', t2.apellidos) as nombreCompleto,
				DATE_FORMAT(t1.fechaCreacion, '%m-%d-%Y %H:%i')  fechaIngreso, 
				t1.codEstado,
				t3.estado,
				ifnull(t4.incidencias, 0) incidencias,
				DATE_FORMAT(t5.ultimaMatricula, '%m-%d-%Y %H:%i') ultimaMatricula ,
				t5.ultimoCursado
		from heroku_39a7bb7a85bd88d.tblalumnos as t1
		left join 
		heroku_39a7bb7a85bd88d.tblpersonas as t2
		on t1.codAlumno = t2.codPersona
		left join heroku_39a7bb7a85bd88d.tblestados as t3
		on t1.codEstado = t3.codEstado
		left join 
		(
			select codAlumno, count(*) incidencias 
			from heroku_39a7bb7a85bd88d.tblincidencias
			group by codAlumno
		) as t4 
		on t1.codAlumno = t4.codAlumno
		left join
		(
			select A.codAlumno, B.codCurso, max(A.fechaMatricula) ultimaMatricula, C.nombreCurso ultimoCursado
			from heroku_39a7bb7a85bd88d.tblmatricula as A
			left join heroku_39a7bb7a85bd88d.tblsecciones as B
			on A.codSeccion = B.codSeccion
			left join heroku_39a7bb7a85bd88d.tblcursos as C
			on B.codCurso = C.codCurso
		) as t5
		on t1.codAlumno = t5.codAlumno
        where t1.codAlumno = in_codAlumno;    
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_asignaturas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_asignaturas`(in incodAsignatura int)
BEGIN
	if incodAsignatura=0 then
		select t1.codAsignatura, t1.asignatura, t1.habilitada, ifnull(t2.cursosAsignados,0) cursosAsignados,
		ifnull(t3.secciones,0) secciones
		from heroku_39a7bb7a85bd88d.tblasignaturas as t1
		left join 
		(
			select codAsignatura, count(codCurso) cursosAsignados
			from heroku_39a7bb7a85bd88d.tblasignaturas_x_curso 
			group by codAsignatura
		) as t2
		on t1.codAsignatura = t2.codAsignatura
		left join 
		(
			select codAsignatura, count(codSeccion) secciones
			from heroku_39a7bb7a85bd88d.tblSecciones
			group by codAsignatura
		) as t3 
		on t1.codAsignatura = t3.codAsignatura;
	elseif incodAsignatura>0 then
		select t1.codAsignatura, t1.asignatura, t1.habilitada, ifnull(t2.cursosAsignados,0) cursosAsignados,
		ifnull(t3.secciones,0) secciones
		from heroku_39a7bb7a85bd88d.tblasignaturas as t1
		left join 
		(
			select codAsignatura, count(codCurso) cursosAsignados
			from heroku_39a7bb7a85bd88d.tblasignaturas_x_curso 
			group by codAsignatura
		) as t2
		on t1.codAsignatura = t2.codAsignatura
		left join 
		(
			select codAsignatura, count(codSeccion) secciones
			from heroku_39a7bb7a85bd88d.tblSecciones
			group by codAsignatura
		) as t3 
		on t1.codAsignatura = t3.codAsignatura
        where t1.codAsignatura = incodAsignatura;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_asignaturas_curso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_asignaturas_curso`(in in_codCurso int, in in_habilitado int)
BEGIN
	#(0,0) muestra todas las asignaturas de todos los cursos, habilitados y deshabilitados
    #(n,0) muestra todas las asignaturas del curso "n", habilitadas y deshabilitadas
    #(n,n) muestra todas las asignaturas del curso "n", habilitadas unicamente
	if in_codCurso = 0 and in_habilitado = 0 then
		 select 
				t2.codAsignatura, 
				t2.asignatura, 
				t1.dias, 
                t1.codCurso,
				ifnull(t3.nombreCurso, 'Sin asignar') nombreCurso, 
				t2.habilitada as habilitado
		from heroku_39a7bb7a85bd88d.tblasignaturas_x_curso as t1
		inner join heroku_39a7bb7a85bd88d.tblasignaturas as t2
		on t1.codAsignatura = t2.codAsignatura
		inner join heroku_39a7bb7a85bd88d.tblcursos as t3
		on t1.codCurso = t3.codCurso;
	elseif in_codCurso >0 and in_habilitado = 0 then
		select 
				t2.codAsignatura, 
				t2.asignatura, 
				t1.dias, 
                t1.codCurso,
				t3.nombreCurso, 
				t2.habilitada as habilitado
		from heroku_39a7bb7a85bd88d.tblasignaturas_x_curso as t1
		left join heroku_39a7bb7a85bd88d.tblasignaturas as t2
		on t1.codAsignatura = t2.codAsignatura
		left join heroku_39a7bb7a85bd88d.tblcursos as t3
		on t1.codCurso = t3.codCurso
		where t1.codCurso = in_codCurso;
	elseif in_codCurso >0 and in_habilitado > 0 then
		select 
				t2.codAsignatura, 
				t2.asignatura, 
				t1.dias, 
				t3.nombreCurso, 
                t1.codCurso,
				t2.habilitada as habilitado
		from heroku_39a7bb7a85bd88d.tblasignaturas_x_curso as t1
		left join heroku_39a7bb7a85bd88d.tblasignaturas as t2
		on t1.codAsignatura = t2.codAsignatura
		left join heroku_39a7bb7a85bd88d.tblcursos as t3
		on t1.codCurso = t3.codCurso
		where t1.codCurso = in_codCurso and t3.habilitado > 0;
	else
		#(0,0) por defecto
		select 
				t2.codAsignatura, 
				t2.asignatura, 
				t1.dias, 
				t3.nombreCurso, 
                t1.codCurso,
				t2.habilitada as habilitado
		from heroku_39a7bb7a85bd88d.tblasignaturas_x_curso as t1
		left join heroku_39a7bb7a85bd88d.tblasignaturas as t2
		on t1.codAsignatura = t2.codAsignatura
		left join heroku_39a7bb7a85bd88d.tblcursos as t3
		on t1.codCurso = t3.codCurso;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_asistencia_alumnos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_asistencia_alumnos`(in in_cod_seccion int)
BEGIN
	select 
			t1.codAlumno, 
			t1.codSeccion,
			t2.identificacion,
			concat(t2.nombres, ' ', t2.apellidos) as nombre
	from tblmatricula as t1
	left join tblpersonas as t2
	on t1.codAlumno = t2.codPersona
	where codSeccion = in_cod_seccion;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_asistencia_fechas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_asistencia_fechas`(
											in fechaRangoInicial varchar(20),
                                            in fechaRangoFinal varchar(20),
                                            in codSeccion int(11),
                                            in codAlumno int(11)
                                            )
BEGIN

SELECT A.codDiaClase,
    A.codAlumno,
    A.codSeccion,
    A.asistio,
    DATE_FORMAT(B.fechaDia,'%Y-%m-%d')
FROM heroku_39a7bb7a85bd88d.tblasistencia as A
inner join tbldiasclase as B
on(A.codDiaClase=B.codDiaClase)
where DATE_FORMAT(B.fechaDia,'%Y-%m-%d')>=fechaRangoInicial AND 
	  DATE_FORMAT(B.fechaDia,'%Y-%m-%d')<= fechaRangoFinal AND 
      A.codSeccion=codSeccion AND
      A.codAlumno=codAlumno;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_asistencia_seccion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_asistencia_seccion`(in in_codSeccion int)
BEGIN
	set @numindex :=0;
	truncate table  heroku_39a7bb7a85bd88d.temp_tbl_alumnos_seccion;
	insert into heroku_39a7bb7a85bd88d.temp_tbl_alumnos_seccion 
	select @numindex := @numindex +1 as num, t1.codAlumno, t3.codSeccion
	from tblmatricula as t1
	left join tblalumnos as t2
	on t1.codAlumno = t2.codAlumno
	left join tblsecciones as t3
	on t1.codSeccion = t3.codSeccion
	where t1.codSeccion = in_codSeccion;
    
    select * from heroku_39a7bb7a85bd88d.temp_tbl_alumnos_seccion;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_aula` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_aula`(in in_codAula int)
BEGIN
	if in_codAula = 0 then 
		select 
			codAula, 
			capacidad,
			descripcion,
			habilitada
		from heroku_39a7bb7a85bd88d.tblaulas;
	elseif in_codAula > 0 then 
		select 
			codAula, 
			capacidad,
			descripcion,
			habilitada
		from heroku_39a7bb7a85bd88d.tblaulas
        where codAula = in_codAula;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_aula_hora` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_aula_hora`(in in_horaInicio varchar(20),in in_horaFin varchar(20))
BEGIN
	
		select 
	A.codAula,
	A.capacidad,
	A.descripcion,
	A.habilitada,
	DATE_FORMAT(B.horaInicio,'%H:%i') as horaInicio,
    DATE_FORMAT(B.horaFin,'%H:%i') as horaFin

from heroku_39a7bb7a85bd88d.tblaulas as A
inner join heroku_39a7bb7a85bd88d.tblsecciones as B
on(A.codAula=B.codAula)
where (in_horaInicio<DATE_FORMAT(B.horaInicio,'%H:%i') and in_horaInicio<DATE_FORMAT(B.horaFin,'%H:%i') and in_horaFin<DATE_FORMAT(B.horaInicio,'%H:%i') and in_horaFin<DATE_FORMAT(B.horaFin,'%H:%i')) or
(in_horaInicio>DATE_FORMAT(B.horaInicio,'%H:%i') and in_horaInicio>DATE_FORMAT(B.horaFin,'%H:%i') and in_horaFin>DATE_FORMAT(B.horaInicio,'%H:%i') and in_horaFin>DATE_FORMAT(B.horaFin,'%H:%i'));
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_bitacora` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_bitacora`(in codUsuario int(11))
BEGIN

	IF codUsuario=0 then

		SELECT
			A.codLog,
			B.nombreUsuario,
			A.fechaHora,
			A.descripcion
		FROM `heroku_39a7bb7a85bd88d`.`tblbitacora` as A
		INNER JOIN `heroku_39a7bb7a85bd88d`.tblusuarios as B
		ON(A.codUsuario=B.codUsuario);


	elseif codUsuario>0 then

		SELECT
			A.codLog,
			B.nombreUsuario,
			A.fechaHora,
			A.descripcion
		FROM `heroku_39a7bb7a85bd88d`.`tblbitacora` as A
		INNER JOIN `heroku_39a7bb7a85bd88d`.tblusuarios as B
		ON(A.codUsuario=B.codUsuario)
		WHERE A.codUsuario=codUsuario;
	
	end if;
		
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_cargo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_cargo`(in codCargo int(11))
BEGIN

	if codCargo=0 then
		SELECT A.codCargo,A.cargo 
		FROM heroku_39a7bb7a85bd88d.tblcargos as A;
	elseif codCargo>0 then
		SELECT A.codCargo,A.cargo 
		FROM heroku_39a7bb7a85bd88d.tblcargos as A
		WHERE A.codCargo=codCargo;

	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_cargo_docente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_cargo_docente`(
        in codDocente int (11)
)
BEGIN

	IF codDocente = 0 THEN
    
		SELECT  C.codCargo, B.nombreDepartamento as departamento, B.codDepartamento,
				C.cargo, A.codDocente, A.institucion, 
				DATE_FORMAT(A.fechaFinal, '%Y-%m-%d') as fechaFinal, 
				DATE_FORMAT(A.fechaInicio, '%Y-%m-%d') as fechaInicio, 
				DATE_FORMAT(A.fechaAsignacion, '%Y-%m-%d') as fechaAsignacion
		FROM heroku_39a7bb7a85bd88d.tblcargos_x_docente as A
		INNER JOIN heroku_39a7bb7a85bd88d.tbldepartamentos as B
		ON(A.codDepartamento=B.codDepartamento)
		INNER JOIN heroku_39a7bb7a85bd88d.tblcargos as C
		ON(A.codCargo=C.codCargo);
    
    ELSEIF codDocente > 0 then 

		SELECT  C.codCargo, B.nombreDepartamento as departamento, B.codDepartamento,
				C.cargo, A.codDocente, A.institucion, 
				DATE_FORMAT(A.fechaFinal, '%Y-%m-%d') as fechaFinal, 
				DATE_FORMAT(A.fechaInicio, '%Y-%m-%d') as fechaInicio, 
				DATE_FORMAT(A.fechaAsignacion, '%Y-%m-%d') as fechaAsignacion
		FROM heroku_39a7bb7a85bd88d.tblcargos_x_docente as A
		INNER JOIN heroku_39a7bb7a85bd88d.tbldepartamentos as B
		ON(A.codDepartamento=B.codDepartamento)
		INNER JOIN heroku_39a7bb7a85bd88d.tblcargos as C
		ON(A.codCargo=C.codCargo)
		WHERE A.codDocente=codDocente;
    
    END IF;
		

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_curso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_curso`(in in_codCurso int)
BEGIN
	if in_codCurso =0 then
		select 
		t1.codCurso, 
        t1.nombreCurso,
        t1.numeroAsignaturas,
        t1.codModalidad,
        ifnull(t2.modalidad, 'Sin asignar') AS modalidad,
        t1.habilitado
		from heroku_39a7bb7a85bd88d.tblcursos as t1
		left join heroku_39a7bb7a85bd88d.tblmodalidades as t2
		on t1.codModalidad = t2.codModalidad;
	elseif in_codCurso > 0 then
			select 
			t1.codCurso, 
			t1.nombreCurso,
			t1.numeroAsignaturas,
            t1.codModalidad,
			ifnull(t2.modalidad, 'N/A') as modalidad,
			t1.habilitado
		from heroku_39a7bb7a85bd88d.tblcursos as t1
		left join heroku_39a7bb7a85bd88d.tblmodalidades as t2
		on t1.codModalidad = t2.codModalidad
		where t1.codCurso = in_codCurso;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_dashboard_admin` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_dashboard_admin`(in codCard int(11))
BEGIN
	if codCard = 1 then 
	
		select count(*) as cantidadUsuario from `heroku_39a7bb7a85bd88d`.`tblusuarios`;

	elseif codCard= 2 then

		select (select count(*) as cantidadInhabilitadosCurso from `heroku_39a7bb7a85bd88d`.`tblcursos` as A 
		where A.habilitado=0) as cantidadInhabilitadosCurso , (select count(*) as cantidadHabilitadosCurso from `heroku_39a7bb7a85bd88d`.`tblcursos` as A 
		where A.habilitado=1) as cantidadHabilitadosCurso;

	elseif codCard= 3 then
	
		select A.nombrePeriodo from heroku_39a7bb7a85bd88d.tblperiodos as A
		where A.anio=DATE_FORMAT(CURDATE(),'%Y') and CURDATE()>=A.fechaInicio and CURDATE()<=A.fechaFin;

	elseif codCard=4 then
		select IFNULL(A.nombre,'No Asignado') as nombreAnioLectivo from heroku_39a7bb7a85bd88d.tbllectivos as A
		where  CURDATE()>=A.fechaInicio and CURDATE()<=A.fechaFinal;

	elseif codCard=5 then

		SELECT count(DISTINCT `tblasignaturas_x_curso`.`codAsignatura`) as cantidadAsignaturasImpartidas
		FROM `heroku_39a7bb7a85bd88d`.`tblasignaturas_x_curso`;

	end if;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_dashboard_evaProm` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_dashboard_evaProm`(in codCard int(11),in codDocente int(11))
BEGIN

	if exists(select * from `heroku_39a7bb7a85bd88d`.`tbldocentes` as A where A.codDocente=codDocente) then 
		if codCard = 1 then 
			
		
			SELECT A.codSeccion, B.nombreCurso, C.asignatura, A.seccion,A.codCurso,A.codAsignatura
			FROM heroku_39a7bb7a85bd88d.tblsecciones as A 
			left join heroku_39a7bb7a85bd88d.tblcursos as B
			on (A.codCurso=B.codCurso) 
			left join heroku_39a7bb7a85bd88d.tblasignaturas as C
			on (A.codAsignatura=C.codAsignatura)
			where A.codDocente=codDocente;
            
	elseif codCard = 2 then 
    
    	select (SELECT count(*)
		FROM heroku_39a7bb7a85bd88d.tblasistencia as A
		INNER JOIN  heroku_39a7bb7a85bd88d.tbldiasclase as B
		on(A.codDiaClase=B.codDiaClase)
		INNER JOIN  heroku_39a7bb7a85bd88d.tblsecciones as C
		on(C.codSeccion=A.codSeccion)
		where C.codDocente=codDocente and B.fechaDia=DATE_FORMAT(CURDATE(),'%Y-%m-%d') and A.asistio=1) as cantAlumnosAsistio,
		(SELECT count(*)
		FROM heroku_39a7bb7a85bd88d.tblasistencia as A
		INNER JOIN  heroku_39a7bb7a85bd88d.tbldiasclase as B
		on(A.codDiaClase=B.codDiaClase)
		INNER JOIN  heroku_39a7bb7a85bd88d.tblsecciones as C
		on(C.codSeccion=A.codSeccion) 
		where C.codDocente=codDocente and B.fechaDia=DATE_FORMAT(CURDATE(),'%Y-%m-%d') and A.asistio=0) as cantAlumnoFaltista,
		(SELECT count(*)
		FROM heroku_39a7bb7a85bd88d.tblasistencia as A
		INNER JOIN  heroku_39a7bb7a85bd88d.tbldiasclase as B
		on(A.codDiaClase=B.codDiaClase)
		INNER JOIN  heroku_39a7bb7a85bd88d.tblsecciones as C
		on(C.codSeccion=A.codSeccion) 
		where C.codDocente=codDocente and B.fechaDia=DATE_FORMAT(CURDATE(),'%Y-%m-%d')) as totalAlumnosLista;
        
	elseif codCard = 3 then
    
    
    
    
        select 0;

	end if;
    end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_dashboard_gest` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_dashboard_gest`(in codCard int(11))
BEGIN
	if codCard = 1 then 
	SELECT count(*) as DocenteHabilitados FROM `heroku_39a7bb7a85bd88d`.`tbldocentes` as A where A.habilitado=1;
	elseif codCard = 2 then
	SELECT (SELECT count(*)  FROM heroku_39a7bb7a85bd88d.tblaulas as A where A.habilitada=1) AS aulasHabilitadas,
	(SELECT count(*)  FROM heroku_39a7bb7a85bd88d.tblaulas as A where A.habilitada=0) AS aulasInhabilitadas;
	elseif codCard = 3 then
	SELECT (SELECT count(*)  FROM heroku_39a7bb7a85bd88d.tblsecciones as A where A.suspendida=1) AS seccionesInhabilitadas,
	(SELECT count(*)  FROM heroku_39a7bb7a85bd88d.tblsecciones as A where A.suspendida=0) AS seccionesHabilitadas;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_dashboard_matriReg` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_dashboard_matriReg`(in codCard int(11))
BEGIN

	if codCard=1 then 
		
        select 	COUNT(DISTINCT t1.codAlumno) as alumnosMatriculados
		from tblmatricula as t1;
        
        
	elseif codCard=2 then
		SELECT(
		(SELECT count(*) as alumnosMatriculados
		FROM heroku_39a7bb7a85bd88d.tblmatricula AS A
		left JOIN heroku_39a7bb7a85bd88d.tblalumnos AS B
		ON(A.codAlumno=B.codAlumno)
		Inner JOIN heroku_39a7bb7a85bd88d.tblpersonas AS C
		ON(A.codAlumno=C.codPersona)
		where C.sexo='M')/ F.alumnosMatriculados)*100 as AlumnosHombres,
		((SELECT count(*) as alumnosMatriculados
		FROM heroku_39a7bb7a85bd88d.tblmatricula AS A
		left JOIN heroku_39a7bb7a85bd88d.tblalumnos AS B
		ON(A.codAlumno=B.codAlumno)
		Inner JOIN heroku_39a7bb7a85bd88d.tblpersonas AS C
		ON(A.codAlumno=C.codPersona)
		where  C.sexo='F')/ F.alumnosMatriculados)*100 as AlumnosMujeres,F.alumnosMatriculados
		from (
		SELECT count(*) as alumnosMatriculados
		FROM heroku_39a7bb7a85bd88d.tblmatricula AS A
		left JOIN heroku_39a7bb7a85bd88d.tblalumnos AS B
		ON(A.codAlumno=B.codAlumno)) as F;
	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_departamento` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_departamento`(in codDepartamento int(11))
BEGIN

	if codDepartamento=0 then
		SELECT A.codDepartamento,A.nombreDepartamento as departamento 
		FROM heroku_39a7bb7a85bd88d.tbldepartamentos as A;
	elseif codDepartamento>0 then
		SELECT A.codDepartamento,A.nombreDepartamento as departamento 
		FROM heroku_39a7bb7a85bd88d.tbldepartamentos as A
		WHERE A.codDepartamento=codDepartamento;

	end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_diasClass` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_diasClass`(in in_inicio int, in in_semana int )
BEGIN

	select 	codDiaClase, 
			semana, 
			concat(date_format(fechaDia, '%a'), ' ', date_format(fechaDia, '%d')) dia 
	from tbldiasclase
	WHERE codDiaClase between in_inicio and in_inicio+80;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_docente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_docente`(in in_codDocente int)
BEGIN
	if in_codDocente = 0 then
		select 
				
				t1.codDocente,
				concat(t2.nombres, ' ', t2.apellidos) as nombreCompleto,
                t2.nombres,
                t2.apellidos,
                t2.identificacion,
                t2.fechaNacimiento,
                t2.telefonoFijo,
                t2.celular,
                t2.correoElectronico,
                t2.direccionActual,
                t2.edad,
                t2.paisOrigen,
                t2.departamentoNacimiento,
                t2.lugarNacimiento,
                t2.nacionalidad,
                t2.sexo,
                t2.tipoSangre,
                t2.efermedadesComunes,
                t2.condicionesEspeciales,
				t1.numExpediente,
                DATE_FORMAT(t1.fechaIngreso, '%m-%d-%Y %H:%i') as fechaIngreso,
				t1.habilitado,
				ifnull(t3.nombreUsuario, 'No asignado') as nombreUsuario
		 from heroku_39a7bb7a85bd88d.tbldocentes as t1
		 left join heroku_39a7bb7a85bd88d.tblpersonas as t2
		 on t1.codDocente = t2.codPersona
		 left join heroku_39a7bb7a85bd88d.tblusuarios as t3
		 on t1.codDocente = t3.codPersona;
	elseif in_codDocente > 0 then 
		select 
				
				t1.codDocente,
				concat(t2.nombres, ' ', t2.apellidos) as nombreCompleto,
                t2.nombres,
                t2.apellidos,
                t2.identificacion,
                t2.fechaNacimiento,
                t2.telefonoFijo,
                t2.celular,
                t2.correoElectronico,
                t2.direccionActual,
                t2.edad,
                t2.paisOrigen,
                t2.departamentoNacimiento,
                t2.lugarNacimiento,
                t2.nacionalidad,
                t2.sexo,
                t2.tipoSangre,
                t2.efermedadesComunes,
                t2.condicionesEspeciales,
				t1.numExpediente,
                DATE_FORMAT(t1.fechaIngreso, '%m-%d-%Y %H:%i') as fechaIngreso,
				t1.habilitado,
				ifnull(t3.nombreUsuario, 'N/A') as nombreUsuario
		 from heroku_39a7bb7a85bd88d.tbldocentes as t1
		 left join heroku_39a7bb7a85bd88d.tblpersonas as t2
		 on t1.codDocente = t2.codPersona
		 left join heroku_39a7bb7a85bd88d.tblusuarios as t3
		 on t1.codDocente = t3.codPersona
         where t1.codDocente = in_codDocente;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_experiencia` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_experiencia`(
		in codDocente int (11),
        in codExperiencia int (11)
)
BEGIN

SELECT A.codExperiencia,
	   A.cargoDesempenado,
       A.fechaInicio,
       A.fechaFin,
       A.areaEnsenanza,
       A.codDocente,
       concat(B.nombres,' ',B.apellidos) nombreCompletoDocente
FROM `heroku_39a7bb7a85bd88d`.`tblexperiencia` as A
INNER JOIN `heroku_39a7bb7a85bd88d`.tblpersonas as B
on(A.codDocente=B.codPersona);



        
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_historial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_historial`(in codAlumno int(11))
BEGIN

if codAlumno=0 then 
SELECT
A.fechaMatricula,
A.codAlumno,
A.codTraslado,
A.asignatura,
A.promedio,
A.comentario,
A.codAula,
A.codDocente,
A.codJornada,
A.codLectivo,
A.codPeriodo,
A.codAsignatura,
A.horaInicio,
A.horaFin,
A.curso,
A.codCurso
FROM heroku_39a7bb7a85bd88d.tblhistorial as A;

elseif codAlumno>0 then 

SELECT
A.fechaMatricula,
A.codAlumno,
A.codTraslado,
A.asignatura,
A.promedio,
A.comentario,
A.codAula,
A.codDocente,
A.codJornada,
A.codLectivo,
A.codPeriodo,
A.codAsignatura,
A.horaInicio,
A.horaFin,
A.curso,
A.codCurso
FROM heroku_39a7bb7a85bd88d.tblhistorial as A
	where A.codAlumno=codAlumno;

end if;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_historial_resumen` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_historial_resumen`(in in_codAlumno int)
BEGIN
	
    if in_codAlumno = 0 then
		select 
			year(t1.fechaMatricula) anio,
			t1.codPeriodo,
			t2.nombrePeriodo,
			t1.codAlumno,
			t1.curso as nombreCurso,
			t1.asignatura,
			t1.promedio as  promedioActual,
			t1.comentario
		from heroku_39a7bb7a85bd88d.tblhistorial as t1
		left join heroku_39a7bb7a85bd88d.tblperiodos as t2
		on t1.codPeriodo = t2.codPeriodo;    
        
    elseif in_codAlumno > 0 then
		select 
			year(t1.fechaMatricula) anio,
			t1.codPeriodo,
			t2.nombrePeriodo,
			t1.codAlumno,
			t1.curso as nombreCurso,
			t1.asignatura,
			t1.promedio as promedioActual,
			t1.comentario
		from heroku_39a7bb7a85bd88d.tblhistorial as t1
		left join heroku_39a7bb7a85bd88d.tblperiodos as t2
		on t1.codPeriodo = t2.codPeriodo
        where t1.codAlumno = in_codAlumno;
	end if;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_incidencias` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_incidencias`(in codReportado int, in parametro int)
BEGIN
	/*
		(0,0) Muestra la consulta sin filtros con campos reportado y reporta
        (n,0) Muestra la consulta del codReportado con campos reportado y reporta
        (n,1) Muestra la consulta del codReportado con campo reportado
        (n,2) Muestra la consulta del codReportado con campo reporta
    */
    
    #(0,0)
	if codReportado = 0 and parametro = 0 then
		select t1.codIncidencia,
				t1.tituloIncidencia,
				t1.descripcion,
				t1.codTipoIncidencia,
				t2.tipoIncidencia,
				t1.codPersonaReporta,
				t4.reporta,
				t1.codPersonaReportada,
				t3.reportado,
                DATE_FORMAT(t1.fecha, '%m-%d-%Y %H:%i:%s') as fecha				
		from heroku_39a7bb7a85bd88d.tblincidencias as t1
		left join heroku_39a7bb7a85bd88d.tbltiposincidencias as t2
		on t1.codTipoIncidencia = t2.codTipoIncidencia
		left join 
		(
			select q1.codPersonaReportada, concat(q2.nombres, ' ', q2.apellidos) as reportado
			from heroku_39a7bb7a85bd88d.tblincidencias as q1
			left join heroku_39a7bb7a85bd88d.tblpersonas as q2
			on q1.codPersonaReportada = q2.codPersona
		) as t3
		on t1.codPersonaReportada = t3.codPersonaReportada
		left join 
		(
			select q1.codPersonaReporta, concat(q2.nombres, ' ', q2.apellidos) as reporta
			from heroku_39a7bb7a85bd88d.tblincidencias as q1
			left join heroku_39a7bb7a85bd88d.tblpersonas as q2
			on q1.codPersonaReporta = q2.codPersona
		) as t4 
		on t1.codPersonaReporta = t4.codPersonaReporta;
	#(n,0) Muestra la consulta del codReportado con campos reportado y reporta
    elseif codReportado >0 and parametro = 0 then
		select t1.codIncidencia,
				t1.tituloIncidencia,
				t1.descripcion,
				t1.codTipoIncidencia,
				t2.tipoIncidencia, 
				t1.codPersonaReportada,
				t3.reportado,
				 DATE_FORMAT(t1.fecha, '%m-%d-%Y %H:%i:%s') as fecha	
		from heroku_39a7bb7a85bd88d.tblincidencias as t1
		left join heroku_39a7bb7a85bd88d.tbltiposincidencias as t2
		on t1.codTipoIncidencia = t2.codTipoIncidencia
		left join 
		(
			select q1.codPersonaReportada, concat(q2.nombres, ' ', q2.apellidos) as reportado
			from heroku_39a7bb7a85bd88d.tblincidencias as q1
			left join heroku_39a7bb7a85bd88d.tblpersonas as q2
			on q1.codPersonaReportada = q2.codPersona
		) as t3
		on t1.codPersonaReporta = t3.codPersonaReportada
		 
		where t1.codPersonaReportada = codReportado;    
    #(n,1) Muestra la consulta las incidencias que han reportado a mi nombre (quien me reporta)
    elseif codReportado >0 and parametro = 1 then
		select t1.codIncidencia,
				t1.tituloIncidencia,
				t1.descripcion,
				t1.codTipoIncidencia,
				t2.tipoIncidencia,
				t1.codPersonaReporta,
				t4.reporta as 'ReportadoPor', 
				 DATE_FORMAT(t1.fecha, '%m-%d-%Y %H:%i:%s') as fecha	
		from heroku_39a7bb7a85bd88d.tblincidencias as t1
		left join heroku_39a7bb7a85bd88d.tbltiposincidencias as t2
		on t1.codTipoIncidencia = t2.codTipoIncidencia  
		left join 
		(
			select q1.codPersonaReporta, concat(q2.nombres, ' ', q2.apellidos) as reporta
			from heroku_39a7bb7a85bd88d.tblincidencias as q1
			left join heroku_39a7bb7a85bd88d.tblpersonas as q2
			on q1.codPersonaReporta = q2.codPersona
		) as t4 
		on t1.codPersonaReporta = t4.codPersonaReporta
		where t1.codPersonaReportada = codReportado; 
	
	#(n,2) Muestra la consulta de lo que yo reporte y a quien reporte
    elseif codReportado >0 and parametro = 2 then 
		 select t1.codIncidencia,
				t1.tituloIncidencia,
				t1.descripcion,
				t1.codTipoIncidencia,
				t2.tipoIncidencia, 
				t1.codPersonaReportada,
				t3.reportado as 'Reportado a',
				 DATE_FORMAT(t1.fecha, '%m-%d-%Y %H:%i:%s') as fecha	
		from heroku_39a7bb7a85bd88d.tblincidencias as t1
		left join heroku_39a7bb7a85bd88d.tbltiposincidencias as t2
		on t1.codTipoIncidencia = t2.codTipoIncidencia
		left join 
		(
			select q1.codPersonaReportada, concat(q2.nombres, ' ', q2.apellidos) as reportado
			from heroku_39a7bb7a85bd88d.tblincidencias as q1
			left join heroku_39a7bb7a85bd88d.tblpersonas as q2
			on q1.codPersonaReportada = q2.codPersona
		) as t3
		on t1.codPersonaReportada = t3.codPersonaReportada 
		where t1.codPersonaReporta = codReportado; 
    
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_jornada` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_jornada`(
		in codJornada int(11)
)
BEGIN

    if codJornada=0 then
    SELECT A.codJornada,
			A.jornada
	FROM `heroku_39a7bb7a85bd88d`.`tbljornadas` as A;
    
    elseif codJornada>0 then
    SELECT A.codJornada,
			A.jornada
	FROM `heroku_39a7bb7a85bd88d`.`tbljornadas` as A
    where A.codJornada=codJornada;
    end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_lectivo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_lectivo`(
		in codLectivo int(11)
)
BEGIN

	if codLectivo=0 then
		SELECT 
		A.codLectivo,
        DATE_FORMAT(A.fechaInicio, '%m-%d-%Y') as fechaInicio,
        DATE_FORMAT(A.fechaFinal, '%m-%d-%Y') as fechaFinal,
		A.nombre
		FROM `heroku_39a7bb7a85bd88d`.`tbllectivos` as A; 
	elseif codLectivo>0 then 
		SELECT 
		A.codLectivo,
		DATE_FORMAT(A.fechaInicio, '%m-%d-%Y') as fechaInicio,
        DATE_FORMAT(A.fechaFinal, '%m-%d-%Y') as fechaFinal,
		A.nombre
		FROM `heroku_39a7bb7a85bd88d`.`tbllectivos` as A
		where A.codLectivo=codLectivo;
	end if;   

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_matricula` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_matricula`(in in_codAlumno int)
BEGIN
	#si es 0 devolver todos los alumnos
    if in_codAlumno=0 then 
		select 	t1.codAlumno, 
				concat(t5.nombres, ' ', t5.apellidos) as nombreAlumno,
				t2.codAula,
				t2.codDocente,
				t2.codJornada,
				t2.codLectivo,
				t2.codPeriodo,
				t2.seccion,
                DATE_FORMAT(t2.horaInicio, ' %H:%i:%s') as horaInicio,
                DATE_FORMAT(t2.horaFin, ' %H:%i:%s') as horaFin, 
				t2.suspendida estadoSeccion,
				t3.nombreDocente, 
				t4.descripcion aula,
                t6.jornada,
                t7.nombreCurso curso
		from tblmatricula as t1
		left join heroku_39a7bb7a85bd88d.tblsecciones as t2
		on t1.codSeccion = t2.codSeccion
		left join 
		(
			select  
				t1.codDocente,
				concat(t2.nombres, ' ', t2.apellidos) as nombreDocente,
				t1.habilitado 
			from heroku_39a7bb7a85bd88d.tbldocentes as t1
			left join heroku_39a7bb7a85bd88d.tblpersonas as t2
			on t1.codDocente = t2.codPersona
			left join heroku_39a7bb7a85bd88d.tblusuarios as t3
			on t1.codDocente = t3.codPersona
		) as t3
		on t2.codDocente = t3.codDocente
		left join tblaulas as t4
		on t2.codAula = t4.codAula
		left join tblpersonas as t5
		on t1.codAlumno = t5.codPersona
        left join heroku_39a7bb7a85bd88d.tbljornadas as t6
        on t2.codJornada = t6.codJornada
        left join heroku_39a7bb7a85bd88d.tblcursos as t7
        on t2.codCurso = t7.codCurso;
        
    #devolver la info de un alumno
    elseif in_codAlumno>0 then 
		select 	t1.codAlumno, 
				concat(t5.nombres, ' ', t5.apellidos) as nombreAlumno,
				t2.codAula,
				t2.codDocente,
				t2.codJornada,
				t2.codLectivo,
				t2.codPeriodo,
				t2.seccion,
                DATE_FORMAT(t2.horaInicio, ' %H:%i:%s') as horaInicio,
                DATE_FORMAT(t2.horaFin, ' %H:%i:%s') as horaFin, 
				t2.suspendida estadoSeccion,
				t3.nombreDocente, 
				t4.descripcion aula,
                t6.jornada,
                t7.nombreCurso curso
		from tblmatricula as t1
		left join heroku_39a7bb7a85bd88d.tblsecciones as t2
		on t1.codSeccion = t2.codSeccion
		left join 
		(
			select  
				t1.codDocente,
				concat(t2.nombres, ' ', t2.apellidos) as nombreDocente,
				t1.habilitado 
			from heroku_39a7bb7a85bd88d.tbldocentes as t1
			left join heroku_39a7bb7a85bd88d.tblpersonas as t2
			on t1.codDocente = t2.codPersona
			left join heroku_39a7bb7a85bd88d.tblusuarios as t3
			on t1.codDocente = t3.codPersona
		) as t3
		on t2.codDocente = t3.codDocente
		left join tblaulas as t4
		on t2.codAula = t4.codAula
		left join tblpersonas as t5
		on t1.codAlumno = t5.codPersona
        left join heroku_39a7bb7a85bd88d.tbljornadas as t6
        on t2.codJornada = t6.codJornada
        left join heroku_39a7bb7a85bd88d.tblcursos as t7
        on t2.codCurso = t7.codCurso
        where t1.codAlumno = in_codAlumno;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_modalidades` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_modalidades`( )
BEGIN
	
    
	SELECT  A.codModalidad, A.modalidad
	FROM `heroku_39a7bb7a85bd88d`.`tblmodalidades` as A;
	
    
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_nombre_personas` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_nombre_personas`()
BEGIN  

	SELECT concat(A.nombres,' ',A.apellidos) as nombreCompleto, A.codPersona as codigoPersona
    FROM heroku_39a7bb7a85bd88d.tblpersonas as A;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_nombre_usuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_nombre_usuarios`()
BEGIN  

	SELECT A.nombreUsuario, A.codUsuario as codigoUsuario
    FROM heroku_39a7bb7a85bd88d.tblusuarios as A;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_notas_seccion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_notas_seccion`(in in_condSeccion int, in in_codDocente int)
BEGIN
	select 
			t.codAlumno, 
            t1.identificacion,
			concat(t1.nombres, ' ', t1.apellidos) nombreCompleto,
			t2.codSeccion, 
			t.na1,t.ne1,t.nf1,
			t.na2,t.ne2,t.nf2,
			t.na3,t.ne3,t.nf3,
			t.na4,t.ne4,t.nf4,
			t.promedio, 
            t2.codDocente
	from heroku_39a7bb7a85bd88d.tblmatricula as t
	left join tblpersonas as t1
	on t.codAlumno = t1.codPersona
    left join tblsecciones as t2
    on t.codSeccion = t2.codSeccion
	where t.codSeccion = in_condSeccion  and t2.codDocente = in_codDocente;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_periodo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_periodo`(
		in codPeriodo int (11)
)
BEGIN

	IF codPeriodo=0 then
	
    SELECT 
		A.codPeriodo, 
        A.nombrePeriodo, 
        DATE_FORMAT(A.fechaInicio, '%Y-%m-%d') as fechaInicio,
        DATE_FORMAT(A.fechafin, '%Y-%m-%d') as fechaFinal,
		A.anio
	FROM
		`heroku_39a7bb7a85bd88d`.`tblperiodos` as A;
    
    ELSEIF codPeriodo>0 then
	SELECT 
		A.codPeriodo, 
        A.nombrePeriodo, 
		DATE_FORMAT(A.fechaInicio, '%m-%d-%Y') as fechaInicio,
        DATE_FORMAT(A.fechafin, '%m-%d-%Y') as fechaFinal,
        A.anio
	FROM
		`heroku_39a7bb7a85bd88d`.`tblperiodos` as A
	WHERE A.codPeriodo=codPeriodo;
    
    end if;
    
                
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_persona` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_persona`(in codPersona int(11))
BEGIN  

	SELECT 	A.codPersona,
			concat(A.nombres,' ',A.apellidos) as nombre
    FROM heroku_39a7bb7a85bd88d.tblpersonas as A
	where A.codPersona=codPersona;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_seccion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_seccion`( 
                                in codSeccion int(11)
							 )
BEGIN     
	if codSeccion =0 then
		SELECT
			A.codSeccion, 
            A.seccion, concat(D.nombres,' ',D.apellidos) as nombreCompletoDocente,
			B.descripcion as nombreAula, 
            ifnull(E.nombreCurso, 'Sin asignar') nombreCurso,
            DATE_FORMAT(A.horaInicio, "%H:%i") as horaInicio,
            DATE_FORMAT(A.horaFin ,"%H:%i") as horaFin, 
			A.suspendida as habilitada, 
            A.codAula,
            A.codDocente,
            A.codPeriodo,
            A.codLectivo,
            A.codCurso,
            A.codSeccion,
            A.codAsignatura,
            A.codJornada,
            F.nombre,
            G.asignatura,
            H.nombrePeriodo,
            I.jornada
		FROM heroku_39a7bb7a85bd88d.tblsecciones AS A
		LEFT JOIN heroku_39a7bb7a85bd88d.tblaulas AS B
		ON(A.codAula=B.codAula)
		LEFT JOIN heroku_39a7bb7a85bd88d.tbldocentes AS C
		ON(A.codDocente=C.codDocente)
		LEFT JOIN heroku_39a7bb7a85bd88d.tblpersonas AS D
		ON(A.codDocente=D.codPersona)
		LEFT JOIN heroku_39a7bb7a85bd88d.tblcursos AS E
		ON(A.codCurso=E.codCurso)
        LEFT JOIN heroku_39a7bb7a85bd88d.tbllectivos AS F
		ON(A.codLectivo=F.codLectivo)
		LEFT JOIN heroku_39a7bb7a85bd88d.tblasignaturas AS G
		ON(A.codAsignatura=G.codAsignatura)
        LEFT JOIN heroku_39a7bb7a85bd88d.tblperiodos AS H
		ON(A.codPeriodo=H.codPeriodo)
        LEFT JOIN heroku_39a7bb7a85bd88d.tbljornadas AS I
		ON(A.codJornada=I.codJornada);    
    elseif codSeccion > 0 then
		SELECT
			A.codSeccion, 
            A.seccion, concat(D.nombres,' ',D.apellidos) as nombreCompletoDocente,
			B.descripcion as nombreAula, 
            ifnull(E.nombreCurso, 'Sin asignar') nombreCurso,
            DATE_FORMAT(A.horaInicio, "%H:%i") as horaInicio,
            DATE_FORMAT(A.horaFin ,"%H:%i") as horaFin, 
			A.suspendida as habilitada, 
            A.codAula,
            A.codDocente,
            A.codPeriodo,
            A.codLectivo,
            A.codCurso,
            A.codSeccion,
            A.codAsignatura,
            A.codJornada,
            F.nombre,
            G.asignatura,
            H.nombrePeriodo,
            I.jornada
		FROM heroku_39a7bb7a85bd88d.tblsecciones AS A
		LEFT JOIN heroku_39a7bb7a85bd88d.tblaulas AS B
		ON(A.codAula=B.codAula)
		LEFT JOIN heroku_39a7bb7a85bd88d.tbldocentes AS C
		ON(A.codDocente=C.codDocente)
		LEFT JOIN heroku_39a7bb7a85bd88d.tblpersonas AS D
		ON(A.codDocente=D.codPersona)
		LEFT JOIN heroku_39a7bb7a85bd88d.tblcursos AS E
		ON(A.codCurso=E.codCurso)
        LEFT JOIN heroku_39a7bb7a85bd88d.tbllectivos AS F
		ON(A.codLectivo=F.codLectivo)
		LEFT JOIN heroku_39a7bb7a85bd88d.tblasignaturas AS G
		ON(A.codAsignatura=G.codAsignatura)
        LEFT JOIN heroku_39a7bb7a85bd88d.tblperiodos AS H
		ON(A.codPeriodo=H.codPeriodo)
        LEFT JOIN heroku_39a7bb7a85bd88d.tbljornadas AS I
		ON(A.codJornada=I.codJornada)
        WHERE A.codSeccion=codSeccion;
    
    end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_secciones_disponibles` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_secciones_disponibles`(in inCodAsignatura int, in inCodCurso int)
BEGIN
	SELECT
		A.codSeccion, 
		A.seccion, concat(D.nombres,' ',D.apellidos) as nombreCompletoDocente,
		B.descripcion as nombreAula,  
		DATE_FORMAT(A.horaInicio, "%H:%i") as horaInicio,
		DATE_FORMAT(A.horaFin ,"%H:%i") as horaFin
		FROM heroku_39a7bb7a85bd88d.tblsecciones AS A
		LEFT JOIN heroku_39a7bb7a85bd88d.tblaulas AS B
		ON(A.codAula=B.codAula) 
		LEFT JOIN heroku_39a7bb7a85bd88d.tblpersonas AS D
		ON(A.codDocente=D.codPersona)
		where A.codAsignatura = inCodAsignatura and A.codCurso = inCodCurso;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_secciones_docente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_secciones_docente`(in in_codDocente int)
BEGIN
SELECT
			A.codSeccion, 
            A.seccion, 
            concat(D.nombres,' ',D.apellidos) as nombreCompletoDocente,
			B.descripcion as nombreAula, 
            ifnull(E.nombreCurso, 'Sin asignar') nombreCurso,
            DATE_FORMAT(A.horaInicio, "%H:%i") as horaInicio,
            DATE_FORMAT(A.horaFin ,"%H:%i") as horaFin
		FROM heroku_39a7bb7a85bd88d.tblsecciones AS A
		LEFT JOIN heroku_39a7bb7a85bd88d.tblaulas AS B
		ON(A.codAula=B.codAula) 
		LEFT JOIN heroku_39a7bb7a85bd88d.tblpersonas AS D
		ON(A.codDocente=D.codPersona)
		LEFT JOIN heroku_39a7bb7a85bd88d.tblcursos AS E
		ON(A.codCurso=E.codCurso) 
        where A.codDocente = in_codDocente;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_seccion_alumnos` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_seccion_alumnos`(
											in codSeccion int(11)
                                            )
BEGIN

if codSeccion=0 then

		select 	
				t1.codAlumno, 
                t2.codSeccion,
				concat(t3.nombres, ' ', t3.apellidos) as nombreAlumno,
                t3.identificacion,
				t2.seccion
		from tblmatricula as t1
		left join heroku_39a7bb7a85bd88d.tblsecciones as t2
		on (t1.codSeccion = t2.codSeccion)
        Inner join heroku_39a7bb7a85bd88d.tblpersonas as t3
		on (t1.codAlumno=t3.codPersona);
    
    elseif codSeccion>0 then 
		select 	
				t1.codAlumno, 
                t2.codSeccion,
				concat(t3.nombres, ' ', t3.apellidos) as nombreAlumno,
				t3.identificacion,
                t2.seccion
		from tblmatricula as t1
		left join heroku_39a7bb7a85bd88d.tblsecciones as t2
		on (t1.codSeccion = t2.codSeccion)
        Inner join heroku_39a7bb7a85bd88d.tblpersonas as t3
		on (t1.codAlumno=t3.codPersona)
        where t2.codSeccion=codSeccion;
    
    end if;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_tipo_incidencias` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_tipo_incidencias`()
BEGIN
	select * from heroku_39a7bb7a85bd88d.tbltiposincidencias;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_tipo_usuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_tipo_usuario`(in in_codTipoUsuario int)
BEGIN
	if in_codTipoUsuario =0 then
		select 	codTipoUsuario, 
			tipoUsuario as rol,
			accesos,
			DATE_FORMAT(fechaCreacion, '%m/%d/%Y %H:%i') as fechaCreacion,
			DATE_FORMAT(fechaModificacion, '%m/%d/%Y %H:%i') as fechaModificacion,
			habilitado
		from heroku_39a7bb7a85bd88d.tbltiposusuarios;
	elseif in_codTipoUsuario > 0 then
		select 	codTipoUsuario, 
				tipoUsuario as rol,
				accesos,
				DATE_FORMAT(fechaCreacion, '%m/%d/%Y %H:%i') as fechaCreacion,
				DATE_FORMAT(fechaModificacion, '%m/%d/%Y %H:%i') as fechaModificacion,
				habilitado
		from heroku_39a7bb7a85bd88d.tbltiposusuarios
		where codTipoUsuario = in_codTipoUsuario;
	end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_traslado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_traslado`( 
                                in codAlumno int(11)
							 )
BEGIN     
	if codAlumno=0 then
		SELECT A.codTraslado,
			B.codAlumno,
			A.institucion,
			A.fechaFinal,
			A.fechaTraslado,
			A.promedioTraslado
		FROM heroku_39a7bb7a85bd88d.tbltraslados AS A
		INNER JOIN heroku_39a7bb7a85bd88d.tblalumnos AS B
		ON(A.codAlumno=B.codAlumno);
	elseif codAlumno>0 then
		SELECT A.codTraslado,
			B.codAlumno,
			A.institucion,
			A.fechaFinal,
			A.fechaTraslado,
			A.promedioTraslado
		FROM heroku_39a7bb7a85bd88d.tbltraslados AS A
		INNER JOIN heroku_39a7bb7a85bd88d.tblalumnos AS B
		ON(A.codAlumno=B.codAlumno)
		where A.codAlumno=codAlumno;
    end if;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_tutores` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_tutores`(in in_codTutor int, in in_codAlumno int)
BEGIN
	#(0,0) todos los tutores de todos los alumnos registrados
    #(0,n) todos los tutores de un alumno en especifico
    #(n,0) un tutor en especifico
    #(n,n) un tutor de un alumno 
    
    #------
    #(0,0) todos los tutores de todos los alumnos registrados
    if in_codTutor =0 and in_codAlumno = 0 then
		select	t1.codTutor,
				t1.codAlumno,
				t1.parentesco,
				t1.ocupacion,
				t1.lugarDeTrabajo,
				t1.telefonoTrabajo,
				t2.codPersona,
				t2.identificacion,
				t2.nombres as nombre, 
				t2.apellidos as apellido,
				t2.telefonoFijo,
				t2.celular,
				t2.correoElectronico
		from heroku_39a7bb7a85bd88d.tbltutores as t1
		left join heroku_39a7bb7a85bd88d.tblpersonas as t2
		on t1.codTutor = t2.codPersona;    
	#(0,n) todos los tutores de un alumno en especifico
    elseif in_codTutor = 0 and in_codAlumno > 0 then
		select	t1.codTutor,
				t1.codAlumno,
				t1.parentesco,
				t1.ocupacion,
				t1.lugarDeTrabajo,
				t1.telefonoTrabajo,
				t2.codPersona,
				t2.identificacion,
				t2.nombres as nombre, 
				t2.apellidos as apellido,
				t2.telefonoFijo,
				t2.celular,
				t2.correoElectronico
		from heroku_39a7bb7a85bd88d.tbltutores as t1
		left join heroku_39a7bb7a85bd88d.tblpersonas as t2
		on t1.codTutor = t2.codPersona
		where t1.codAlumno = in_codAlumno;
	
    #(n,0) un tutor en especifico
    elseif in_codTutor > 0 and in_codAlumno = 0 then
		select	t1.codTutor,
				t1.codAlumno,
				t1.parentesco,
				t1.ocupacion,
				t1.lugarDeTrabajo,
				t1.telefonoTrabajo,
				t2.codPersona,
				t2.identificacion,
				t2.nombres as nombre, 
				t2.apellidos as apellido,
				t2.telefonoFijo,
				t2.celular,
				t2.correoElectronico
		from heroku_39a7bb7a85bd88d.tbltutores as t1
		left join heroku_39a7bb7a85bd88d.tblpersonas as t2
		on t1.codTutor = t2.codPersona
		where t1.codTutor = in_codTutor;
        
    #(n,n) un tutor de un alumno 
    elseif in_codTutor > 0 and in_codAlumno > 0 then
		select	t1.codTutor,
				t1.codAlumno,
				t1.parentesco,
				t1.ocupacion,
				t1.lugarDeTrabajo,
				t1.telefonoTrabajo,
				t2.codPersona,
				t2.identificacion,
				t2.nombres as nombre, 
				t2.apellidos as apellido,
				t2.telefonoFijo,
				t2.celular,
				t2.correoElectronico
		from heroku_39a7bb7a85bd88d.tbltutores as t1
		left join heroku_39a7bb7a85bd88d.tblpersonas as t2
		on t1.codTutor = t2.codPersona
		where t1.codTutor = in_codTutor and t1.codAlumno = in_codAlumno;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_usuario_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_usuario_details`(in in_codUsuario int)
BEGIN
	if in_codUsuario = 0 then
		#muestra todos los usuarios en la tabla
        select 
				t1.codPersona,
                t1.codUsuario,
				concat(t3.nombres, ' ' ,t3.apellidos ) as nombreCompleto, 
				t1.nombreUsuario,
				t2.tipoUsuario as Rol,
				DATE_FORMAT(t1.fechaCreacion, '%m-%d-%Y %H:%i') as fechaCreacion,
                DATE_FORMAT(t1.fechaModificacion, '%m-%d-%Y %H:%i') as fechaModificacion,
				t1.habilitado,
                t2.accesos,
                t2.codTipoUsuario
		from heroku_39a7bb7a85bd88d.tblusuarios as t1
		left join heroku_39a7bb7a85bd88d.tbltiposusuarios as t2
		on t1.codTipoUsuario = t2.codTipoUsuario
		left join heroku_39a7bb7a85bd88d.tblpersonas as t3
		on t1.codPersona = t3.codPersona;
	elseif in_codUsuario > 0 then
		select 
				t1.codPersona,
                t1.codUsuario,
				concat(t3.nombres, ' ' ,t3.apellidos ) as nombreCompleto, 
				t1.nombreUsuario,
				t2.tipoUsuario as Rol,
				DATE_FORMAT(t1.fechaCreacion, '%m-%d-%Y %H:%i') as fechaCreacion,
                DATE_FORMAT(t1.fechaModificacion, '%m-%d-%Y %H:%i') as fechaModificacion,
				t1.habilitado,
                t2.accesos,                
                t2.codTipoUsuario,
                t1.contrasena
		from heroku_39a7bb7a85bd88d.tblusuarios as t1
		left join heroku_39a7bb7a85bd88d.tbltiposusuarios as t2
		on t1.codTipoUsuario = t2.codTipoUsuario
		left join heroku_39a7bb7a85bd88d.tblpersonas as t3
		on t1.codPersona = t3.codPersona
		where t1.codUsuario = in_codUsuario;
    end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_get_verificar_credenciales` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_get_verificar_credenciales`( nombreUsuario varchar(20),contrasena varchar(20) )
BEGIN  
     
	
     SELECT A.codUsuario as codigoUsuario ,A.codPersona as codigoPersona,
		A.codTipoUsuario as tipoUsuario,A.codTipoUsuario as codtipoUsuario, B.accesos, count(*) as resultado, A.habilitado
	 FROM heroku_39a7bb7a85bd88d.tblUsuarios as A
	 INNER JOIN heroku_39a7bb7a85bd88d.tblTiposUsuarios as B
	 ON (A.codTipoUsuario=B.codTipoUsuario)
	 WHERE A.nombreUsuario=nombreUsuario AND A.contrasena=contrasena; 

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_alumno` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_alumno`( 
								in nombre varchar(20),
								in apellido varchar(20),
								in identidad varchar(20),
								in fechaNacimiento varchar(20),
								in telefono varchar(20),
								in celular varchar(20),
								in correo varchar(20),
								in direccion varchar(20),
								in edad int(11),
								in paisOrigen varchar(20),
								in departamentoNacimiento varchar(20),
								in lugarNacimiento varchar(20),
								in nacionalidad varchar(20),
								in imagenPerfil varchar(20),
								in sexo varchar(20),
								in tipoSangre varchar(20),
								in enfermedadesComunes varchar(20),
								in condicionesEspeciales varchar(20)		
							 )
BEGIN     
SET time_zone = '-06:00'; 

INSERT INTO heroku_39a7bb7a85bd88d.tblpersonas 
(
	codPersona,
	identificacion,
	nombres,
	apellidos,
	fechaNacimiento,
	edad,
	sexo,
	paisOrigen,
	nacionalidad,
	departamentoNacimiento,
	lugarNacimiento,
	direccionActual,
	celular,
	telefonoFijo,
	correoElectronico,
	tipoSangre,
	efermedadesComunes,
	condicionesEspeciales,
	aspirante,
	urlFotoPerfil)
VALUES
(
	null,
	identidad,
	nombre,
    apellido,
	fechaNacimiento,
	edad,
	sexo,
	paisOrigen,
	lugarNacimiento,
	departamentoNacimiento,
	lugarNacimiento,
	direccion,
	celular,
	telefono,
	correo,
	tipoSangre,
	enfermedadesComunes,
	condicionesEspeciales,
	null,
	imagenPerfil
    );
    
    INSERT INTO `heroku_39a7bb7a85bd88d`.`tblalumnos`
	(`codAlumno`,
	`numExpediente`,
	`indiceGeneral`,
	`traslado`,
	`fechaCreacion`,
	`fechaModificacion`)
	VALUES
	(LAST_INSERT_ID(),
	concat(LAST_INSERT_ID(),'-',identidad),
	null,
	null,
	NOW(),
	null);
    
    if ROW_COUNT()>0 then             
		select LAST_INSERT_ID() as IdAlumnoInsertado, ROW_COUNT() as rowCount;
	else
		select 0 as IdAlumnoInsertado, ROW_COUNT() as rowCount;
	end if;
    

	

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_asignatura` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_asignatura`( 
                                in asignatura varchar(20),
                                in habilitada int(11)
							 )
BEGIN     

	INSERT INTO `heroku_39a7bb7a85bd88d`.`tblasignaturas`
		(`codAsignatura`,
		`asignatura`,
        `habilitada`)
	VALUES
		(null,
		asignatura,
        habilitada);
        
        
        
        

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_asignatura_de_curso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_asignatura_de_curso`( 
                                in codCurso int(11),
                                in codAsignatura int(11),
                                in dias varchar(30)
		
							 )
BEGIN     

	INSERT INTO `heroku_39a7bb7a85bd88d`.`tblasignaturas_x_curso`
		(`codCurso`,
		`codAsignatura`,
		`dias`)
	VALUES
		(codCurso,
		codAsignatura,
		dias);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_asistencia_seccion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_asistencia_seccion`(
																			in codSeccion int(11),
																			in codAlumno int(11),
																			in fechaHoy varchar(20),
																			in asistio varchar(20)
)
BEGIN
	INSERT INTO `heroku_39a7bb7a85bd88d`.`tbldiasclase`
		(`codDiaClase`,
		`semana`,
		`fechaDia`,
		`habilitado`)
	VALUES
		(
		null,
		null,
		fechaHoy,
		1
		);

	
	INSERT INTO `heroku_39a7bb7a85bd88d`.`tblasistencia`
		(`codDiaClase`,
		`codAlumno`,
		`codSeccion`,
		`asistio`)
	VALUES
	(
		LAST_INSERT_ID(),
		codAlumno,
		codSeccion,
		asistio
	);







END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_aula` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_aula`( 
								in capacidad int(11),
                                in descripcion varchar(20),
                                in habilitada int(11)
								)
BEGIN     

		INSERT INTO `heroku_39a7bb7a85bd88d`.`tblaulas`
			(`codAula`,
			`capacidad`,
			`descripcion`,
			`habilitada`)
		VALUES
			(null,
			capacidad,
			descripcion,
			habilitada);

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_bitacora` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_bitacora`(in codUsuario int(11), in descripcion varchar(45),in accion int(11))
BEGIN

	#accion=1 se registra una accion con referencia a un procedimiento de obtner datos
	#accion=2 se registra una accion de insertar un registro
	#accion=3 se registra una accion de actualizar un registro 
	DECLARE descripcionConcatenada varchar(100);
	DECLARE nombreUsuario varchar(45);

SELECT  A.nombreUsuario INTO nombreUsuario
from heroku_39a7bb7a85bd88d.tblusuarios as A
where A.codUsuario=codUsuario;


set descripcionConcatenada= concat(descripcion,' el usuario "',nombreUsuario,'"');

	if accion = 1 then
		set descripcionConcatenada= concat(descripcionConcatenada,' realizo una consulta de datos');		 	
	elseif accion = 2 then
		set descripcionConcatenada= concat(descripcionConcatenada,' realizo una inserccion de datos');
	elseif accion = 3 then
		set descripcionConcatenada= concat(descripcionConcatenada,' realizo una actualizacion de datos');		
	end if;

	SET time_zone = '-06:00';
	INSERT INTO `heroku_39a7bb7a85bd88d`.`tblbitacora`
					(`codLog`,
					`codUsuario`,
					`fechaHora`,
					`descripcion`)
					VALUES
					(
					null,
					codUsuario,
					NOW(),
					descripcionConcatenada
					);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_cargo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_cargo`(
        in codDocente int (11),
        in codDepartamento int (11),
        in cargo varchar(20)
)
BEGIN

	INSERT INTO `heroku_39a7bb7a85bd88d`.`tblcargos`
		(`codCargo`,
		`codDepartamento`,
		`cargo`,
		`codDocente`)
	VALUES
		(null,
		codDepartamento,
		cargo,
		codDocente);
        
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_cargo_docente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_cargo_docente`(
        in codDocente int (11),
		in codCargo int (11),
		in fechaAsignacion varchar(20),
		in codDepartamento int (11),
		in fechaInicio varchar (20),
		in fechaFinal varchar (20),
		in institucion varchar (20)
)
BEGIN
		INSERT INTO `heroku_39a7bb7a85bd88d`.`tblcargos_x_docente`
(`codDocente`,
`codCargo`,
`fechaAsignacion`,
`codDepartamento`,
`fechaInicio`,
`fechaFinal`,
`institucion`)
VALUES
(
codDocente,
codCargo,
fechaAsignacion,
codDepartamento,
fechaInicio,
fechaFinal,
institucion
);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_curso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_curso`( 
                                in codModalidad int(11),
                                in nombreCurso varchar(20),
                                in numeroAsignaturas int(11),
                                in habilitada int(11)
		
							 )
BEGIN     
		INSERT INTO `heroku_39a7bb7a85bd88d`.`tblcursos`
			(`codCurso`,
			`codModalidad`,
			`nombreCurso`,
			`numeroAsignaturas`,
			`habilitado`)
		VALUES
			(null,
			 codModalidad ,
			 nombreCurso,
			 numeroAsignaturas,
			 habilitada);
             
			if ROW_COUNT()>0 then             
				select LAST_INSERT_ID() as IdCursoInsertado, ROW_COUNT() as rowCount;
			else
				select 0 as IdCursoInsertado,ROW_COUNT() as rowCount;
            end if;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_docente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_docente`(
        in nombreDocente varchar(20),
        in apellidoDocente varchar(20),
        in identidadDocente varchar(20),
        in direccionDocente varchar(30),
        in correoDocente varchar(20),
		in paisOrigenDocente varchar(20),
        in nacionalidadDocente varchar(20),
        in fechaNacimientoDocente varchar(20),
        in telefonoDocente varchar(20),
        in imagenDocente varchar(40),
        in celular varchar(30),
        in edad int (11),
        in lugarNacimiento varchar(30),
        in sexo varchar(10),
        in tipoSangre varchar (30),
        in enfermedadesComunes varchar(30),
        in condicionesEspeciales varchar(30),
        in departamentoNacimiento varchar(30)
        
        
)
BEGIN
SET time_zone = '-06:00'; 


INSERT INTO heroku_39a7bb7a85bd88d.tblpersonas
	(codPersona, identificacion, nombres, apellidos,
	fechaNacimiento, edad, sexo, paisOrigen, nacionalidad, 
    departamentoNacimiento, lugarNacimiento, direccionActual, 
    celular, telefonoFijo, correoElectronico, tipoSangre, 
    efermedadesComunes, condicionesEspeciales, aspirante, urlFotoPerfil)
VALUES
(null, identidadDocente, nombreDocente, apellidoDocente,
fechaNacimientoDocente, edad, sexo, paisOrigenDocente, nacionalidadDocente, 
departamentoNacimiento, lugarNacimiento, direccionDocente,
celular, telefonoDocente, correoDocente,tipoSangre, 
enfermedadesComunes, condicionesEspeciales, null, imagenDocente);


INSERT INTO heroku_39a7bb7a85bd88d.tbldocentes
(codDocente,
numExpediente,
fechaIngreso,
habilitado)
VALUES
(LAST_INSERT_ID(),
null,
NOW(),
1);

			if ROW_COUNT()>0 then             
				select LAST_INSERT_ID() as IdDocenteInsertado, ROW_COUNT() as rowCount;
			else
				select 0 as IdDocenteInsertado, ROW_COUNT() as rowCount;
            end if;
    

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_experiencia` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_experiencia`(
		in codDocente int (11),
        in cargoDesempanado varchar(20),
        in fechaInicio varchar(20),
        in fechaFin varchar(20),
        in areaEnsenanza varchar(20)
)
BEGIN

INSERT INTO `heroku_39a7bb7a85bd88d`.`tblexperiencia`
	(`codExperiencia`,
	`cargoDesempenado`,
	`fechaInicio`,
	`fechaFin`,
	`areaEnsenanza`,
	`institucion`,
	`codDocente`)
VALUES
	(null,
	cargoDesempanado,
	fechaInicio,
	fechaFin,
	areaEnsenanza,
	'',
	codDocente);


        
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_historial` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_historial`(
in fechaMatricula VARCHAR(20),
in	 codAlumno int(11),
in	 codTraslado int(11),
in	 asignatura VARCHAR(20),
in	 promedio VARCHAR(20),
in	 comentario VARCHAR(20),
in	 codAula int(11),
in	 codDocente int(11),
in	 codJornada int(11),
in	 codLectivo int(11),
in	 codPeriodo int(11),
in	 codAsignatura int(11),
in	 horaInicio VARCHAR(20),
in	 horaFin VARCHAR(20),
in     curso VARCHAR(20), 
in     codCurso int(11)
)
BEGIN
	#aqui debe ir una sentencia insert into tblhistorial
	INSERT INTO `heroku_39a7bb7a85bd88d`.`tblhistorial`
	(`fechaMatricula`,
	 `codAlumno`,
	 `codTraslado`,
	 `asignatura`,
	 `promedio`,
	 `comentario`,
	 `codAula`,
	 `codDocente`,
	 `codJornada`,
	 `codLectivo`,
	 `codPeriodo`,
	 `codAsignatura`,
	 `horaInicio`,
	 `horaFin`,
     `curso`, 
     `codCurso`) 
	 VALUES(
fechaMatricula,
	 codAlumno,
	 codTraslado,
	 asignatura,
	 promedio,
	 comentario,
	 codAula,
	 codDocente,
	 codJornada,
	 codLectivo,
	 codPeriodo,
	 codAsignatura,
	 horaInicio,
	 horaFin,
     curso, 
     codCurso);
		
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_incidente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_incidente`(
        in tituloIncidencia varchar(20),
        in descripcion varchar(20),
        in codTipoIncidencia int(11),
        in codAlumno int(11),
        in codDocente int(11),
        in fecha varchar(20),
        in codPersonaReporta int(11),
        in codPersonaReportada int(11)
        
)
BEGIN

INSERT INTO `heroku_39a7bb7a85bd88d`.`tblincidencias`
(`codIncidencia`,
`tituloIncidencia`,
`descripcion`,
`codTipoIncidencia`,
`codAlumno`,
`codDocente`,
`fecha`,
`codPersonaReporta`,
`codPersonaReportada`)
VALUES
(null,
tituloIncidencia,
descripcion,
codTipoIncidencia,
codAlumno,
codDocente,
fecha,
codPersonaReporta,
codPersonaReportada);



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_jornada` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_jornada`(
        in jornada varchar(20)
)
BEGIN

    INSERT INTO `heroku_39a7bb7a85bd88d`.`tbljornadas`
		(`codJornada`,
		`jornada`)
	VALUES
		(null,
		jornada);


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_lectivo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_lectivo`(
		
        in fechaInicio varchar(20),
        in fechaFinal varchar(20),
        in nombre varchar(20)
)
BEGIN
	INSERT INTO `heroku_39a7bb7a85bd88d`.`tbllectivos`
	(`codLectivo`,
	`fechaInicio`,
	`fechaFinal`,
	`nombre`)
	VALUES
	(null,
	fechaInicio,
	fechaFinal,
	nombre);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_matricula` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_matricula`(
 in in_codAlumno int(11),
 in in_codSeccion int(11),
 in in_promedio decimal(4,2),
 in in_fechaMatricula varchar(20),
 in in_na1 decimal(4,2),
 in in_ne1 decimal(4,2),
 in in_nf1 decimal(4,2),
 in in_na2 decimal(4,2),
 in in_ne2 decimal(4,2),
 in in_nf2 decimal(4,2),
 in in_na3 decimal(4,2),
 in in_ne3 decimal(4,2),
 in in_nf3 decimal(4,2),
 in in_na4 decimal(4,2),
 in in_ne4 decimal(4,2),
 in in_nf4 decimal(4,2),
 in in_fechaModificacion varchar(11)
)
BEGIN
/*
	este procedimiento inserta en tblmatricula una seccion y las calificaciones 
    correspondietes para cada seccion matriculada. 
    Esta tabla se visualiza en la el modulo matricula y en el modulo de evaluacion en la seccion
    de notas.
*/


	INSERT INTO heroku_39a7bb7a85bd88d.tblmatricula 
	(
	 codAlumno,
	 codSeccion,
	 promedio,
	 fechaMatricula,
	 na1,
	 ne1,
	 nf1,
	 na2,
	 ne2,
	 nf2,
	 na3,
	 ne3,
	 nf3,
	 na4,
	 ne4,
	 nf4,
	 fechaModificacion
	 ) 
	VALUES ( 
	 in_codAlumno ,
	 in_codSeccion ,
	 in_promedio ,
	 in_fechaMatricula ,
	 in_na1 ,
	 in_ne1 ,
	 in_nf1 ,
	 in_na2 ,
	 in_ne2 ,
	 in_nf2 ,
	 in_na3 ,
	 in_ne3 ,
	 in_nf3 ,
	 in_na4 ,
	 in_ne4 ,
	 in_nf4 ,
     now()
     );


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_modalidades` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_modalidades`( in modalidad varchar(45))
BEGIN
    
		INSERT INTO `heroku_39a7bb7a85bd88d`.`tblmodalidades`
			(codModalidad,	modalidad)	
		VALUES
			(null,modalidad);    
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_new_user` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_new_user`( in nombreUsuario varchar(20),
									  in contrasena varchar(20),
									  in codPersona int(11),
									  in codTipoUsuario int(11))
BEGIN
	SET time_zone = '-06:00';  
	INSERT INTO `heroku_39a7bb7a85bd88d`.`tblusuarios`
		(`codUsuario`,
		`nombreUsuario`,
		`contrasena`,
		`codPersona`,
		`codTipoUsuario`,
		`habilitado`,
		`fechaCreacion`,
		`fechaModificacion`)
		VALUES
		(null,
		nombreUsuario,
		contrasena,
		codPersona,
		codTipoUsuario,
		1,
		NOW(),
		NOW());

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_periodo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_periodo`(
		in nombrePeriodo varchar(45),
        in fechaInicio varchar(20),
        in fechaFin varchar(20),
        in anio int(11)
)
BEGIN

    
    INSERT INTO `heroku_39a7bb7a85bd88d`.`tblperiodos`
		(`codPeriodo`,
		`nombrePeriodo`,
		`fechaInicio`,
		`fechafin`,
		`anio`)
	VALUES
		(null,
		nombrePeriodo,
		fechaInicio,
		fechaFin,
		anio);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_seccion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_seccion`(
        in seccion varchar(20),
        in codDocente int (11),
        in horaInicio varchar (11),
        in horaFin varchar (11),
		in codCurso int(11),
        in codAula int(11),
        in habilitado int(11),
        in codPeriodo int(11),
        in codLectivo int (11),
        in codAsignatura int(11),
        in codJornada int(11)
)
BEGIN

INSERT INTO `heroku_39a7bb7a85bd88d`.`tblsecciones`
(`codSeccion`,
`seccion`,
`horaInicio`,
`horaFin`,
`codAula`,
`codDocente`,
`codPeriodo`,
`codLectivo`,
`codCurso`,
`codAsignatura`,
`suspendida`,
`codJornada`)
VALUES
(null,
seccion,
concat("0000-00-00 ",horaInicio),
concat("0000-00-00 ",horaFin),
codAula,
codDocente,
codPeriodo,
codLectivo,
codCurso,
codAsignatura,
habilitado,
codJornada);

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_tiposusuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_tiposusuarios`( 
										   in tipoUsuario varchar(20),
										   in accesos varchar(20),
                                           in habilitado int(11)
                                      )
BEGIN         
        SET time_zone = '-06:00'; 
        INSERT INTO `heroku_39a7bb7a85bd88d`.`tbltiposusuarios`
			(`codTipoUsuario`,
			`tipoUsuario`,
			`accesos`,
			`fechaCreacion`,
			`fechaModificacion`,
			`habilitado`)
		VALUES
			(null, tipoUsuario, accesos, NOW(), '', habilitado);

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_traslado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_traslado`( 
                                in codAlumno int(11),
                                in institucion varchar(20),
								in fechaFinal varchar(20),
								in fechaTraslado varchar(20),
								in promedioTraslado int(11)
							 )
BEGIN     
	INSERT INTO `heroku_39a7bb7a85bd88d`.`tbltraslados`
(`codTraslado`,
`codAlumno`,
`institucion`,
`fechaFinal`,
`fechaTraslado`,
`promedioTraslado`)
VALUES
(null,
codAlumno,
institucion,
fechaFinal,
fechaTraslado,
promedioTraslado);

if ROW_COUNT()>0 then             
				select LAST_INSERT_ID() as IdTrasladoInsertado, ROW_COUNT() as rowCount;
			else
				select 0 as IdTrasladoInsertado, ROW_COUNT() as rowCount;
            end if;




END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_insert_tutor` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_insert_tutor`( 
		in in_t_codAlumno int,
        in in_t_parentesco varchar(40),
        in in_t_ocupacion varchar(40),
        in in_t_lugarDeTrabajo varchar(40),
        in in_t_telefonoTrabajo varchar(40),
        in in_p_identificacion varchar(40),
        in in_p_nombres varchar(40), 
        in in_p_apellidos varchar(40),
        in in_p_telefonoFijo varchar(40),
        in in_p_celular varchar(40),
        in in_p_correoElectronico varchar(40),
        in in_p_codPersona int
)
BEGIN
	#Cuando in_p_codPersona > 0 se asume que el registro ya existe en tblpersonas y solo se agregaran campos respectivos
    #a la tabla tutores, de lo contrario se registrara como un tutor nuevo. 
	declare in_codPersona int;
	#primero insertamos en la tabla personas
    insert into heroku_39a7bb7a85bd88d.tblpersonas(
		identificacion,
        nombres, 
        apellidos,
        telefonoFijo,
        celular,
        correoElectronico
        )
	values (
		in_p_identificacion,
        in_p_nombres, 
        in_p_apellidos,
        in_p_telefonoFijo,
        in_p_celular,
        in_p_correoElectronico);
    
    select LAST_INSERT_ID()  into in_codPersona;
    
    #luego insertamos en la tabla tutores
    insert into heroku_39a7bb7a85bd88d.tbltutores(
								codTutor,
								codAlumno,
								parentesco,
								ocupacion,
								lugarDeTrabajo,
								telefonoTrabajo
						)
						values(
								in_codPersona,
								in_t_codAlumno,
								in_t_parentesco,
								in_t_ocupacion,
								in_t_lugarDeTrabajo,
								in_t_telefonoTrabajo
						);
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_alumno` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_alumno`( 
									in codAlumno varchar(20),
                                    in nombre varchar(20),
									in apellido varchar(20),
									in identidad varchar(20),
									in fechaNacimiento varchar(20),
									in telefono varchar(20),
									in celular varchar(20),
									in correo varchar(20),
									in direccion varchar(20),
									in edad int(11),
									in paisOrigen varchar(20),
									in departamentoNacimiento varchar(20),
									in lugarNacimiento varchar(20),
									in nacionalidad varchar(20),
									in imagenPerfil varchar(20),
									in sexo varchar(20),
									in tipoSangre varchar(20),
									in enfermedadesComunes varchar(20),
									in condicionesEspeciales varchar(20)
							 )
BEGIN     
	UPDATE heroku_39a7bb7a85bd88d.tblpersonas as A
	SET
	A.identificacion = identidad,
	A.nombres = nombre,
	A.apellidos = apellido,
	A.fechaNacimiento = fechaNacimiento,
	A.edad = edad,
	A.sexo = sexo,
	A.paisOrigen = paisOrigen,
	A.nacionalidad = nacionalidad,
	A.departamentoNacimiento = departamentoNacimiento,
	A.lugarNacimiento = lugarNacimiento,
	A.direccionActual = direccion,
	A.celular = celular,
	A.telefonoFijo = telefono,
	A.correoElectronico = correo,
	A.tipoSangre = tipoSangre,
	A.efermedadesComunes = enfermedadesComunes,
	A.condicionesEspeciales = condicionesEspeciales,
	A.aspirante = condicionesEspeciales,
	A.urlFotoPerfil = imagenPerfil
	WHERE A.codPersona = codAlumno;

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_asignatura_x_curso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_asignatura_x_curso`( 
                                in codCurso int(11),
                                in codAsignatura int(11),
                                in asignatura varchar(45),
                                in dias varchar(30),
                                in habilitada int(11),
                                in nuevoCodCurso int(11)
							 )
BEGIN     
	
    if nuevoCodCurso >0 then
    UPDATE `heroku_39a7bb7a85bd88d`.`tblasignaturas_x_curso` as A
	SET
		A.codCurso = nuevoCodCurso,
		A.codAsignatura = codAsignatura,
		A.dias = dias
	WHERE A.codCurso = codCurso AND A.codAsignatura = codAsignatura;
    else 
    UPDATE `heroku_39a7bb7a85bd88d`.`tblasignaturas_x_curso` as A
	SET
		A.codCurso = codCurso,
		A.codAsignatura = codAsignatura,
		A.dias = dias
	WHERE A.codCurso = codCurso AND A.codAsignatura = codAsignatura;
    
    end if;
    
    
	UPDATE `heroku_39a7bb7a85bd88d`.`tblasignaturas` as A
	SET
		A.asignatura = asignatura,
        A.habilitada=habilitada
	WHERE A.codAsignatura = codAsignatura;
    
    


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_aula` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_aula`( 
								in in_codAula int(11),
                                in in_capacidad int(11),
                                in in_descripcion varchar(20),
                                in in_habilitada int(11)
							 )
BEGIN     
	UPDATE `heroku_39a7bb7a85bd88d`.`tblaulas` as A
	SET
		A.capacidad = in_capacidad,
		A.descripcion = in_descripcion,
		A.habilitada = in_habilitada
	WHERE A.codAula = in_codAula;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_cargo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_cargo`(
		in codCargo int (11),
        in codDocente int (11),
        in codDepartamento int (11),
        in cargo varchar(20)
)
BEGIN

	UPDATE `heroku_39a7bb7a85bd88d`.`tblcargos` as A
	SET
		A.codDepartamento = codDepartamento,
		A.cargo = cargo,
		A.codDocente = codDocente
	WHERE A.codCargo = codCargo;

        
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_cargo_docente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_cargo_docente`(
        in codDocente int (11),
		in codCargo int (11),
		in fechaAsignacion varchar(20),
		in codDepartamento int (11),
		in fechaInicio varchar (20),
		in fechaFinal varchar (20),
		in institucion varchar (20)
)
BEGIN

		UPDATE `heroku_39a7bb7a85bd88d`.`tblcargos_x_docente` as A
		SET
		A.codCargo = codCargo,
		A.fechaAsignacion = fechaAsignacion,
		A.codDepartamento = codDepartamento,
		A.fechaInicio = fechaInicio,
		A.fechaFinal = fechaFinal,
		A.institucion = institucion
		WHERE A.codDocente=codDocente;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_curso` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_curso`( 
								in in_codCurso int(11),
                                in in_codModalidad int(11),
                                in in_nombreCurso varchar(20),
                                in in_numeroAsignaturas int(11),
                                in in_habilitada int(11)
		
							 )
BEGIN     
		UPDATE `heroku_39a7bb7a85bd88d`.`tblcursos` as A
		SET
			A.codModalidad = in_codModalidad,
			A.nombreCurso = in_nombreCurso,
			A.numeroAsignaturas = in_numeroAsignaturas,
			A.habilitado = in_habilitada
		WHERE A.codCurso = in_codCurso;

    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_docente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_docente`(
        in in_codDocente int(11),
        in nombreDocente varchar(20),
        in apellidoDocente varchar(20),
        in identidadDocente varchar(20),
        in direccionDocente varchar(30),
        in correoDocente varchar(20),
		in paisOrigenDocente varchar(20),
        in nacionalidadDocente varchar(20),
        in fechaNacimientoDocente varchar(20),
        in telefonoDocente varchar(20),
        in imagenDocente varchar(40),
        in celular varchar(20),
        in edad int (11),
        in lugarNacimiento varchar(20),
        in sexo varchar(10),
        in tipoSangre varchar(20),
        in enfermedadesComunes varchar(20),
        in condicionesEspeciales varchar(20),
        in departamentoNacimiento varchar(20),
        in in_habilitado  int(11)
        
)
BEGIN

UPDATE heroku_39a7bb7a85bd88d.tblpersonas as A
SET
A.identificacion = identidadDocente,
A.nombres = nombreDocente,
A.apellidos = apellidoDocente,
A.fechaNacimiento = fechaNacimientoDocente,
A.edad = edad,
A.sexo = sexo,
A.paisOrigen = paisOrigenDocente,
A.nacionalidad = nacionalidadDocente,
A.departamentoNacimiento = departamentoNacimiento,
A.lugarNacimiento = lugarNacimiento,
A.direccionActual = direccionDocente,
A.celular = celular,
A.telefonoFijo = telefonoDocente,
A.correoElectronico = correoDocente,
A.tipoSangre = tipoSangre,
A.efermedadesComunes = enfermedadesComunes,
A.condicionesEspeciales = condicionesEspeciales,
A.aspirante = null,
A.urlFotoPerfil = imagenDocente
WHERE A.codPersona = in_codDocente;


UPDATE heroku_39a7bb7a85bd88d.tbldocentes
SET
`habilitado` = in_habilitado
WHERE `codDocente` = in_codDocente;




END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_experiencia` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_experiencia`(
		in codDocente int (11),
        in codExperiencia int (11),
        in cargoDesempanado varchar(20),
        in fechaInicio varchar(20),
        in fechaFin varchar(20),
        in areaEnsenanza varchar(20)
)
BEGIN

	UPDATE `heroku_39a7bb7a85bd88d`.`tblexperiencia` as A
	SET
		A.cargoDesempenado = cargoDesempanado,
		A.fechaInicio = fechaInicio,
		A.fechaFin = fechaFin,
		A.areaEnsenanza = areaEnsenanza,
		A.institucion = '',
		A.codDocente = codDocente
	WHERE A.codExperiencia = codExperiencia;

                
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_incidente` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_incidente`(
		in codIncidencia int(11),
        in tituloIncidencia varchar(20),
        in descripcion varchar(20),
        in codTipoIncidencia int(11),
        in codAlumno int(11),
        in codDocente int(11),
        in fecha varchar(20),
        in codPersonaReporta int(11),
        in codPersonaReportada int(11)
        
)
BEGIN

UPDATE heroku_39a7bb7a85bd88d.tblincidencias as A
SET
A.tituloIncidencia = tituloIncidencia,
A.descripcion = descripcion,
A.codTipoIncidencia = codTipoIncidencia,
A.codAlumno = codAlumno,
A.codDocente = codDocente,
A.fecha = fecha,
A.codPersonaReporta = codPersonaReporta,
A.codPersonaReportada = codPersonaReportada
WHERE A.codIncidencia = codIncidencia;

END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_jornada` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_jornada`(
		in codJornada int(11),
        in jornada varchar(20)
)
BEGIN

	  UPDATE `heroku_39a7bb7a85bd88d`.`tbljornadas` as A
	SET
	A.jornada = jornada
	WHERE A.codJornada = codJornada;



END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_lectivo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_lectivo`(
		in codLectivo int(11),
        in fechaInicio varchar(20),
        in fechaFinal varchar(20),
        in nombre varchar(45)
)
BEGIN
	UPDATE `heroku_39a7bb7a85bd88d`.`tbllectivos`  as A
	SET 
		A.fechaInicio = fechaInicio,
		A.fechaFinal = fechaFinal,
		A.nombre = nombre
	WHERE
		A.codLectivo = codLectivo;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_matricula` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_matricula`(
 in in_codAlumno int(11),
 in in_codSeccion int(11),
 in in_promedio decimal(4,2), 
 in in_na1 decimal(4,2),
 in in_ne1 decimal(4,2),
 in in_nf1 decimal(4,2),
 in in_na2 decimal(4,2),
 in in_ne2 decimal(4,2),
 in in_nf2 decimal(4,2),
 in in_na3 decimal(4,2),
 in in_ne3 decimal(4,2),
 in in_nf3 decimal(4,2),
 in in_na4 decimal(4,2),
 in in_ne4 decimal(4,2),
 in in_nf4 decimal(4,2)
)
BEGIN

update heroku_39a7bb7a85bd88d.tblmatricula 
set  promedio = in_promedio,
	 na1 = in_na1,
	 ne1 = in_ne1,
	 nf1 = in_nf1,
	 na2 = in_na2,
	 ne2 = in_ne2,
	 nf2 = in_nf2,
	 na3 = in_na3,
	 ne3 = in_ne3,
	 nf3 = in_nf3,
	 na4 = in_na4,
	 ne4 = in_ne4,
	 nf4 = in_nf4,
	 fechaModificacion = now()
where codAlumno = in_codAlumno and codSeccion = in_codSeccion;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_modalidades` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_modalidades`( in  codModalidad int(11), in modalidad varchar(45))
BEGIN
    
		UPDATE `heroku_39a7bb7a85bd88d`.`tblmodalidades` as A
		SET
		A.modalidad = modalidad
		WHERE A.codModalidad = codModalidad;
  
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_periodo` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_periodo`(
		in codPeriodo int (11),
		in nombrePeriodo varchar(45),
        in fechaInicio varchar(20),
        in fechaFin varchar(20),
        in anio int(11)
)
BEGIN

	UPDATE `heroku_39a7bb7a85bd88d`.`tblperiodos` as A
SET 
    A.nombrePeriodo = nombrePeriodo,
    A.fechaInicio = fechaInicio,
    A.fechafin = fechaFin,
    A.anio = anio
WHERE
    A.codPeriodo = codPeriodo;


                
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_seccion` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_seccion`(
        in codSeccion int(11),
        in seccion varchar(20),
        in codDocente int (11),
        in horaInicio varchar (11),
        in horaFin varchar (11),
		in codCurso int(11),
        in codAula int(11),
        in habilitado int(11),
        in codPeriodo int(11),
        in codLectivo int (11),
        in codAsignatura int(11),
        in codJornada int(11)
)
BEGIN


UPDATE heroku_39a7bb7a85bd88d.tblsecciones as A
SET
A.seccion = seccion,
A.horaInicio = CONCAT('0000-00-00 ',horaInicio),
A.horaFin = CONCAT('0000-00-00 ',horaFin),
A.codAula = codAula,
A.codDocente = codDocente,
A.codCurso = codCurso,
A.suspendida = habilitado,
A.codPeriodo = codPeriodo,
A.codLectivo = codLectivo,
A.codCurso = codCurso,
A.codAsignatura = codAsignatura,
A.codJornada = codJornada
WHERE A.codSeccion = codSeccion;
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_tiposUsuarios` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_tiposUsuarios`( 
										in codTipoUsuario int(11), 
										in tipoUsuario varchar(20),
										in accesos varchar(20),
                                        in habilitado int(11)
                                      )
BEGIN     
		SET time_zone = '-06:00'; 
		UPDATE `heroku_39a7bb7a85bd88d`.`tbltiposusuarios` as A
		SET
			A.tipoUsuario = tipoUsuario,
			A.accesos = accesos,
			A.fechaModificacion = NOW(),
			A.habilitado = habilitado
		WHERE A.codTipoUsuario = codTipoUsuario;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_traslado` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_traslado`( 
                                in codTraslado int (11),
                                in codAlumno int(11),
                                in institucion varchar(20),
								in fechaFinal varchar(20),
								in fechaTraslado varchar(20),
								in promedioTraslado int(11)
							 )
BEGIN     
	UPDATE heroku_39a7bb7a85bd88d.tbltraslados as A
SET
A.codAlumno = codAlumno,
A.institucion = institucion,
A.fechaFinal = fechaFinal,
A.fechaTraslado = fechaTraslado,
A.promedioTraslado = promedioTraslado
WHERE A.codTraslado = codTraslado;


END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `sp_update_user_details` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`b1e86835ce3929`@`%` PROCEDURE `sp_update_user_details`( in codigoUsuario int(11),
									  in nombreUsuario varchar(20),
									  in contrasena varchar(20),
									  in codTipoUsuario int(11),
                                      in habilitado int(11)
                                      )
BEGIN         
        
        if contrasena=" " then
        UPDATE `heroku_39a7bb7a85bd88d`.`tblusuarios` as A
		SET 
		A.nombreUsuario = nombreUsuario,
		A.codTipoUsuario = codTipoUsuario,
		A.habilitado = habilitado,
		A.fechaModificacion = DATE_SUB(current_timestamp(), INTERVAL 6 hour)
		WHERE A.codUsuario = codigoUsuario;
        elseif contrasena!=" " then
        UPDATE `heroku_39a7bb7a85bd88d`.`tblusuarios` as A
		SET 
		A.nombreUsuario = nombreUsuario,
		A.contrasena = contrasena,
		A.codTipoUsuario = codTipoUsuario,
		A.habilitado = habilitado,
		A.fechaModificacion = DATE_SUB(current_timestamp(), INTERVAL 6 hour)
		WHERE A.codUsuario = codigoUsuario;
        end if;
   
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-05-19 22:05:03
