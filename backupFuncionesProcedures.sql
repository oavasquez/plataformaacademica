CREATE DATABASE  IF NOT EXISTS `heroku_39a7bb7a85bd88d` /*!40100 DEFAULT CHARACTER SET utf8 */;
USE `heroku_39a7bb7a85bd88d`;
-- MySQL dump 10.13  Distrib 5.7.20, for Linux (x86_64)
--
-- Host: localhost    Database: heroku_39a7bb7a85bd88d
-- ------------------------------------------------------
-- Server version	5.7.21-0ubuntu0.16.04.1

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `category`
--

DROP TABLE IF EXISTS `category`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `category` (
  `category_id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  PRIMARY KEY (`category_id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `category`
--

LOCK TABLES `category` WRITE;
/*!40000 ALTER TABLE `category` DISABLE KEYS */;
/*!40000 ALTER TABLE `category` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblAlumnos`
--

DROP TABLE IF EXISTS `tblAlumnos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAlumnos` (
  `codAlumno` int(11) NOT NULL,
  `numExpediente` varchar(25) NOT NULL,
  `indiceGeneral` decimal(6,2) DEFAULT NULL,
  `traslado` int(11) DEFAULT NULL,
  PRIMARY KEY (`codAlumno`),
  KEY `fk_table1_tblPersona1_idx` (`codAlumno`),
  CONSTRAINT `fk_table1_tblPersona1` FOREIGN KEY (`codAlumno`) REFERENCES `tblPersonas` (`codPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblAlumnos`
--

LOCK TABLES `tblAlumnos` WRITE;
/*!40000 ALTER TABLE `tblAlumnos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblAlumnos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblAsignaturas`
--

DROP TABLE IF EXISTS `tblAsignaturas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAsignaturas` (
  `codAsignatura` int(11) NOT NULL AUTO_INCREMENT,
  `asignatura` varchar(45) DEFAULT NULL,
  `dias` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`codAsignatura`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblAsignaturas`
--

LOCK TABLES `tblAsignaturas` WRITE;
/*!40000 ALTER TABLE `tblAsignaturas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblAsignaturas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblAsignaturas_x_curso`
--

DROP TABLE IF EXISTS `tblAsignaturas_x_curso`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAsignaturas_x_curso` (
  `codCurso` int(11) NOT NULL,
  `codAsignatura` int(11) NOT NULL,
  PRIMARY KEY (`codCurso`,`codAsignatura`),
  KEY `fk_tblAsignaturas_x_curso_tblCursos1_idx` (`codCurso`),
  KEY `fk_tblAsignaturas_x_curso_tblAsignaturas1_idx` (`codAsignatura`),
  CONSTRAINT `fk_tblAsignaturas_x_curso_tblAsignaturas1` FOREIGN KEY (`codAsignatura`) REFERENCES `tblAsignaturas` (`codAsignatura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblAsignaturas_x_curso_tblCursos1` FOREIGN KEY (`codCurso`) REFERENCES `tblCursos` (`codCurso`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblAsignaturas_x_curso`
--

LOCK TABLES `tblAsignaturas_x_curso` WRITE;
/*!40000 ALTER TABLE `tblAsignaturas_x_curso` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblAsignaturas_x_curso` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblAsistencia`
--

DROP TABLE IF EXISTS `tblAsistencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAsistencia` (
  `codSeccion` int(11) NOT NULL,
  `codDiaClase` int(11) NOT NULL,
  KEY `fk_tblAsistencia_tblSecciones1_idx` (`codSeccion`),
  KEY `fk_tblAsistencia_tblDiasClase1_idx` (`codDiaClase`),
  CONSTRAINT `fk_tblAsistencia_tblDiasClase1` FOREIGN KEY (`codDiaClase`) REFERENCES `tblDiasClase` (`codDiaClase`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblAsistencia_tblSecciones1` FOREIGN KEY (`codSeccion`) REFERENCES `tblSecciones` (`codSeccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblAsistencia`
--

LOCK TABLES `tblAsistencia` WRITE;
/*!40000 ALTER TABLE `tblAsistencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblAsistencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblAulas`
--

DROP TABLE IF EXISTS `tblAulas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblAulas` (
  `codAula` int(11) NOT NULL AUTO_INCREMENT,
  `capacidad` int(11) DEFAULT NULL,
  `descripcion` varchar(50) DEFAULT NULL,
  `habilitada` int(11) DEFAULT NULL,
  PRIMARY KEY (`codAula`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblAulas`
--

LOCK TABLES `tblAulas` WRITE;
/*!40000 ALTER TABLE `tblAulas` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblAulas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblBitacora`
--

DROP TABLE IF EXISTS `tblBitacora`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblBitacora` (
  `codLog` int(11) NOT NULL AUTO_INCREMENT,
  `codUsuario` int(11) NOT NULL,
  `fechaHora` datetime DEFAULT NULL,
  `descripcion` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codLog`),
  KEY `fk_tblBitacora_tblUsuarios1_idx` (`codUsuario`),
  CONSTRAINT `fk_tblBitacora_tblUsuarios1` FOREIGN KEY (`codUsuario`) REFERENCES `tblUsuarios` (`codUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblBitacora`
--

LOCK TABLES `tblBitacora` WRITE;
/*!40000 ALTER TABLE `tblBitacora` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblBitacora` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblCargos`
--

DROP TABLE IF EXISTS `tblCargos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblCargos` (
  `codCargo` int(11) NOT NULL AUTO_INCREMENT,
  `codDepartamento` int(11) NOT NULL,
  `cargo` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codCargo`),
  KEY `fk_tblCargos_tblDepartamentos1_idx` (`codDepartamento`),
  CONSTRAINT `fk_tblCargos_tblDepartamentos1` FOREIGN KEY (`codDepartamento`) REFERENCES `tblDepartamentos` (`codDepartamento`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblCargos`
--

LOCK TABLES `tblCargos` WRITE;
/*!40000 ALTER TABLE `tblCargos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblCargos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblCursos`
--

DROP TABLE IF EXISTS `tblCursos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblCursos` (
  `codCurso` int(11) NOT NULL AUTO_INCREMENT,
  `codModalidad` int(11) NOT NULL,
  `nombreCurso` varchar(45) DEFAULT NULL,
  `numeroAsignaturas` int(11) DEFAULT NULL,
  PRIMARY KEY (`codCurso`),
  KEY `fk_tblCursos_tblModalidades1_idx` (`codModalidad`),
  CONSTRAINT `fk_tblCursos_tblModalidades1` FOREIGN KEY (`codModalidad`) REFERENCES `tblModalidades` (`codModalidad`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblCursos`
--

LOCK TABLES `tblCursos` WRITE;
/*!40000 ALTER TABLE `tblCursos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblCursos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblDepartamentos`
--

DROP TABLE IF EXISTS `tblDepartamentos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblDepartamentos` (
  `codDepartamento` int(11) NOT NULL AUTO_INCREMENT,
  `nombreDepartamento` varchar(30) DEFAULT NULL,
  PRIMARY KEY (`codDepartamento`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblDepartamentos`
--

LOCK TABLES `tblDepartamentos` WRITE;
/*!40000 ALTER TABLE `tblDepartamentos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblDepartamentos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblDiasClase`
--

DROP TABLE IF EXISTS `tblDiasClase`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblDiasClase` (
  `codDiaClase` int(11) NOT NULL AUTO_INCREMENT,
  `semana` int(11) DEFAULT NULL,
  `fechaDia` datetime DEFAULT NULL,
  PRIMARY KEY (`codDiaClase`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblDiasClase`
--

LOCK TABLES `tblDiasClase` WRITE;
/*!40000 ALTER TABLE `tblDiasClase` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblDiasClase` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblDocentes`
--

DROP TABLE IF EXISTS `tblDocentes`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblDocentes` (
  `codDocente` int(11) NOT NULL AUTO_INCREMENT,
  `codCargo` int(11) NOT NULL,
  `numExpediente` varchar(25) DEFAULT NULL,
  `fechaIngreso` date DEFAULT NULL,
  PRIMARY KEY (`codDocente`),
  KEY `fk_tblDocentes_tblPersona_idx` (`codDocente`),
  KEY `fk_tblDocentes_tblCargos1_idx` (`codCargo`),
  CONSTRAINT `fk_tblDocentes_tblCargos1` FOREIGN KEY (`codCargo`) REFERENCES `tblCargos` (`codCargo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblDocentes_tblPersona` FOREIGN KEY (`codDocente`) REFERENCES `tblPersonas` (`codPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblDocentes`
--

LOCK TABLES `tblDocentes` WRITE;
/*!40000 ALTER TABLE `tblDocentes` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblDocentes` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblEstados`
--

DROP TABLE IF EXISTS `tblEstados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblEstados` (
  `codEstado` int(11) NOT NULL AUTO_INCREMENT,
  `estado` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codEstado`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblEstados`
--

LOCK TABLES `tblEstados` WRITE;
/*!40000 ALTER TABLE `tblEstados` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblEstados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblExperiencia`
--

DROP TABLE IF EXISTS `tblExperiencia`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblExperiencia` (
  `codExperiencia` int(11) NOT NULL AUTO_INCREMENT,
  `cargoDesempenado` varchar(45) DEFAULT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechaFin` date DEFAULT NULL,
  `areaEnsenanza` varchar(45) DEFAULT NULL,
  `institucion` varchar(50) DEFAULT NULL,
  `codDocente` int(11) NOT NULL,
  PRIMARY KEY (`codExperiencia`),
  KEY `fk_tblExperiencia_tblDocentes1_idx` (`codDocente`),
  CONSTRAINT `fk_tblExperiencia_tblDocentes1` FOREIGN KEY (`codDocente`) REFERENCES `tblDocentes` (`codDocente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblExperiencia`
--

LOCK TABLES `tblExperiencia` WRITE;
/*!40000 ALTER TABLE `tblExperiencia` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblExperiencia` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblHistorial`
--

DROP TABLE IF EXISTS `tblHistorial`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblHistorial` (
  `codAlumno` int(11) NOT NULL,
  `seccion` varchar(20) DEFAULT NULL,
  `horainicio` int(11) DEFAULT NULL,
  `horafinal` int(11) DEFAULT NULL,
  `codDocente` int(11) DEFAULT NULL,
  `codAsignatura` int(11) DEFAULT NULL,
  `codPeriodo` int(11) DEFAULT NULL,
  `codLectivo` int(11) DEFAULT NULL,
  `promedio` decimal(12,2) DEFAULT NULL,
  `observacion` varchar(25) DEFAULT NULL,
  `codTraslado` int(11) NOT NULL,
  KEY `fk_tblHistorial_tblAlumnos1_idx` (`codAlumno`),
  KEY `fk_tblHistorial_tblTraslados1_idx` (`codTraslado`),
  CONSTRAINT `fk_tblHistorial_tblAlumnos1` FOREIGN KEY (`codAlumno`) REFERENCES `tblAlumnos` (`codAlumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblHistorial_tblTraslados1` FOREIGN KEY (`codTraslado`) REFERENCES `tblTraslados` (`codTraslado`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblHistorial`
--

LOCK TABLES `tblHistorial` WRITE;
/*!40000 ALTER TABLE `tblHistorial` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblHistorial` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblIncidencias`
--

DROP TABLE IF EXISTS `tblIncidencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblIncidencias` (
  `codIncidencia` int(11) NOT NULL AUTO_INCREMENT,
  `tituloIncidencia` varchar(25) DEFAULT NULL,
  `descripcion` varchar(300) DEFAULT NULL,
  `codTipoIncidencia` int(11) NOT NULL,
  `codAlumno` int(11) NOT NULL,
  `codDocente` int(11) NOT NULL,
  `fecha` date DEFAULT NULL,
  PRIMARY KEY (`codIncidencia`),
  KEY `fk_tblIncidencias_tblTiposIncidencias1_idx` (`codTipoIncidencia`),
  KEY `fk_tblIncidencias_tblAlumnos1_idx` (`codAlumno`),
  KEY `fk_tblIncidencias_tblDocentes1_idx` (`codDocente`),
  CONSTRAINT `fk_tblIncidencias_tblAlumnos1` FOREIGN KEY (`codAlumno`) REFERENCES `tblAlumnos` (`codAlumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblIncidencias_tblDocentes1` FOREIGN KEY (`codDocente`) REFERENCES `tblDocentes` (`codDocente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblIncidencias_tblTiposIncidencias1` FOREIGN KEY (`codTipoIncidencia`) REFERENCES `tblTiposIncidencias` (`codTipoIncidencia`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblIncidencias`
--

LOCK TABLES `tblIncidencias` WRITE;
/*!40000 ALTER TABLE `tblIncidencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblIncidencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblLectivos`
--

DROP TABLE IF EXISTS `tblLectivos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblLectivos` (
  `codLectivo` int(11) NOT NULL AUTO_INCREMENT,
  `fechaInicio` date DEFAULT NULL,
  `fechaFinal` date DEFAULT NULL,
  `nombre` varchar(100) DEFAULT NULL,
  PRIMARY KEY (`codLectivo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblLectivos`
--

LOCK TABLES `tblLectivos` WRITE;
/*!40000 ALTER TABLE `tblLectivos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblLectivos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblMatricula`
--

DROP TABLE IF EXISTS `tblMatricula`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblMatricula` (
  `codAlumno` int(11) NOT NULL,
  `codSeccion` int(11) NOT NULL,
  `promedio` decimal(6,2) DEFAULT NULL,
  `codEstado` int(11) NOT NULL,
  KEY `fk_tblMatricula_tblAlumnos1_idx` (`codAlumno`),
  KEY `fk_tblMatricula_tblSecciones1_idx` (`codSeccion`),
  KEY `fk_tblMatricula_tblEstados1_idx` (`codEstado`),
  CONSTRAINT `fk_tblMatricula_tblAlumnos1` FOREIGN KEY (`codAlumno`) REFERENCES `tblAlumnos` (`codAlumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblMatricula_tblEstados1` FOREIGN KEY (`codEstado`) REFERENCES `tblEstados` (`codEstado`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblMatricula_tblSecciones1` FOREIGN KEY (`codSeccion`) REFERENCES `tblSecciones` (`codSeccion`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblMatricula`
--

LOCK TABLES `tblMatricula` WRITE;
/*!40000 ALTER TABLE `tblMatricula` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblMatricula` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblModalidades`
--

DROP TABLE IF EXISTS `tblModalidades`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblModalidades` (
  `codModalidad` int(11) NOT NULL AUTO_INCREMENT,
  `modalidad` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codModalidad`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblModalidades`
--

LOCK TABLES `tblModalidades` WRITE;
/*!40000 ALTER TABLE `tblModalidades` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblModalidades` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblPeriodos`
--

DROP TABLE IF EXISTS `tblPeriodos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblPeriodos` (
  `codPeriodo` int(11) NOT NULL AUTO_INCREMENT,
  `nombrePeriodo` varchar(45) NOT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechafin` date DEFAULT NULL,
  `anio` int(11) DEFAULT NULL,
  PRIMARY KEY (`codPeriodo`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblPeriodos`
--

LOCK TABLES `tblPeriodos` WRITE;
/*!40000 ALTER TABLE `tblPeriodos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblPeriodos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblPersonas`
--

DROP TABLE IF EXISTS `tblPersonas`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblPersonas` (
  `codPersona` int(11) NOT NULL AUTO_INCREMENT,
  `identificacion` varchar(25) DEFAULT NULL,
  `nombres` varchar(45) DEFAULT NULL,
  `apellidos` varchar(45) DEFAULT NULL,
  `fechaNacimiento` date DEFAULT NULL,
  `edad` int(11) DEFAULT NULL,
  `sexo` varchar(5) DEFAULT NULL,
  `paisOrigen` varchar(20) DEFAULT NULL,
  `nacionalidad` varchar(20) DEFAULT NULL,
  `departamentoNacimiento` varchar(45) DEFAULT NULL,
  `lugarNacimiento` varchar(45) DEFAULT NULL,
  `direccionActual` varchar(45) DEFAULT NULL,
  `celular` varchar(15) DEFAULT NULL,
  `telefonoFijo` varchar(15) DEFAULT NULL,
  `correoElectronico` varchar(45) DEFAULT NULL,
  `tipoSangre` varchar(10) DEFAULT NULL,
  `efermedadesComunes` varchar(200) DEFAULT NULL,
  `condicionesEspeciales` varchar(200) DEFAULT NULL,
  `aspirante` int(11) DEFAULT NULL,
  `urlFotoPerfil` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codPersona`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblPersonas`
--

LOCK TABLES `tblPersonas` WRITE;
/*!40000 ALTER TABLE `tblPersonas` DISABLE KEYS */;
INSERT INTO `tblPersonas` VALUES (1,'12345667','prueba','pruebaapellido','2018-03-11',21,'1','asd','asda','asdas','asdasd','asdasd','asdasda','asdasda','asdas','asdas','asdasd','asdasd',2,'asdasd');
/*!40000 ALTER TABLE `tblPersonas` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblSecciones`
--

DROP TABLE IF EXISTS `tblSecciones`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblSecciones` (
  `codSeccion` int(11) NOT NULL AUTO_INCREMENT,
  `seccion` varchar(20) DEFAULT NULL,
  `horaInicio` int(11) DEFAULT NULL,
  `horaFin` int(11) DEFAULT NULL,
  `codAula` int(11) NOT NULL,
  `codDocente` int(11) NOT NULL,
  `codPeriodo` int(11) NOT NULL,
  `codLectivo` int(11) NOT NULL,
  `codCurso` int(11) NOT NULL,
  `codAsignatura` int(11) NOT NULL,
  `suspendida` int(11) DEFAULT NULL,
  PRIMARY KEY (`codSeccion`),
  KEY `fk_tblSecciones_tblAulas1_idx` (`codAula`),
  KEY `fk_tblSecciones_tblDocentes1_idx` (`codDocente`),
  KEY `fk_tblSecciones_tblPeriodos1_idx` (`codPeriodo`),
  KEY `fk_tblSecciones_tblLectivos1_idx` (`codLectivo`),
  KEY `fk_tblSecciones_tblAsignaturas_x_curso1_idx` (`codCurso`,`codAsignatura`),
  CONSTRAINT `fk_tblSecciones_tblAsignaturas_x_curso1` FOREIGN KEY (`codCurso`, `codAsignatura`) REFERENCES `tblAsignaturas_x_curso` (`codCurso`, `codAsignatura`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblSecciones_tblAulas1` FOREIGN KEY (`codAula`) REFERENCES `tblAulas` (`codAula`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblSecciones_tblDocentes1` FOREIGN KEY (`codDocente`) REFERENCES `tblDocentes` (`codDocente`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblSecciones_tblLectivos1` FOREIGN KEY (`codLectivo`) REFERENCES `tblLectivos` (`codLectivo`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblSecciones_tblPeriodos1` FOREIGN KEY (`codPeriodo`) REFERENCES `tblPeriodos` (`codPeriodo`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblSecciones`
--

LOCK TABLES `tblSecciones` WRITE;
/*!40000 ALTER TABLE `tblSecciones` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblSecciones` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTiposIncidencias`
--

DROP TABLE IF EXISTS `tblTiposIncidencias`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTiposIncidencias` (
  `codTipoIncidencia` int(11) NOT NULL AUTO_INCREMENT,
  `tipoIncidencia` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codTipoIncidencia`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTiposIncidencias`
--

LOCK TABLES `tblTiposIncidencias` WRITE;
/*!40000 ALTER TABLE `tblTiposIncidencias` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblTiposIncidencias` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTiposUsuarios`
--

DROP TABLE IF EXISTS `tblTiposUsuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTiposUsuarios` (
  `codTipoUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `tipoUsuario` varchar(45) DEFAULT NULL,
  `tipoAcceso` varchar(45) DEFAULT NULL,
  PRIMARY KEY (`codTipoUsuario`)
) ENGINE=InnoDB AUTO_INCREMENT=2 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTiposUsuarios`
--

LOCK TABLES `tblTiposUsuarios` WRITE;
/*!40000 ALTER TABLE `tblTiposUsuarios` DISABLE KEYS */;
INSERT INTO `tblTiposUsuarios` VALUES (1,'administrador','acceso1');
/*!40000 ALTER TABLE `tblTiposUsuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTitulos`
--

DROP TABLE IF EXISTS `tblTitulos`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTitulos` (
  `codTitulo` int(11) NOT NULL AUTO_INCREMENT,
  `nombreTitulo` varchar(50) DEFAULT NULL,
  `fechaInicio` date DEFAULT NULL,
  `fechaFinal` date DEFAULT NULL,
  `areaEnsenanza` varchar(45) DEFAULT NULL,
  `institucion` varchar(45) DEFAULT NULL,
  `codDocente` int(11) NOT NULL,
  PRIMARY KEY (`codTitulo`),
  KEY `fk_tblTitulos_tblDocentes1_idx` (`codDocente`),
  CONSTRAINT `fk_tblTitulos_tblDocentes1` FOREIGN KEY (`codDocente`) REFERENCES `tblDocentes` (`codDocente`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTitulos`
--

LOCK TABLES `tblTitulos` WRITE;
/*!40000 ALTER TABLE `tblTitulos` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblTitulos` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTraslados`
--

DROP TABLE IF EXISTS `tblTraslados`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTraslados` (
  `codTraslado` int(11) NOT NULL AUTO_INCREMENT,
  `codAlumno` int(11) NOT NULL,
  `institucion` varchar(45) DEFAULT NULL,
  `fechaFinal` date DEFAULT NULL,
  `fechaTraslado` date DEFAULT NULL,
  `promedioTraslado` decimal(12,2) DEFAULT NULL,
  PRIMARY KEY (`codTraslado`),
  KEY `fk_tblTraslados_tblAlumnos1_idx` (`codAlumno`),
  CONSTRAINT `fk_tblTraslados_tblAlumnos1` FOREIGN KEY (`codAlumno`) REFERENCES `tblAlumnos` (`codAlumno`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTraslados`
--

LOCK TABLES `tblTraslados` WRITE;
/*!40000 ALTER TABLE `tblTraslados` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblTraslados` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblTutores`
--

DROP TABLE IF EXISTS `tblTutores`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblTutores` (
  `codTutor` int(11) NOT NULL,
  `codAlumno` int(11) NOT NULL,
  `parentesco` varchar(20) DEFAULT NULL,
  `ocupacion` varchar(20) DEFAULT NULL,
  `lugarDeTrabajo` varchar(20) DEFAULT NULL,
  `telefonoTrabajo` varchar(20) DEFAULT NULL,
  PRIMARY KEY (`codTutor`),
  KEY `fk_tblTutores_tblPersona1_idx` (`codTutor`),
  KEY `fk_tblTutores_tblAlumnos1_idx` (`codAlumno`),
  CONSTRAINT `fk_tblTutores_tblAlumnos1` FOREIGN KEY (`codAlumno`) REFERENCES `tblAlumnos` (`codAlumno`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblTutores_tblPersona1` FOREIGN KEY (`codTutor`) REFERENCES `tblPersonas` (`codPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblTutores`
--

LOCK TABLES `tblTutores` WRITE;
/*!40000 ALTER TABLE `tblTutores` DISABLE KEYS */;
/*!40000 ALTER TABLE `tblTutores` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `tblUsuarios`
--

DROP TABLE IF EXISTS `tblUsuarios`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `tblUsuarios` (
  `codUsuario` int(11) NOT NULL AUTO_INCREMENT,
  `nombreUsuario` varchar(45) DEFAULT NULL,
  `contrasena` varchar(200) DEFAULT NULL,
  `codPersona` int(11) NOT NULL,
  `codTipoUsuario` int(11) NOT NULL,
  PRIMARY KEY (`codUsuario`),
  KEY `fk_tblUsuarios_tblPersona1_idx` (`codPersona`),
  KEY `fk_tblUsuarios_tblTiposUsuarios1_idx` (`codTipoUsuario`),
  CONSTRAINT `fk_tblUsuarios_tblPersona1` FOREIGN KEY (`codPersona`) REFERENCES `tblPersonas` (`codPersona`) ON DELETE NO ACTION ON UPDATE NO ACTION,
  CONSTRAINT `fk_tblUsuarios_tblTiposUsuarios1` FOREIGN KEY (`codTipoUsuario`) REFERENCES `tblTiposUsuarios` (`codTipoUsuario`) ON DELETE NO ACTION ON UPDATE NO ACTION
) ENGINE=InnoDB AUTO_INCREMENT=6 DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `tblUsuarios`
--

LOCK TABLES `tblUsuarios` WRITE;
/*!40000 ALTER TABLE `tblUsuarios` DISABLE KEYS */;
INSERT INTO `tblUsuarios` VALUES (5,'asdas','asdas',1,1);
/*!40000 ALTER TABLE `tblUsuarios` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `timestamps`
--

DROP TABLE IF EXISTS `timestamps`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `timestamps` (
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP,
  `update_time` timestamp NULL DEFAULT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `timestamps`
--

LOCK TABLES `timestamps` WRITE;
/*!40000 ALTER TABLE `timestamps` DISABLE KEYS */;
/*!40000 ALTER TABLE `timestamps` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `user`
--

DROP TABLE IF EXISTS `user`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!40101 SET character_set_client = utf8 */;
CREATE TABLE `user` (
  `username` varchar(16) NOT NULL,
  `email` varchar(255) DEFAULT NULL,
  `password` varchar(32) NOT NULL,
  `create_time` timestamp NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `user`
--

LOCK TABLES `user` WRITE;
/*!40000 ALTER TABLE `user` DISABLE KEYS */;
/*!40000 ALTER TABLE `user` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Dumping events for database 'heroku_39a7bb7a85bd88d'
--

--
-- Dumping routines for database 'heroku_39a7bb7a85bd88d'
--
/*!50003 DROP FUNCTION IF EXISTS `FK_VerificarCredendicales` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` FUNCTION `FK_VerificarCredendicales`(nombreUsuario varchar(20),contrasena varchar(20)) RETURNS int(11)
BEGIN

  
  DECLARE Resultado INT;
  
  SELECT count(*) as resultado INTO Resultado 
  FROM heroku_39a7bb7a85bd88d.tblUsuarios as A
  WHERE A.nombreUsuario=nombreUsuario AND A.contrasena=contrasena ; 
    

  RETURN Resultado;
    
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!50003 DROP PROCEDURE IF EXISTS `P_ObtenerDatosUsuario` */;
/*!50003 SET @saved_cs_client      = @@character_set_client */ ;
/*!50003 SET @saved_cs_results     = @@character_set_results */ ;
/*!50003 SET @saved_col_connection = @@collation_connection */ ;
/*!50003 SET character_set_client  = utf8 */ ;
/*!50003 SET character_set_results = utf8 */ ;
/*!50003 SET collation_connection  = utf8_general_ci */ ;
/*!50003 SET @saved_sql_mode       = @@sql_mode */ ;
/*!50003 SET sql_mode              = 'ONLY_FULL_GROUP_BY,STRICT_TRANS_TABLES,NO_ZERO_IN_DATE,NO_ZERO_DATE,ERROR_FOR_DIVISION_BY_ZERO,NO_AUTO_CREATE_USER,NO_ENGINE_SUBSTITUTION' */ ;
DELIMITER ;;
CREATE DEFINER=`root`@`localhost` PROCEDURE `P_ObtenerDatosUsuario`( nombreUsuario varchar(20),contrasena varchar(20) )
BEGIN  
   SELECT A.codTipoUsuario as tipoUsuario, B.tipoAcceso as tipoAcceso
   FROM heroku_39a7bb7a85bd88d.tblUsuarios as A
   INNER JOIN heroku_39a7bb7a85bd88d.tblTiposUsuarios as B
   ON (A.codTipoUsuario=B.codTipoUsuario)
   WHERE A.nombreUsuario=nombreUsuario AND A.contrasena=contrasena;  
END ;;
DELIMITER ;
/*!50003 SET sql_mode              = @saved_sql_mode */ ;
/*!50003 SET character_set_client  = @saved_cs_client */ ;
/*!50003 SET character_set_results = @saved_cs_results */ ;
/*!50003 SET collation_connection  = @saved_col_connection */ ;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2018-03-11 16:25:37
