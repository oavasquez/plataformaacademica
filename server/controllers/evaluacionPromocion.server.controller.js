const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const config = require('../config/config');
const verificarToken = require('../config/verificarToken');

const app = express();
app.set('claveSecreta', config.claveSecreta);

const dashboard = require('../models/dashboard.model');
const calificaciones = require('../models/calificacion.model');
const matricula = require('../models/matricula.model');
const seccion = require('../models/seccion.model');
const asistencia = require('../models/asistencia.model');

const jwt = require('jsonwebtoken');



router.use(bodyParser.urlencoded({ extended: false }));
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();


exports.mostrarMensaje = function (req, res) {

    res.status(200).json({ mensaje: "Bienvenido" });

};


//**********************************************Dashboard EvaluacionPromocion********************************************************
// Handle create on POST.
exports.dashboard_evaluacion_promocion_detalles_post = function (req, res) {
    console.log("en la funcion para obtener todos los datos de dashboard de evaluacion y promocion");
    dashboard.mostrarDashboardEvaluacionPromocion(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};



//**********************************************Calificaciones********************************************************
// Handle create on POST.
exports.notas_secciones_detalles_post = function (req, res) {

    calificaciones.mostrarCuadroNotas(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};


//**********************************************matricula********************************************************
// Handle create on POST.
exports.matricula_crear_post = function (req, res) {

    matricula.crearMatricula(req, function (row) {


        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }
    });
};


// Handle create on POST.
exports.matricula_actualizar_post = function (req, res) {
    console.log("en la funcion de actualizar matricula notas");
    matricula.actualizarMatricula(req, function (row) {

        if (row > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
}

/*******************************************************seccion************************************************************/
// Handle create on POST.
exports.seccion_alumnos_detalles_post = function (req, res) {

    seccion.mostrarSeccionAlumnos(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};

//**********************************************REST Asistencia********************************************************
// Handle create on POST.
exports.asistencia_alumnos_fecha_detalles_post = function (req, res) {
    console.log("en la funcion para obtener todos los datos asistencias");
    asistencia.mostrarAsistenciaFecha(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};


exports.asistencia_seccion_crear_post = function (req, res) {

    asistencia.crearAsistencia(req, function (row) {


        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }
    });
};






