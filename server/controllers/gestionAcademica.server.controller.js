const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();
const config = require('../config/config');
const verificarToken = require('../config/verificarToken');

//models import
const aula = require('../models/aula.model');
const docente = require('../models/docente.model');
const secciones = require('../models/seccion.model');
const dashboard = require('../models/dashboard.model');
const cargo = require('../models/cargo.model');
const departamento = require('../models/departamento.model');

const asistencia = require('../models/asistencia.model');

const app = express();
app.set('claveSecreta', config.claveSecreta);

const jwt = require('jsonwebtoken');



router.use(bodyParser.urlencoded({ extended: false }));
var urlencodedParser = bodyParser.urlencoded({ extended: false });
var jsonParser = bodyParser.json();


exports.mostrarMensaje = function (req, res) {

    res.status(200).json({ mensaje: "Bienvenido" });

};

//-----------------------------------Aulas-------------------------------------------------
exports.aula_mostrar_post = function (req, res) {
    aula.mostrarAula(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

exports.aula_crear_post = function (req, res) {
    aula.crearAula(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

exports.aula_actualizar_post = function (req, res) {
    aula.actualizarAula(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

exports.aula_borrar_post = function (req, res) {
    aula.eliminarAula(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

//-----------------------------------Docentes-------------------------------------------------

exports.docente_mostrar_post = function (req, res) {
    docente.mostrarDocente(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

exports.docente_crear_post = function (req, res) {
    docente.crearDocente(req, function (row) {

        if (row > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

exports.docente_actualizar_post = function (req, res) {
    docente.actualizarDocente(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

exports.docente_borrar_post = function (req, res) {
    docente.eliminarDocente(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

//-----------------------------------Secciones-------------------------------------------------

exports.seccion_mostrar_post = function (req, res) {
    secciones.mostrarSeccion(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }


    });


}

exports.seccion_crear_post = function (req, res) {
    secciones.crearSeccion(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }


    });

}


exports.seccion_docente_mostrar_post = function (req, res) {
    secciones.mostrarSeccionDocente(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }


    });


}



exports.seccion_actualizar_post = function (req, res) {
    secciones.actualizarSeccion(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

exports.seccion_borrar_post = function (req, res) {
    secciones.eliminarSeccion(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

//-----------------------------------Cargos-------------------------------------------------
exports.cargo_mostrar_post = function (req, res) {
    cargo.mostrarCargo(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

exports.cargo_docente_mostrar_post = function (req, res) {
    cargo.mostrarCargoDocente(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

exports.cargo_crear_post = function (req, res) {
    cargo.crearCargo(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }


    });

}

exports.cargo_actualizar_post = function (req, res) {
    cargo.actualizarCargo(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

//-----------------------------------Departamentos-------------------------------------------------
exports.departamento_mostrar_post = function (req, res) {
    departamento.mostrarDepartamento(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}

exports.departamento_crear_post = function (req, res) {
    departamento.crearDepartamento(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }


    });

}

exports.departamento_actualizar_post = function (req, res) {
    departamento.actualizarDepartamento(req, function (row) {

        if (row.affectedRows > 0) {

            res.status(200).json({ mensaje: 1 });

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });

}





//**********************************************Dashboard GestionAcademica********************************************************
// Handle create on POST.
exports.dashboard_gestion_academica_detalles_post = function (req, res) {
    console.log("en la funcion para obtener todos los datos de dashboard de gestiona academica");
    dashboard.mostrarDashboardGestionAcademica(req, function (row) {

        if (row.length > 0) {

            res.status(200).json(JSON.parse(JSON.stringify(row[0])));

        } else {
            res.status(200).json({ mensaje: 0 });
        }

    });
};