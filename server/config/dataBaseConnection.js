var mysql = require('mysql') //, async = require('async')


var PRODUCTION_DATABASE = 'heroku_39a7bb7a85bd88d',
  PRODUCTION_HOST = 'us-cdbr-iron-east-05.cleardb.net',
  PRODUCTION_USER = 'b1e86835ce3929',
  PRODUCTION_PASSWORD = '62c9476d';


var TEST_DATABASE = 'heroku_39a7bb7a85bd88d',
  TEST_HOST = 'localhost',
  TEST_USER = 'root',
  TEST_PASSWORD = '';


var state = {
  pool: null,
}

exports.MODE_TEST = 'mode_test';
exports.MODE_PRODUCTION = 'mode_production';

exports.getDataBaseName = function () {
  return "nombre";
}


exports.connect = function (mode, done) {
  state.pool = mysql.createPool({
    host: mode === exports.MODE_PRODUCTION ? PRODUCTION_HOST : TEST_HOST,
    user: mode === exports.MODE_PRODUCTION ? PRODUCTION_USER : TEST_USER,
    password: mode === exports.MODE_PRODUCTION ? PRODUCTION_PASSWORD : TEST_PASSWORD,
    database: mode === exports.MODE_PRODUCTION ? PRODUCTION_DATABASE : TEST_DATABASE

  })
  done()
}

exports.get = function () {
  return state.pool
}


/*
exports.fixtures = function(data) {
  var pool = state.pool
  if (!pool) return done(new Error('Missing database connection.'))

  var names = Object.keys(data.tables)
  async.each(names, function(name, cb) {
    async.each(data.tables[name], function(row, cb) {
      var keys = Object.keys(row)
        , values = keys.map(function(key) { return "'" + row[key] + "'" })

      pool.query('INSERT INTO ' + name + ' (' + keys.join(',') + ') VALUES (' + values.join(',') + ')', cb)
    }, cb)
  }, done)
}



exports.drop = function(tables, done) {
  var pool = state.pool
  if (!pool) return done(new Error('Missing database connection.'))

  async.each(tables, function(name, cb) {
    pool.query('DELETE * FROM ' + name, cb)
  }, done)
}
*/