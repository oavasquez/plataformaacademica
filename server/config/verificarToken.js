const jwt = require('jsonwebtoken');
var config = require('./config');


function verificarToken(req, res, next) {
    var token = req.headers['x-access-token'];
    if (!token)
        return res.status(403).send({ auth: false, mensaje: 'token no proporcionado.' });
    jwt.verify(token, config.claveSecreta, function (err, decoded) {
        if (err)
            return res.status(500).send({ auth: false, mensaje: 'falla de autentificacion de token.' });
        req = decoded
        next();
    });
}
module.exports = verificarToken;