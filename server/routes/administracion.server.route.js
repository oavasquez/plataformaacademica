const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();

var administracion_server_controller = require('../controllers/administracion.server.controller');

const verificarToken = require('../config/verificarToken');


router.use(bodyParser.urlencoded({ extended: false }));

var urlencodedParser = bodyParser.urlencoded({ extended: false });

var jsonParser = bodyParser.json();


router.get('/', (req, res) => {

    //console.log(JSON.stringify(respuesta));
    res.status(200).json({
        mensaje: 'raiz administracion'
    });
});


//REST usuario
//Mostrar usuarios
router.post('/mostrarUsuario', jsonParser, administracion_server_controller.usuario_detalles_post);
//Crear usuario
router.post('/crearUsuario', jsonParser, administracion_server_controller.usuario_crear_post);
//Actualizar Usuario
router.post('/actualizarUsuario', jsonParser, administracion_server_controller.usuario_actualizar_post);

//REST rol
//Mostrar roles
router.post('/mostrarRoles', jsonParser, administracion_server_controller.rol_detalles_post);
//Crear rol
router.post('/crearRol', jsonParser, administracion_server_controller.rol_crear_post);
//Actualizar rol
router.post('/actualizarRol', jsonParser, administracion_server_controller.rol_actualizar_post);

//REST curso
//Mostrar curso
router.post('/mostrarCursos', jsonParser, administracion_server_controller.curso_detalles_post);
//Crear curso
router.post('/crearCurso', jsonParser, administracion_server_controller.curso_crear_post);
//Actualizar curso
router.post('/actualizarCurso', jsonParser, administracion_server_controller.curso_actualizar_post);

//REST Asignatura
//Mostrar Asignatura
router.post('/mostrarAsignatura', jsonParser, administracion_server_controller.asignatura_detalles_post);
//mostrar un lista de las asignatura
router.post('/mostrarAsignaturaLista', jsonParser, administracion_server_controller.asignatura_list_detalles_post);
//Crear asignatura
router.post('/crearAsignatura', jsonParser, administracion_server_controller.asignatura_crear_post);
//Actualizar asignatura
router.post('/actualizarAsignatura', jsonParser, administracion_server_controller.asignatura_actualizar_post);
//otras funciones sobre asignatura 
//Asignar asignatura 
router.post('/asignaturaCurso', jsonParser, administracion_server_controller.asignatura_cursos_post);
router.post('/actualizarAsignaturaCurso', jsonParser, administracion_server_controller.asignatura_cursos_actualizar_post);

//REST Modalidad
//Mostrar modalidad
router.post('/mostrarModalidad', jsonParser, administracion_server_controller.modalidad_detalles_post);
//Crear modalidad
router.post('/crearModalidad', jsonParser, administracion_server_controller.modalidad_crear_post);
//Actualizar modalidad
router.post('/actualizarModalidad', jsonParser, administracion_server_controller.modalidad_actualizar_post);


//REST Periodo
//Mostrar periodo
router.post('/mostrarPeriodo', jsonParser, administracion_server_controller.periodo_detalles_post);
//Crear periodo
router.post('/crearPeriodo', jsonParser, administracion_server_controller.periodo_crear_post);
//Actualizar periodo
router.post('/actualizarPeriodo', jsonParser, administracion_server_controller.periodo_actualizar_post);


//REST Jornada
//Mostrar Jornada
router.post('/mostrarJornada', jsonParser, administracion_server_controller.jornada_detalles_post);
//Crear Jornada
router.post('/crearJornada', jsonParser, administracion_server_controller.jornada_crear_post);
//Actualizar Jornada
router.post('/actualizarJornada', jsonParser, administracion_server_controller.jornada_actualizar_post);

//REST Lectivo
//Mostrar Lectivo
router.post('/mostrarLectivo', jsonParser, administracion_server_controller.lectivo_detalles_post);
//Crear Lectivo
router.post('/crearLectivo', jsonParser, administracion_server_controller.lectivo_crear_post);
//Actualizar Lectivo
router.post('/actualizarLectivo', jsonParser, administracion_server_controller.lectivo_actualizar_post);


//other funciones obtener personas
//obtiene el nombre de las personas junto con su id
router.post('/mostrarNombresPersonas', jsonParser, administracion_server_controller.get_nombres_personas_post);
//obtiene el nombre de los usuarios junto con su id
router.post('/mostrarNombreUsuarios', jsonParser, administracion_server_controller.get_nombres_usuarios_post);
//obtiene todos los datos para mostrar en los dashboard de administracion
router.post('/mostrarDashboardAdministracion', jsonParser, administracion_server_controller.dashboard_administracion_detalles_post);






module.exports = router;