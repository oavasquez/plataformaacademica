const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();

var gestionAcademica_server_controller = require('../controllers/gestionAcademica.server.controller');

const verificarToken = require('../config/verificarToken');


router.use(bodyParser.urlencoded({ extended: false }));

var urlencodedParser = bodyParser.urlencoded({ extended: false });

var jsonParser = bodyParser.json();


router.get('/', (req, res) => {


    //console.log(JSON.stringify(respuesta));
    res.status(200).json({
        mensaje: 'raiz gestionAcademica'
    });
});

//REST Aulas
//Mostrar Aulas
router.post('/mostrarAulas', jsonParser, gestionAcademica_server_controller.aula_mostrar_post);
//Crear Aula
router.post('/crearAula', jsonParser, gestionAcademica_server_controller.aula_crear_post);
//Actualizar Aula
router.post('/actualizarAula', jsonParser, gestionAcademica_server_controller.aula_actualizar_post);

//REST Docentes
//Mostrar Docentes
router.post('/mostrarDocentes', jsonParser, gestionAcademica_server_controller.docente_mostrar_post);
//Crear Docente
router.post('/crearDocente', jsonParser, gestionAcademica_server_controller.docente_crear_post);
//Actualizar Docente
router.post('/actualizarDocente', jsonParser, gestionAcademica_server_controller.docente_actualizar_post);

//REST Secciones
//Mostrar Secciones
router.post('/mostrarSecciones', jsonParser, gestionAcademica_server_controller.seccion_mostrar_post);
router.post('/mostrarSeccionesDocente', jsonParser, gestionAcademica_server_controller.seccion_docente_mostrar_post);
//Crear Seccion
router.post('/crearSeccion', jsonParser, gestionAcademica_server_controller.seccion_crear_post);
//Actualizar Seccion
router.post('/actualizarSeccion', jsonParser, gestionAcademica_server_controller.seccion_actualizar_post);


//REST Cargos
//Mostrar Cargos
router.post('/mostrarCargos', jsonParser, gestionAcademica_server_controller.cargo_mostrar_post);
//Mostrar Cargos por docente
router.post('/mostrarCargoDocente', jsonParser, gestionAcademica_server_controller.cargo_docente_mostrar_post);
//Crear Cargos
router.post('/crearCargo', jsonParser, gestionAcademica_server_controller.cargo_crear_post);
//Actualizar Cargos
router.post('/actualizarCargo', jsonParser, gestionAcademica_server_controller.cargo_actualizar_post);


//REST Departamentos
//Mostrar Departamentos
router.post('/mostrarDepartamentos', jsonParser, gestionAcademica_server_controller.departamento_mostrar_post);
//Crear Departamentos
router.post('/crearDepartamento', jsonParser, gestionAcademica_server_controller.departamento_crear_post);
//Actualizar Departamentos
router.post('/actualizarDepartamento', jsonParser, gestionAcademica_server_controller.departamento_actualizar_post);


//obtiene todos los datos para mostrar en los dashboard de gestion academica
router.post('/mostrarDashboardGestionAcademica', jsonParser, gestionAcademica_server_controller.dashboard_gestion_academica_detalles_post);


module.exports = router;