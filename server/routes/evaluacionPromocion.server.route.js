const express = require('express');
const bodyParser = require('body-parser');
const router = express.Router();


var evaluacionPromocion_server_controller = require('../controllers/evaluacionPromocion.server.controller');

const verificarToken = require('../config/verificarToken');


router.use(bodyParser.urlencoded({ extended: false }));

var urlencodedParser = bodyParser.urlencoded({ extended: false });

var jsonParser = bodyParser.json();


router.get('/', (req, res) => {

    //console.log(JSON.stringify(respuesta));
    res.status(200).json({
        mensaje: 'raiz evaluacionPromocion'
    });
});

//obtiene todos los datos para mostrar en los dashboard de evaluacion y promocion
router.post('/mostrarDashboardEvaluacionPromocion', jsonParser, evaluacionPromocion_server_controller.dashboard_evaluacion_promocion_detalles_post);

router.post('/mostrarCuadroNotas', jsonParser, evaluacionPromocion_server_controller.notas_secciones_detalles_post);

router.post('/crearMatricula', jsonParser, evaluacionPromocion_server_controller.matricula_crear_post);

router.post('/actualizarMatriculaNotas', jsonParser, evaluacionPromocion_server_controller.matricula_actualizar_post);
//seccion
router.post('/mostrarSeccionAlumnos', jsonParser, evaluacionPromocion_server_controller.seccion_alumnos_detalles_post);




//asistencia
//Actualizar Departamentos
router.post('/mostrarAsistenciaFecha', jsonParser, evaluacionPromocion_server_controller.asistencia_alumnos_fecha_detalles_post);
//agregar asitencia
router.post('/agregarAsistencia', jsonParser, evaluacionPromocion_server_controller.asistencia_seccion_crear_post);


module.exports = router;