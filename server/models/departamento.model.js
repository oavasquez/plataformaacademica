var dataBaseConnection = require('../config/dataBaseConnection');

//REST Departamento
//Mostrar Departamento
exports.mostrarDepartamento = function (req, done) {
    dataBaseConnection.get().query('call sp_get_departamento(?)', [req.body.codDepartamento], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Departamento
exports.crearDepartamento = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_tiposusuarios(?,?)', [req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Departamento
exports.actualizarDepartamento = function (req, done) {
    dataBaseConnection.get().query('call sp_update_tiposUsuarios(?,?,?)', [req.body.codTipoUsuario, req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Departamento
exports.eiminarDepartamento = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}