var dataBaseConnection = require('../config/dataBaseConnection');

//REST curso
//Mostrar curso
exports.mostrarCurso = function (req, done) {
    dataBaseConnection.get().query('call sp_get_curso(?)', [req.body.codCurso], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear curso
exports.crearCurso = function (req, done) {

    dataBaseConnection.get().query('call sp_insert_curso(?,?,?,?)', [req.body.codModalidad, req.body.nombreCurso, req.body.arrayAsignatura.length, req.body.estadoCurso], function (err, rows) {
        if (err) return done(err);

        console.log(rows[0][0].IdCursoInsertado);
        console.log(rows[0][0].rowCount);


        if (rows[0][0].IdCursoInsertado > 0) {

            setTimeout(function () {

                req.body.arrayAsignatura.forEach(asignatura => {
                    console.log(asignatura);
                    dataBaseConnection.get().query('CALL sp_insert_asignatura_de_curso(?,?,?)', [rows[0][0].IdCursoInsertado, asignatura.codAsignatura, asignatura.dias], function (err, rowsAsignaturaCargos) {
                        if (err) return done(err);
                    })
                })

            }
                , 1500);





            done(rows[0][0].rowCount);
        }
    });
}



//Actualizar curso
exports.actualizarCurso = function (req, done) {
    dataBaseConnection.get().query('call sp_update_curso(?,?,?,?,?)', [req.body.codCurso, req.body.codModalidad, req.body.nombreCurso, req.body.arrayAsignatura.length, req.body.estadoCurso], function (err, rowsActualizar) {
        if (err) return done(err)
        console.log("rowsActualizar.affectedRows")
        console.log(rowsActualizar.affectedRows)
        if (rowsActualizar.affectedRows > 0) {
            dataBaseConnection.get().query('call sp_delete_asignatura_x_curso(?)', [req.body.codCurso], function (err, rowsdelete) {
                if (err) return done(err)
               

                    setTimeout(function () {

                    req.body.arrayAsignatura.forEach(asignatura => {
                        console.log(asignatura);
                        dataBaseConnection.get().query('CALL sp_insert_asignatura_de_curso(?,?,?)', [req.body.codCurso, asignatura.codAsignatura, asignatura.dias], function (err, rowsAsignaturaCurso) {
                            if (err) return done(err);
                        });


                    })

                }, 1500);


                    done(rowsActualizar);


            });




        }
        else {
            done(err)
        }
    });
}
//Eliminar curso
exports.eiminarCurso = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}


exports.asignarAsignaturasXCurso = function (req, done) {
    console.log(req.body);
    dataBaseConnection.get().query('INSERT INTO tblasignaturas_x_curso (codCurso,codAsignatura,dias) VALUES ?', [[req.body.codCurso, req.body.arrayAsignatura.codAsignatura, req.body.arrayAsignatura.diasAsignatura]], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}