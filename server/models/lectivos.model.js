var dataBaseConnection = require('../config/dataBaseConnection');

//REST Lectivo
//Mostrar Lectivo
exports.mostrarLectivo = function (req, done) {
    dataBaseConnection.get().query('call sp_get_lectivo(?)', [req.body.codLectivo], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Lectivo
exports.crearLectivo = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_lectivo(?,?,?)',
        [
            req.body.fechaInicio,
            req.body.fechaFinal,
            req.body.nombre

        ], function (err, rows) {
            if (err) return done(err)
            done(rows);
        })
}
//Actualizar Lectivo
exports.actualizarLectivo = function (req, done) {
    dataBaseConnection.get().query('call sp_update_lectivo(?,?,?,?)',
        [
            req.body.codLectivo,
            req.body.fechaInicio,
            req.body.fechaFinal,
            req.body.nombre

        ], function (err, rows) {
            if (err) return done(err)
            done(rows);
        })
}
//Eliminar Lectivo
exports.eiminarLectivo = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}