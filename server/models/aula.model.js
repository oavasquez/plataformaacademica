var dataBaseConnection = require('../config/dataBaseConnection');

//REST Aula
//Mostrar Aula
exports.mostrarAula = function (req, done) {
    dataBaseConnection.get().query('call sp_get_aula(?)', [req.body.codAula], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Aula
exports.crearAula = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_aula(?,?,?)', [req.body.capacidad, req.body.descripcion, req.body.habilitada], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Aula
exports.actualizarAula = function (req, done) {
    dataBaseConnection.get().query('call sp_update_aula(?,?,?,?)', [req.body.codAula, req.body.capacidad, req.body.descripcion, req.body.habilitada], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Aula
exports.eliminarAula = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}