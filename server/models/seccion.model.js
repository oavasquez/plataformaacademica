var dataBaseConnection = require('../config/dataBaseConnection');

//REST Seccion
//Mostrar Seccion
exports.mostrarSeccion = function (req, done) {
    dataBaseConnection.get().query('call sp_get_seccion(?)', [req.body.codSeccion], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Seccion
exports.crearSeccion = function (req, done) {

    dataBaseConnection.get().query('call sp_insert_seccion(?,?,?,?,?,?,?,?,?,?,?)',
        [
            req.body.seccion, req.body.codDocente,
            req.body.horaInicio, req.body.horaFin,
            req.body.codCurso, req.body.codAula,
            req.body.suspendida, req.body.codPeriodo,
            req.body.codLectivo, req.body.codAsignatura,
            req.body.codJornada
        ],
        function (err, rows) {
            if (err) return done(err)
            done(rows);
        })
}

exports.mostrarSeccionDocente = function (req, done) {

    dataBaseConnection.get().query('call sp_get_secciones_docente(?)',
        [req.body.codDocente],
        function (err, rows) {
            if (err) return done(err)
            done(rows);
        })
}

exports.mostrarSeccionAlumnos = function (req, done) {

    dataBaseConnection.get().query('call sp_get_seccion_alumnos(?)',
        [req.body.codAlumno],
        function (err, rows) {
            if (err) return done(err)
            done(rows);
        })
}


//Actualizar Seccion
exports.actualizarSeccion = function (req, done) {
    dataBaseConnection.get().query('call sp_update_seccion(?,?,?,?,?,?,?,?,?,?,?,?)',
        [
            req.body.codSeccion, req.body.seccion, req.body.codDocente,
            req.body.horaInicio, req.body.horaFin, req.body.codCurso,
            req.body.codAula, req.body.suspendida, req.body.codPeriodo,
            req.body.codLectivo, req.body.codAsignatura,
            req.body.codJornada],
        function (err, rows) {
            if (err) return done(err)
            done(rows);
        })
}


//crear Seccion
exports.mostrarSeccionesDisponibles = function (req, done) {

    dataBaseConnection.get().query('call sp_get_secciones_disponibles(?,?)',
        [
            req.body.codAsignatura, req.body.codCurso
        ],
        function (err, rows) {
            if (err) return done(err)
            done(rows);
        })
}


//Eliminar Seccion
exports.eliminarSeccion = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}