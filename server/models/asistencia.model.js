var dataBaseConnection = require('../config/dataBaseConnection');

//REST Asistencia
//Mostrar Asistencia
exports.mostrarAsistenciaFecha = function (req, done) {
    console.log(req.body)
    dataBaseConnection.get().query('call sp_get_asistencia_fechas(?,?,?,?)', [
        req.body.fechaInicial,
        req.body.fechaFinal,
        req.body.codSeccion,
        req.body.codAlumno

    ], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Asistencia
exports.crearAsistencia = function (req, done) {
    console.log(req.body)
    dataBaseConnection.get().query('call sp_insert_asistencia_seccion(?,?,?,?)', [
        req.body.codSeccion, 
        req.body.codAlumno,
        req.body.fechaHoy, 
        req.body.asistio
    
    ], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Asistencia
exports.actualizarAsistencia = function (req, done) {
    dataBaseConnection.get().query('call sp_update_tiposUsuarios(?,?,?)', [req.body.codTipoUsuario, req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Asistencia
exports.eiminarAsistencia = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}