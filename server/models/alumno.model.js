var dataBaseConnection = require('../config/dataBaseConnection');

//REST Alumno 
//Mostrar Alumno 
exports.mostrarAlumnos = function (req, done) {
    dataBaseConnection.get().query('call sp_get_alumno(?)', [req.body.codAlumno], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}

exports.mostrarAlumnoResumen = function (req, done) {
    dataBaseConnection.get().query('call sp_get_alumno_resumen(?)', [req.body.codAlumno], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Alumno 
exports.crearAlumno = function (req, done) {
    console.log(req.body)
    dataBaseConnection.get().query('call sp_insert_alumno(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
        req.body.nombre,
        req.body.apellido,
        req.body.identidad,
        req.body.fechaNacimiento,
        req.body.telefono,
        req.body.celular,
        req.body.correo,
        req.body.direccion,
        req.body.edad,
        req.body.paisOrigen,
        req.body.departamentoNacimiento,
        req.body.lugarNacimiento,
        req.body.nacionalidad,
        req.body.imagenPerfil,
        req.body.sexo,
        req.body.tipoSangre,
        req.body.efermedadesComunes,
        req.body.condicionesEspeciales

    ], function (err, rows) {
        if (err) return done(err)

        console.log(rows[0][0].IdAlumnoInsertado);
        console.log(rows[0][0].rowCount);


        if (rows[0][0].IdAlumnoInsertado > 0) {

            setTimeout(function () {

                req.body.arrayTutores.forEach(tutor => {
                    console.log(tutor);

                    console.log("insertando tutor")
                    dataBaseConnection.get().query('CALL sp_insert_tutor(?,?,?,?,?,?,?,?,?,?,?,?)', [rows[0][0].IdAlumnoInsertado, tutor.parentesco, tutor.ocupacion,
                    tutor.lugarTrabajo, tutor.telefonoTrabajo, tutor.identidad, tutor.nombre, tutor.apellido, tutor.telefono,
                    tutor.celular, tutor.correo, tutor.codPersona], function (err, rowsTutorAlumno) {
                        if (err) return done(err);
                    })
                })
                done(rows[0][0].rowCount);
            }
                , 1500);


        }
    })
}
//Actualizar Alumno 
exports.actualizarAlumno = function (req, done) {
    dataBaseConnection.get().query('call sp_update_alumno(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)', [
        req.body.codPersona,
        req.body.nombre,
        req.body.apellido,
        req.body.identidad,
        req.body.fechaNacimiento,
        req.body.telefono,
        req.body.celular,
        req.body.correo,
        req.body.direccion,
        req.body.edad,
        req.body.paisOrigen,
        req.body.departamentoNacimiento,
        req.body.lugarNacimiento,
        req.body.nacionalidad,
        req.body.imagenPerfil,
        req.body.sexo,
        req.body.tipoSangre,
        req.body.enfermedadesComunes,
        req.body.condicionesEspeciales,

    ], function (err, rowsActualizar) {
        if (err) return done(err)
        if (rowsActualizar.affectedRows > 0) {
            dataBaseConnection.get().query('call sp_delete_tutores_alumno(?)', [req.body.codAlumno], function (err, rowsdelete) {
                if (err) return done(err)


                setTimeout(function () {

                    req.body.arrayTutores.forEach(tutor => {
                        console.log(tutor);
                        console.log("insertando tutor")
                        dataBaseConnection.get().query('CALL sp_insert_tutor(?,?,?,?,?,?,?,?,?,?,?,?)', [req.body.codAlumno, tutor.parentesco, tutor.ocupacion,
                        tutor.lugarTrabajo, tutor.telefonoTrabajo, tutor.identidad, tutor.nombre, tutor.apellido, tutor.telefono,
                        tutor.celular, tutor.correo, tutor.codPersona], function (err, rowsTutorAlumno) {
                            if (err) return done(err);
                        })

                    });


                }, 1500);


                done(rowsActualizar);
            });
        }
        else {
            done(err)
        }
    })
}
//Eliminar Alumno 
exports.eiminarAlumno = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        
    })
}

