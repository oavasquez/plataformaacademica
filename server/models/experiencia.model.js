var dataBaseConnection = require('../config/dataBaseConnection');

//REST Experiencia
//Mostrar Experiencia
exports.mostrarExperiencia = function (req, done) {
    dataBaseConnection.get().query('call sp_get_tipo_usuario(?)', [req.body.codTipoUsuarioie], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Experiencia
exports.crearExperiencia = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_tiposusuarios(?,?)', [req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Experiencia
exports.actualizarExperiencia = function (req, done) {
    dataBaseConnection.get().query('call sp_update_tiposUsuarios(?,?,?)', [req.body.codTipoUsuario, req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Experiencia
exports.eiminarExperiencia = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}