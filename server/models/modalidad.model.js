var dataBaseConnection = require('../config/dataBaseConnection');

//REST Modalidad
//Mostrar Modalidad
exports.mostrarModalidad = function (req, done) {
    dataBaseConnection.get().query('call sp_get_modalidades()', [], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Modalidad
exports.crearModalidad = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_modalidades(?)', [req.body.modalidad], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Modalidad
exports.actualizarModalidad = function (req, done) {
    dataBaseConnection.get().query('call sp_update_modalidades(?,?)', [req.body.codModalidad, req.body.modalidad], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Modalidad
exports.eiminarModalidad = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}