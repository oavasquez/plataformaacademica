var dataBaseConnection = require('../config/dataBaseConnection');

//REST Periodo
//Mostrar Periodo
exports.mostrarPeriodo = function (req, done) {
    dataBaseConnection.get().query('call sp_get_periodo(?)', [req.body.codPeriodo], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Periodo
exports.crearPeriodo = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_periodo(?,?,?,?)',
        [
            req.body.nombrePeriodo, req.body.fechaInicio,
            req.body.fechaFinal, req.body.anio
        ], function (err, rows) {
            if (err) return done(err)
            done(rows);
        })
}
//Actualizar Periodo
exports.actualizarPeriodo = function (req, done) {
    dataBaseConnection.get().query('call sp_update_periodo(?,?,?,?,?)',
        [
            req.body.codPeriodo, req.body.nombrePeriodo, req.body.fechaInicio,
            req.body.fechaFinal, req.body.anio
        ], function (err, rows) {
            if (err) return done(err)
            done(rows);
        })
}
//Eliminar Periodo
exports.eiminarPeriodo = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}