var dataBaseConnection = require('../config/dataBaseConnection');

//REST Traslado
//Mostrar Traslado
exports.mostrarTraslado = function (req, done) {
    dataBaseConnection.get().query('call sp_get_traslado(?)', [req.body.codAlumno], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Traslado
exports.crearTraslado = function (req, done) {
    
    dataBaseConnection.get().query('call sp_insert_traslado(?,?,?,?,?)', [
        req.body[0].codAlumno,
        req.body[0].institucion,
        req.body[0].fechaTraslado,
        req.body[0].fechaTraslado,
        req.body[0].promedioActual

    ], function (err, rows) {
        if (err) return done(err)

        if (rows[0][0].IdTrasladoInsertado > 0) {
            
            setTimeout(function () {
                
                
                req.body.forEach(asignaturaTraslado => {
                    console.log("/////////asignarua////////////////")
                    console.log(asignaturaTraslado);
                    console.log(rows[0][0].IdTrasladoInsertado);
                    
                    dataBaseConnection.get().query('call sp_insert_historial(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
                     [
                        "0000-00-00", 
                        asignaturaTraslado.codAlumno,
                        rows[0][0].IdTrasladoInsertado,
                        asignaturaTraslado.asignatura,
                        asignaturaTraslado.promedioActual,
                        "asignatura para traslado",
                        null,
                        null,
                        asignaturaTraslado.codJornada,
                        asignaturaTraslado.codLectivo,
                        asignaturaTraslado.codPeriodo,
                        asignaturaTraslado.codAsignatura,
                        "0000-00-00",
                        "0000-00-00",
                        asignaturaTraslado.nombreCurso,
                        asignaturaTraslado.codCurso
                    ], function (err, rowsInsertHistorial) {
                            if (err) return done(err)
                           
                        })

                });

                done(rows[0][0].rowCount);
            }
                , 1500);
        }





    })
}
//Actualizar Traslado
exports.actualizarTraslado = function (req, done) {
    dataBaseConnection.get().query('call sp_update_traslado(?,?,?,?,?,?)', [
        req.body.codTraslado,
        req.body.codAlumno,
        req.body.institucion,
        req.body.fechaFinal,
        req.body.fechaTraslado,
        req.body.promedioTraslado

    ], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Traslado
exports.eiminarTraslado = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}