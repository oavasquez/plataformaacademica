
var dataBaseConnection = require('../config/dataBaseConnection');



//REST Usuario
//crear Usuario
exports.crearUsuario = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//actualizar Usuario
exports.actualizarUsuario = function (req, done) {

    console.log(req.body)
    dataBaseConnection.get().query('call sp_update_user_details(?,?,?,?,?)', [
        req.body.codigoUsuario,
        req.body.nombreUsuario,
        req.body.contrasena,
        req.body.codTipoUsuario,
        req.body.habilitado

    ], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}

exports.mostrarUsuarios = function (req, done) {
    dataBaseConnection.get().query('call sp_get_usuario_details(?)', [req.body.codUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}





//other functions
//obtiene todos los nombres de usuario junto con su id
exports.obtenerNombresUsuarios = function (req, done) {
    dataBaseConnection.get().query('call sp_get_nombre_usuarios()', function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}


exports.obtnerVerificacionCredenciasles = function (req, done) {

    dataBaseConnection.get().query('call sp_get_verificar_credenciales(?,?)', [req.body.nombreUsuario, req.body.contrasena], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}


exports.obtnerDatosUsuario = function (req, done) {
    dataBaseConnection.get().query('SELECT FK_VerificarCredendicales(?,?) as resultado', [req.body.NombreUsuario, req.body.Contrasena], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}





