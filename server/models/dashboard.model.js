var dataBaseConnection = require('../config/dataBaseConnection');

//REST Departamento
//Mostrar Departamento
exports.mostrarDashboardGestionAcademica = function (req, done) {
    dataBaseConnection.get().query('call sp_get_dashboard_gest(?)', [req.body.codCard], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}

exports.mostrarDashboardMatriculaRegistro = function (req, done) {
    dataBaseConnection.get().query('call sp_get_dashboard_matriReg(?)', [req.body.codCard], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}

exports.mostrarDashboardAdministracion = function (req, done) {
    dataBaseConnection.get().query('call sp_get_dashboard_admin(?)', [req.body.codCard], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}

exports.mostrarDashboardEvaluacionPromocion = function (req, done) {
    dataBaseConnection.get().query('call sp_get_dashboard_evaProm(?,?)', [req.body.codCard,req.body.codDocente], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}


