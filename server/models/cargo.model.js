var dataBaseConnection = require('../config/dataBaseConnection');

//REST Cargo
//Mostrar Cargo
exports.mostrarCargo = function (req, done) {
    dataBaseConnection.get().query('call sp_get_cargo(?)', [req.body.codCargo], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}

exports.mostrarCargoDocente = function (req, done) {
    dataBaseConnection.get().query('call sp_get_cargo_docente(?)', [req.body.codPersona], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Cargo
exports.crearCargo = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_tiposusuarios(?,?)', [req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Cargo
exports.actualizarCargo = function (req, done) {
    dataBaseConnection.get().query('call sp_update_tiposUsuarios(?,?,?)', [req.body.codTipoUsuario, req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Cargo
exports.eiminarCargo = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}