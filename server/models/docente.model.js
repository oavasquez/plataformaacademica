var dataBaseConnection = require('../config/dataBaseConnection');

//REST Docente
//Mostrar Docente
exports.mostrarDocente = function (req, done) {
    dataBaseConnection.get().query('call sp_get_docente(?)', [req.body.codDocente], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Docente
exports.crearDocente = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_docente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [
            req.body.nombre, req.body.apellido,
            req.body.identidad, req.body.direccion,
            req.body.correo, req.body.paisOrigen,
            req.body.nacionalidad, req.body.fechaNacimiento,
            req.body.telefono, req.body.imagenPerfil,
            req.body.celular, req.body.edad,
            req.body.lugarNacimiento, req.body.sexo,
            req.body.tipoSangre, req.body.enfermedadesComunes,
            req.body.condicionesEspeciales, req.body.departamentoNacimiento

        ], function (err, rows) {
            if (err) return done(err)

            console.log(rows[0][0].IdDocenteInsertado);
            console.log(rows[0][0].rowCount);


            if (rows[0][0].IdDocenteInsertado > 0) {

                setTimeout(function () {

                    req.body.cargoArray.forEach(cargo => {
                        console.log(cargo);
                        console.log(cargo.codCargo);
                        console.log("insertando cargos")
                        dataBaseConnection.get().query('CALL sp_insert_cargo_docente(?,?,?,?,?,?,?)', [rows[0][0].IdDocenteInsertado, cargo.codCargo, cargo.fechaAsignacion, cargo.codDepartamento,cargo.fechaInicio,cargo.fechaFinal,cargo.institucion], function (err, rowsCargosDocente) {
                            if (err) return done(err);
                        })
                    })
                    done(rows[0][0].rowCount);
                }
                    , 1500);

               
            }

        })
}


//Actualizar Docente
exports.actualizarDocente = function (req, done) {
    dataBaseConnection.get().query('call sp_update_docente(?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?,?)',
        [
            req.body.codDocente,
            req.body.nombre, req.body.apellido,
            req.body.identidad, req.body.direccion,
            req.body.correo, req.body.paisOrigen,
            req.body.nacionalidad, req.body.fechaNacimiento,
            req.body.telefono, req.body.imagenPerfil,
            req.body.celular, req.body.edad,
            req.body.lugarNacimiento, req.body.sexo,
            req.body.tipoSangre, req.body.enfermedadesComunes,
            req.body.condicionesEspeciales, req.body.departamentoNacimiento,
            req.body.habilitado

        ], function (err, rowsActualizar) {
            if (err) return done(err)
            if (rowsActualizar.affectedRows > 0) {
                dataBaseConnection.get().query('call sp_delete_cargo_docente(?)', [req.body.codDocente], function (err, rowsdelete) {
                    if (err) return done(err)
                    
    
                        setTimeout(function () {
    
                        req.body.cargoArray.forEach(cargo => {
                            console.log(cargo);
                            dataBaseConnection.get().query('CALL sp_insert_cargo_docente(?,?,?,?,?,?,?)', [req.body.codDocente, cargo.codCargo, cargo.fechaAsignacion, cargo.codDepartamento,cargo.fechaInicio,cargo.fechaFinal,cargo.institucion,], function (err, rowsAsignaturaCargos) {
                   
                                if (err) return done(err);
                            });
                        })
    
                    }, 1500);
    
    
                        done(rowsActualizar);
                });
            }
            else {
                done(err)
            }
        })
}
//Eliminar Docente
exports.eliminarDocente = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}