var dataBaseConnection = require('../config/dataBaseConnection');

//REST Tutor
//Mostrar Tutor
exports.mostrarTutores = function (req, done) {
    dataBaseConnection.get().query('call sp_get_tutores(?,?)', [req.body.codTutor, req.body.codAlumno], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Tutor
exports.crearTutor = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_tutor(?,?,?,?,?,?,?,?,?,?,?)', [
        req.body.codAlumno, req.body.parentesco,
        req.body.ocupacion, req.body.lugarDeTrabajo,
        req.body.telefonoTrabajo, req.body.identificacion,
        req.body.nombres, req.body.apellidos,
        req.body.telefonoFijo, req.body.celular,
        req.body.correoElectronico
    ], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Tutor
exports.actualizarTutor = function (req, done) {
    dataBaseConnection.get().query('call sp_update_tiposUsuarios(?,?,?)', [req.body.codTipoUsuario, req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Tutor
exports.eiminarTutor = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}