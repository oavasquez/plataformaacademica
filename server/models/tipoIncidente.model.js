var dataBaseConnection = require('../config/dataBaseConnection');

//REST TipoIncidente
//Mostrar TipoIncidente
exports.mostrarTipoIncidente = function (req, done) {
    dataBaseConnection.get().query('call sp_get_tipo_incidencias()', function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear TipoIncidente
exports.crearTipoIncidente = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_tiposusuarios(?,?)', [req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar TipoIncidente
exports.actualizarTipoIncidente = function (req, done) {
    dataBaseConnection.get().query('call sp_update_tiposUsuarios(?,?,?)', [req.body.codTipoUsuario, req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar TipoIncidente
exports.eiminarTipoIncidente = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}