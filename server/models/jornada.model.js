var dataBaseConnection = require('../config/dataBaseConnection');

//REST Jornada
//Mostrar Jornada
exports.mostrarJornada = function (req, done) {
    dataBaseConnection.get().query('call sp_get_jornada(?)', [req.body.codJornada], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Jornada
exports.crearJornada = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_jornada(?)', [req.body.jornada], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Jornada
exports.actualizarJornada = function (req, done) {
    dataBaseConnection.get().query('call sp_update_jornada(?,?)', [req.body.codJornada, req.body.jornada], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Jornada
exports.eiminarJornada = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}