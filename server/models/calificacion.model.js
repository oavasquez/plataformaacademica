var dataBaseConnection = require('../config/dataBaseConnection');

//REST Calificacion
//Mostrar Calificacion
exports.mostrarCalificacion = function (req, done) {
    dataBaseConnection.get().query('call sp_get_tipo_usuario(?)', [req.body.codTipoUsuarioie], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}

exports.mostrarCuadroNotas = function (req, done) { 
    dataBaseConnection.get().query('call sp_get_notas_seccion(?,?)', [req.body.codSeccion, req.body.codDocente], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Calificacion
exports.crearCalificacion = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_tiposusuarios(?,?)', [req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Calificacion
exports.actualizarCalificacion = function (req, done) {
    dataBaseConnection.get().query('call sp_update_tiposUsuarios(?,?,?)', [req.body.codTipoUsuario, req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Calificacion
exports.eiminarCalificacion = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}