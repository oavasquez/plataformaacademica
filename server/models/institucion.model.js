var dataBaseConnection = require('../config/dataBaseConnection');

//REST Institucion
//Mostrar Institucion
exports.mostrarInstitucion = function (req, done) {
    dataBaseConnection.get().query('call sp_get_tipo_usuario(?)', [req.body.codTipoUsuarioie], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Institucion
exports.crearInstitucion = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_tiposusuarios(?,?)', [req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Institucion
exports.actualizarInstitucion = function (req, done) {
    dataBaseConnection.get().query('call sp_update_tiposUsuarios(?,?,?)', [req.body.codTipoUsuario, req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Institucion
exports.eiminarInstitucion = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}