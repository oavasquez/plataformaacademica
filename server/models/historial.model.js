var dataBaseConnection = require('../config/dataBaseConnection');

//REST Historial
//Mostrar Historial
exports.mostrarHistorial = function (req, done) {
    dataBaseConnection.get().query('call sp_get_historial_resumen(?)', [req.body.codAlumno], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear Historial
exports.crearHistorial = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_tiposusuarios(?,?)', [req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar Historial
exports.actualizarHistorial = function (req, done) {
    dataBaseConnection.get().query('call sp_update_tiposUsuarios(?,?,?)', [req.body.codTipoUsuario, req.body.tipoUsuario, req.body.accesos], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar Historial
exports.eiminarHistorial = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}