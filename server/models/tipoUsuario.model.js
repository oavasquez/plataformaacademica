var dataBaseConnection = require('../config/dataBaseConnection');

//REST rol
//Mostrar rol
exports.mostrarRol = function (req, done) {
    console.log(req.body);
    dataBaseConnection.get().query('call sp_get_tipo_usuario(?)', [req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//crear rol
exports.crearRol = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_tiposusuarios(?,?,?)', [req.body.tipoUsuario, req.body.tipoAcceso, req.body.habilitado], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Actualizar rol
exports.actualizarRol = function (req, done) {
    dataBaseConnection.get().query('call sp_update_tiposUsuarios(?,?,?,?)', [req.body.codTipoUsuario, req.body.tipoUsuario, req.body.tipoAcceso, req.body.habilitado], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}
//Eliminar rol
exports.eiminarRol = function (req, done) {
    dataBaseConnection.get().query('call sp_insert_new_user(?,?,?,?)', [req.body.nombreUsuario, req.body.contrasena, req.body.codPersona, req.body.codTipoUsuario], function (err, rows) {
        if (err) return done(err)
        done(rows);
    })
}